<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>

<c:if test="${not empty message}">
	<div class="${message.type.cssClass}">${message.text}</div>
</c:if>

<article id="intro">
    <hgroup>
        <div class="page-header">
                   <h3>About Us</h3>
        </div>

        <div>
           <h3>Management Team</h3>
        </div>

       <fieldset class="fieldsetBox">
          <legend class="frameTitle">Amit Sharma - Founder and Chief Executive</legend>
                  <div>
                   Amit founded Tag This Car in 2012 soon after having narrowly avoided an accident with a speeding bus at a dangerous intersection while he sat helplessly in his car watching his life flash in front of him.
                   The incident gave him a purpose to work toward: sharing vehicle and location data with drivers, in turn, helping them become smarter on the road.
                   <br><br>
                   Amit has more than nine years of professional enterprise architecture experience in building scalable systems from scratch.
                   His unique technology background stems from specializing in Parallel and Distributed Systems while working as a research assistant,
                   first for Networks and Simulation Lab and then at Machine Learning and Inference Lab while earning his Masters in Computer Science at George Mason University.
                  <br> <br>
                   Amit is an Oracle Service Oriented Architecture (SOA) Architect Certified Expert, Oracle SOA Infrastructure Implementation Certified Expert as well as Cloudera Certified Hadoop Developer.
                  </div>
      </fieldset>

      <br>
      <fieldset class="fieldsetBox">
           <legend class="frameTitle">Ramesh Sahoo - Chief Technologist</legend>
                   <div>
                    Ramesh is a leading edge technology expert with fifteen years of experience in a number of verticals, such as - finance, insurance, telecommunications, education and Government.
                    He is a technology visionary in productizing various mobile and web solutions and is an expert in Big Data technology.
                    Prior to joining Tag This Car, he worked on a Federal Government agency's major initiative to build an intelligence collaboration platform using Big Data solutions. <br> <br>
                    Ramesh holds bachelor's degree in engineering from the Indian Institute of Technology (IIT), Kharagpur.
                   </div>
      </fieldset>

      <br>
      <fieldset class="fieldsetBox">
           <legend class="frameTitle">Amit Khare - Chief Strategist</legend>
                   <div>
                     Amit has over eleven years of success in meeting critical software engineering challenges in both Federal Government and commercial space.
                     He has a proven history of leading the turnaround of underperforming IT/software projects.
                     His specialties include project management, project execution, strategic IT planning, business development and enterprise architecture.
                     His strong technical background blends well with his project management and business development skills in acquisition of projects and clients.
                     His responsibilities at previous positions included offering project guidance, job coaching, software execution and overall product delivery.
                     <br><br>
                     Amit is a certified ScrumMaster, certified PMP, Oracle SOA Architect Certified Expert and Oracle SOA Infrastructure Implementation Certified Expert.
                     He holds Masters in Computer Management from the University of Pune, India.
                   </div>
      </fieldset>

      <br>
      <fieldset class="fieldsetBox">
         <legend class="frameTitle">Michael Sturm - Chief of Operations</legend>
         <div>
           Michael brings over twenty-two years of combined military and civilian project management experience.
           From training soldiers for basic combat training to mentoring project managers on CMMI audit requirements,
           he has a history of showing others the right path.
           Most recently, he has been involved in analyzing project risks across his organization,
           conducting training, and performing quality assurance audits on projects with US Government clients.
           His knowledge spans conducting quality assurance audits,
           Independent Verification and Validation (IV&V) tests,
           business process flow management, and earned value management analysis.
           As a member of the US Army's Military Intelligence Corps,
           he served as a Russian linguist and Russian language instructor at the Defense Language Institute Foreign Language Center,
           Presidio of Monterey, CA.
           <br><br>
           Michael is a certified PMP, CA Clarity Functional Professional and an Oracle Primavera Portfolio Management expert.
           He holds bachelor's degree in Journalism from West Virginia University and is a graduate of the Russian Basic and Intermediate courses at the Defense Language Institute.
         </div>
      </fieldset>
      <br>

      <div>
          <h3>Advisory Board</h3>
      </div>

      <fieldset class="fieldsetBox">
           <legend class="frameTitle">Paco Nathan</legend>
           <div>
           Paco Nathan is the Director of Data Science at Concurrent in SF and a committer on the Cascading open source project. He has expertise in Hadoop, R, AWS, machine learning, predictive analytics, and NLP. For the past 10 years Paco has led innovative Data teams. He is also the author of an upcoming O'Reilly book on Enterprise Data Workflows.
           </div>
      </fieldset>
      <br>
      <fieldset class="fieldsetBox">
                 <legend class="frameTitle">Brian Sletten</legend>
                 <div>
                 Brian Sletten is a liberal arts-educated software engineer with a focus on forward-leaning technologies. His experience has spanned many industries including retail, banking, online games, defense, finance, hospitality and health care. He has a B.S. in Computer Science from the College of William and Mary and lives in Auburn, CA. He focuses on web architecture, resource-oriented computing, social networking, the Semantic Web, data science, 3D graphics, visualization, scalable systems, security consulting and other technologies of the late 20th and early 21st Centuries.
                 </div>
            </fieldset>
            <br>
       <fieldset class="fieldsetBox">
            <legend class="frameTitle">Al Gray</legend>
            <div>
            Alan Gray is a Lead Systems Architect at CareFirst Blue Cross Blue Shield. His 40+ years experience began in a Programmer Trainee position in mainframe systems. He is currently in the Systems Performance and Capacity Planning area and deals with computers of many sizes.
            </div>
       </fieldset>
       <br>
      <div>
        <h3>Legal Advisors</h3>
      </div>

      <fieldset class="fieldsetBox">
         <legend class="frameTitle">Ryan Alley - IP Counsel</legend>
                 <div>
                  Ryan is the founder of Ryan Alley Intellectual Property Law and provides IP Expertise Counsel to the Tag This Car LLC.
                  Prior to founding his firm, Ryan was a patent attorney in a large IP boutique law firm in the Washington, DC, area for several years.

                 <br><br>
                 Ryan has worked with a wide array of clients, including start-ups, small businesses, foreign corporations, institutions of higher education,
                 and international conglomerates.
                 He welcomes clients across the creative spectrum to work with him and is especially focused on delivering IP advice and strategy tailored to each client,
                 whether it be a corporate IP counsel looking to secure broad IP portfolios for cross-licensing, a start-up seeking to lock down their IP to secure funding, a small business needing to outsource IP work for rapid growth, or a foreign law firm seeking a US agent.

                 <br><br>
                 Ryan is a registered Patent Attorney before the US Patent and Trademark Office and a member of the Virginia and California Bar Associations. Ryan is a member of the American Intellectual Property Law Association.
                 </div>
      </fieldset>
      <br>

    </hgroup>
</article>
	
	
	
	
