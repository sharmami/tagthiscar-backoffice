<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


      <!-- START THE JUMBO CTA-->
      <section class="jumbo_cta">
        <div class="container">
            <div class="row span12" align="center">
                <span class='st_sharethis_vcount' displayText='ShareThis'></span>
                <span class='st_fblike_vcount' displayText='Facebook Like'></span>
               <span class='st_googleplus_vcount' displayText='Google +'></span>
                <span class='st_twitter_vcount' displayText='Tweet'></span>
                <span class='st_linkedin_vcount' displayText='LinkedIn'></span>
            </div>

            <div class="row">
            <div class="span9">
              <h1>Making drivers smarter!</h1>
                <p class="lead">Imagine having the power to predict whatever the road might throw at you. Now you can!</p>
              </div>
              <div class="span3 pull-right" style="text-align:center;">
                <a class="btn btn-large btn-success" href="<c:url value="/signup" />" style="margin-top:20px;">Get started today</a>
            </div>
     </section>
      <!-- /END THE JUMBO CTA-->

<!-- START THE FEATURES-->
<section class="features">
    <div class="container">

      <article id="intro">

            <!-- Feature 1 -->
            <div class="row-fluid featurette">
              <div class="span6">
                <h2 class="featurette-heading">
                  Search for a location.
                  <span class="muted">Enter a zip code or address.</span>
                </h2>
                <p class="lead">
                  Look up <b>any location</b> for a detailed history of accidents, congested areas,
                  construction sites, closed roads, fallen debris, icy roads, warning signs, detours,
                  missing road signs, and just about everything on the road - things that maps
                  or your navigation devices don't tell you.
                </p>
              </div>

              <div class="span5 offset1 pull-right">
                <img src="<c:url value="/static/tagthiscar/standard/images/features_001.png" />">
              </div>
            </div>

            <hr class="featurette-divider">

            <!-- Feature 2 -->
            <div class="row featurette">
              <div class="span5 pull-left">
                <img src="<c:url value="/static/tagthiscar/standard/images/features_002.png" />">
              </div>
              <div class="span6 offset1 pull-right">
                <h2 class="featurette-heading">
                  Get Suggestions.
                  <span class="muted">Our goal is your safety.</span>
                </h2>
                <p class="lead">
                  We want to give you all the information you need to be <b>safe on the road</b>.
                  Our <b>patent-pending</b> technology will generate real-time suggestions for you,drivers, based on your search criteria and the vehicle or location data provided.
                </p>
              </div>
            </div>

            <hr class="featurette-divider">

            <!-- Feature 3 -->
            <div class="row-fluid featurette">
              <div class="span6">
                <h2 class="featurette-heading">
                  And the most important one:
                  <span class="muted">Save Lives.</span>
                </h2>
                <p class="lead">
                  We plan to give US drivers the tools to help save <b>10 million</b> motor vehicle accidents, save <b>$300 billion</b> in costs,
                  and above all, prevent <b>35,000</b> deaths per year. Help identify anything (we call it <b>"tagging"</b>) on the road that others can learn from.
                  This could be tagging a slippery road on a snowy day or tagging an intersection due to accident.
                </p>
              </div>
              <div class="span5 offset1 pull-right">
                <img src="<c:url value="/static/tagthiscar/standard/images/features_003.png" />">
              </div>
            </div>

        </article>

    <div id="push"></div>
  </div>
  <!-- /container -->
</section>
<!-- /END THE FEATURES -->