<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%--
    <form class="navbar-search pull-left" action="<c:url value="/" />" method="POST">
          <input id="latitudeId" name="latitude" type="hidden"  value="${search.latitude}"/>
          <input id="longitudeId" name="longitude" type="hidden"  value="${search.longitude}"/>
          <input type="hidden" id="addressId" name="address" value="${search.address}" />
          <input type="hidden" id="noOfMilesId" name="noOfMiles" value="${search.value}" />
          <input type="hidden" id="destinationId" name="destination" value="${search.destination}" />
          <input type="hidden" id="avoidHighwaysId" name="avoidHighwaysHidden" value="${search.avoidHighways}" />
          <input type="hidden" id="avoidTollsId" name="avoidTollsHidden" value="${search.avoidTolls}" />
          <input type="hidden" id="travelModeHiddenId" name="travelModeHidden" value="${search.travelMode}"/>
          <input type="hidden" id="csvFlagTypesId" name="csvFlagTypesHidden" value="${search.csvFlagTypes}"/>
          <input type="hidden" id="limitId" name="limitHidden" value="${search.limit}"/>
            <input type="hidden" id="noOfDaysId" name="noOfDaysHidden" value="${search.noOfDays}"/>
  --%>

          <!-- Filter starts here
                      <div class="row-fluid">
                          <div class="form-inline">
                          <label for="whatId"><strong>Look for:</strong></label>
                          <select name="csvFlagTypes" id="whatId" multiple="multiple" class="span2 multiple-select">
                               <option value="caution">Caution</option>
                               <option value="car">Car</option>
                               <option value="caraccident">Car Accident</option>
                               <option value="accident">Accident</option>
                           </select>

                            <label for="whatId"><strong>within</strong></label>
                           <select class="span1" name="value" id="whatId">
                             <option <c:if test="${empty search.value or search.value eq 1}">selected</c:if>>1</option>
                             <option <c:if test="${ search.value eq 2}">selected</c:if>>2</option>
                             <option <c:if test="${ search.value eq 5}">selected</c:if>>5</option>
                             <option <c:if test="${search.value eq 10}">selected</c:if>>10</option>
                             <option <c:if test="${search.value eq 15}">selected</c:if>>15</option>
                          </select>

                          <label for="vehicleOrLocationId"><strong>miles of:</strong></label>
                          <input class="span4 input-medium" name="searchTerm" id="vehicleOrLocationId" type="text" placeholder="type a location - Sterling, Va"  value="${search.searchTerm}" x-webkit-speech="" required>

                          <label for="lastId"><strong>in the last:</strong></label>
                          <select class="span1" name="noOfDays" id="lastId">
                               <option value="-1" <c:if test="${search.noOfDays eq -1}">selected</c:if>>1</option>
                               <option value="-2" <c:if test="${search.noOfDays eq -2}">selected</c:if>>2</option>
                               <option value="-5" <c:if test="${search.noOfDays eq -5}">selected</c:if>>5</option>
                               <option value="-10" <c:if test="${search.noOfDays eq -10}">selected</c:if>>10</option>
                               <option value="-15" <c:if test="${search.noOfDays eq -15}">selected</c:if>>15</option>
                          </select>  days.
                           <button id="homeSubmitId" class="btn btn-primary" type="submit" > <i class="icon-search icon-white"></i></button>
                           </div>
                      </div>
           filter ends here -->

          <!-- Location Search Section
          <div class="row">
          <div class="span3"></div>
          <div id="searchWithinDivId" class="span6 form-horizontal">
            <h4 class="text-center">
            Search for tags within
            <select class="span1" name="value">
               <option <c:if test="${search.value eq 1}">selected</c:if>>1</option>
               <option <c:if test="${ search.value eq 2}">selected</c:if>>2</option>
               <option <c:if test="${ search.value eq 5}">selected</c:if>>5</option>
               <option <c:if test="${search.value eq 10}">selected</c:if>>10</option>
               <option <c:if test="${empty search.value or search.value eq 15}">selected</c:if>>15</option>
               <option <c:if test="${search.value eq 25}">selected</c:if>>25</option>
               <option <c:if test="${search.value eq 50}">selected</c:if>>50</option>
           </select> miles of
            </h4>
          </div>
          <div class="span3"></div>
          </div>

          <div class="row">
            <div class="span2"></div>
            <div class="span8 form-horizontal">
                <input class="span6 input-large" name="searchTerm" id="vehicleOrLocationId" type="text" placeholder="type a location - Sterling, Va"  value="${search.searchTerm}" x-webkit-speech="" required>
                <button id="homeSubmitId" class="btn btn-large btn-primary" type="submit">Go!</button>
            </div>
            <div class="span2"></div>
          </div>
           -->
       <!--   Location Search Section ends -->

         <div id="destinationDivId" class="row form-horizontal">
             <br>
             <div class="span2"></div>
             <div class="span8">
                 <!--
                 <input class="span6 input-large" name="destinationTerm" id="destId" type="text" placeholder="Where do you plan to go? e.g. Chantilly, Va"  value="${search.destination}" x-webkit-speech="">
                 <div class="span8 row">
                  <input type="checkbox" id="skipHighwaysId" name="avoidHighways" value="true" />  Avoid Highways
                  <input type="checkbox" id="skipTollsId" name="avoidTolls" value="true" />   Avoid Tolls
                  <br>
                  Mode:
                  <input type="radio" name="travelMode" value="DRIVING" checked>DRIVING
                  <input type="radio" name="travelMode" value="BICYCLING">BICYCLING
                  <input type="radio" name="travelMode" value="TRANSIT">TRANSIT
                  <input type="radio" name="travelMode" value="WALKING">WALKING
                </div>
                -->
              </div>
             <div class="span2"></div>

         </div>


        <div class="row">
            <%--
             <div class="span3"></div>
             <div class="span6">
                     Let us help you plan your trip:<a id="showDestinationDivLinkId" href="#">+ Add Additional Location</a>
                     <br>
             </div>
             <div class="span3"></div>
             --%>
        </div>
<%--
    </form>
--%>

