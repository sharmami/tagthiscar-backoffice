<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<article id="intro">

      <div class="jumbotron">
        <h1>Making drivers smarter!</h1>
        <p class="lead">Imagine having the power to predict whatever the road might throw at you. Now you can!</p>
        <a class="btn btn-large btn-success" href="<c:url value="/signup" />">Get started today</a>
      </div>
      <!--
      <hr>
      <div class="container marketing">
          <div class="row-fluid">
            <div class="span4">
              <img src="<c:url value="/static/tagthiscar/standard/bootstrap/img/download.png" />" class="img-rounded">
              <h2>Search a location</h2>
              <p>
                Look up <b>any location</b> for a detailed history of accidents, congested areas,
                construction sites, closed roads, fallen debris, icy roads, cautionary signs, detours,
                missing road signs and just about everything on the road that maps
                or your navigation devices won't tell you.
              </p>
              <p><a class="btn" href="#">View details &raquo;</a></p>
            </div>
            <div class="span4">
              <img src="<c:url value="/static/tagthiscar/standard/bootstrap/img/download.png" />" class="img-circle">
              <h2>Get Suggestions</h2>
              <p>
              Our goal aligns with your safety.
              We want to give you all the information you need to be <b>safe on the road</b>.
              Our <b>patent pending</b> technology will generate real time suggestions for you (the drivers)
              based on your search criteria and the vehicle or location data ever reported.
              </p>
              <p><a class="btn" href="#">View details &raquo;</a></p>
            </div>
            <div class="span4">
              <img src="<c:url value="/static/tagthiscar/standard/bootstrap/img/download.png" />" class="img-polaroid">
              <h2>Save Lives</h2>
              <p>
              <b>We plan to avoid 10 million motor vehicle accidents, save $300 billion in costs
              and above all prevent 35,000 deaths this year.</b><br>
              Help identify anything (we call it <b>'tagging'</b>) on the road that others can learn from.
              This could be tagging a slippery road on a snowy day or tagging a vehicle for not following traffic safety rules.
              </p>
              <p><a class="btn" href="#">View details &raquo;</a></p>
            </div>
          </div>
      <div>
      -->

      <!-- START THE FEATURETTES -->

            <hr class="featurette-divider">

            <div class="featurette">
              <img class="featurette-image pull-right" src="<c:url value="/static/tagthiscar/standard/bootstrap/img/browser-icon-chrome.png" />">
              <h2 class="featurette-heading">Search for a location. <span class="muted">Enter a zip code or address.</span></h2>
              <p class="lead">
              Look up <b>any location</b> for a detailed history of accidents, congested areas,
              construction sites, closed roads, fallen debris, icy roads, warning signs, detours,
              missing road signs, and just about everything on the road - things that maps
              or your navigation devices don't tell you.
              </p>
            </div>

            <hr class="featurette-divider">

            <div class="featurette">
              <img class="featurette-image pull-left" src="<c:url value="/static/tagthiscar/standard/bootstrap/img/browser-icon-firefox.png" />">
              <h2 class="featurette-heading">Get Suggestions. <span class="muted">Our goal is your safety.</span></h2>
              <p class="lead">We want to give you all the information you need to be <b>safe on the road</b>.
              Our <b>patent-pending</b> technology will generate real-time suggestions for you,drivers, based on your search criteria and the vehicle or location data provided.</p>
            </div>

            <hr class="featurette-divider">

            <div class="featurette">
              <img class="featurette-image pull-right" src="<c:url value="/static/tagthiscar/standard/bootstrap/img/browser-icon-safari.png" />">
              <h2 class="featurette-heading">And the most important one: <span class="muted">Save Lives.</span></h2>
              <p class="lead">
              We plan to give US drivers the tools to help save <b>10 million</b> motor vehicle accidents, save <b>$300 billion</b> in costs,
              and above all, prevent <b>35,000</b> deaths per year. Help identify anything (we call it <b>"tagging"</b>) on the road that others can learn from.
              This could be tagging a slippery road on a snowy day or tagging an intersection due to accident.
              </p>
            </div>

            <hr class="featurette-divider">

            <!-- /END THE FEATURETTES -->

</article>