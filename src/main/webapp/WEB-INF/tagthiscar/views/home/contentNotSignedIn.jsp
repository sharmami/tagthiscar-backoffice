<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<article id="intro">
    <hgroup>
            <div class="row-fluid">
                    <c:if test="${not empty search.destination}">
                        <div class="row-fluid">
                          <div class="span3">
                            <fieldset class="fieldsetBox">
                                <legend class="frameTitle">Routes</legend>
                                <div id="directions" class="directions"/>
                            </fieldset>
                          </div>
                          <div class="span6">
                            <fieldset class="fieldsetBox">
                                <legend class="frameTitle">Map</legend>
                                 <!-- <small>(<a id="updatePageId" href="#">I moved the map around.update my results.</a>)</small> -->
                                <div id="largemap"></div>
                           </fieldset>
                           </div>
                           <div class="span3">
                           <fieldset class="fieldsetBox">
                                <div id="mapUpdatedResultsId"/>
                          </fieldset>
                           <fieldset class="fieldsetBox">
                                 <legend class="frameTitle">Tags</legend>
                                 <div id="tagorSitesResultsId"/>
                            </fieldset>
                          </div>
                        </div>
                    </c:if>
                    <c:if test="${empty search.destination}">
                        <div class="row-fluid">
                          <div class="span6">
                                    <label class="checkbox">
                                       <small>
                                      <input type="checkbox" name="parkingEnabled" id="parkingEnabledId" value="" <c:if test="${search.parkingEnabled}">checked</c:if> ><strong>show parking tags</strong> |
                                       <a id="updatePageId" href="#">I moved the map around. Update my results.</a>
                                       </small>
                                    </label>

                                <div id="largemap"></div>
                           </div>
                           <div class="span6">
                           <fieldset class="fieldsetBox">
                             <div id="mapUpdatedResultsId"/>
                           </fieldset>
                           <fieldset class="fieldsetBox">
                                 <legend class="frameTitle">Tags
                                 <small>
                                 <ul class="nav nav-pills pull-right">
                                   <li class="dropdown">
                                     <a class="dropdown-toggle" data-toggle="dropdown" href="#">Sort <b class="caret"></b></a>
                                     <ul class="dropdown-menu">
                                      <li><a href="share?searchTerm=${search.searchTerm}&limit=${search.limit}&noOfDays=${search.noOfDays}&value=${search.value}&sortBy=submittedDate">Latest</a></li>
                                      <li><a href="share?searchTerm=${search.searchTerm}&limit=${search.limit}&noOfDays=${search.noOfDays}&value=${search.value}&sortBy=updatedDate">Last Updated</a></li>
                                        <li><a href="share?searchTerm=${search.searchTerm}&limit=${search.limit}&noOfDays=${search.noOfDays}&value=${search.value}&sortBy=location.address">Address</a></li>
                                     </ul>
                                   </li>
                                 </ul>
                                 </small>
                                 </legend>
                                  <div id="tagorSitesResultsId"/>
                            </fieldset>
                          </div>
                        </div>
                    </c:if>

            </div>

    </hgroup>
</article>