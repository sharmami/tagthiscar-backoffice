<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>

<article id="intro">
	<div id="intro">
		<h2>Welcome to tagthiscar <security:authentication property="name"/>! </h2>
		
		<!--  
		<small><i>We can have a quick quiz here: I hope this login process wasn't painful? 
			<input type="radio" name="loginProcessPain">Good grief. Do something about it! 
			<input type="radio" name="loginProcessPain">No.. it was fine.
			<input type="radio" name="loginProcessPain">Dont bug me.I am busy.
			</i>
		</small>
			
		<br><br>	
		-->
		
		<!--  
		<p>
		Oh a first timer... well first things first.... <a href="<c:url value="/aboutthekid" />">Tell me more about the kid</a>.
		</p>
		OR
		<p>Did you want to update KIDS_NAME_HERE's information? <a href="<c:url value="/aboutthekid" />">Click here</a> if you want to add another kid</p>
		
		<br>
		-->
		
		
		<security:authorize ifAnyGranted="ROLE_PARENT">
			<p>
				First things first .... Have you filled out our <a href="<c:url value="/reachme" />">How to reach me form</a>?.
				This information will be shared with your kid's teachers so they know how to reach you for any questions.
				<!--  	
				<small>And if it wasn't clear... we are not calling you for anything.We will email you if we need anything from you.</small>
				-->
			</p>
			
			<br>
		
			<p>
				<table border=0>
					<tr>
						<td>
							<!--  
							<font color=red>A place for each kids' link here below.</font>
							-->
						</td>
					</tr>
					<tr>
						<td>
							If you are here looking for Akule's Activities, Dig right in <a href="<c:url value="activity/1/akule" />">here</a>.
							Check out their <a href="<c:url value="report/daily/akule" />">end-of-day</a> report. 
							Click <a href="<c:url value="report/akule" />">here</a> to see past reports in case you missed them.
							<!--  
							<small>we need a place to update Akule profile information somewhere</small>
							-->
						</td>
					</tr>
					<!--  
					<tr>
						<td>
							If you are here looking for Ahavi's Activities, Dig right in <a href="<c:url value="activity/1/ahavi" />">here</a>.
							<small>we need a place to update Ahavi profile information somewhere</small>
						</td>
					</tr>
					-->
				</table>
			</p>
		
			<br>
			
			<p>
				What does the teacher need to know today (his food time, nap time)? <a href="<c:url value="daily/1/akule" />">Add their daily schedule here</a>
				
				<!--  
				<small>
					<i>
					Topics we are covering here:
					Add kids daily schedule - food time, nap time, etc
					- Activity Free text (example Cereal, Fruit, Bottle or nap)
					- Aproximate time (ex - 10:00am, 11:30am....)
					</i>
				</small>
				-->
			</p>
			
			<br>
			<p>
				Are kids going on vacation? <a href="<c:url value="vacation/1/akule" />">Add their vacation schedule here</a>
				<!--  
				Who is going? <input type="checkbox" name="">Akule <input type="checkbox" name="">Ahavi 		
				Going on: <input type="date" name="" placeholder="MM/DD/YY"> Coming back on: <input type="date" name="" placeholder="MM/DD/YY"> <input type="button" name="" value="Submit">
				<table border=1>
					<tr>
					 <td>Who?</td>
					 <td>
					 Going On?
					 </td>
					 <td>
					 Coming back?
					 </td>
					</tr>
					<tr>
						<td>Akule</td>
						<td>05/01/12</td>
						<td>05/05/12</td>
					</tr>
					<tr>
						<td>Akule</td>
						<td>05/01/12</td>
						<td>05/05/12</td>
					</tr>
				</table>
				-->
			</p>
			
			<br>
			<p>
				Have you uploaded a scanned copy of Kids' Immunization records? <a href="<c:url value="immunization" />">Update them here</a>
			</p>
			
			<br>
			<p>
				Payments Section?
				Print out your <a href="">Tuition payment report for FSA reimbursement</a>. Click <a href="">here</a> to print a payment report for certain period.
			</p>
			
			
			<br>
			<p>
				Announcement or reminder for events: <br><br>
				
				BTW, following events are coming up:
				<ul>
					<li>Bike Day on 4th April for <a href="">Tadpole 3</a></li>
					<li>Magic Show on 5th May for <a href="">Toddler Session II</a></li>
					<li>Water Fun Day on 27th May <a href="">Infant I</a></li>
				</ul>
				
				<small>
					<i>
					(either school principal or any authorized teacher can start this .Parents can then subscribe to it)- 
						- Event name. Example - bike day, magic show, water fun day, 
						- date and time
					</i>
				</small>
			</p>
			
			<br>
			<p>
				If you haven't already, Register for paid activities
				<small><i>
					Example: Soccer Activities, Stretch and Grow, Music Lesson. Maybe this can be tied in with the Announcement or reminder.
				</i>
				</small>
			</p>
			
			<br>
			<p>
				Parents, Please take a moment to <a href="<c:url value="/ratetheprogram" />">Rate the program here</a>
				<small>See the preschool evaluation form in svn mockups</small>
			</p>
			
		</security:authorize>
		
		<security:authorize ifAnyGranted="ROLE_TEACHER">
			<p>
				Have you filled out our <a href="<c:url value="/reachme" />">How to reach me form</a>?.
				This information will be shared with the parents of the kids in your class so they know how to reach you for any questions.
				<small>And if it wasn't clear... we are not calling you for anything.We will email you if we need anything from you.</small>
			</p>
			
			<br>
			<p>
				Join your session for Today : <a href="<c:url value="session/2/tadpole3" />">Tadpole III</a>
			</p>
			
			<br>
			<p>
				Manage Registration for paid activities
				<small><i>
				Example: Soccer Activities, Stretch and Grow, Music Lesson
				</i>
				</small>
			</p>
			
			<br>
			<p>
				Announcement or reminder for events 
				
				BTW, following events are coming up:
				<ul>
					<li>Bike Day on 4th April for <a href="">Tadpole 3</a></li>
					<li>Magic Show on 5th May for <a href="">Toddler Session II</a></li>
					<li>Water Fun Day on 27th May <a href="">Infant I</a></li>
				</ul>
				
				<small>
					<i>
					(either school principal or any authorized teacher can do it)- 
						- Event name. Example - bike day, magic show, water fun day, 
						- date and time
					</i>
				</small>
			</p>

			<p>
				Vacation Reminders: (if any)<br>
				<ul>
					<li><a href="<c:url value="/activity/1/akule" />">Akule</a> will be going on Vacation starting 1st May - 3rd May. He will be back on 5th May, Monday.</li>
					<li><a href="<c:url value="/activity/1/akule" />">Anahi</a> is currently on Vacation. She is back on 5th May, Monday.</li>
				</ul> 
			</p>
			
			<br>
			<p>
				<a href="">Evaluate your student here</a>
			</p>
			
		</security:authorize>
		
		
		
		<security:authorize ifAnyGranted="ROLE_ADMIN">
			<p>
			Do you Admin thing here : <a href="<c:url value="/admin" />">Admin</a>
			</p>	
			
			<br>
			<p>
				Manage Registration for paid activities
				<small><i>
				Example: Soccer Activities, Stretch and Grow, Music Lesson
				</i>
				</small>
			</p>
			
			<br>
			<p>
				Anouncement or reminder for events
				
				BTW, following events are coming up:
				<ul>
					<li>Bike Day on 4th April for <a href="">Tadpole 3</a></li>
					<li>Magic Show on 5th May for <a href="">Toddler Session II</a></li>
					<li>Water Fun Day on 27th May <a href="">Infant I</a></li>
				</ul>
				 
				<small>
					<i>
					(either school principal or any authorized teacher can do it)- 
						- Event name. Example - bike day, magic show, water fun day, 
						- date and time
					</i>
				</small>
			</p>	
		</security:authorize>
		
		
	</div>
	
	<!-- Shows how to pull images from our static folder.  
	<div id="appIcons">
		<a href="http://itunes.apple.com/us/app/greenhouse/id395862873"><img src="<c:url value="/static/tagthiscar/mobile/images/icon-apple-appstore.gif" />" /></a>
		<a href="https://market.android.com/details?id=com.springsource.greenhouse"><img src="<c:url value="/static/tagthiscar/mobile/images/icon-android-marketplace.gif" />" /></a>
	</div>
	-->

</article>