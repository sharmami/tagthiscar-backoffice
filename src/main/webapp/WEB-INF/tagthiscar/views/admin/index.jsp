<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${not empty message}">
	<div class="${message.type.cssClass}">${message.text}</div>
</c:if>

<article id="intro">
    <hgroup>
        <div class="page-header">
                   <h3>Admin</h3>
        </div>

         <table class="table table-striped table-bordered table-hover table-condensed" id="dataTable">
                    <caption><b>Feed List Total: ${fn:length(allFeedFiles)}</b></caption>
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>File Name</th>
                         <th>Others</th>
                    </tr>
                     </thead>
                     <tbody>
                        <c:forEach var="file" items="${allFeedFiles}">
                            <tr>
                                <td><a href="<c:url value="/${file.id}" />">${file.id}</a></td>
                                <td>${file.filename}</td>
                                <td>${file}</td>
                            </tr>
                        </c:forEach>
                      </tbody>
                  </table>
    </hgroup>
</article>
	
	
	
	
