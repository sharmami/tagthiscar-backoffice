<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>

<c:if test="${not empty message}">
	<div class="${message.type.cssClass}">${message.text}</div>
</c:if>

<article id="intro">
    <hgroup>
        <div class="page-header">
            <h3>Frequently Asked Questions</h3>
            <strong>Please do not use this app or website while driving.</strong>
        </div>

            <fieldset class="fieldsetBox">
              <legend class="frameTitle">General</legend>
              <div>
                <p>
                    <strong>Q. What is Tag This Car?</strong>
                    <br>
                    A. Tag This Car LLC is Sterling, Loudoun County, Va based technology startup formed to help you save money and avoid stress on the road.
                    We provide real-time data and analytical tools for drivers to become smarter on the road.
                    We believe drivers should have access to all local information on the road in making smarter automotive (vehicular and location-based) decisions.
                </p>


                <p>
                    <strong>Q. What is an alpha version?</strong>
                    <br>
                    A. It is an early access version of the site (with potential bugs) while we fix up and bring out the better version.
                    This gives you a chance to try it early and recommend features you'd like to see in it. This gives us a chance to validate our offerings.
                    You can help us by liking, tweeting, sharing, recommending us.
                </p>

                <p>
                <strong>Q. Which cities are you covering?</strong>
                <br>
                A. We are currently targeting DC metro area for our early access (alpha) version but will be expanding to other areas soon. If you would like to request your city coverage, please let us know <a href="mailto:requestcoverage@tagthiscar.com">here</a>.
                </p>

                <p>
                  <strong>Q. What kind of information do you provide to drivers?</strong>
                  <br>
                  A. We provide all information that drivers can learn from to be <b>safe</b> on the road - accidents, potholes, traffic congestion, road blocks, detours, weather, missing signs, speed limits, obstructed views etc..
                </p>

                <p>
                     <strong>Q. How much do you charge the drivers for this information?</strong>
                     <br>
                     A. zero. zilch. nada.
               </p>

                <p>
                    <strong>Q. Where do you pull this data from?</strong>
                    <br>
                    A. We are in the process of gathering data from various statewide, county, city, metro alerts, weather systems, twitter feeds, federal agency emergency response systems and several paid and non-paid data providers.
                    A feature to report this data to our site by drivers is in works already.
                  </p>

                  <p>
                      <strong>Q. How can I report a feature or a problem?</strong>
                      <br>
                      A. We welcome your feedback. please dont hesitate to send us an email at info@tagthiscar.com.
                  </p>

              </div>
            </fieldset>
           <br>

         <fieldset class="fieldsetBox">
                 <legend class="frameTitle">Credits</legend>
                 <div>
                   <p>
                   <img src="<c:url value="/static/tagthiscar/standard/images/markers/miclogo-88x31.gif" />" alt="Map Icons Collection" />
                   <a href="http://mapicons.nicolasmollet.com" target="_blank">Maps Icons Collection</a>
                    </p>

                 </div>
              </fieldset>
             <br>
    </hgroup>
</article>
	
	
