<%@ page session="false" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${not empty message}">
	<div class="${message.type.cssClass}">${message.text}</div>
</c:if>

 <div class="page-header">
                <h3>Sign In</h3>
</div>
<article id="intro">
    <div class="container">
        <c:if test="${not empty param['error']}">
            <div class="span1"></div>
            <div class="span7">
                <div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Error!</strong> Your sign in information was incorrect.
                    Please try again or <a href="<c:url value="/signup" />">sign up</a>.
                </div>
            </div>
            <div class="span1"></div>
        </c:if>

         <div class="row-fluid">
           <div class="span12">
              <form class="form-signin" id="signin" action="<c:url value="/signin/authenticate" />" method="post">
                              <h2 class="form-signin-heading">Login</h2>
                              <input id="login" name="j_username" type="text" class="input-block-level" placeholder="Email address" required>
                              <input id="password" name="j_password" type="password" class="input-block-level" placeholder="Password"  required>
                              <button class="btn btn-primary" type="submit" data-loading-text="authenticating...">Sign in</button>
                          </form>
            </div>
            <%--
             <div class="span6">
                       <form class="form-signin" id="signin" action="<c:url value="/signin/authenticate" />" method="post">
                        <h3 >Forgot Password?</h3>
                          <input id="login" name="j_username" type="text" class="input-block-level" placeholder="Email address" required>
                          <button class="btn btn-primary" type="submit">Send</button>
                       </form>
           </div>
           --%>
         </div>


    </div>

</article>