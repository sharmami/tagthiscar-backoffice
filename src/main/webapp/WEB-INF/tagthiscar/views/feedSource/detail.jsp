<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:if test="${not empty message}">
	<div class="${message.type.cssClass}">${message.text}</div>
</c:if>

<article id="intro">
    <hgroup>
        <div class="page-header">
                   <h3>Feed Source Details</h3>
        </div>

              <form method="POST" action="<c:url value="/feedSource/save" />">
              <input type="hidden" name="id" value="${feedSource.id}"/>

              <table class="table table-striped table-bordered table-hover table-condensed">
                 <caption>Feed Source</caption>
                  <tbody>

                            <tr>
                              <td>Name</td>
                              <td>
                                        <input class="span8" type="text" name="feedName" value="${feedSource.feedName}">
                              </td>
                        </tr>

                         <tr>
                                  <td>URL</td>
                                  <td>
                                            <input class="span8" type="text" name="feedUrl" value="${feedSource.feedUrl}">
                                  </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <input type="submit" class="btn" name="" value="Save">
                                </td>
                            </tr>
                   </tbody>
               </table>
               </form>
    </hgroup>
</article>
	
	
	
	
