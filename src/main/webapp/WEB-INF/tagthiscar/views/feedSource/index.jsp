<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<article id="intro">
    <hgroup>
        <div class="page-header">
                   <h3>Feed Sources</h3>
        </div>
         <form method="POST" action="<c:url value="/feedSource/delete" />">
             <div class="row pull-right">
                <input class="btn" type="submit" name="saveBtn" value="Delete Checked" />
             </div>

             <br><br>

             <table class="table table-striped table-bordered table-hover table-condensed" id="dataTable">
                <caption><b>Feed Sources Total: ${fn:length(feedSources)}</b></caption>
                <thead>
                <tr>
                    <th></th>
                    <th>Id</th>
                    <th>Feed Source Name</th>
                    <th>Feed Url</th>
                </tr>
                 </thead>
                 <tbody>
                    <c:forEach var="feedSource" items="${feedSources}">
                        <tr>
                             <th><input type="checkbox" name="checkedIds" value="${feedSource.id}" /></th>
                            <td><a href="<c:url value="/feedSource/${feedSource.id}" />">${feedSource.id}</a></td>
                            <td>${feedSource.feedName}</td>
                            <td>${feedSource.feedUrl}</td>
                        </tr>
                    </c:forEach>
                  </tbody>
              </table>
          </form>
    </hgroup>
</article>
	
	
	
	
