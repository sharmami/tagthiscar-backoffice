<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
 <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:if test="${not empty message}">
	<div class="${message.type.cssClass}">${message.text}</div>
</c:if>

<article id="intro">
    <hgroup>
        <div class="page-header">
            <h3>Subscriptions / Analytics</h3>
        </div>

         <c:forEach var="subscription" items="${subscriptions}">
                              ${subscription}  <br>
                  </c:forEach>

         <br> <br>
        <table class="table table-striped table-bordered table-hover table-condensed" id="dataTable1">
                    <caption><b>Total: ${page.totalElements}</b></caption>
                    <thead>
                    <tr>
                        <th>Id</th>
                         <th>date</th>
                         <th>OS</th>
                         <th>session Id</th>
                         <th>browser</th>
                         <th>version</th>
                         <th>username</th>
                         <th>page Url</th>
                         <th>IP address</th>
                         <th>Screen Resolution</th>
                         <th>Comments</th>
                     </tr>
                     </thead>
                     <tbody>
                        <c:forEach var="userStat" items="${page.content}">
                                    <tr>
                                        <td>${userStat.id}</td>
                                       <td>${userStat.submittedDate}</td>
                                        <td>${userStat.operatingSystem}</td>
                                        <td>${userStat.sessionId}</td>
                                        <td>${userStat.browser}</td>
                                        <td>${userStat.version}</td>
                                        <td>${userStat.username}</td>
                                        <td>${userStat.pageUrl}</td>
                                        <td>${userStat.ipAddress}</td>
                                        <td>${userStat.screenResolution}</td>
                                        <td>${userStat.comments}</td>
                                     </tr>
                                </c:forEach>
                      </tbody>
                  </table>
                   Showing ${((page.number)*(page.size))+1} to ${((page.number)*(page.size))+(page.numberOfElements)} of  ${page.totalElements} entries

                                      <br><br>
                                       <c:choose>
                                                <c:when test="${not page.firstPage}">
                                                        <spring:url value="" var="first">
                                                            <spring:param name="page.page" value="1" ></spring:param>
                                                            <spring:param name="page.size" value="10" ></spring:param>
                                                       </spring:url>
                                                       <a href="${first}">First</a>
                                                </c:when>
                                                <c:otherwise>
                                                  First
                                                </c:otherwise>
                                    </c:choose>

                                       <c:choose>
                                                               <c:when test="${not page.firstPage}">
                                                              <spring:url value="" var="previous">
                                                                  <spring:param name="page.page" value="${page.number}" ></spring:param>
                                                                  <spring:param name="page.size" value="${page.size}" ></spring:param>
                                                             </spring:url>
                                                              <a href="${previous}">Previous</a>
                                                               </c:when>
                                                              </c:choose>

                                                   <c:choose>
                                                               <c:when test="${not page.lastPage}">
                                                                  <spring:url value="" var="next">
                                                                      <spring:param name="page.page" value="${page.number+2}" ></spring:param>
                                                                      <spring:param name="page.size" value="${page.size}" ></spring:param>
                                                                 </spring:url>
                                                                  <a href="${next}">Next</a>
                                                                  </c:when>
                                                  </c:choose>


                                      <c:choose>
                                       <c:when test="${not page.lastPage}">
                                               <spring:url value="" var="last">
                                                   <spring:param name="page.page" value="${page.totalPages}" ></spring:param>
                                                   <spring:param name="page.size" value="10" ></spring:param>
                                              </spring:url>
                                              <a href="${last}">Last</a>
                                       </c:when>
                                       <c:otherwise>
                                         Last
                                       </c:otherwise>
                                      </c:choose>
    </hgroup>
</article>


	
