<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>

<c:if test="${not empty message}">
<div class="${message.type.cssClass}">${message.text}</div>
</c:if>
<article id="intro">

	<div class="page-header">
                <h3>Registration</h3>
               <blockquote>
                 <p>
                    At this point, we are accepting user registrations via invites only.
                    Please email us at:  info at tagthiscar.com if you are interested in obtaining a user account with us.
                    <small>Tag This Car Team</small>
                 </p>
               </blockquote>

            </div>
</article>	
