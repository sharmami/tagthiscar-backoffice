<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>

<c:if test="${not empty message}">
<div class="${message.type.cssClass}">${message.text}</div>
</c:if>
<article id="intro">

    <div class="page-header">
                <h3>Sign Up</h3>
    </div>


	         <div class="row-fluid">
               <div class="span12">

                 <div class="row-fluid">
                   <div class="span12">
                             <form class="form-signin">
                                             <div class="control-group">
                                               <label class="control-label" for="email">Email</label>
                                               <div class="controls">
                                                 <input type="email" id="email" placeholder="email@address.com" required>
                                               </div>
                                             </div>

                                             <div class="control-group">
                                               <label class="control-label" for="password">Password</label>
                                               <div class="controls">
                                                 <input type="password" id="password" placeholder="Password">
                                               </div>
                                             </div>
                                             <div class="control-group">
                                                <label class="control-label" for="confirmPassword">Confirm Password</label>
                                                <div class="controls">
                                                  <input type="password" id="confirmPassword" placeholder="Password">
                                                </div>
                                              </div>
                                             <div class="control-group">
                                               <div class="controls">
                                                 <label class="checkbox">
                                                   <input type="checkbox"> I agree to the <a target="_blank" href="<c:url value="/terms_of_use" />">Terms of Service</a>.
                                                 </label>
                                                 <button type="submit" class="btn btn-primary">Sign Up</button>
                                               </div>
                                             </div>
                                           </form>
                   </div>
                   <%--
                   <div class="span6 form-signin ">
                            <p>You can also sign up using any of the existing accounts below.</p>
                            <span id="signinButton">
                              <span
                                class="g-signin"
                                data-callback="signinCallback"
                                data-clientid="870164560384.apps.googleusercontent.com"
                                data-cookiepolicy="single_host_origin"
                                data-requestvisibleactions="http://schemas.google.com/AddActivity"
                                data-scope="https://www.googleapis.com/auth/plus.login">
                              </span>
                            </span>

                            <br><br>
                            <a href=""><img src="https://dev.twitter.com/sites/default/files/images_documentation/sign-in-with-twitter-gray.png"/></a>
                              <br><br>
                            <img src="http://static.ak.fbcdn.net/rsrc.php/zB6N8/hash/4li2k73z.gif">

                   </div>
                   --%>
                 </div>
               </div>
             </div>

           <script>
           function signinCallback(authResult) {
             //alert('here1');
              if (authResult['access_token']) {
                // Successfully authorized
                // Hide the sign-in button now that the user is authorized, for example:
                document.getElementById('signinButton').setAttribute('style', 'display: none');
                alert('here: '+(authResult['id_token']));
               alert('code: '+(authResult['code']));


                 //var revokeUrl = 'https://accounts.google.com/o/oauth2/revoke?token=' + access_token;
                  //var revokeUrl = 'https://www.googleapis.com/auth/plus.me?token='+(authResult['access_token']);
                  //var revokeUrl = 'https://www.googleapis.com/plus/v1/people/sharmami';
                 var revokeUrl = 'https://www.googleapis.com/oauth2/v1/userinfo?access_token='+(authResult['access_token']);
                  // Perform an asynchronous GET request.
                  $.ajax({
                    type: 'GET',
                    url: revokeUrl,
                    async: false,
                    contentType: "application/json",
                    dataType: 'jsonp',
                    success: function(nullResponse) {
                      // Do something now that user is disconnected
                      // The response is always undefined.
                      alert('success');
                      alert('id: '+nullResponse['id']);
                      alert('email : '+nullResponse['email']);
                      alert('verified_email: '+nullResponse['verified_email']);
                      alert('name: '+nullResponse['name']);
                      alert('given_name: '+nullResponse['given_name']);
                      alert('family_name: '+nullResponse['family_name']);
                      alert('picture: '+nullResponse['picture']);
                      alert('gender: '+nullResponse['gender']);
                      alert('locale: '+nullResponse['locale']);

                      //alert(nullResponse.toSource());
                      //({id:"110717306438352336670"})
                    },
                    error: function(e) {
                      // Handle the error
                      // console.log(e);
                      // You could point users to manually disconnect if unsuccessful
                      // https://plus.google.com/apps
                    }
                  });


              } else if (authResult['error']) {
                // There was an error.
                // Possible error codes:
                //   "access_denied" - User denied access to your app
                //   "immediate_failed" - Could not automatically log in the user
                // console.log('There was an error: ' + authResult['error']);
              }
            }
           </script>


</article>	
