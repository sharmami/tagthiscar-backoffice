<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
 <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<article id="intro">
    <hgroup>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span12">
                        <form action="<c:url value="/sites/search" />" method="POST">
                            <div class="form-inline">
                                <input type="text" name="searchTerm"  value=""/>
                                <input type="hidden" name="noOfDays"  value="-10000"/>
                                <input type="submit" name="searchSites" value="Search" class="btn"/>
                            </div>
                        </form>
                 </div>
                  <div class="span12">
                        <form action="<c:url value="/sites/addToLabel" />" method="POST">

                        <div class="row pull-left form-inline span9">
                            <input type="submit" name="addToLabels" value="Add to Labels" class="btn"/>
                            <select name="label" id="labelId" class="span4">
                                 <option></option>
                                <c:forEach var="label" items="${allLabels}">
                                    <option value="${label.id}">${label.name}</option>
                                </c:forEach>
                            </select>
                         </div>
                         <div class="row pull-right form-inline span3">
                            <input type="submit" name="deleteSites" value="Delete Checked" class="btn"/>
                         </div>

                         <br><br>
                        <table class="table table-striped table-bordered table-hover table-condensed">
                                   <caption><b>Sites List Total HERE : ${fn:length(sites)}</b></caption>
                                   <thead>
                                   <tr>
                                       <th><input type="checkbox" name="selectedSiteIds"></th>
                                       <th>Id</th>
                                       <th>Teaser</th>
                                       <th>Address</th>
                                       <th>Submitted Date</th>
                                       <th>Submitted By</th>
                                   </tr>
                                    </thead>
                                    <tbody>
                                       <c:forEach var="site" items="${sites}">
                                           <tr>
                                               <td><input type="checkbox" name="selectedSiteIds" value="${site.id}"></td>
                                               <td><a href="<c:url value="sites/${site.id}" />" target="_blank">${site.id}</a></td>
                                               <td>${site.teaser}</td>
                                               <td>${site.location.address} ${site.location.address1} ${site.location.city} ${site.location.state} ${site.location.country} ${site.location.zip}</td>
                                               <td>${site.submittedDate}</td>
                                               <td>${site.submittedBy}</td>
                                           </tr>
                                       </c:forEach>
                                     </tbody>
                                 </table>
                          </form>
                    </div>
            </div>
        </div>

    </hgroup>
</article>
	
	
	
	
