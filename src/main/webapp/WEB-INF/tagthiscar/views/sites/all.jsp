<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
 <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<article id="intro">
    <hgroup>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span12">

                        <form action="" method="POST">
                        <div class="row pull-right">
                            <input type="submit" name="deleteSites" value="Delete Checked" class="btn"/>
                         </div>
                         <br><br>
                        <table class="table table-striped table-bordered table-hover table-condensed">
                                   <caption><b>Sites List Total: ${page.totalElements}</b></caption>
                                   <thead>
                                   <tr>
                                       <th><input type="checkbox" name="selectedSiteIds"></th>
                                       <th>Id</th>
                                       <th>Teaser</th>
                                       <th>Address</th>
                                       <th>Submitted Date</th>
                                       <th>Submitted By</th>
                                   </tr>
                                    </thead>
                                    <tbody>
                                       <c:forEach var="site" items="${page.content}">
                                           <tr>
                                               <td><input type="checkbox" name="selectedSiteIds" value="${site.id}"></td>
                                               <td><a href="<c:url value="sites/${site.id}" />" target="_blank">${site.id}</a></td>
                                               <td>${site.teaser}</td>
                                               <td>${site.location.address} ${site.location.address1} ${site.location.city} ${site.location.state} ${site.location.country} ${site.location.zip}</td>
                                               <td>${site.submittedDate}</td>
                                               <td>${site.submittedBy}</td>
                                           </tr>
                                       </c:forEach>
                                     </tbody>
                                 </table>
                          </form>
                    </div>
            </div>
        </div>

          Showing ${((page.number)*(page.size))+1} to ${((page.number)*(page.size))+(page.numberOfElements)} of  ${page.totalElements} entries

                    <br><br>
                     <c:choose>
                              <c:when test="${not page.firstPage}">
                                      <spring:url value="" var="first">
                                          <spring:param name="page.page" value="1" ></spring:param>
                                          <spring:param name="page.size" value="10" ></spring:param>
                                     </spring:url>
                                     <a href="${first}">First</a>
                              </c:when>
                              <c:otherwise>
                                First
                              </c:otherwise>
                  </c:choose>

                     <c:choose>
                                             <c:when test="${not page.firstPage}">
                                            <spring:url value="" var="previous">
                                                <spring:param name="page.page" value="${page.number}" ></spring:param>
                                                <spring:param name="page.size" value="${page.size}" ></spring:param>
                                           </spring:url>
                                            <a href="${previous}">Previous</a>
                                             </c:when>
                                            </c:choose>

                                 <c:choose>
                                             <c:when test="${not page.lastPage}">
                                                <spring:url value="" var="next">
                                                    <spring:param name="page.page" value="${page.number+2}" ></spring:param>
                                                    <spring:param name="page.size" value="${page.size}" ></spring:param>
                                               </spring:url>
                                                <a href="${next}">Next</a>
                                                </c:when>
                                </c:choose>


                    <c:choose>
                     <c:when test="${not page.lastPage}">
                             <spring:url value="" var="last">
                                 <spring:param name="page.page" value="${page.totalPages}" ></spring:param>
                                 <spring:param name="page.size" value="10" ></spring:param>
                            </spring:url>
                            <a href="${last}">Last</a>
                     </c:when>
                     <c:otherwise>
                       Last
                     </c:otherwise>
                    </c:choose>

    </hgroup>
</article>
	
	
	
	
