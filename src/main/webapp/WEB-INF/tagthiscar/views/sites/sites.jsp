<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<c:if test="${not empty message}">
	<div class="${message.type.cssClass}">${message.text}</div>
</c:if>

<article id="intro">
    <hgroup>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span12">
                    <ul class="breadcrumb">
                      <li><a href="<c:url value="/" />"><i class="icon-search"></i></a> <span class="divider">></span></li>
                      <li><a href="<c:url value="/share?searchTerm=${site.location.state}" />">${site.location.state}</a> <span class="divider">></span></li>
                      <li><a href="<c:url value="/share?searchTerm=${site.location.city}" />">${site.location.city}</a> <span class="divider">></span></li>
                      <li><a href="<c:url value="/share?searchTerm=${site.location.zip}" />">${site.location.zip}</a> <span class="divider">></span></li>
                      <li class="active">${site.location.address}</li>
                    </ul>

                    <h3>
                    ${site.teaser}
                    <small>
                        <em>first reported on:<b>${site.submittedDateStr}</b></em>
                        <c:if test="${not empty site.updatedDate}"><em>Last updated:</em>:<b>${site.updatedDateStr}</b></c:if>
                    </small>
                    </h3>

                    <!--
                    <table class="table table-bordered table-striped">
                      <tbody>
                        <tr>
                          <td>Description</td>
                          <td>${site.description}</td>
                          <td>Address</td>
                          <td>${site.location.address}</td>
                          <td>Labels</td>
                          <td>${site.csvFlagTypes}</td>
                        </tr>
                        <tr>
                          <td>Traffic Impacted:</td>
                          <td>${site.trafficImpacted}</td>
                          <td>Problem Fixed:</td>
                          <td>${site.problemFixed}</td>
                          <td>Lanes Impacted:</td>
                          <td>${site.noOfLanesImpacted} of ${site.lanesImpacted} lanes on ${site.sidesImpacted} side of the road.</td>
                        </tr>
                       </tbody>
                    </table>
                    -->

                    <form action="" method="POST">
                        <input type="hidden" id="siteLatitudeId" name="siteLatitude" value="${site.location.latitude}" />
                        <input type="hidden" id="siteLongitudeId" name="siteLongitude" value="${site.location.longitude}" />
                        <input type="hidden" id="siteTeaserId" name="siteTeaser" value="${site.teaser}" />
                        <input type="hidden" id="siteCsvFlagTypesId" name="siteCsvFlagTypes" value="${site.csvFlagTypes}" />

                        <div class="row-fluid">
                            <div class="span8">
                                <!-- map -->
                                <fieldset class="fieldsetBox">
                                   <legend class="frameTitle">
                                    Location:
                                    <small>
                                        ${site.location.address} ${site.location.address1} ${site.location.city} ${site.location.state} ${site.location.zip} ${site.location.country}
                                    </small>
                                    </legend>
                                   <div id="map"></div>
                                   <br>
                                   <div>
                                    <ul class="nav nav-pills inline">
                                        <li><a href="#">Email any future updates</a></li>
                                        <li><a href="#">Save this site</a></li>
                                        <li class="dropdown">
                                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">more options <b class="caret"></b></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Edit site information</a></li>
                                                <li><a href="#">Report a problem</a></li>
                                                <li><a href="#">Add a Label</a></li>
                                            </ul>
                                          </li>
                                    </ul>
                                    </div>
                                </fieldset>

                                <!-- site updates -->
                                <fieldset class="fieldsetBox">
                                   <legend class="frameTitle">Site Updates</legend>
                                   <div class="span11">

                                       <textarea class="row-fluid" rows="3" placeholder="add a comment"></textarea>
                                       <br>
                                       <p class="pull-right">
                                           <button type="submit" class="btn btn-small btn-primary">Add an Image</button>
                                           <button type="submit" class="btn btn-small btn-primary">Post</button>
                                       </p>


                                       <!--
                                       <div class="boxed">
                                           <font color="green">Traffic Congestion is cleared up - 9pm by <a href="">Amit</a>.</font><br/>
                                       </div>

                                        <br/>

                                       <div class="boxed">
                                           <font color="red">Traffic is still congested - 8.30 pm by <a href="">Amit</a>.</font><br/>
                                       </div>

                                        <br/>

                                       <div class="boxed">
                                           <a href="">Amit</a> added a picture 'intersection' on 14th Sept 2012.<br/>
                                           <img src="<c:url value="/static/tagthiscar/standard/images/nemo.jpg" />" alt="image06" width="100" height="100"/>
                                       </div>

                                        <br/>

                                       <div align="center"><a href="">Show More ...</a></div>
                                       -->
                                   </div>
                                </fieldset>
                            </div>
                            <div class="span4">
                                <fieldset class="fieldsetBox">
                                    <legend class="frameTitle">Site Details</legend>
                                    <strong>Labels:</strong>
                                    <c:forEach var="flag" items="${site.flagTypes}">
                                            <img src="<c:url value="/static/tagthiscar/standard/images/markers/${flag}.png"/>"/>
                                    </c:forEach> ${site.csvFlagTypes} <br>
                                    <strong>Description:</strong>${site.description} <br>
                                    <strong>Traffic Impacted:</strong>${site.trafficImpacted}<br>
                                    <strong>Problem Fixed:</strong>${site.problemFixed}<br>
                                    <strong>Lanes Impacted:</strong>
                                    ${site.noOfLanesImpacted} of ${site.lanesImpacted} lanes on ${site.sidesImpacted} side of the road.
                                </fieldset>
                                <fieldset class="fieldsetBox">
                                    <legend class="frameTitle">Weather</legend>
                                    <div class="media">
                                      <a class="pull-left" href="#">
                                        <img src="<c:url value="/static/tagthiscar/standard/images/weather/sunny.gif"/>"/>
                                      </a>
                                      <div class="media-body">
                                        <h4 class="media-heading">Sunny</h4>

                                      </div>
                                    </div>
                                    <div>

                                    </div>
                                </fieldset>
                                <fieldset class="fieldsetBox">
                                    <legend class="frameTitle">Site Analysis</legend>
                                            <div>
                                                <font color="red">Warning: Drive Cautiously</font> <br/>
                                                    <font color="red"><b>Major accident reported recently.</b></font> <br/>
                                                    10 incidents within a year <br/>
                                                    All of them involved accidents <br/>
                                                    Average traffic impacted was tagged as major <br/>
                                                    Most incidents occured during 7pm - 9pm <br/>
                                                    2 of the vehicles reported have been involved in accidents before <br/>
                                                </ul>
                                            </div>
                                            <div align="right"><a href="">See more ..</a></div>
                                </fieldset>

                                <c:if test="${not empty site.vehicles}">
                                <fieldset class="fieldsetBox">
                                    <legend class="frameTitle">Vehicles Involved</legend>
                                            <div>
                                                <c:forEach var="vehicle" items="${site.vehicles}" varStatus="vehicleCount">
                                                        <a href="<c:url value="/tags/${vehicle.id}" />"><b>${vehicleCount.count}.</b> ${vehicle.namePlate},${vehicle.color}, ${vehicle.make} ${vehicle.model}, ${vehicle.type}</a> <br/>
                                                </c:forEach>
                                            </div>
                                </fieldset>
                                </c:if>


                                <fieldset class="fieldsetBox">
                                    <legend class="frameTitle">Nearby Sites</legend>
                                            <div>
                                                <c:if test="${not empty site.location.sites and fn:length(site.location.sites) > 1}">
                                                     <b>Same Location:</b> <br/>
                                                     <c:forEach var="sameLocSite" items="${site.location.sites}" varStatus="siteCount">
                                                          <c:if test="${site.id ne sameLocSite.id}">
                                                              <a href="${sameLocSite.id}">${sameLocSite.teaser}- ${sameLocSite.submittedDate}</a>
                                                              <ul>
                                                              <c:choose>
                                                                  <c:when test="${not empty sameLocSite.vehicles}">
                                                                       <c:forEach var="nearBySiteVehicle" items="${sameLocSite.vehicles}" varStatus="siteCount">
                                                                            <li><a href="<c:url value="/tags/${nearBySiteVehicle.id}" />">${nearBySiteVehicle.namePlate},${nearBySiteVehicle.color}, ${nearBySiteVehicle.make} ${nearBySiteVehicle.model}, ${nearBySiteVehicle.type}</a></li>
                                                                       </c:forEach>
                                                                  </c:when>
                                                                  <%--
                                                                  <c:otherwise>
                                                                       <font color="red">No vehicles involved.</font>
                                                                  </c:otherwise>
                                                                  --%>
                                                              </c:choose>
                                                              </ul>
                                                          </c:if>
                                                     </c:forEach>
                                                </c:if>
                                            </div>

                                            <div>
                                                <c:if test="${not empty locationsWithinOneMile[0].sites and (site.location.id ne locationsWithinOneMile[0].id) }">
                                                     <b>Within a mile radius:</b> <br/>
                                                     <ul>
                                                        <c:forEach var="locOneMile" items="${locationsWithinOneMile}">
                                                            <c:if test="${site.location.id ne locOneMile.id}">
                                                               <c:forEach var="siteOneMile" items="${locOneMile.sites}">
                                                                 <li>
                                                                    <a href="${siteOneMile.id}">${siteOneMile.teaser}- ${siteOneMile.submittedDate}</a>
                                                                    <ul>
                                                                      <c:choose>
                                                                          <c:when test="${not empty siteOneMile.vehicles}">
                                                                               <c:forEach var="vehicle" items="${siteOneMile.vehicles}" varStatus="siteCount">
                                                                                    <li><a href="<c:url value="/tags/${vehicle.id}" />">${vehicle.namePlate},${vehicle.color}, ${vehicle.make} ${vehicle.model}, ${vehicle.type}</a></li>
                                                                               </c:forEach>
                                                                          </c:when>
                                                                          <%--
                                                                          <c:otherwise>
                                                                               <font color="red">No vehicles involved.</font>
                                                                          </c:otherwise>
                                                                          --%>
                                                                      </c:choose>
                                                                    </ul>
                                                                 </li>
                                                               </c:forEach>
                                                            </c:if>
                                                        </c:forEach>
                                                     </ul>
                                                </c:if>
                                            </div>

                                            <div>
                                                <c:if test="${not empty locationsWithinFiveMile}">
                                                     <b>Within 5 miles radius:</b>  <br/>
                                                     <ul>
                                                         <c:forEach var="locFiveMile" items="${locationsWithinFiveMile}">
                                                              <c:if test="${site.location.id ne locFiveMile.id}">
                                                                  <c:forEach var="siteFiveMile" items="${locFiveMile.sites}">
                                                                    <li>
                                                                        <a href="${siteFiveMile.id}">${siteFiveMile.teaser}- ${siteFiveMile.submittedDate}</a>
                                                                        <ul>
                                                                          <c:choose>
                                                                              <c:when test="${not empty siteFiveMile.vehicles}">
                                                                                   <c:forEach var="vehicle" items="${siteFiveMile.vehicles}" varStatus="siteCount">
                                                                                        <li><a href="<c:url value="/tags/${vehicle.id}" />">${vehicle.namePlate},${vehicle.color}, ${vehicle.make} ${vehicle.model}, ${vehicle.type}</a></li>
                                                                                   </c:forEach>
                                                                              </c:when>
                                                                              <%--
                                                                              <c:otherwise>
                                                                                   <font color="red">No vehicles involved.</font>
                                                                              </c:otherwise>
                                                                              --%>
                                                                          </c:choose>
                                                                        </ul>
                                                                    </li>
                                                                  </c:forEach>
                                                              </c:if>
                                                         </c:forEach>
                                                     </ul>
                                                </c:if>
                                            </div>
                                            <!--
                                            <div align="right"><a href="">See more ..</a></div>
                                            -->
                                </fieldset>
                                <!--
                                <fieldset class="fieldsetBox">
                                    <legend class="frameTitle">Related Ads</legend>
                                    <div>
                                            <a href="">Battlefield towing company</a> <br/>
                                            <a href="">Get a cars.com quote for this vehicle</a> <br/>
                                        </ul>
                                    </div>
                                </fieldset>
                                -->
                            </div>
                        </div>
                    </form>
                    <hr>
                </div>
            </div>
        </div>
    </hgroup>
</article>
	
	
	
	
