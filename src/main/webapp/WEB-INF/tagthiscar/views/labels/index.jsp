<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<article id="intro">
    <hgroup>
        <div class="page-header">
                   <h3>Labels</h3>
        </div>
         <form method="POST" action="<c:url value="/labels/delete" />">
             <div class="row pull-right">
                <input class="btn" type="submit" name="saveBtn" value="Delete Checked" />
             </div>

             <br><br>

             <table class="table table-striped table-bordered table-hover table-condensed" id="dataTable">
                <caption><b>Labels Total: ${fn:length(labels)}</b></caption>
                <thead>
                <tr>
                    <th></th>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Title</th>
                </tr>
                 </thead>
                 <tbody>
                    <c:forEach var="label" items="${labels}">
                        <tr>
                             <th><input type="checkbox" name="checkedIds" value="${label.id}" /></th>
                            <td><a href="<c:url value="/labels/${label.id}" />">${label.id}</a></td>
                            <td>${label.name}</td>
                            <td>${label.title}</td>
                        </tr>
                    </c:forEach>
                  </tbody>
              </table>
          </form>
    </hgroup>
</article>
	
	
	
	
