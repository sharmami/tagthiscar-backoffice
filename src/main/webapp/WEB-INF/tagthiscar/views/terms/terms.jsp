<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>

<c:if test="${not empty message}">
	<div class="${message.type.cssClass}">${message.text}</div>
</c:if>

<article id="intro">
    <hgroup>
        <div class="page-header">
            <h3>Terms of Use</h3>
            <small>Version 1.1 updated 6th June, 2013</small>
        </div>


                <p>
                By accessing or using the tagthiscar.com website or its associated mobile application (together, the <b>"Site"</b>),
                you agree to these <b>TERMS OF USE</b> (this <b>"Agreement"</b>).
                The Site is owned, operated, and provided to you (<b>"You"</b>) by <b>Tag This Car LLC</b>,
                a Virginia based domestic limited liability company (<b>"Company"</b>), and its affiliates.
                Your access and use of the Site is governed by this Agreement.
                By accessing or using the Site or otherwise consenting to these terms and conditions,
                You agree to the terms and conditions contained in this Agreement.
                If You do not wish to abide by this Agreement, You may not access or use the Site.
                </p>

                <p>
                From time to time, Company may revise this Agreement without any notice to You.
                You agree that, each time you access or use the Site,
                You shall review and abide by the then-current version of this Agreement.
                </p>

                <p>
                You and Company agree as follows: <br/>
                <ol>
                  <li><b>GENERAL INFORMATION</b>.  You acknowledge that the Site permits users of the Site (each, a "User") to browse, view, and post information about driving conditions, behavior, and incidents observable on public roads in the United States of America. </li>
                  <li><b>PRIVACY POLICY</b>.  Company's Privacy Policy (located <a href="<c:url value="/privacy_policy" />">here</a>, the "Privacy Policy") is incorporated herein, by reference, as if fully set forth herein.  In case of a conflict between any provision of the Privacy Policy and any provision of this Agreement, the provision of this Agreement will apply.</li>
                  <li>
                      <b>PERMITTED USE.</b>
                       <ul>
                          <li>
                          <b>3.1</b>
                          The Site must be used only for the purposes expressly set forth on the Site and in this Agreement.
                          Any other use of the Site is prohibited.
                          In case of a conflict between any statement on the Site and any provision of this Agreement,
                          the provision of this Agreement will apply.
                          </li>
                          <li>
                              <b>3.2</b>
                              Notwithstanding any other provision of this Agreement, You shall not:
                              (a) access or use the Site for any purpose that is unlawful;
                              (b) access or use the Site for any purpose that is not expressly permitted by the Site or this Agreement;
                              (c) access or use the Site in any manner that could damage, disable, overburden, or impair any Company computer system, server, or network;
                              (d) access or use the Site in any manner that interferes with any other person's access or use of the Site;
                              (e) attempt to gain unauthorized access to the Site, other accounts, or any Company computer system, server, or network;
                              or (f) access or use materials or information through any means not intentionally made available by Company.
                          </li>
                          <li>
                              <b>3.3</b>
                              Notwithstanding any other provision of this Agreement,
                              You shall ensure that your use and access of the Site complies with all applicable laws and regulations.
                          </li>
                          <li>
                              <b>3.4</b>
                              The Site is not intended to be used by persons under the age of 18.  You represent and warrant that you are at least 18 years of age.
                          </li>
                          <li>
                              <b>3.5</b>
                              If You fail to abide by this Agreement in any way, or fail to pay any amount when due under this Agreement, Company may prohibit you from accessing or using the Site.
                              No action or omission by Company shall be deemed to be a waiver of any right or remedy provided under this Agreement or under applicable law.
                          </li>
                       </ul>
                  </li>
                  <li>
                      <b>USER CONTENT.</b>
                       <ul>
                          <li>
                              <b>4.1</b>
                               As to any content You post to or through the Site including, without limitation, any post (such content, <b>"Content"</b>),
                               You represent and warrant that the Content is wholly complete, true, and accurate;
                               and that You are the sole owner of the Content or have all rights, licenses, consents, and releases that are necessary and appropriate to grant to Company
                               and all users all rights in such Content, as contemplated under this Agreement.
                               You shall not post to or through the Site any Content that is not wholly complete, true, and accurate; or of which you are not the sole owner or the possessor of all rights,
                               licenses, consents, and releases that are necessary and appropriate to grant to Company and all Users all rights in such Content, as contemplated b this Agreement.
                          </li>
                          <li>
                              <b>4.2</b>
                              You shall not post any Content that:<br/>
                              (a) is harmful; vulgar; obscene; profane; sexually explicit; abusive; threatening; privacy invading; defamatory; racially, ethnically, or otherwise objectionable; or unlawful in any way;<br/>
                              (b) infringes, violates, or may infringe or violate, any intellectual property or other right of another person; or<br/>
                              (c) is not observable on or from a public road in the United States of America.<br/>
                          </li>
                          <li>
                              <b>4.3</b>
                              You acknowledge that Company does not endorse or pre-screen Content.
                              However, Company and its designees shall have the right, but not the obligation (in Company's sole discretion) to reject, edit, move, or remove any Content
                          </li>
                          <li>
                            <b>4.4</b>
                            As to any Content, You grant to Company and all Users an irrevocable, non-exclusive, non-transferable, non-sublicensable, fully paid,
                            perpetual, worldwide license to use, copy, encode, store, archive, distribute, transmit, modify, translate, publicly display, and create derivative works from, the Content.
                            Additionally, You grant to Company the right to use the name you submit in connection with any Content.
                          </li>
                          <li>
                              <b>4.5</b>
                              You shall not copy, distribute, or display, in any way, any information You find on the Site on any other website or electronic forum without the express written consent of Company.
                          </li>
                        </ul>
                  </li>
                <li>
                   <b>DMCA NOTICE PROCEDURE.</b>
                    <ul>
                       <li>
                           <b>5.1</b>
                            Company will respond to allegations of copyright infringement in accordance with the Digital Millennium Copyright Act (the <b>"DMCA"</b>).
                       </li>
                      <li>
                          <b>5.2</b>
                          If you believe that your work was copied or posted on the Site in a way that constitutes copyright infringement, please contact our designated agent [Amit Sharma, Email:amit@tagthiscar.com].
                      </li>
                      <li>
                          <b>5.3</b>
                          Your notification of alleged infringement must comply with the provisions of the DMCA and must include the following information: <br/>
                          <ol>
                              <li>
                                  a description of the copyrighted work which You claim has been infringed (if you are not the owner of the work, you must also include your electronic or digital signature as a person authorized to act on behalf of the copyright owner);
                              </li>
                              <li>
                                  a description of where the allegedly infringing material is located on the Site;
                              </li>
                              <li>
                                  information reasonably sufficient to permit Company to contact you (such as an address, telephone number, and, if available, an email address where you may be reached);
                              </li>
                              <li>
                              a statement that you have a good faith belief that the use of the allegedly infringing material is not authorized by the copyright owner, its agent, or the law; and
                              </li>
                              <li>
                              a statement by You, made under penalty of perjury, that the information in your notification is accurate, and that you are the copyright owner or are authorized to act on the copyright owner's behalf.
                              </li>
                          </ol>
                      </li>
                      <li>
                          <b>5.4</b>
                          Upon receipt of such written notification, conforming to the DMCA and containing the information described in Section 5.3,
                          Company will remove or disable access to the allegedly infringing material, forward the written notification to the alleged infringer,
                          and attempt to promptly notify the alleged infringer that the allegedly infringing material has been removed.
                      </li>
                      <li>
                          <b>5.5</b>
                          If allegedly infringing material is removed by Company, the alleged infringer may deliver a counter-notification to Company's designated agent which complies with the provisions of the DMCA and includes the following information:<br/>
                          <ol>
                              <li>
                              a physical or electronic signature of the alleged infringer;
                              </li>
                              <li>
                              a description of the of the material that has been removed, or to which access has been disabled, and the location at which the material appeared on the Site before it was removed or access to it was disabled;
                              </li>
                              <li>
                              a statement, under penalty of perjury, that the alleged infringer has a good faith belief that the material was removed or disabled as a result of mistake or misidentification of the material to be removed or disabled;
                              </li>
                              <li>
                              the alleged infringer's name, address, and telephone number, and a statement that the alleged infringer consents to the jurisdiction of United States District Court for the judicial district in which the address is located,
                              or if the alleged infringer's address is outside of the United States, for any judicial district in which Company may be found, and that the alleged infringer will accept service of process from the person who provided the notification or an agent of such person
                              </li>
                          </ol>
                      </li>
                      <li>
                      <b>5.6</b>
                      Upon receipt of a counter-notification, conforming to the DMCA and containing the information described in Section 5.5,
                      Company will promptly provide You, the party who delivered the original notification, with a copy of the counter-notification
                      and inform you that it will replace the removed material, or cease disabling access to it, within ten business days.
                       If Company's designated agent does not receive notice from You that an action has been filed seeking a court order to prohibit the alleged infringer from engaging in the infringing activity complained of in the original notification,
                       Company will replace the removed material, or cease disabling access to it, within ten to fourteen business days after receipt of the counter-notification.
                      </li>
                     </ul>
                 </li>
                 <li>
                  <b>THIRD-PARTY WEBSITES, COMPANIES, AND PRODUCTS.</b>
                  Mention of, or linking to, third party websites, companies, and products on the Site is for informational purposes only and constitutes neither an endorsement nor a recommendation.  Certain links on the Site will permit you to leave the Site.
                  The websites linked by the Site are not under the control of Company and Company is not responsible for the content of any linked website.
                 </li>
                 <li>
                  <b>INDEMNIFICATION.</b>
                   You shall defend, indemnify, and hold harmless Company, its officers, directors, employees, and agents, from and against any claims, actions or demands, including, without limitation, all reasonable attorney's fees and costs, made by any third party due to or resulting from your access or use of the Site, any breach of this Agreement by You, any false representation made by You in this Agreement, or any breach of a warranty made by You in this Agreement.
                 </li>
                 <li>
                  <b>DISCLAIMERS; LIMITATION OF LIABILITY.</b>
                  <ol>
                      <li>
                          <b>
                              THE SITE AND ALL OF ITS CONTENT ARE PROVIDED "AS IS, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED.  TO THE FULL EXTENT PERMISSIBLE BY APPLICABLE LAW, COMPANY DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
                          </b>
                      </li>
                     <li>
                          <b>YOUR USE OF THE SITE IS AT YOUR OWN RISK.  THE SITE MAY CONTAIN LINKS TO OTHER SITES. COMPANY IS NOT RESPONSIBLE FOR THE CONTENT OR PRIVACY POLICIES OF THOSE SITES.</b>
                      </li>
                      <li>
                          <b>ANY MATERIAL DOWNLOADED OR OTHERWISE OBTAINED THROUGH THE SITE IS ACCESSED AT YOUR OWN RISK AND YOU ARE SOLELY RESPONSIBLE FOR ANY DAMAGE TO YOUR COMPUTER SYSTEM OR LOSS OF DATA THAT RESULTS FROM THE DOWNLOAD OF SUCH MATERIAL.</b>
                      </li>
                      <li>
                          <b>
                          IN NO EVENT SHALL COMPANY BE LIABLE FOR ANY SPECIAL, INCIDENTAL, INDIRECT, OR CONSEQUENTIAL DAMAGES WHATSOEVER ARISING FROM YOUR ACCESS OR USE OF, OR INABILITY TO ACCESS OR USE, THE SITE.
                          </b>
                      </li>
                      <li>
                          <b>
                              IN NO EVENT SHALL COMPANY BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING IN ANY WAY FROM CONTENT PROVIDED BY OR REPRESENTATIONS MADE BY ANOTHER USER OF THE SITE OR THE ACTIONS OR OMISSIONS OF ANY USER OF THE SITE.
                           </b>
                      </li>
                      <li>
                          <b>
                              IN ANY EVENT, COMPANY'S ENTIRE LIABILITY TO YOU UNDER ANY PROVISION OF THIS AGREEMENT OR ARISING FROM THE ACCESS OR USE OF THE SITE BY YOU OR ANY OTHER USER SHALL BE LIMITED TO THE AMOUNT ACTUALLY PAID BY YOU TO COMPANY PURSUANT TO THIS AGREEMENT.
                          </b>
                      </li>
                   </ol>
                 </li>
                 <li>
                  <b>GOVERNING LAW.</b>
                  This Agreement is governed and shall be construed by the laws of the Commonwealth of Virginia, without regard to its conflict of law provisions.
                 </li>
                 <li>
                 <b>CHOICE OF FORUM.</b>
                  Any party commencing against any other party any legal proceeding (including, without limitation, any tort claim) arising out of, relating to, or concerning this Agreement shall bring that proceeding in the United States District Court for the Eastern District of Virginia or in the courts of Fairfax County, Virginia. Each party hereby submits to the exclusive jurisdiction of those courts for the purposes of any such proceeding and waives any claim that any legal proceeding (including, without limitation, any tort claim) brought in accordance with this Section 11 has been brought in an inconvenient forum or that the venue of that proceeding is improper
                 </li>
                 <li>
                  <b>ATTORNEY'S FEES AND COSTS.</b>
                  Should any party breach this Agreement, the non-breaching party shall be entitled to an award of its costs and reasonable attorneys' fees expended in any action based upon the terms of this Agreement in any case in which it is the substantially prevailing party.
                 </li>
                 <li>
                 <b>SEVERABILITY.</b>
                 In the event that any provision of this Agreement is invalidated by a court of competent jurisdiction, then all of the remaining provisions of this Agreement shall continue unabated and in full force and effect.
                 </li>
                 <li>
                  <b>BINDING EFFECT.</b>
                   This Agreement shall be binding upon each of the parties and upon their respective successors and assigns, and shall inure to the benefit of each of the parties and to their respective successors and assigns.
                 </li>
                 <li>
                  <b>ENTIRE AGREEMENT.</b>
                  This Agreement contains the entire understanding and agreement between the parties as to its subject matter and shall not be modified or superseded except upon the express written consent of both parties.  This Agreement is not intended to confer upon any person, other than the parties, any rights or remedies.
                 </li>
                </ol>
                </p>


    </hgroup>
</article>


	
