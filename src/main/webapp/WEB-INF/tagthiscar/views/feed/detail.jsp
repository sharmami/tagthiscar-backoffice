<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:if test="${not empty message}">
	<div class="${message.type.cssClass}">${message.text}</div>
</c:if>

<article id="intro">
    <hgroup>
        <div class="page-header">
                   <h3>Feed Details</h3>
        </div>

        <table class="table table-striped table-bordered table-hover table-condensed">
                    <caption>RSS Feed Information ${feed.rawSite.id}</caption>
                     <tbody>
                            <tr>
                                <th>Id</td>
                                <th>Title</td>
                                <th>Link</td>
                                <th>Author</td>
                                <th>Pub Date</td>
                                 <th>GUID</td>
                                 <th>File</td>
                            </tr>
                            <tr>
                                <td>${feed.id}</td>
                                <td>${feed.title}</td>
                                <td><a href="${feed.link}">${feed.link}</a></td>
                                <td>${feed.author}</td>
                                <td>${feed.pubDate}</td>
                                <td>${feed.guid}</td>
                                <td><a target="_blank" href="<c:url value="/feed/download/${feed.fileId}" />">${feed.fileName}</a></td>
                            </tr>
                            <tr>
                                <th colspan="8">
                                    Description
                                </th>
                            </tr>
                            <tr>
                                <td colspan="8">
                                    ${feed.description}
                                </td>
                            </tr>
                      </tbody>
                  </table>

              <form method="POST" action="<c:url value="/feed/add" />">
              <input type="hidden" name="id" value="${feed.id}"/>
              <input type="hidden" name="rawSite.id" value="${feed.rawSite.id}"/>
              <input type="hidden" name="rawSite.location.id" value="${feed.rawSite.location.id}"/>

              <table class="table table-striped table-bordered table-hover table-condensed">
                 <caption>Site Form</caption>
                  <tbody>
                         <tr>
                              <td>Internal comments (these don't go to the site):</td>
                              <td><textarea name="comments">${feed.comments} feed1: ${feed.rawSite.id}</textarea></td>
                          </tr>
                         <tr>
                             <td>Site Teaser</td>
                             <td>
                                <c:if test="${not empty feed.rawSite}">
                                <input class="span6" type="text" name="rawSite.teaser" value="${feed.rawSite.teaser}">
                                </c:if>
                                <c:if test="${empty feed.rawSite}">
                                    <input class="span6" type="text" name="rawSite.teaser" value="${feed.title}">
                                </c:if>
                              </td>
                         </tr>
                         <tr>
                              <td>Site Description</td>
                              <td>
                              <c:if test="${not empty feed.rawSite}">
                                      <textarea name="rawSite.description" rows="5" class="span6">${feed.rawSite.description}</textarea>
                              </c:if>
                              <c:if test="${empty feed.rawSite}">
                                   <textarea name="rawSite.description" rows="5" class="span6">${feed.description}</textarea>
                              </c:if>
                              </td>
                          </tr>
                          <tr>
                                <td>Traffic Impacted</td>
                                <td>
                                    <select name="rawSite.trafficImpacted">
                                        <option value="SEVERE">Severe</option>
                                        <option value="MAJOR">Major</option>
                                        <option value="MEDIUM">Medium</option>
                                        <option value="MINOR">Minor</option>
                                    </select>
                                </td>
                          </tr>
                          <tr>
                                  <td colspan="2">
                                        <label>Example: 2 of Right/Left lanes on Same/Both sides of the road.</label>
                                        <input type="text" class="input-mini" value="${feed.rawSite.noOfLanesImpacted}" name="rawSite.noOfLanesImpacted">
                                        of
                                        <select name="rawSite.lanesImpacted" class="span2" >
                                            <option value="">&nbsp;</option>
                                            <option value="All">ALL</option>
                                            <option value="Right">Right</option>
                                            <option value="Left">Left</option>
                                            <option value="Middle">Middle</option>
                                         </select>
                                         Lanes on
                                         <select name="rawSite.sidesImpacted" class="span2" >
                                            <option value="">&nbsp;</option>
                                             <option value="SAME">SAME</option>
                                             <option value="BOTH">BOTH</option>
                                             <option value="OUTBOUND">Outbound</option>
                                          </select>
                                           side(s) of the road.
                                  </td>
                            </tr>
                             <tr>
                                    <td>Problem Fixed</td>
                                    <td>
                                        <select name="rawSite.problemFixed">
                                            <option value=""></option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                            <option value="Partially">Partially</option>
                                            <option value="Idontknow">I don't know</option>
                                            <option value="notsure">Not Sure</option>
                                        </select>
                                    </td>
                              </tr>
                               <tr>
                                  <td>Submitted By</td>
                                  <td>
                                      <c:choose>
                                        <c:when test="${empty rawSite.submittedBy}">
                                            <input type="text" name="rawSite.submittedBy" value="${feed.author}">
                                        </c:when>
                                        <c:otherwise>
                                            <input type="text" name="rawSite.submittedBy" value="${feed.rawSite.submittedBy}">
                                        </c:otherwise>
                                      </c:choose>


                                  </td>
                            </tr>
                            <tr>
                                  <td>Updated By</td>
                                  <td>
                                            <input type="text" name="rawSite.updatedBy" value="${feed.rawSite.updatedBy}">
                                  </td>
                            </tr>

                            <tr>
                                  <td>geoRssPolygon</td>
                                  <td>
                                            <textarea name="geoRssPolygon" class="span4" rows="5">${feed.geoRssPolygon}</textarea>
                                  </td>
                            </tr>

                            <tr>
                                <td colspan=2>
                                          <div>
                                                <label for="address">Address:</label>
                                                <input type="text" name="address" id="addressId"  value="" />
                                                <input type="button" id="addressButton" value="Get Coordinates" />
                                              </div>
                                </td>

                            </tr>

                            <tr>
                                <td colspan=2>
                                    <div id="widemap"></div>
                                </td>
                            </tr>

                            <tr>
                              <td>Latitude</td>
                              <td>
                                        <input type="text" id="latId" name="latitude" value="${feed.rawSite.location.latitude}">
                              </td>
                        </tr>

                             <tr>
                                      <td>Longitude</td>
                                      <td>
                                                <input type="text" id="longId" name="longitude" value="${feed.rawSite.location.longitude}">
                                      </td>
                                </tr>
                                 <tr>
                                                                       <td>Address</td>
                                                                       <td>
                                                                                 <input type="text" id="addyId" name="rawSite.location.address" value="${feed.rawSite.location.address}">
                                                                                 <input type="text" id="address1Id" name="rawSite.location.address1" value="${feed.rawSite.location.address1}">
                                                                                 <br>
                                                                                 <input type="text" id="cityId" name="rawSite.location.city" value="${feed.rawSite.location.city}" placeholder="City">
                                                                                 <br>
                                                                                 <input type="text" id="stateId" name="rawSite.location.state" value="${feed.rawSite.location.state}" placeholder="State">
                                                                                 <br>
                                                                                 <input type="text" id="countryId" name="rawSite.location.country" value="${feed.rawSite.location.country}" placeholder="Country">
                                                                                 <br>
                                                                                 <input type="text" id="zipId" name="rawSite.location.zip" value="${feed.rawSite.location.zip}" placeholder="Zip">
                                                                       </td>
                                                                 </tr>
                               <tr>
                                  <td><p class="pull-left">Flags</p></td>
                                    <td>
                                               <c:set var="allFlagTypesSize" value="${fn:length(allFlagTypes)}"/>
                                               <c:forEach var="allFlagType" items="${allFlagTypes}" varStatus="stat">
                                                    <c:choose>
                                                         <c:when test="${ stat.count == 1 }">
                                                                    <input name="flagTypes" type="checkbox" value="${allFlagType}" id="inlineCheckbox1" <c:if test="${fn:contains(flagTypes, allFlagType)}">checked</c:if> >
                                                                    <img src="<c:url value="/static/tagthiscar/standard/images/markers/${allFlagType}.png" />">
                                                                    ${allFlagType}
                                                          </c:when>

                                                          <c:when test="${ stat.count == allFlagTypesSize }">
                                                                    <input name="flagTypes" type="checkbox"value="${allFlagType}" id="inlineCheckbox1" <c:if test="${fn:contains(flagTypes, allFlagType)}">checked</c:if> >
                                                                     <img src="<c:url value="/static/tagthiscar/standard/images/markers/${allFlagType}.png" />">
                                                                     ${allFlagType}
                                                          </c:when>

                                                           <c:when test="${stat.count % 3 == 0 && stat.count < allFlagTypesSize }">
                                                                    <input name="flagTypes" type="checkbox" value="${allFlagType}" id="inlineCheckbox1" <c:if test="${fn:contains(flagTypes, allFlagType)}">checked</c:if> >
                                                                    <img src="<c:url value="/static/tagthiscar/standard/images/markers/${allFlagType}.png" />">
                                                                    ${allFlagType}
                                                               <br><br>
                                                           </c:when>

                                                            <c:otherwise>
                                                                 <input name="flagTypes" type="checkbox" value="${allFlagType}" id="inlineCheckbox1" <c:if test="${fn:contains(flagTypes, allFlagType)}">checked</c:if> >
                                                                 <img src="<c:url value="/static/tagthiscar/standard/images/markers/${allFlagType}.png" />">
                                                                 ${allFlagType}
                                                             </c:otherwise>

                                                       </c:choose>
                                                    </c:forEach>
                                                </td>
                                                </tr>
                                                <tr>
                                                    <td>Labels</td>
                                                    <td>
                                                     <select name="csvLabels" id="labelsId" multiple="multiple" class="span6 multiple-select">
                                                            <c:forEach var="label" items="${allLabels}">
                                                                <option value="${label.id}">${label.name}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <input type="submit" class="btn" name="" value="Save">
                                                    </td>
                                                </tr>
                                              </table>
                                  </td>
                            </tr>

                   </tbody>
               </table>
               </form>
    </hgroup>
</article>
	
	
	
	
