<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>

<c:if test="${not empty message}">
	<div class="${message.type.cssClass}">${message.text}</div>
</c:if>

<article id="intro">
	<hgroup>
		<div class="container-fluid">
            <div class="row-fluid">
                <div class="span12">

                   <!--
                    <div class="well">

                        <select class="span2">
                           <option>Sedan</option>
                           <option>2</option>
                           <option>3</option>
                           <option>4</option>
                           <option>5</option>
                         </select>

                      <select class="span2">
                                                 <option>Grey</option>
                                                 <option>2</option>
                                                 <option>3</option>
                                                 <option>4</option>
                                                 <option>5</option>
                                               </select>



                       <select class="span2">
                                                                        <option>Toyota</option>
                                                                        <option>2</option>
                                                                        <option>3</option>
                                                                        <option>4</option>
                                                                        <option>5</option>
                                                                      </select>


                       <select class="span2">
                                                                                              <option>Corolla</option>
                                                                                              <option>2</option>
                                                                                              <option>3</option>
                                                                                              <option>4</option>
                                                                                              <option>5</option>
                                                                                            </select>


                      <input type="text" value="XLS 2921" class="span2">

                      within <select class="span1">
                               <option>1</option>
                               <option>2</option>
                               <option>3</option>
                               <option>4</option>
                               <option>5</option>
                             </select> miles of
                             <select class="span2">
                               <option>VA</option>
                               <option>2</option>
                               <option>3</option>
                               <option>4</option>
                               <option>5</option>
                             </select>
                        <button type="submit" class="btn btn-link">Search</button>
                    </div>
                    -->

                    <h3>
                        Tag(s) for Vehicle: ${vehicle.namePlate}
                    </h3>
                    <small>
                        <em>Reported:${vehicle.submittedDate}</em>
                        <c:if test="${not empty site.updatedDate}"><em>Last updated:</em>${vehicle.updatedDate}</c:if>
                    </small>

                    <!--
                    <hr>
                    <table class="table table-bordered table-striped">
                      <tbody>
                        <tr>
                          <td>Description</td>
                          <td>${site.description}</td>
                        </tr>
                        <tr>
                          <td>Labels</td>
                          <td>${site.csvFlagTypes}</td>
                        </tr>
                        <tr>
                          <td>Traffic Impacted:</td>
                          <td>${site.trafficImpacted}</td>
                        </tr>
                        <tr>
                          <td>Problem Fixed:</td>
                          <td>${site.problemFixed}</td>
                        </tr>
                        <tr>
                          <td>Lanes Impacted:</td>
                          <td>${site.noOfLanesImpacted} of ${site.lanesImpacted}(Right/Left/Both) lanes on ${site.sidesImpacted}(same/other) side(s) of the road.</td>
                        </tr>
                      </tbody>
                    </table>
                    -->
                    <form action="" method="POST">
                        <input type="hidden" id="vehicleId" name="vehicleId" value="${vehicle.id}" />
                        <div class="row-fluid">
                            <div class="span8">
                                 <fieldset class="fieldsetBox">
                                     <legend class="frameTitle">Vehicle Map</legend>
                                     <div id="map"></div>
                                     <br>
                                    <div>
                                     <ul class="nav nav-pills inline">

                                         <li><a href="#">Email any future updates</a></li>
                                         <li><a href="#">Save this vehicle</a></li>
                                         <li class="dropdown">
                                             <a class="dropdown-toggle" data-toggle="dropdown" href="#">more options <b class="caret"></b></a>
                                             <ul class="dropdown-menu">
                                                 <li><a href="#">Edit vehicle information</a></li>
                                                 <li><a href="#">Claim your vehicle</a></li>
                                                 <li><a href="#">Tag this vehicle</a></li>
                                                 <li><a href="#">Other vehicles were involved with this? (Site it)</a></li>
                                                 <li><a href="#">carfax this vehicle</a></li>
                                                 <li><a href="#">pull DMV records</a></li>
                                                 <li><a href="#">Put this car for sale (owner)</a></li>
                                                 <li><a href="#">Report a problem</a></li>
                                             </ul>
                                           </li>
                                     </ul>
                                     </div>
                                 </fieldset>
                                 <fieldset class="fieldsetBox">
                                     <legend class="frameTitle">Activity</legend>
                                     <div class="span11">
                                         <!--
                                         <a href="<c:url value="/signout" />">Add pictures</a> &nbsp;
                                         <a href="<c:url value="/signout" />">Add comment</a>
                                        <br/><br/>
                                        -->


                                         <textarea class="row-fluid" rows="3" placeholder="add a comment"></textarea>
                                         <br>
                                         <p class="pull-right">
                                            <button type="submit" class="btn btn-small btn-primary">Add an Image</button>
                                            <button type="submit" class="btn btn-small btn-primary">Post</button>
                                         </p>

                                         <!--
                                         <div class="boxed">
                                             <b>4.</b>Checked in for oil changes at <a href="">Jiffy Lube</a> (Good side)
                                         </div>

                                         <br/>

                                         <div class="boxed">
                                             <b>3.</b> <a href="">Anonymous</a> tagged it 'Stating Facts' on 4th July 2012.<br/>
                                             Broken mirror on the right side. <br/>
                                             Comments: "Speeding atleast 20 miles over speed limit (55)" <br/>
                                         </div>

                                          <br/>


                                         <div class="boxed">
                                             <b>2.</b> Anonymous tagged it 'Hit and Run' on 14th Sept 2012.<br/>
                                         </div>

                                          <br/>

                                         <div class="boxed">
                                             <b>1.</b> <a href="">Amit</a> added pictures on 14th Sept 2011.<br/>
                                             <img src="<c:url value="/static/tagthiscar/standard/images/toystory.jpg" />" alt="image06" width="100" height="100"/>
                                         </div>

                                          <br/>

                                         <div class="boxed">
                                             <b>1.</b> <a href="">Amit</a> tagged this vehicle on 1st Sept 2011.<br/>
                                             Running a red light, spotted on intersection 66 and 28, <br/>
                                             broken window.
                                             Comments: Possibly drunk.<br/>
                                         </div>

                                         <br/>

                                         <div align="center"><a href="">Show More ...</a></div>

                                         -->
                                     </div>
                                  </fieldset>
                            </div>
                            <div class="span4">
                               <fieldset class="fieldsetBox">
                                   <legend class="frameTitle">Vehicle Details</legend>
                                    <strong>Labels:</strong>
                                   <c:forEach var="flag" items="${site.flagTypes}">
                                           <img src="<c:url value="/static/tagthiscar/standard/images/markers/${flag}.png"/>"/>
                                   </c:forEach> ${site.csvFlagTypes} <br>
                                   <strong>Make:</strong>${vehicle.make}
                                   <strong>Model:</strong>${vehicle.model}<br>
                                   <strong>Color:</strong>${vehicle.color}
                                   <strong>Type:</strong> ${vehicle.type}<br>
                                   <strong>Edition:</strong>${vehicle.edition}
                                   <strong>VIN #</strong> ${vehicle.vinNumber}
                               </fieldset>

                               <fieldset class="fieldsetBox">
                                <legend class="frameTitle">Vehicle Analysis</legend>
                                <div>
                                    <font color="red">Warning: Stay away from this vehicle</font> <br/>
                                    OR <font color="green">Gold star for driving carefully in the last 3 months.</font><br/>
                                        3 incidents within a year <br/>
                                        All of them were violations <br/>
                                        Vehicle is local to this area <br/>
                                        tagged within 11 miles of vicinity <br/>
                                    </ul>
                                </div>
                                <div align="right"><a href="">See more ..</a></div>
                               </fieldset>

                               <fieldset class="fieldsetBox">
                                <legend class="frameTitle">Vehicle history</legend>
                                <c:forEach var="site" items="${vehicle.sites}" varStatus="siteCount">
                                    <div>
                                        <a href="<c:url value="/sites/${site.id}" />"><b>Site ${siteCount.count}: </b> ${site.teaser} - ${site.submittedDate}</a>  <br/>
                                        <ul>
                                            <c:forEach var="tag" items="${site.vehicles}" varStatus="vehicleCount">
                                                <li>
                                                   <c:choose>
                                                       <c:when test="${tag.id == vehicle.id}">
                                                        <b> >> </b> ${tag.namePlate}, ${tag.make} ${tag.model} <br>
                                                       </c:when>
                                                       <c:otherwise>
                                                       <a href="<c:url value="/tags/${tag.id}" />">${tag.namePlate}, ${tag.make} ${tag.model}</a> <br>
                                                       </c:otherwise>
                                                   </c:choose>
                                               </li>
                                            </c:forEach>
                                        </ul>
                                    </div>
                                </c:forEach>
                               <!--
                                <div align="right"><a href="">See more ..</a></div>
                               -->
                               </fieldset>
                               <!--
                               <fieldset class="fieldsetBox">
                                   <legend class="frameTitle">Related Ads</legend>
                                   <div>
                                           <a href="">Battlefield towing company</a> <br/>
                                           <a href="">Get a cars.com quote for this vehicle</a> <br/>
                                       </ul>
                                   </div>
                               </fieldset>
                               -->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
       </div>
	</hgroup>
</article>
	
	
	
	
