<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>

<nav class="right-side">
	 <div class="masthead">
          <ul class="nav nav-pills pull-right">
            <security:authorize ifAllGranted="ROLE_ANONYMOUS">
                <li class="active"><a href="<c:url value="/" />">Home</a></li>
                <li><a href="<c:url value="/signin" />">Login</a></li>
                <li><a href="<c:url value="/signup" />">Sign Up</a></li>
            </security:authorize>
            <security:authorize ifNotGranted="ROLE_ANONYMOUS">
                <li class="active"><a href="<c:url value="/" />">Home</a></li>
                <li><a href="<c:url value="/signout" />">Sign Out</a></li>
            </security:authorize>
          </ul>
          <h3 class="muted"></h3>
     </div>
</nav>

<link rel="stylesheet" href="<c:url value="/static/tagthiscar/standard/css/map.css" />" type="text/css" media="screen" />

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="<c:url value="/static/tagthiscar/standard/script/markerclusterer_packed.js" />"></script>
<script type="text/javascript" src="<c:url value="/static/tagthiscar/standard/script/tags.js" />"></script>

