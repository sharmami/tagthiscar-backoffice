<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>

<c:if test="${not empty message}">
	<div class="${message.type.cssClass}">${message.text}</div>
</c:if>

<article id="intro">
	<hgroup>
		<h3>Add a Tag.</h3>
		<p>
			<div class="formInfo">
                <a href="<c:url value="/signout" />">Sign Out</a>
			</div>		
		</p>
	</hgroup>

</article>	
	
	
	
	
