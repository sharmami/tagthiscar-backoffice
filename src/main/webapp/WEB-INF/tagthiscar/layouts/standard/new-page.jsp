<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html>
<html>
<head>
	<title><tiles:insertAttribute name="title" defaultValue="Tag This Car" /></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="description" content="Making Drivers Smarter" />
    <meta name="keywords" content="Tag This Car LLC, car tags, car analysis, vehicle and location predictive analysis" />


    <link href='<c:url value="/static/tagthiscar/favicon.ico" />' rel='SHORTCUT ICON' />

    <!-- Bootstrap -->
    <link href="<c:url value="/static/tagthiscar/standard/bootstrap/css/bootstrap.min.css" />" rel="stylesheet" media="screen">

    <!-- uncomment this when you have the new look and feel //TODO
    <link rel="stylesheet" href="<c:url value="/static/tagthiscar/standard/css/style.css" />" type="text/css" media="screen" />
    -->
    <link rel="stylesheet" href="<c:url value="/static/tagthiscar/standard/css/tagthiscar.css" />" type="text/css" media="screen" />

    <link href="<c:url value="/static/tagthiscar/standard/bootstrap/css/bootstrap-responsive.css" />" rel="stylesheet">

    <link rel="stylesheet" href="<c:url value="/static/tagthiscar/standard/css/map.css" />" type="text/css" media="screen" />

    <%--
	<tiles:useAttribute id="styles" name="styles" classname="java.util.List" ignore="true" />
	
	<c:forEach var="style" items="${styles}">
		<link rel="stylesheet" href="<c:url value="/static/${style}" />" type="text/css" media="all" />
	</c:forEach>
	<c:forEach var="meta" items="${metadata}">
	    <meta name="${meta.key}" content="${meta.value}"/>
	</c:forEach>
	--%>
    <script src="http://code.jquery.com/jquery.js"></script>
    <script type="text/javascript">
        var CONTEXT_ROOT = '<%= request.getContextPath() %>';
    </script>
    <!-- datatables -->
    <style type="text/css" title="currentStyle">
        			@import "<c:url value="/static/tagthiscar/standard/DataTables-1.9.4/media/css/demo_page.css" />";
        			@import "<c:url value="/static/tagthiscar/standard/DataTables-1.9.4/media/css/demo_table.css" />";
    </style>
     <script type="text/javascript" charset="utf-8" src="<c:url value="/static/tagthiscar/standard/DataTables-1.9.4/media/js/jquery.dataTables.js" />"></script>

     <!-- multiple select master -->
     <script type="text/javascript" charset="utf-8" src="<c:url value="/static/tagthiscar/standard/multiple-select-master/jquery.multiple.select.js" />"></script>
     <link rel="stylesheet" href="<c:url value="/static/tagthiscar/standard/multiple-select-master/multiple-select.css" />" type="text/css" media="screen" />

     <script type="text/javascript">
    $(document).ready(function(){
        $('#dataTable').dataTable();

        $(".multiple-select").multipleSelect({
                    placeholder: "Flags",
                    /*
                    width: 460,
                    multiple: true,
                    multipleWidth: 105,
                    */
                    filter: true,
                    maxHeight:250
                });
        /*
        $('#dataTable').dataTable( {
                "bPaginate": true,
                "bLengthChange": false,
                "bFilter": true,
                "bSort": false,
                "bInfo": false,
                "bAutoWidth": true,
                "iDisplayLength": 5
            } );
        */
    });
    </script>
    	<!-- datatables ends -->

    	<link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300,100' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="<c:url value="/static/tagthiscar/standard/css/new.css" />" type="text/css" media="screen" />

        <!-- sharethis -->
        <script type="text/javascript">var switchTo5x=true;</script>
        <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
        <script type="text/javascript">stLight.options({publisher: "ddc52262-824e-47a8-b563-7823f9174095", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>

</head>
<body>

    <div class="navbar navbar-fixed-top">
                  <div class="navbar-inner">
                    <div class="container">
                      <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                      <a class="brand" href="<c:url value="/" />"><img width="25" height="25" src="<c:url value="/static/tagthiscar/standard/images/G_250x250_LORES.png" />"></a></a>
                      <div class="nav-collapse collapse">
                        <ul class="nav">
                          <li>
                              <form class="navbar-search pull-left" action="<c:url value="/" />" method="POST">
                                       <input id="latitudeId" name="latitude" type="hidden"  value="${search.latitude}"/>
                                       <input id="longitudeId" name="longitude" type="hidden"  value="${search.longitude}"/>
                                       <input type="hidden" id="addressId" name="address" value="${search.address}" />
                                       <input type="hidden" id="noOfMilesId" name="noOfMiles" value="${search.value}" />
                                       <input type="hidden" id="destinationId" name="destination" value="${search.destination}" />
                                       <input type="hidden" id="destId" name="destinationTerm" value="${search.destination}" />
                                       <input type="hidden" id="avoidHighwaysId" name="avoidHighwaysHidden" value="${search.avoidHighways}" />
                                       <input type="hidden" id="avoidTollsId" name="avoidTollsHidden" value="${search.avoidTolls}" />
                                       <input type="hidden" id="travelModeHiddenId" name="travelModeHidden" value="${search.travelMode}"/>
                                       <input type="hidden" id="limitId" name="limitHidden" value="${search.limit}"/>
                                       <input type="hidden" id="csvFlagTypesId" name="csvFlagTypesHidden" value="${search.csvFlagTypes}"/>
                                       <input type="hidden" id="csvLabelsId" name="csvLabelsHidden" value="${search.labels}"/>
                                       <input type="hidden" id="noOfDaysId" name="noOfDaysHidden" value="${search.noOfDays}"/>
                                       <input type="hidden" id="sortById" name="sortByHidden" value="${search.sortBy}"/>

                                        <div class="form-inline">
                                                       <label for="whatId"><strong>Look for:</strong></label>
                                                       <select name="csvFlagTypes" id="whatId" multiple="multiple" class="span2.5 multiple-select">
                                                            <c:forEach var="flagType" items="${flagTypes}">
                                                                <option value="${flagType}">${flagType}</option>
                                                            </c:forEach>
                                                        </select>

                                                         <label for="whatId"><strong>within</strong></label>
                                                        <select class="span1" name="value" id="whatId">
                                                          <option <c:if test="${empty search.value or search.value eq 1}">selected</c:if>>1</option>
                                                          <option <c:if test="${ search.value eq 2}">selected</c:if>>2</option>
                                                          <option <c:if test="${ search.value eq 5}">selected</c:if>>5</option>
                                                          <option <c:if test="${search.value eq 10}">selected</c:if>>10</option>
                                                          <option <c:if test="${search.value eq 15}">selected</c:if>>15</option>
                                                       </select>

                                                       <label for="vehicleOrLocationId"><strong>miles of:</strong></label>
                                                       <input class="span3 input-medium" name="searchTerm" id="vehicleOrLocationId" type="text" placeholder="type a location - Sterling, Va"  value="${search.searchTerm}" x-webkit-speech="" required>

                                                       <label for="lastId"><strong>in the last:</strong></label>
                                                       <select class="span1" name="noOfDays" id="lastId">
                                                            <option value="-1" <c:if test="${search.noOfDays eq -1}">selected</c:if>>1</option>
                                                            <option value="-2" <c:if test="${search.noOfDays eq -2}">selected</c:if>>2</option>
                                                            <option value="-5" <c:if test="${search.noOfDays eq -5}">selected</c:if>>5</option>
                                                            <option value="-10" <c:if test="${search.noOfDays eq -10}">selected</c:if>>10</option>
                                                            <option value="-15" <c:if test="${search.noOfDays eq -15}">selected</c:if>>15</option>
                                                       </select>  days.

                                                        <button id="homeSubmitId" class="btn btn-primary" type="submit" > <i class="icon-search icon-white"></i></button>
                                        </div>
                               </form>
                          </li>
                        </ul>
                         <div class="pull-right">
                         <ul class="nav pull-right">
                             <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Menu <b class="caret"></b></a>
                                  <ul class="dropdown-menu">
                                      <li><a href="<c:url value="/" />"><i class="icon-home"></i> Home</a></li>
                                      <!--
                                      <li><a href="/help/support"><i class="icon-envelope"></i> Contact Support</a></li>
                                      <li class="divider"></li>
                                      <li><a href="/auth/logout"><i class="icon-off"></i> Sign In</a></li>
                                     -->
                                  </ul>
                             </li>
                         </ul>
                         </div>
                       </div>
                    </div>
                  </div>
                </div>

    <div class="container">

        <header class="site-wide">
            <tiles:insertAttribute name="header" />
        </header>

        <div id="wrap">
          <div class="container-narrow">
           <%--
            <div class="row-fluid">
              <div class="span12 pagination-centered"><a href="<c:url value="/" />"><img src="<c:url value="/static/tagthiscar/standard/images/WEB_TagThisCarLogo_Alpha1.jpg" />" class="logo"></a></div>
            </div>
            --%>
            <tiles:insertAttribute name="search-form" />
          </div>

          <section class="html5-intro">
            <tiles:insertAttribute name="content" />
          </section>

          <!-- <hr> -->
          <div id="push"></div>
        </div>
    </div>

    <footer class="bottom">
            <tiles:insertAttribute name="footer" />
    </footer>

	<%--
	<tiles:useAttribute id="scripts" name="scripts" classname="java.util.List" ignore="true" />
	<c:forEach var="script" items="${scripts}">
		<script type="text/javascript" src="<c:url value="/static/${script}" />"></script>	
	</c:forEach>
	--%>
	<script type="text/javascript">
		$.cookie('Greenhouse.timeZoneOffset', new Date().getTimezoneOffset() * 60000);
	</script>
    <script src="<c:url value="/static/tagthiscar/standard/script/BrowserDetect.js" />"></script>
    <script src="<c:url value="/static/tagthiscar/standard/script/main.js" />"></script>
</body>
</html>