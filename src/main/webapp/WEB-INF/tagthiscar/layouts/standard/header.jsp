<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>

<%--
<div class="row pull-left">
    <a href="https://www.facebook.com/TagThisCar" target="_blank">Like</a> |
     <a href="https://plus.google.com/u/0/115519436078165782151/posts/p/pub" target="_blank">Plus</a> |
     <a href="https://twitter.com/tagthiscar" target="_blank">Follow</a>  us.
</div>
<nav class="right-side">
	 <div class="masthead">
          <ul class="inline pull-right">
            <security:authorize ifAllGranted="ROLE_ANONYMOUS">
                <li class="active"><a href="<c:url value="/" />">Home</a></li> |
                <li><a href="<c:url value="/signin" />">Login</a></li> |
                <li><a href="<c:url value="/signup" />">Sign Up</a></li>
            </security:authorize>
            <security:authorize ifNotGranted="ROLE_ANONYMOUS">
                <li class="active"><a href="<c:url value="/" />">Home</a></li>
                <li><a href="<c:url value="/signout" />">Sign Out</a></li>
            </security:authorize>
          </ul>
          <h3 class="muted"></h3>
     </div>
</nav>
--%>
<link rel="stylesheet" href="<c:url value="/static/tagthiscar/standard/css/map.css" />" type="text/css" media="screen" />

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=places"></script>
<script type="text/javascript" src="https://github.com/douglascrockford/JSON-js/blob/master/json2.js"></script>
<script type="text/javascript" src="<c:url value="/static/tagthiscar/standard/script/markerclusterer_packed.js" />"></script>
<script type="text/javascript" src="<c:url value="/static/tagthiscar/standard/script/home.js" />"></script>



