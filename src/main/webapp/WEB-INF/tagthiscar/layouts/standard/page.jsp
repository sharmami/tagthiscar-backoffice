<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html>
<html>
<head>
	<title><tiles:insertAttribute name="title" defaultValue="Tag This Car" /></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="description" content="Making Drivers Smarter" />
    <meta name="keywords" content="Tag This Car LLC, car tags, car analysis, vehicle and location predictive analysis" />


    <link href='<c:url value="/static/tagthiscar/favicon.ico" />' rel='SHORTCUT ICON' />

    <!-- Bootstrap -->
    <link href="<c:url value="/static/tagthiscar/standard/bootstrap/css/bootstrap.min.css" />" rel="stylesheet" media="screen">

    <link rel="stylesheet" href="<c:url value="/static/tagthiscar/standard/css/style.css" />" type="text/css" media="screen" />

    <link href="<c:url value="/static/tagthiscar/standard/bootstrap/css/bootstrap-responsive.css" />" rel="stylesheet">

    <link rel="stylesheet" href="<c:url value="/static/tagthiscar/standard/css/map.css" />" type="text/css" media="screen" />

    <%--
	<tiles:useAttribute id="styles" name="styles" classname="java.util.List" ignore="true" />
	
	<c:forEach var="style" items="${styles}">
		<link rel="stylesheet" href="<c:url value="/static/${style}" />" type="text/css" media="all" />
	</c:forEach>
	<c:forEach var="meta" items="${metadata}">
	    <meta name="${meta.key}" content="${meta.value}"/>
	</c:forEach>
	--%>
    <script src="http://code.jquery.com/jquery.js"></script>
    <script type="text/javascript">
        var CONTEXT_ROOT = '<%= request.getContextPath() %>';
    </script>
    <!-- datatables -->
    <style type="text/css" title="currentStyle">
        			@import "<c:url value="/static/tagthiscar/standard/DataTables-1.9.4/media/css/demo_page.css" />";
        			@import "<c:url value="/static/tagthiscar/standard/DataTables-1.9.4/media/css/demo_table.css" />";
    </style>
     <script type="text/javascript" charset="utf-8" src="<c:url value="/static/tagthiscar/standard/DataTables-1.9.4/media/js/jquery.dataTables.js" />"></script>

    <!-- multiple select master -->
    <script type="text/javascript" charset="utf-8" src="<c:url value="/static/tagthiscar/standard/multiple-select-master/jquery.multiple.select.js" />"></script>
    <link rel="stylesheet" href="<c:url value="/static/tagthiscar/standard/multiple-select-master/multiple-select.css" />" type="text/css" media="screen" />

     <script type="text/javascript">
    $(document).ready(function(){
        $('#dataTable').dataTable();
         $(".multiple-select").multipleSelect({
            placeholder: "Flags",
            /*
            width: 460,
            multiple: true,
            multipleWidth: 105,
            */
            filter: true,
            maxHeight:250
         });

        /*
        $('#dataTable').dataTable( {
                "bPaginate": true,
                "bLengthChange": false,
                "bFilter": true,
                "bSort": false,
                "bInfo": false,
                "bAutoWidth": true,
                "iDisplayLength": 5
            } );
        */
    });
    </script>
    	<!-- datatables ends -->

    	<link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300,100' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="<c:url value="/static/tagthiscar/standard/css/new.css" />" type="text/css" media="screen" />

        <!-- sharethis -->
        <script type="text/javascript">var switchTo5x=true;</script>
        <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
        <script type="text/javascript">stLight.options({publisher: "ddc52262-824e-47a8-b563-7823f9174095", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>

</head>
<body>
    <div class="container">
        <header class="site-wide">
            <tiles:insertAttribute name="header" />
        </header>

        <div id="wrap">
          <div class="container-narrow">
            <div class="row-fluid">
              <div class="span12 pagination-centered"><a href="<c:url value="/" />"><img src="<c:url value="/static/tagthiscar/standard/images/WEB_TagThisCarLogo_Alpha1.jpg" />" class="logo"></a></div>
            </div>
            <tiles:insertAttribute name="search-form" />
          </div>

          <section class="html5-intro">
            <tiles:insertAttribute name="content" />
          </section>

          <!-- <hr> -->
          <div id="push"></div>
        </div>
    </div>

    <footer class="bottom">
            <tiles:insertAttribute name="footer" />
    </footer>

	<%--
	<tiles:useAttribute id="scripts" name="scripts" classname="java.util.List" ignore="true" />
	<c:forEach var="script" items="${scripts}">
		<script type="text/javascript" src="<c:url value="/static/${script}" />"></script>	
	</c:forEach>
	--%>
	<script type="text/javascript">
		$.cookie('Greenhouse.timeZoneOffset', new Date().getTimezoneOffset() * 60000);
	</script>
    <script src="<c:url value="/static/tagthiscar/standard/script/BrowserDetect.js" />"></script>
    <script src="<c:url value="/static/tagthiscar/standard/script/main.js" />"></script>
</body>
</html>