<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div id="footer">
    <div class="container">
        <p class="muted credit">
            <form id="subscribeFormId">
              <div class="control-group">
              <label class="control-label" for="inputIcon">Your email is safe with us.</label>
              <div class="controls">
                <div class="input-prepend input-append">
                  <span class="add-on"><i class="icon-envelope"></i></span>
                  <input class="span3" id="subscribeEmailId" type="email" placeholder="email@address.com" required>
                  <input id="subscribeId" type="submit" name="submit" value="Keep me informed!" class="btn btn-inverse">
                </div>
              </div>
            </div>
            </form>

            <!-- Thank you Modal -->
            <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <!--
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 id="myModalLabel">Tag This Car - Keep me informed!</h3>
              </div>
              -->
              <div class="modal-body">
                <p class="alert alert-success">Thank you for your interest!</p>
              </div>
              <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
              </div>
            </div>
            <!-- modal ends -->
          <div class="row">
          <span class="pull-left">
          <a href="<c:url value="/" />">Home</a> |
          <a href="<c:url value="/terms_of_use" />">Terms Of Use</a> |
          <a href="<c:url value="/privacy_policy" />">Privacy Policy</a> |
          <a href="<c:url value="/faq" />">FAQ</a> |
          <a href="<c:url value="/about_us" />">About Us</a> |
          <a href="mailto:info@tagthiscar.com">Contact Us</a>
          </span>
             <span class="pull-right">
                 <a href="https://www.facebook.com/TagThisCar" target="_blank">Like</a> |
                 <a href="https://plus.google.com/u/0/115519436078165782151/posts/p/pub" target="_blank">Plus</a> |
                 <a href="https://twitter.com/tagthiscar" target="_blank">Follow</a>  us.
             </span>
          </div>
          &nbsp; &nbsp; &copy; Copyright 2013 Tag This Car LLC
        </p>
    </div>
</div>

<!--
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<script src="http://code.jquery.com/jquery.js"></script>
-->
 <!-- Google Plus Place this asynchronous JavaScript just before your </body> tag -->
 <script type="text/javascript">
      (function() {
       var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
       po.src = 'https://apis.google.com/js/client:plusone.js';
       var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
     })();
 </script>
<script src="<c:url value="/static/tagthiscar/standard/bootstrap/js/bootstrap.min.js" />"></script>





