(function() {

  window.onload = function()
  {
    geocoder = new google.maps.Geocoder();

    // Checking if geo positioning is available
    if (geo_position_js.init())
    {
        var settings = {
            enableHighAccuracy: true
        };
        // Trying to determine the location of the user
        geo_position_js.getCurrentPosition(setPosition, handleError, settings);
    } else {
        $('div#geolocationMessageId').text('Sorry I could not determine your Lat/Long. Do you mind entering it?' );
    }

      //add autocomplete
     var sourceAC = /** @type {HTMLInputElement} */(document.getElementById('vehicleOrLocationId'));
     new google.maps.places.Autocomplete(sourceAC);
     var destinationAC = /** @type {HTMLInputElement} */(document.getElementById('destId'));
     new google.maps.places.Autocomplete(destinationAC);

  };

  function handleError(error) {
    alert('Error = ' + error.message);
  }
  
  function setPosition(position)
  {
    var lat =  position.coords.latitude;
    var long =  position.coords.longitude;
    $('div#geolocationMessageId').text('Your Latitude: '+lat+' Longitude: '+long);
    getAddress(lat,long);
  } //end of setPosition.

  function getAddress(lat,lng)
  {

    var latlng = new google.maps.LatLng(lat, lng);

    geocoder.geocode({'latLng': latlng}, function(results, status)
    {
        if (status == google.maps.GeocoderStatus.OK)
        {
            $('#vehicleOrLocationId').val(results[0].formatted_address);
        }
        else
        {
            alert("Geocoder failed due to: " + status);
        }
    });

  } // end of getAddress.

})();