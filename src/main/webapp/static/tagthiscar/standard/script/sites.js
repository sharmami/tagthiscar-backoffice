(function() {

     // Enable the visual refresh
    google.maps.visualRefresh = true;

    var map, geocoder, marker, bounds;
    var infowindow = new google.maps.InfoWindow();

    // Creating an array that will store the markers
    var markers = [];
    var route = [];

    // Creating an array that will contain all of our flag icons
    var markerIcons = [];

    //static data that is always available
    markerIcons['accident'] = new google.maps.MarkerImage(
     '../static/tagthiscar/standard/images/markers/caraccident.png',
     new google.maps.Size(32, 37),
     null,
     new google.maps.Point(24, 24)
    );

    markerIcons['caution'] = new google.maps.MarkerImage(
     '../static/tagthiscar/standard/images/markers/caution.png',
     new google.maps.Size(32, 37),
     null,
     new google.maps.Point(24, 24)
    );

    markerIcons['construction'] = new google.maps.MarkerImage(
     '../static/tagthiscar/standard/images/markers/construction.png',
     new google.maps.Size(48, 48),
     null,
     new google.maps.Point(24, 24)
    );

    markerIcons['car'] = new google.maps.MarkerImage(
                 '../static/tagthiscar/standard/images/markers/car.png',
                 new google.maps.Size(48, 48),
                 null,
                 new google.maps.Point(24, 24)
    );

    //you are here
    markerIcons['you-are-here-2'] = new google.maps.MarkerImage(
                     '../static/tagthiscar/standard/images/markers/you-are-here-2.png',
                     new google.maps.Size(48, 48),
                     null,
                     new google.maps.Point(24, 24)
        );

    //you are here
    markerIcons['tires'] = new google.maps.MarkerImage(
                     '../static/tagthiscar/standard/images/markers/tires.png',
                     new google.maps.Size(48, 48),
                     null,
                     new google.maps.Point(24, 24)
        );

   // new signs
       markerIcons['closedroad'] = new google.maps.MarkerImage(
            'static/tagthiscar/standard/images/markers/closedroad.png',
            new google.maps.Size(32, 37),
            null,
            new google.maps.Point(24, 24)
           );

       markerIcons['accesdenied'] = new google.maps.MarkerImage(
                'static/tagthiscar/standard/images/markers/accesdenied.png',
                new google.maps.Size(32, 37),
                null,
                new google.maps.Point(24, 24)
               );

        markerIcons['airport'] = new google.maps.MarkerImage(
                     'static/tagthiscar/standard/images/markers/airport.png',
                     new google.maps.Size(32, 37),
                     null,
                     new google.maps.Point(24, 24)
                    );

        markerIcons['bulldozer'] = new google.maps.MarkerImage(
                         'static/tagthiscar/standard/images/markers/bulldozer.png',
                         new google.maps.Size(32, 37),
                         null,
                         new google.maps.Point(24, 24)
                        );

       markerIcons['busstop'] = new google.maps.MarkerImage(
                             'static/tagthiscar/standard/images/markers/busstop.png',
                             new google.maps.Size(32, 37),
                             null,
                             new google.maps.Point(24, 24)
                            );

       markerIcons['caraccident'] = new google.maps.MarkerImage(
                                 'static/tagthiscar/standard/images/markers/caraccident.png',
                                 new google.maps.Size(32, 37),
                                 null,
                                 new google.maps.Point(24, 24)
                                );
        markerIcons['cycling'] = new google.maps.MarkerImage(
                                      'static/tagthiscar/standard/images/markers/cycling.png',
                                      new google.maps.Size(32, 37),
                                      null,
                                      new google.maps.Point(24, 24)
                                     );

         markerIcons['doublebendright'] = new google.maps.MarkerImage(
                                           'static/tagthiscar/standard/images/markers/doublebendright.png',
                                           new google.maps.Size(32, 37),
                                           null,
                                           new google.maps.Point(24, 24)
                                          );


        markerIcons['fallingrocks'] = new google.maps.MarkerImage(
                                               'static/tagthiscar/standard/images/markers/fallingrocks.png',
                                               new google.maps.Size(32, 37),
                                               null,
                                               new google.maps.Point(24, 24)
                                              );


        markerIcons['fillingstation'] = new google.maps.MarkerImage(
                                                   'static/tagthiscar/standard/images/markers/fillingstation.png',
                                                   new google.maps.Size(32, 37),
                                                   null,
                                                   new google.maps.Point(24, 24)
                                                  );

        markerIcons['highway'] = new google.maps.MarkerImage(
                                                   'static/tagthiscar/standard/images/markers/highway.png',
                                                   new google.maps.Size(32, 37),
                                                   null,
                                                   new google.maps.Point(24, 24)
                                                  );

        markerIcons['icy_road'] = new google.maps.MarkerImage(
                                                   'static/tagthiscar/standard/images/markers/icy_road.png',
                                                   new google.maps.Size(32, 37),
                                                   null,
                                                   new google.maps.Point(24, 24)
                                                  );

        markerIcons['junction'] = new google.maps.MarkerImage(
                                                   'static/tagthiscar/standard/images/markers/junction.png',
                                                   new google.maps.Size(32, 37),
                                                   null,
                                                   new google.maps.Point(24, 24)
                                                  );

        markerIcons['levelcrossing'] = new google.maps.MarkerImage(
                                                   'static/tagthiscar/standard/images/markers/levelcrossing.png',
                                                   new google.maps.Size(32, 37),
                                                   null,
                                                   new google.maps.Point(24, 24)
                                                  );

        markerIcons['mainroad'] = new google.maps.MarkerImage(
                                                   'static/tagthiscar/standard/images/markers/mainroad.png',
                                                   new google.maps.Size(32, 37),
                                                   null,
                                                   new google.maps.Point(24, 24)
                                                  );

        markerIcons['motorcycle'] = new google.maps.MarkerImage(
                                                   'static/tagthiscar/standard/images/markers/motorcycle.png',
                                                   new google.maps.Size(32, 37),
                                                   null,
                                                   new google.maps.Point(24, 24)
                                                  );

        markerIcons['parkandride'] = new google.maps.MarkerImage(
                                                   'static/tagthiscar/standard/images/markers/parkandride.png',
                                                   new google.maps.Size(32, 37),
                                                   null,
                                                   new google.maps.Point(24, 24)
                                                  );
        markerIcons['parkinggarage'] = new google.maps.MarkerImage(
                                                   'static/tagthiscar/standard/images/markers/parkinggarage.png',
                                                   new google.maps.Size(32, 37),
                                                   null,
                                                   new google.maps.Point(24, 24)
                                                  );

        markerIcons['parking-meter-export'] = new google.maps.MarkerImage(
                                                   'static/tagthiscar/standard/images/markers/parking-meter-export.png',
                                                   new google.maps.Size(32, 37),
                                                   null,
                                                   new google.maps.Point(24, 24)
                                                  );
        markerIcons['pedestriancrossing'] = new google.maps.MarkerImage(
                                                   'static/tagthiscar/standard/images/markers/pedestriancrossing.png',
                                                   new google.maps.Size(32, 37),
                                                   null,
                                                   new google.maps.Point(24, 24)
                                                  );
        markerIcons['repair'] = new google.maps.MarkerImage(
                                                   'static/tagthiscar/standard/images/markers/repair.png',
                                                   new google.maps.Size(32, 37),
                                                   null,
                                                   new google.maps.Point(24, 24)
                                                  );
        markerIcons['road'] = new google.maps.MarkerImage(
                                                   'static/tagthiscar/standard/images/markers/road.png',
                                                   new google.maps.Size(32, 37),
                                                   null,
                                                   new google.maps.Point(24, 24)
                                                  );
        markerIcons['roadtype_gravel'] = new google.maps.MarkerImage(
                                                   'static/tagthiscar/standard/images/markers/roadtype_gravel.png',
                                                   new google.maps.Size(32, 37),
                                                   null,
                                                   new google.maps.Point(24, 24)
                                                  );
        markerIcons['speedhump'] = new google.maps.MarkerImage(
                                                   'static/tagthiscar/standard/images/markers/speedhump.png',
                                                   new google.maps.Size(32, 37),
                                                   null,
                                                   new google.maps.Point(24, 24)
                                                  );
        markerIcons['stop'] = new google.maps.MarkerImage(
                                                   'static/tagthiscar/standard/images/markers/stop.png',
                                                   new google.maps.Size(32, 37),
                                                   null,
                                                   new google.maps.Point(24, 24)
                                                  );
        markerIcons['taxi'] = new google.maps.MarkerImage(
                                                   'static/tagthiscar/standard/images/markers/taxi.png',
                                                   new google.maps.Size(32, 37),
                                                   null,
                                                   new google.maps.Point(24, 24)
                                                  );
        markerIcons['tires'] = new google.maps.MarkerImage(
                                                   'static/tagthiscar/standard/images/markers/tires.png',
                                                   new google.maps.Size(32, 37),
                                                   null,
                                                   new google.maps.Point(24, 24)
                                                  );
        markerIcons['tollstation'] = new google.maps.MarkerImage(
                                                   'static/tagthiscar/standard/images/markers/tollstation.png',
                                                   new google.maps.Size(32, 37),
                                                   null,
                                                   new google.maps.Point(24, 24)
                                                  );
        markerIcons['trafficcamera'] = new google.maps.MarkerImage(
                                                   'static/tagthiscar/standard/images/markers/trafficcamera.png',
                                                   new google.maps.Size(32, 37),
                                                   null,
                                                   new google.maps.Point(24, 24)
                                                  );
        markerIcons['trafficlight'] = new google.maps.MarkerImage(
                                                   'static/tagthiscar/standard/images/markers/trafficlight.png',
                                                   new google.maps.Size(32, 37),
                                                   null,
                                                   new google.maps.Point(24, 24)
                                                  );

        markerIcons['tramway'] = new google.maps.MarkerImage(
                                                   'static/tagthiscar/standard/images/markers/tramway.png',
                                                   new google.maps.Size(32, 37),
                                                   null,
                                                   new google.maps.Point(24, 24)
                                                  );

        markerIcons['truck3'] = new google.maps.MarkerImage(
                                                   'static/tagthiscar/standard/images/markers/truck3.png',
                                                   new google.maps.Size(32, 37),
                                                   null,
                                                   new google.maps.Point(24, 24)
                                                  );

        markerIcons['tunnel'] = new google.maps.MarkerImage(
                                                   'static/tagthiscar/standard/images/markers/tunnel.png',
                                                   new google.maps.Size(32, 37),
                                                   null,
                                                   new google.maps.Point(24, 24)
                                                  );

        markerIcons['underground'] = new google.maps.MarkerImage(
                                                   'static/tagthiscar/standard/images/markers/underground.png',
                                                   new google.maps.Size(32, 37),
                                                   null,
                                                   new google.maps.Point(24, 24)
                                                  );

    //get the map options ready
    var options = {
         zoom: 5,
         mapTypeId: google.maps.MapTypeId.ROADMAP,
         mapTypeControl: true,
         mapTypeControlOptions: {
             style: google.maps.MapTypeControlStyle.DEFAULT,
             //style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
             position: google.maps.ControlPosition.TOP_RIGHT,
             //style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR
             mapTypeIds: [
                 google.maps.MapTypeId.ROADMAP,
                 google.maps.MapTypeId.SATELLITE
             ]
         },
         disableDefaultUI:true,
         navigationControl:true,
         navigationControlOptions:
         {
              position: google.maps.ControlPosition.TOP_LEFT,
              //style: google.maps.NavigationControlStyle.ZOOM_PAN
              //style: google.maps.NavigationControlStyle.ANDROID
              style: google.maps.NavigationControlStyle.DEFAULT
         }
         //,
        //backgroundColor: '#ff0000'
    };

    //shadow
    var shadow = new google.maps.MarkerImage(
          '../static/tagthiscar/standard/images/markers/shadow.png',
           new google.maps.Size(51, 37),
           new google.maps.Point(0, 0),
           new google.maps.Point(16, 35)
    );


     // Creating a LatLngBounds object (to zoom in automatically based on the dynamic data)
     bounds = new google.maps.LatLngBounds();

    window.onload = function()
    {

        // Creating the map
        map = new google.maps.Map(document.getElementById('map'), options);
        var latLongPos =  new google.maps.LatLng('36.1834', '-117.4960');
        map.setCenter(latLongPos);

        //add the traffic layer
        var trafficLayer2 = new google.maps.TrafficLayer();
        trafficLayer2.setMap(map);

        // Making the Geocoder call
        var latitudeIdStr = $('#siteLatitudeId').val();
        var longitudeIdStr = $('#siteLongitudeId').val();
        var teaserIdStr = $('#siteTeaserId').val();
        //var csvFlagTypesIdStr = $('#siteCsvFlagTypesId').val();
        var csvFlagTypesIdStr = 'caution';

        drawMap(latitudeIdStr,longitudeIdStr, teaserIdStr,csvFlagTypesIdStr, map);

        // Creating the polyline object
        var polyline = new google.maps.Polyline({
            path: route,
            strokeColor: "#ff0000",
            strokeOpacity: 0.6,
            strokeWeight: 5
        });

        // Adding the polyline to the map
        polyline.setMap(map);

        //var markerclusterer = new MarkerClusterer(map, markers);
        map.fitBounds(bounds);
        //zoom out a little if you are zoomed in too much.
        var listener = google.maps.event.addListener(map, "idle", function() {
                  if (map.getZoom()>16) map.setZoom(16);
                  google.maps.event.removeListener(listener);
                });
    }; // end of window load

    function drawMap(lat,long, teaser,markerIcon, map){
                 var latLng = new google.maps.LatLng(lat,long);
                 route.push(latLng);
                 bounds.extend(latLng);

                 var marker = new google.maps.Marker({
                             position: latLng,
                             map: map,
                             icon: markerIcons[markerIcon],
                             shadow: shadow
                         });
                 markers.push(marker);

                 google.maps.event.addListener(marker, 'click', function() {
                   // Check to see if we already have an InfoWindow
                   if (!infowindow) {
                     infowindow = new google.maps.InfoWindow();
                   }

                   // Setting the content of the InfoWindow
                   infowindow.setContent(teaser);

                   // Tying the InfoWindow to the marker
                   infowindow.open(map, marker);

                  });
         }


})();
