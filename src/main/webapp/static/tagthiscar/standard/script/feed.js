(function() {

      // Enable the visual refresh
      google.maps.visualRefresh = true;

     // Defining some global variables
      var map, geocoder, marker, infowindow;

    window.onload = function()
    {

         $(document).on('click', '#addressButton', function (e) {

                                 var address = $('#addressId').val();
                                 //alert(address);
                                 // Making the Geocoder call
                                getLatLong(address);

                                 // Preventing the form from doing a page submit
                                return false;
                              });


        // Creating a new map
           var options = {
             zoom: 3,
             center: new google.maps.LatLng(37.09, -95.71),
             mapTypeId: google.maps.MapTypeId.ROADMAP
           };

        // Creating the map
        map = new google.maps.Map(document.getElementById('widemap'), options);
        //add the traffic layer
        var trafficLayer2 = new google.maps.TrafficLayer();
        trafficLayer2.setMap(map);

     // Attaching a click event to the map
     	google.maps.event.addListener(map, 'click', function(e) {

         //alert('here '+e.latLng);
         //latitude

           // Getting the address for the position being clicked
           getAddress(e.latLng);

     	});


    }; // end of window load

                 // Create a function the will return the coordinates for the address
                  function getLatLong(address) {
                    // Check to see if we already have a geocoded object. If not we create one
                    if(!geocoder) {
                      geocoder = new google.maps.Geocoder();
                    }

                    // Creating a GeocoderRequest object
                    var geocoderRequest = {
                      address: address
                    }

                    // Making the Geocode request
                    geocoder.geocode(geocoderRequest, function(results, status) {

                      // Check if status is OK before proceeding
                      if (status == google.maps.GeocoderStatus.OK) {

                        // Center the map on the returned location
                        map.setCenter(results[0].geometry.location);

                        // Check to see if we've already got a Marker object
                        if (!marker) {
                          // Creating a new marker and adding it to the map
                          marker = new google.maps.Marker({
                            map: map
                          });
                        }

                        // Setting the position of the marker to the returned location
                        marker.setPosition(results[0].geometry.location);

                        // Check to see if we've already got an InfoWindow object
                        if (!infowindow) {
                          // Creating a new InfoWindow
                          infowindow = new google.maps.InfoWindow();
                        }

                        // Creating the content of the InfoWindow to the address
                        // and the returned position
                        var content = '<strong>' + results[0].formatted_address + '</strong><br />';
                        content += 'Latitude: ' + results[0].geometry.location.lat() + '<br />';
                        content += 'Longitude: ' + results[0].geometry.location.lng();

                        // Adding the content to the InfoWindow
                        infowindow.setContent(content);

                        // Opening the InfoWindow
                        infowindow.open(map, marker);

                       populateFormFields(results);
                      }

                    });

                  }


                  function getAddress(latLng) {


                      // Check to see if a geocoder object already exists
                      if (!geocoder) {
                        geocoder = new google.maps.Geocoder();
                      }

                      // Creating a GeocoderRequest object
                      var geocoderRequest = {
                        latLng: latLng
                      }


                      geocoder.geocode(geocoderRequest, function(results, status) {

                        // If the infowindow hasn't yet been created we create it
                        if (!infowindow) {
                          infowindow = new google.maps.InfoWindow();
                        }

                        // Setting the position for the InfoWindow
                        infowindow.setPosition(latLng);

                        // Creating content for the InfoWindow
                        var content = '<h3>Position: ' + latLng.toUrlValue() + '</h3>';

                         // Check to see if the request went allright
                        if (status == google.maps.GeocoderStatus.OK) {

                          // Looping through the result
                          for (var i = 0; i < results.length; i++) {
                            if (results[0].formatted_address) {
                              content += i + '. ' + results[i].formatted_address + '<br />';
                            }
                          }

                           populateFormFields(results);

                        } else {
                          content += '<p>No address could be found. Status = ' + status + '</p>';
                        }

                        // Adding the content to the InfoWindow
                        infowindow.setContent(content);

                        // Opening the InfoWindow
                        infowindow.open(map);

                      });

                }


                function populateFormFields(results){
                        var addr = {};
                        if (results.length >= 1) {

                             $('#latId').val(results[0].geometry.location.lat());
                             $('#longId').val(results[0].geometry.location.lng());
                             addr.street_number = '';
                             addr.route = '';
                             addr.city = '';
                             addr.state = '';
                             addr.zipcode = '';
                             addr.country = '';

                             for (var ii = 0; ii < results[0].address_components.length; ii++){
                             	 var street_number = route = street = city = state = zipcode = country = formatted_address = '';
                             	 var types = results[0].address_components[ii].types.join(",");

                             	 if (types == "street_number"){
                                    addr.street_number = results[0].address_components[ii].short_name;
                             	 }

                             	 if (types == "route" || types == "point_of_interest,establishment"){
                                        addr.route = results[0].address_components[ii].long_name;
                             	 }

                             	 if (types == "locality,political"){
                                             addr.city = (city == '' || types == "locality,political") ? results[0].address_components[ii].long_name : city;
                                 }

                             	 /*if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_3,political"){
                                    alert(results[0].address_components[ii].long_name+" tpe:"+types);
                                    addr.city = (city == '' || types == "locality,political") ? results[0].address_components[ii].long_name : city;
                             	 }*/


                             	 if (types == "administrative_area_level_1,political"){
                                    addr.state = results[0].address_components[ii].short_name;
                             	 }

                             	 if (types == "postal_code" || types == "postal_code_prefix,postal_code"){
                                    addr.zipcode = results[0].address_components[ii].long_name;
                             	 }

                             	 if (types == "country,political"){
                                    addr.country = results[0].address_components[ii].short_name;
                             	 }

                             	 }


                               $('#addyId').val(addr.street_number+' '+addr.route);

                               //$('#address1Id').val(addr.route);
                               $('#cityId').val(addr.city);
                               $('#stateId').val( addr.state);
                               $('#zipId').val(addr.zipcode);
                               $('#countryId').val(addr.country);




                        }
                }
})();
