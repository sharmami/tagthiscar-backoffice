(function() {

     // Enable the visual refresh
    google.maps.visualRefresh = true;

    var directionDisplay;
    var directionsService = new google.maps.DirectionsService();

    var map, geocoder, marker, bounds;
    var infowindow = new google.maps.InfoWindow();

    var latLongMarkerArray = [];

    // Creating an array that will store the markers
    var markers = [];
    var route = [];

    // Creating an array that will contain all of our flag icons
    var markerIcons = [];

    //static data that is always available
    //you are here
    markerIcons['you-are-here-2'] = new google.maps.MarkerImage('static/tagthiscar/standard/images/markers/you-are-here-2.png', new google.maps.Size(48, 48), null, new google.maps.Point(24, 24));


    //get the map options ready
    var options = {
         zoom: 11,
         panControl: true,
         zoomControl: true,
         scaleControl: true,
         mapTypeId: google.maps.MapTypeId.ROADMAP,
         mapTypeControl: true,
         mapTypeControlOptions: {
             style: google.maps.MapTypeControlStyle.DEFAULT,
             //style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
             position: google.maps.ControlPosition.TOP_RIGHT,
             //style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR
             mapTypeIds: [
                 google.maps.MapTypeId.ROADMAP,
                 google.maps.MapTypeId.SATELLITE
             ]
         },
         disableDefaultUI:true,
         navigationControl:true,
         navigationControlOptions:
         {
              position: google.maps.ControlPosition.TOP_LEFT,
              //style: google.maps.NavigationControlStyle.ZOOM_PAN
              //style: google.maps.NavigationControlStyle.ANDROID
              style: google.maps.NavigationControlStyle.DEFAULT
         }
         //,
        //backgroundColor: '#ff0000'
    };

    //shadow
    var shadow = new google.maps.MarkerImage(
          'static/tagthiscar/standard/images/markers/shadow.png',
           new google.maps.Size(51, 37),
           new google.maps.Point(0, 0),
           new google.maps.Point(16, 35)
    );

    // Creating a LatLngBounds object (to zoom in automatically based on the dynamic data)
    bounds = new google.maps.LatLngBounds();

    window.onload = function()
    {


        $("#parkingEnabledId").change(function() {
            if(this.checked) {
                $('#parkingEnabledId').val('true');
            }  else{
                $('#parkingEnabledId').val('false');
            }

             var center = map.getCenter();
             var latLongPos =  new google.maps.LatLng(center.lat(),center.lng());
             $('#addressId').val('');
             bounds = new google.maps.LatLngBounds();
             //map.clearOverlays();
             getResultsFromTheServer(latLongPos,false);
             e.preventDefault();
        });

        $(document).on('click', '#updatePageId', function (e) {
                   var center = map.getCenter();
                   var latLongPos =  new google.maps.LatLng(center.lat(),center.lng());
                   $('#addressId').val('');
                   bounds = new google.maps.LatLngBounds();
                   //map.clearOverlays();
                   getResultsFromTheServer(latLongPos,false);
                   e.preventDefault();
                });

        // open infowindow if someone hovers over the link
        $(document).on('mouseenter', '[id^="mylink"]', function (e) {
                var id = $(this).attr('id');
                var locationId= id.replace('mylink','');
                var markerId = latLongMarkerArray[locationId];
                google.maps.event.trigger(markers[markerId], "click");
                e.preventDefault();
            });

        // close infowindow on mouseleave
        $(document).on('mouseleave', '[id^="mylink"]', function (e) {
            var id = $(this).attr('id');
            var locationId= id.replace('mylink','');
            var markerId = latLongMarkerArray[locationId];
            if (infowindow) {
                        infowindow.close();
            };
        });


        var address = $('#addressId').val();
        var destination = $('#destinationId').val();
       var skipHighways = false;
       var skipTolls  = false;
       var travelMode = $('#travelModeHiddenId').val() ;
       var googleTravelMode = google.maps.DirectionsTravelMode.DRIVING;

       if(travelMode=='BICYCLING'){
             googleTravelMode = google.maps.DirectionsTravelMode.BICYCLING;
       }

       if(travelMode=='TRANSIT'){
           googleTravelMode = google.maps.DirectionsTravelMode.TRANSIT;
       }

      if(travelMode=='WALKING'){
            googleTravelMode = google.maps.DirectionsTravelMode.WALKING;
       }

        $("input[name=travelMode][value=" + travelMode + "]").prop('checked', true);

         if($('#avoidTollsId').val()=='true'){
            $('#skipTollsId').prop('checked', true);
            skipTolls = true;
         } else{
            $('#skipTollsId').prop('checked', false);
            skipTolls = false;
         }

         if($('#avoidHighwaysId').val()=='true'){
            $('#skipHighwaysId').prop('checked', true);
            skipHighways = true;
         }else{
              $('#skipHighwaysId').prop('checked', false);
              skipHighways = false;
         }


        // Creating the map
        map = new google.maps.Map(document.getElementById('largemap'), options);
        //add the traffic layer
        var trafficLayer2 = new google.maps.TrafficLayer();
        trafficLayer2.setMap(map);

        //add autocomplete
        var sourceAC = /** @type {HTMLInputElement} */(document.getElementById('vehicleOrLocationId'));
        new google.maps.places.Autocomplete(sourceAC);
        var destinationAC = /** @type {HTMLInputElement} */(document.getElementById('destId'));
        new google.maps.places.Autocomplete(destinationAC);

        if(destination!=null && destination!=''){
           $('#destinationDivId').show();
           $('#searchWithinDivId').hide();
        }
        else{
         // Call with just the address
         getCoordinates(address);
        }

        // directions?
        if(destination!=null && destination!=''){
            directionsDisplay = new google.maps.DirectionsRenderer();
            directionsDisplay.setMap(map);
            directionsDisplay.setPanel(document.getElementById('directions'));

            var request = {
                origin:address,
                destination:destination,
                /*travelMode: google.maps.DirectionsTravelMode.TRANSIT,*/
                travelMode: googleTravelMode,
                unitSystem: google.maps.DirectionsUnitSystem.IMPERICAL,
                avoidHighways: skipHighways,
                avoidTolls: skipTolls,
                provideRouteAlternatives: true,
                durationInTraffic:true
            };
            directionsService.route(request, function(response, status) {
              if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
              }
            });

        }// if destination check.

        google.maps.event.addListener(directionsDisplay, 'routeindex_changed', function() {
            var response = directionsDisplay.getDirections();
            var myRoute =  response.routes[this.routeIndex];
            var allPoints = [];
            //alert(this.routeIndex+'steps: '+myRoute.legs[0].steps.length);
            for(var j=0;j<=myRoute.legs[0].steps.length;j++){
                 if(myRoute.legs[0].steps[j]!=null){
                     allPoints.push(myRoute.legs[0].steps[j].end_location.lat()+':'+myRoute.legs[0].steps[j].end_location.lng());
                 }
             }
            //alert(allPoints);
            // send the points to server and get the results.
            getTripResultsFromTheServer(allPoints,true);
        });


        //if map is dragged, things should change
        google.maps.event.addListener(map,'dragend',function(event) {
            });

        // if zoom changes, see if the other things on the page can change?
        google.maps.event.addListener(map, 'zoom_changed', function() {
        });




    }; // end of window load


    // Create a function the will return the coordinates for the address
    function getCoordinates(address)
    {

        // Check to see if we already have a geocoded object. If not we create one
        if(!geocoder) {
            geocoder = new google.maps.Geocoder();
        }

        // Creating a GeocoderRequest object
        var geocoderRequest = {
            address: address
        }

        // Making the Geocode request
        geocoder.geocode(geocoderRequest, function(results, status)
        {
            // Check if status is OK before proceeding
            if (status == google.maps.GeocoderStatus.OK)
            {
                // you have the lat long. now call the method.
                getCoordinatesFromLatLong(results[0].geometry.location.lat(),results[0].geometry.location.lng());
            }
            else{
                 // you have the lat long. now call the method.
              getCoordinatesFromLatLong(0,0);
            }
        }); //end of geocode
    }//end of function

    function getCoordinatesFromLatLong(latitudeIdStr,longitudeIdStr)
        {
            // if lat and long are passed. use them to get the result for map.

                var latLongPos =  new google.maps.LatLng(latitudeIdStr,longitudeIdStr);
                if(latitudeIdStr!='0'){
                    map.setCenter(latLongPos);
                    // Go to the server and fetch all the results here.
                getResultsFromTheServer(latLongPos,false);
                } else{
                 // Go to the server and fetch all the results here.
                 getResultsFromTheServer(latLongPos,true);
                }




        }// end of function

    function getResultsFromTheServer(latLongPos,skipMarker)
    {
        clearMarkers();
        pageData = null;
        $.ajax({
          				type : "GET",
          				url : 'find_my_loc',
          				dataType : "json",
          				data : {
          					zoom: map.getZoom(),
          					longitude:latLongPos.lng(),
          					latitude: latLongPos.lat(),
          					searchTerm: $('#addressId').val(),
          					noOfMiles:$('#noOfMilesId').val(),
          					csvFlagTypes:$('#csvFlagTypesId').val(),
          					labels:$('#csvLabelsId').val(),
          					limit:$('#limitId').val(),
                            noOfDays:$('#noOfDaysId').val(),
                            parkingEnabled:$('#parkingEnabledId').val(),
                            sortBy:$('#sortById').val(),
          					time:$.now()
          				},
                        beforeSend: function(){
                            //$("#spinnerImg").show();
                        },
                        error: function(error) {
                            alert('Error finding address for Lat/Long provided.');
                            //alert(JSON.stringify(error));
                        },
                        success: function(pageData){
                        drawResultsOnMap(pageData);
                        //add what you searched for here
                        if(!skipMarker){
                         drawMap(latLongPos.lat(),latLongPos.lng(), 'You are here','you-are-here-2', map)
                        } else{
                            if(latLongPos!=null && latLongPos.lat()!=0){
                                bounds.extend(latLongPos);
                            }
                        }

                         // Adjusting the map to new bounding box
                         map.fitBounds(bounds);
                         map.panToBounds(bounds);

                        // if you can't find anything on the map stay at zoom 14.
                        if(pageData.automotive.length<1){
                            map.setZoom(14);
                        }


                        //cluser all points.
                        //var markerclusterer = new MarkerClusterer(map, markers);
                        drawSearchAnalysis(pageData,map);

                        drawTagsOrSites(pageData);

                        },
                        complete: function(){
                            //$("#spinnerImg").hide();
                        },
                        cache: false
          			});
    }// end of getresultsfromTheServer.


    //send source and destination to the server.
    function getTripResultsFromTheServer(allPoints,skipMarker)
        {
            clearMarkers();
            pageData = null;
            $.ajax({
              				type : "GET",
              				url : 'find_my_trip',
              				dataType : "json",
              				data : {
              					allPoints:JSON.stringify(allPoints),
              					searchTerm: $('#addressId').val(),
                                destinationTerm:$('#destinationId').val(),
                                csvFlagTypes:$('#csvFlagTypesId').val(),
                                parkingEnabled:true,
                                noOfDays:-15,
              					time:$.now()
              				},
                            beforeSend: function(){
                                //$("#spinnerImg").show();
                            },
                            error: function() {
                                alert('Error finding trip information.');
                            },
                            success: function(pageData){

                            drawResultsOnMap(pageData);

                            //add what you searched for here
                            /*if(!skipMarker){
                             drawMap(latLongPos.lat(),latLongPos.lng(), 'You are here','you-are-here-2', map)
                            } else{
                                bounds.extend(latLongPos);
                            }*/

                            // if you can't find anything on the map stay at zoom 14.
                            if(pageData.automotive.length<1){
                                //map.setZoom(14);
                            } else{
                               // Adjusting the map to new bounding box
                               map.fitBounds(bounds);
                               map.panToBounds(bounds);
                            }


                            //cluser all points.
                            //var markerclusterer = new MarkerClusterer(map, markers);
                            drawSearchAnalysis(pageData,map);

                            drawTagsOrSites(pageData);

                            },
                            complete: function(){
                                //$("#spinnerImg").hide();
                            },
                            cache: false
              			});
        }// end of getresultsfromTheServer.


    // Take the automotiveData and draw points on a map.
    function drawResultsOnMap(automotiveData)
        {
             for (var i = 0; i < automotiveData.automotive.length; i++)
             {

                var site = automotiveData.automotive[i];
                var submittedDate = new Date(site.submittedDate);
                var siteTeaser = '';
                siteTeaser += '<div id="infoWindowReSize">';
                 siteTeaser += '<small><a href="#">'+site.teaser+'</a> <br> '+site.location.address+' <br> '+new Date(site.submittedDate)+'<br/></small>';
                siteTeaser += '</div>';
                if(site.flagTypes!=null){
                  drawMap(site.location.latitude,site.location.longitude,siteTeaser,site.flagTypes[0], map);
                }
                latLongMarkerArray[site.location.id]=markers.length-1;
             }
        } // end of drawResultsOnAMap

    function drawSearchAnalysis(automotiveData,map)
    {
         $('#mapUpdatedResultsId').empty();
         var totalReports='',totalLocations='';
         if(automotiveData.analysis['totalReports']!=null)
         {

            var $ul1 = $('<ul class="nav nav-pills">').appendTo($('#mapUpdatedResultsId'));
            totalReports = '<a href="#"> '+automotiveData.analysis['totalReports']+' tags found.</a>';
            $('<li class="disabled">').html(totalReports).appendTo($ul1);

            totalLocations = '<a href="#"> '+automotiveData.analysis['totalLocations']+' locations found.</a>';
            $('<li class="disabled">').html(totalLocations).appendTo($ul1);

             //var dropDownHtml = "<a class='dropdown-toggle' data-toggle='dropdown' href='#'> Flags <b class='caret'></b> </a>";
             //var $li1 = $('<li class="dropdown">').html(dropDownHtml).appendTo($ul1);
             //var $ul2 = $('<ul class="dropdown-menu">').appendTo($li1);

             $.each(automotiveData.analysis, function(index, value) {
                if(index!='totalReports' && index!='totalLocations'){
                    var searchTerm=$('#addressId').val();
                    var destinationTerm=$('#destinationId').val();
                    var limit = $('#limitId').val();
                    var noOfDays =  $('#noOfDaysId').val();
                    var noOfMiles = $('#noOfMilesId').val();
                    var flag= index;
                    var flagType = '<a href="share?searchTerm='+searchTerm+'&limit='+limit+'&destinationTerm='+destinationTerm+'&noOfDays='+noOfDays+'&value='+noOfMiles+'&csvFlagTypes='+flag+'">'+value+' '+index+' flags</a>';
                   // $('<li class="divider">').html(flagType).appendTo($ul1);
                    //html(flagType).appendTo($ul2);
                    $('<li>').html(flagType).appendTo($ul1);
                    //$ul2.append(flagType);

                }
             });

             // add labels
           //var $ul2 = $('<ul class="nav nav-pills">').appendTo($('#mapUpdatedResultsId'));
           totalLabels = '<a href="#"> '+automotiveData.labels.length+' labels found.</a>';
           $('<li class="disabled">').html(totalLabels).appendTo($ul1);
           $.each(automotiveData.labels, function(index, label) {
                  $('<li>').html('<a href="share?labels='+label.name+'"> '+label.title+' </a>').appendTo($ul1);
            });

         }

        automotiveData = null;
    }// end of drawSearchAnalysis

    function drawTagsOrSites(automotiveData)
    {
        var tagOrSitesContent = '';
        $('div#tagorSitesResultsId').empty();

        if(automotiveData.automotive.length<1){
          tagOrSitesContent += '<div class="alert alert-info">No results found.</div>';
          $('div#tagorSitesResultsId').html(tagOrSitesContent);
        }

         tagOrSitesContent += '<table id="dataTable"> <thead> <tr> <th></th> </tr></thead><tbody> ';

        for (var i = 0; i < automotiveData.automotive.length; i++)
        {
              var site = automotiveData.automotive[i];
              var submittedDate = new Date(site.submittedDate);

              tagOrSitesContent += '<tr><td> <span class="badge">'+(i+1)+'</span> &nbsp;';
              tagOrSitesContent += '<b>Location:</b> '+site.location.address+' ';


               for(var j=0; j < automotiveData.duplicateLocationCoordinates.length; j++){
                var coordinates = automotiveData.duplicateLocationCoordinates[j];
                var currentCoordinates = site.location.latitude+', '+site.location.longitude;
                 if(coordinates == currentCoordinates){
                   //tagOrSitesContent += ' <span class="label label-info"><small>Multiple sites at this address</small></span> ';

                   var searchTerm=coordinates;
                   var limit = $('#limitId').val();
                   var noOfDays =  $('#noOfDaysId').val();
                   var noOfMiles =0.05;

                   var duplicateAddressSearch = '<small><a href="share?searchTerm='+searchTerm+'&limit='+limit+'&noOfDays='+noOfDays+'&value='+noOfMiles+'">Multiple sites at this address</a></small>';
                   tagOrSitesContent += duplicateAddressSearch;


                 }
               }

                //tagOrSitesContent += '<ul>';
                //tagOrSitesContent += '<li>';
                 tagOrSitesContent += '<br> <b>Flags:</b><a id="mylink'+site.location.id+'" href="#">'+getImagesFromFlagTypes(site.flagTypes)+'</a> '+site.csvFlagTypes+'<br>';
                 tagOrSitesContent += '<b>Title:</b> <a id="mylink'+site.location.id+'" href="#">'+site.teaser+'</a>';
                 if(site.labels.length>0){
                    tagOrSitesContent += '<br><b>Labels:</b> ';
                     $.each( site.labels, function( key, label ) {
                        tagOrSitesContent += ' <a href="share?labels='+label.name+'">'+label.title+'</a> ';
                     });
                 }

                 //tagOrSitesContent += '<br> Address:'+site.location.address+' '+site.location.address1+', '+site.location.city+' '+site.location.state+' '+site.location.zip+
                 tagOrSitesContent += ' <br> <em>submitted by: '+site.submittedBy+' on '+new Date(site.submittedDate)+' </em> <hr>';
                 //tagOrSitesContent += '</li>';
                //tagOrSitesContent += '</ul>';
                tagOrSitesContent += '</td></tr>';
         }

          tagOrSitesContent += ' </tbody></table>';

         $('div#tagorSitesResultsId').html(tagOrSitesContent);

           jQuery('#dataTable').dataTable({
                                                        "sScrollY": "550px",
                                                        "bPaginate": false,
                                                       "bLengthChange": false,
                                                       "bFilter": false,
                                                       "bSort": false,
                                                       "bInfo": true,
                                                       "bAutoWidth": true,
                                                       "iDisplayLength": 99,
                                                       "asStripeClasses": []
                                                    });

    }

    function getImagesFromFlagTypes(flagTypes)
    {
          var images = '';
          for(flagType in flagTypes){
            images += '<img src="static/tagthiscar/standard/images/markers/'+flagTypes[flagType]+'.png"/>';
          }

          return images;
    }

    function clearMarkers() {
      for (var i = 0; i < markers.length; i++ ) {
        markers[i].setMap(null);
      }
    }

    function drawMap(lat,long, teaser,markerIcon, map){
                 var latLng = new google.maps.LatLng(lat,long);
                 route.push(latLng);
                 bounds.extend(latLng);

                 var myIcon = new google.maps.MarkerImage('static/tagthiscar/standard/images/markers/'+markerIcon+'.png',new google.maps.Size(32, 37), null,new google.maps.Point(24, 24));
                 var marker = new google.maps.Marker({
                             position: latLng,
                             map: map,
                             /*icon: markerIcons[markerIcon],*/
                             icon: myIcon,
                             shadow: shadow
                         });
                 markers.push(marker);

                 google.maps.event.addListener(marker, 'click', function() {
                   // Check to see if we already have an InfoWindow
                   if (!infowindow) {
                     infowindow = new google.maps.InfoWindow();
                   }

                   // Setting the content of the InfoWindow
                   infowindow.setContent(teaser);

                   // Tying the InfoWindow to the marker
                   infowindow.open(map, marker);

                  });
         }

})();
