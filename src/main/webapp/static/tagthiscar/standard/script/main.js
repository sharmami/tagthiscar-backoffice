(function() {

    BrowserDetect.init();
    $.ajax({
        type : "GET",
        url : CONTEXT_ROOT+'/analytics',
        dataType : "json",
        data : {
            page_url:window.location.href,
            browser: BrowserDetect.browser,
            version:BrowserDetect.version,
            operating_system : BrowserDetect.OS,
            screen_resolution : screen.width+'X'+screen.height
        }
    });


    $('#subscribeFormId').submit(function() {
            $.ajax({
                type : "GET",
                url : CONTEXT_ROOT+'/subscribe',
                dataType : "json",
                data : {
                    email: $('#subscribeEmailId').val(),
                    time:$.now()
                },
                cache: false
            });

           $('#myModal').modal('show');
           // empty the field.
           $('#subscribeEmailId').val('');
           return false;
         });

    //show/hide destination
    $('#destinationDivId').hide();
    $(document).on('click', '#showDestinationDivLinkId', function (e) {
           $('#destinationDivId').toggle();
           $('#searchWithinDivId').toggle();
    });

})();
