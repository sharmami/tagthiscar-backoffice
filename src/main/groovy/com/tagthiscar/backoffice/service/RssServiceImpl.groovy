package com.tagthiscar.backoffice.service
import com.tagthiscar.backoffice.dao.FeedDao
import com.tagthiscar.backoffice.domain.Feed
import com.tagthiscar.domain.Location
import com.tagthiscar.domain.Site
import com.tagthiscar.service.ExperimentService
import com.tagthiscar.service.SiteService
import groovy.json.JsonException
import groovy.json.JsonSlurper
import org.apache.commons.io.IOUtils
import org.apache.commons.validator.GenericValidator
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service

import java.text.SimpleDateFormat
/**
 * @author asharma
 *
 */

@Service
public class RssServiceImpl implements RssService {

    @Autowired
    FeedDao feedDao;

    @Autowired
    SiteService siteService;


    @Autowired
    ExperimentService experimentService;
    //TODO can we bring the code to this service itself so we dont make an outside call.

    @Override
    String getRawFeed(String url) {
        new URL(url).text;
    }

    @Override
    List<Feed> rssFeed(String fileName, String url) {
        new File(fileName).text = new URL(url).text;// save the feed as an xml.

        def xmlFeed = new XmlParser().parse(url);
        def feedList = []

        (0..< xmlFeed.channel.item.size()).each {

            def item = xmlFeed.channel.item.get(it);

            Feed feedExists =  feedDao.findByGuid(item.guid.text());
            if(feedExists != null){
           }
           else{

               Feed feed = new Feed();
               feed.title = item.title.text();
               feed.link = item.link.text();
               feed.description = item.description.text();
               feed.author = item.author.text();
               feed.pubDate = item.pubDate.text();
               feed.guid = item.guid.text();
               feed.fileName = fileName;
               feed.submittedDate = new Date();
               feed.geoRssPolygon =  item.'georss:polygon'.text();

               feedDao.save(feed);
               feedList << feed
           }
        }
        feedList
    }

    void getWeather(String zip){
        println "WEATHER STARTS";
        def baseUrl = "http://weather.yahooapis.com/forecastrss"

        //def zip = zip;
        def url = baseUrl + "?p=" + zip
        def xml = url.toURL().text

        def rss = new XmlSlurper().parseText(xml)
        println rss.channel.title
        println "Sunrise: ${rss.channel.astronomy.@sunrise}"
        println "Sunset: ${rss.channel.astronomy.@sunset}"
        println "Currently:"
        println "\t" + rss.channel.item.condition.@date
        println "\t" + rss.channel.item.condition.@temp
        println "\t" + rss.channel.item.condition.@text
        println "WEATHER ENDS";
    }

    void crimeIncidentsInDc311Feeds(String fileName){
        def feed = new XmlParser().parse(fileName);
        //println "XML size:"+xml.li.size();
        (0..< feed.entry.size()).each {

            def entry = feed.entry.get(it);
            println(" HERE : id  : "+entry.id.text());
            println(" HERE : updated : "+entry.updated.text());

            println(" HERE : report create date : : "+entry.content.'dcst:ReportedCrime'.'dcst:reportdatetime'.text());

            println(" HERE : address  : "+entry.content.'dcst:ReportedCrime'.'dcst:blocksiteaddress'.text());
            def address =  entry.content.'dcst:ReportedCrime'.'dcst:blocksiteaddress'.text();
            println(" debug: 0.1 ");
            double[] coordinates = getLatLongFromAddress(address);
            println(" debug: 0.2");
            if(/*coordinates[0]!=null && coordinates[1]!=null*/ coordinates.length>1){
                println(" debug: 0.3");
               println(" lat :"+coordinates[0]);
               println(" long :"+coordinates[1]);

           } else{
                println(" debug: 1");
               String [] addy = address.toUpperCase().split("BLOCK OF");
                println(" debug: 2");
                if(addy.length>1){
                    println(" debug: 3");
                    // so there did exist a BLOCK OF occurence
                    def number = addy[0].findAll( /\d+/ )*.toInteger();
                    println(" debug: 4");
                 def  street =  addy[1];
                    println(" debug: 5");
                    println 'parsed address : '+(number[0]+street);
                    double[] coordinatesAgain = getLatLongFromAddress((number[0]+street));
                    println(" debug: 6");
                    if(/*coordinatesAgain[0]!=null && coordinatesAgain[1]!=null*/coordinatesAgain.length>1){
                        println(" debug: 7");
                        println(" lat again :"+coordinatesAgain[0]);
                        println(" long again :"+coordinatesAgain[1]);
                    } else{
                        println 'no result with second try either !! for address: '+address;
                    }

                }   else{
                   // no BLOCK OF in this address. try to find some other way to parse this address.
                    println "can't parse this address: "+addy;
                }




          }



            //City: Washington
            println(" HERE : state : "+entry.content.'dcst:ReportedCrime'.'rdf:Description'.'dc:coverage'.text());
            //println(" HERE : zip : "+entry.content.'dcst:ServiceRequest'.'dcst:zipcode'.text());
            //USA
            println(" HERE : lat : "+entry.content.'dcst:ReportedCrime'.'dcst:blockxcoord'.text());
            println(" HERE : long : "+entry.content.'dcst:ReportedCrime'.'dcst:blockycoord'.text());


            //dcst:method

            //labels
            println(" HERE : labels : "+entry.content.'dcst:ReportedCrime'.'dcst:offense'.text());
            println(" HERE : method : "+entry.content.'dcst:ReportedCrime'.'dcst:method'.text());
            println(" HERE : shift : "+entry.content.'dcst:ReportedCrime'.'dcst:shift'.text());


        }
    }



   // read 311 service requests from http://data.octo.dc.gov/. see  http://data.octo.dc.gov/feeds/src/src_current.xml
    void serviceRequestsDc311Feeds(String url){

        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS");
        String fileName= "servicerequestdc311feeds-"+fmt.format(new Date())+ ".xml";

        String xmlAsString =  getRawFeed(url);
        String fileId = saveToGrid(xmlAsString,fileName,'servicerequestdc311feeds',url);// save the xml to grid so have a copy of it.
        //println " fileId: "+fileId+" for filename: "+fileName;

        // map of label codes for flag types
        def flagTypesMap = [
                "ABAVEHOP":"abandoned_vehicle",
                "ADJUSERV":"adjudication",
                "CFSA":"amber_alert",
                "DEPAHEAL":"caution",
                "DRIVEHSE":"driver_vehicle_service",
                "PARKENFO":"parking_enforcement",
                "PRSVAVOP":"abandoned_vehicle",
                "SIGNS":"signs",
                "STRALLCL":"street_cleaning",
                "STRBRIMA":"street_bridge_maintenance",
                "SWCODEAN":"dead_animal",
                "SWMCLEGR":"graffiti",
                "SWMCOLBU":"bulk_collection",
                "SWMCOLRE":"recycling",
                "SWMCOLSU":"supercans",
                "SWMCOLTR":"trash_collection",
                "SWMSWEDU":"illegal_dumping",
                "SWMSWEEN":"sanitation_enforcement",
                "SWSTALCS":"street_cleaning",
                "TRAFSIGN":"traffic_signal",
                "TRAOP001":"street_repair",
                "URBAFORR":"tree_pruning",
                "SISYINOD":"street_repair",
                "FEMSMOAL":"smoke_alarm",
                "":"caution"
        ]

        def feed = new XmlParser().parse(url);
        //println "XML size:"+xml.li.size();
        (0..< feed.entry.size()).each {

            def flagList = []

            def myData =  new Expando();
            myData.fileName = fileName;

            def entry = feed.entry.get(it);
            //println(" HERE : it  : "+entry.id.text());
            //println(" HERE : address  : "+entry.content.'dcst:ServiceRequest'.'dcst:siteaddress'.text());
            myData.address =  entry.content.'dcst:ServiceRequest'.'dcst:siteaddress'.text();
            myData.country='USA';


            //println(" HERE : lat : "+entry.content.'dcst:ServiceRequest'.'geo:lat'.text());
            //println(" HERE : long : "+entry.content.'dcst:ServiceRequest'.'geo:long'.text());
            double [] coordinates = [new Double(entry.content.'dcst:ServiceRequest'.'geo:long'.text()),new Double(entry.content.'dcst:ServiceRequest'.'geo:lat'.text())]
            myData.coordinates = coordinates;

            //City: Washington
            //println(" HERE : state : "+entry.content.'dcst:ServiceRequest'.'rdf:Description'.'dc:coverage'.text());
            myData.state = entry.content.'dcst:ServiceRequest'.'rdf:Description'.'dc:coverage'.text();

            //println(" HERE : zip : "+entry.content.'dcst:ServiceRequest'.'dcst:zipcode'.text());
            myData.zip = entry.content.'dcst:ServiceRequest'.'dcst:zipcode'.text();
            //USA

            myData.updatedBy = "amit";

            //println(" HERE : submitted by agency : "+entry.content.'dcst:ServiceRequest'.'dcst:agencyabbreviation'.text());
            myData.submittedBy = entry.content.'dcst:ServiceRequest'.'dcst:agencyabbreviation'.text();

            //<dcst:servicerequestid><![CDATA[13-00155351]]></dcst:servicerequestid>
            //println "GUID "+entry.content.'dcst:ServiceRequest'.'dcst:servicerequestid'.text();
            myData.guid = entry.content.'dcst:ServiceRequest'.'dcst:servicerequestid'.text();

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

              //http://stackoverflow.com/questions/4099862/convert-java-date-into-xml-date-format-and-vice-versa
            // http://stackoverflow.com/questions/2375222/java-simpledateformat-for-time-zone-with-a-colon-seperator
            // this is the xml date format : http://en.wikipedia.org/wiki/ISO_8601

            //adddate and service order data seems to be the same always.
            //println(" HERE : adddate : "+entry.content.'dcst:ServiceRequest'.'dcst:adddate'.text());
            //println(" HERE : resolutiondate : "+entry.content.'dcst:ServiceRequest'.'dcst:resolutiondate'.text());

            //println(" HERE : serviceduedate : "+entry.content.'dcst:ServiceRequest'.'dcst:serviceduedate'.text());
            String incidentValidUntilDateString =   entry.content.'dcst:ServiceRequest'.'dcst:serviceduedate'.text();
            if(!GenericValidator.isBlankOrNull(incidentValidUntilDateString)){
                try{
                myData.incidentValidUntilDate = sdf.parse(/*sdf.format(incidentValidUntilDateString)*/incidentValidUntilDateString);
                }catch(Exception e){
                    e.printStackTrace();
                }
            }


            //String updatedDateString = sdf.format(new Date());
            //println " updatedDateString : "+updatedDateString;
            //println(" HERE : lastmodifieddate : "+entry.content.'dcst:ServiceRequest'.'dcst:lastmodifieddate'.text());
            String updatedDateString = entry.content.'dcst:ServiceRequest'.'dcst:lastmodifieddate'.text();
            if(!GenericValidator.isBlankOrNull(updatedDateString)){
                try{
                    myData.updatedDate = sdf.parse(/*sdf.format(updatedDateString)*/updatedDateString);
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
            //myData.updatedDate = sdf.parse(updatedDateString);

            //println(" HERE : serviceorderdate : "+entry.content.'dcst:ServiceRequest'.'dcst:serviceorderdate'.text());
            //String submittedDateString = sdf.format(new Date());
            //println " submittedDateString : "+submittedDateString;
            String submittedDateString = entry.content.'dcst:ServiceRequest'.'dcst:serviceorderdate'.text();
            if(!GenericValidator.isBlankOrNull(submittedDateString)){
                try{
                    myData.submittedDate = sdf.parse(/*sdf.format(submittedDateString)*/submittedDateString);
                    myData.incidentStartDate = sdf.parse(/*sdf.format(submittedDateString)*/submittedDateString);
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
            //println " mydata submittedDate : "+myData.submittedDate;

            //println(" HERE : geoRssPolygon : "+entry.content.'dcst:ServiceRequest'.'georss:point'.text());

            //println(" HERE : status : "+entry.content.'dcst:ServiceRequest'.'dcst:serviceorderstatus'.text());
            myData.status =  entry.content.'dcst:ServiceRequest'.'dcst:serviceorderstatus'.text();

            //println(" HERE : resolution : "+entry.content.'dcst:ServiceRequest'.'dcst:resolution'.text());
            myData.resolution = entry.content.'dcst:ServiceRequest'.'dcst:resolution'.text();

            //println(" HERE : description : "+entry.content.'dcst:ServiceRequest'.'dcst:servicecodedescription'.text());
            //println " SERVICENOTES : "+entry.content.'dcst:ServiceRequest'.'dcst:servicenotes'.text();
            myData.description = entry.content.'dcst:ServiceRequest'.'dcst:servicecodedescription'.text();

            String teaser = entry.content.'dcst:ServiceRequest'.'dcst:servicecodedescription'.text();
            if(myData.incidentValidUntilDate!=null){
                teaser = teaser+" due "+myData.incidentValidUntilDate+" ";
            }
            //println "teaser : "+teaser;
            myData.teaser = teaser;



            //println(" HERE : label types : "+entry.content.'dcst:ServiceRequest'.'dcst:servicetypecodedescription'.text() + " code: "+entry.content.'dcst:ServiceRequest'.'dcst:servicetypecode'.text());//dcst:servicetypecode
            String code = entry.content.'dcst:ServiceRequest'.'dcst:servicetypecode'.text();
            if(flagTypesMap.containsKey(code)){
                flagList <<   flagTypesMap[code]
            }
            if(flagList.isEmpty()){
                flagList << "caution"
            }
            //String[] flagTypes = ["caution"];
            myData.flagTypes = (flagList as String[]);
            //println "flagTypes : "+myData.flagTypes;

            saveOrUpdateSite(myData);// save everything.
        }

    }

    @Async
    public void processFeed(String key, String value) {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS");
        String fileName= key+"-"+fmt.format(new Date())+ ".xml";

        try{
            List<Feed> feeds = rssFeed(fileName,value);
            if(feeds!=null && feeds.size()>0){
                System.out.println(" RssServiceImpl.groovy : "+feeds.size()+" feeds added for "+key+" from URL: "+value);
                String text = getRawFeed(value);
                //System.out.println(" TEXT beign saveed: "+text);
                String fileId = experimentService.save(IOUtils.toInputStream(text),fileName, key, value);
                //System.out.println("FeedController : fileId: generated: "+fileId+" for fileName: "+fileName);
            }
        }
        catch(Exception ex){
            System.out.println(" RssServiceImpl.groovy: Key: "+key+" Value:"+value+" filename:"+fileName+" failed!!");
            ex.printStackTrace();
        }
    }


    //try slurper
    public void tryJsonSlurping(){

        def url = 'http://api.icndb.com/jokes/random?limitTo=[nerdy]'

        try {
            def json = new JsonSlurper().parseText(url.toURL().text)
            def joke = json?.value?.joke
            assert joke
            println joke
        } catch (JsonException e) {
            e.printStackTrace()
        }


    }

    public double[] getLatLongFromAddress(address){
        def coordinates = [];
         //def url = 'http://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&sensor=false'

          //replaceAll('[^a-zA-Z0-9]+','_')
        def url = 'http://maps.googleapis.com/maps/api/geocode/json?address='+java.net.URLEncoder.encode(address,"UTF-8")+'&sensor=false'
        try{
          def json = new JsonSlurper().parseText(url.toURL().text)
            if(json.results.geometry.location.lat[0]!=null){

                println json.results.formatted_address[0]
                println json.results.geometry.location.lat[0]
                println json.results.geometry.location.lng[0]

                coordinates[0] = json.results.geometry.location.lat[0]
                coordinates[1] = json.results.geometry.location.lng[0]

            }  else{
               /* println(" lets play a little");
                String [] addy = address.toUpperCase().split("BLOCK OF");
                def number = addy[0].findAll( /\d+/ )*.toInteger();
                def  street =  addy[1];*/

                /*
                println " numbers : "+addy[0];
                println addy[0].findAll( /\d+/ )*.toInteger();
                println "addres "+ addy[1];
                */


            }

         }
        catch (JsonException e) {
             e.printStackTrace()
         }
        catch(Exception ex){
            ex.printStackTrace();
        }

        return coordinates;
    }

    @Override
    void readParkingData(String fileName) {
        def parkingDestinations = new XmlParser().parse(fileName);
        //println "XML size:"+xml.li.size();
        (0..< parkingDestinations.ParkingDestination.size()).each {

            def parkingDestination =    parkingDestinations.ParkingDestination.get(it);

            def paymentTypesList = [];
            def paymentMethodsAccepted =   parkingDestination.PaymentMethodsAccepted;
            (0..< paymentMethodsAccepted.PaymentMethod.size()).each {
                def  paymentMethod =  paymentMethodsAccepted.PaymentMethod.get(it);
               paymentTypesList.add(paymentMethod.text());
            }

            Location location = new Location();
            double [] coordinates = [new Double(parkingDestination.Longitude.text()),new Double(parkingDestination.Latitude.text())]
            location.setCoordinates(coordinates);
            location.setAddress(parkingDestination.DestinationName.text());
            location.setCountry("USA");
            location.setSubmittedBy("streetline");
            location.setUpdatedBy("amit");
            location.setUpdatedDate(new Date());
            location.setSubmittedDate(new Date());

            Site site = new Site();
            String[] flagType = ["parking-meter-export"];
            site.setFlagTypes(flagType);
            if(paymentTypesList.size()>0){
                site.setTeaser("Parking at "+(parkingDestination.DestinationName.text())+" Payment Methods Accepted:"+(paymentTypesList.join(",")));
                site.setDescription("Parking at "+(parkingDestination.DestinationName.text())+" Payment Methods Accepted:"+(paymentTypesList.join(",")));
            }else{
                site.setTeaser("Parking at "+(parkingDestination.DestinationName.text()));
                site.setDescription("Parking at "+(parkingDestination.DestinationName.text()));
            }

            site.setSubmittedDate(new Date());
            site.setSubmittedBy("amit");
            site.setGuid(parkingDestination.DestinationID.text());
            site.setUpdatedBy("amit");
            site.setUpdatedDate(new Date());
            site.setLocation(location);
            siteService.saveOrUpdateParkingInfo(site);

        }
    }

    @Override
    @Async
    void readMarylandStateIncidents(String fileName) {
        def incidentsAndClosures = new XmlParser().parse(fileName);

        //println "XML size:"+(incidentsAndClosures.incidents.incident.size());

        //TODO other than incidents it also has laneclosures but they dont have lat/long.
        (0..< incidentsAndClosures.incidents.incident.size()).each {
            def incident =    incidentsAndClosures.incidents.incident.get(it);
            //println(" Incident : "+incident.ID.text());

            Location location = new Location();
            double [] coordinates = [new Double(incident.longitude.text()),new Double(incident.latitude.text())]
            location.setCoordinates(coordinates);
            location.setAddress(incident.location.text());
            location.setAddress1(incident.county.text());
            location.setState("MD")
            location.setCountry("USA");
            location.setSubmittedBy("Maryland DOT");
            location.setUpdatedBy("amit");
            location.setUpdatedDate(new Date());
            location.setSubmittedDate(new Date());

            Site site = new Site();
            String[] flagType = ["caution"];//TODO MD knows the incident details and we should have flags for it.
            site.setFlagTypes(flagType);
            site.setTeaser(incident.incidentDetails.text()+": "+incident.location.text()+" Direction:"+incident.direction.text()+" at : "+incident.timeReceived.text());
            site.setDescription("Incident Details : "+incident.incidentDetails.text()+": Location: "+incident.location.text()+" Direction:"+incident.direction.text()+" Time received: "+incident.timeReceived.text()+" County: "+incident.county.text()+" opcenter: "+incident.opcenter.text()+" "+incident.vehiclesInvolved.text());
            site.setSubmittedDate(new Date());
            site.setSubmittedBy("amit");
            site.setGuid(incident.ID.text());
            site.setUpdatedBy("amit");
            site.setUpdatedDate(new Date());
            site.setLocation(location);
            siteService.saveOrUpdateParkingInfo(site);


        }

    }

    @Override
    @Async
    String get511feedsFor7States(String url, String username, String password,String state) {

        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS");
        String fileName= state+"-511feedsfor7states-"+fmt.format(new Date())+ ".xml";

        def xmlAsString = basicAuthenticationResults(url, username, password);

        String fileId = saveToGrid(xmlAsString,fileName,state+'-511feedsfor7states',url);// save the xml to grid so have a copy of it.
        println " fileId: "+fileId+" for filename: "+fileName;

        def xml = new XmlParser().parseText(xmlAsString);

        (0..< xml.'feu:full-event-update'.size()).each {
            def event = xml.'feu:full-event-update'.get(it);

            def myData = myData(event);
            myData.state = state;
            myData.country = "USA";
            myData.updatedBy = "amit";
            myData.fileName = fileName;
            saveOrUpdateSite(myData);// save everything.

        }

        return "OK";
    }

    /**
     *  BASIC authentication
     *
     * @param url
     * @param username
     * @param password
     * @return
     */
    private def basicAuthenticationResults(String url, String username, String password) {
        def authString = "${username}:${password}".getBytes().encodeBase64().toString()
        def conn = url.toURL().openConnection()
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Authorization", "Basic ${authString}")
        //conn.setRequestProperty("Content-Type","text/xml")
        conn.connect();
        if(conn.responseCode == 200){
            return conn.inputStream.text;
        }else{
            return "ERROR: Webservices : $url failed with an error : ${conn.responseCode}: ${conn.responseMessage} ";
        }
        return "OK";
    }

    /**
     *   convert lat/long from microdegrees to Degrees
     * @param microDegrees
     * @return
     */
    public static double microDegreesToDegrees(int microDegrees) {
        return microDegrees / 1E6;
    }

    /**
     * key and value dont really matter here but i usually add key as a unique name for where the feed came from and value is the url of the feed.
     *
     * @param xmlContent
     * @param fileName
     * @param key
     * @param value
     * @return
     */
    public String saveToGrid(String xmlContent,String fileName,String key,String value){
        String fileId = experimentService.save(IOUtils.toInputStream(xmlContent),fileName, key, value);
        return fileId;

    }

    private def myData(def event){

        /*(0..< xml.'feu:full-event-update'.size()).each {*/

            def myData =  new Expando(); //Thanks to Groovy expando. this will contain all my data.

            //def event = xml.'feu:full-event-update'.get(it);
            //println " ========================================================================================"

            //println " Submitted By: "+event.'message-header'.sender.'organization-id'.text();
            myData.submittedBy = event.'message-header'.sender.'organization-id'.text();

            //println " GUID: "+event.'event-reference'.'event-id'.text()+"-"+event.'event-reference'.update.text();
            myData.guid =  event.'event-reference'.'event-id'.text()+"-"+event.'event-reference'.update.text();


            //SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS");
            //myData.updatedDate(new Date());


            //this will give me the flag.
            /*(0..< event.headline.headline.size()).each {
                def  flagTypes =  event.headline.headline.get(it);
                //println 'here flagTypes : '+flagTypes.children().toString()+' '+flagTypes.children().text();
                flagTypes.children().each(){
                    //String[] flagType = [it.name()];
                    //site.setFlagTypes(flagType);
                    println " Reason: "+it.name()+" teaser content 1: "+it.text();
                }
            }*/

            // details
            // println " how many details : "+event.details.detail.size()
            (0..< event.details.detail.size()).each {
                def  detail =  event.details.detail.get(it);

                //descriptions  - they are 'phrase', 'cause' and 'additional-text'
                def flagList = []
                def teaser = "";
                def siteDescription = "";

                // teaser and flagtypes
                (0..< detail.descriptions.description.size()).each {
                    def  description =  detail.descriptions.description.get(it);
                    description.children().each(){

                        def firstLevelDescription = it;
                        //println " description  :"+firstLevelDescription.name()
                        firstLevelDescription.children().each(){
                            if("phrase".equalsIgnoreCase(firstLevelDescription.name())){
                                flagList << it.name()
                            }
                            if("cause".equalsIgnoreCase(firstLevelDescription.name())){
                                flagList << it.name()
                            }
                            if(!GenericValidator.isBlankOrNull(it.text())){
                                if("additional-text".equalsIgnoreCase(firstLevelDescription.name())){
                                    siteDescription +=  it.text();
                                    siteDescription += ",";
                                }
                                else{
                                    teaser +=  it.text();
                                    teaser += ",";
                                }
                            }
                            /*if("additional-text".equalsIgnoreCase(firstLevelDescription.name())){
                                if(!GenericValidator.isBlankOrNull(it.text())){
                                    siteDescription +=  it.text();
                                    siteDescription += ",";
                                }
                            } else{
                                if(!GenericValidator.isBlankOrNull(it.text())){
                                    teaser +=  it.text();
                                    teaser += ",";
                                }
                            }*/

                            //println " level 2 description  :"+it.name()+" value: "+it.text()
                        }
                    }
                }
                myData.teaser = teaser;
                myData.flagTypes =   (flagList as String[])
                myData.description = siteDescription;

                // locations
                (0..< detail.locations.location.size()).each {
                    def  location =  detail.locations.location.get(it);
                    //println " Area : "+location.'area-location'.'area-id'.text();// if they have area-location defined.

                    //println " Location on link : who submitted this : "+location.'location-on-link'.'link-ownership'.text();// if they have location-on-link defined.

                    // street name and actual location combined to give address.
                   // println " Location on link : cross-street-name: "+location.'location-on-link'.'primary-location'.'cross-street-name'.text();//OPTIONAL
                    String streetName =   location.'location-on-link'.'primary-location'.'cross-street-name'.text();
                    //println " Location on link : where was the incident:  "+location.'location-on-link'.'route-designator'.text();
                    String actualLocation =  location.'location-on-link'.'route-designator'.text();
                    String address = actualLocation;
                    if(!GenericValidator.isBlankOrNull(streetName)){
                        address = streetName+", "+actualLocation;
                    }
                    myData.address = address;

                    //lat ,long
                    //println " Location on link : latitude: "+location.'location-on-link'.'primary-location'.'geo-location'.'latitude'.text();
                    int latitude = (location.'location-on-link'.'primary-location'.'geo-location'.'latitude'.text()).toInteger();
                    //println "latitude : "+microDegreesToDegrees(latitude);
                    //println " Location on link : longitude: "+location.'location-on-link'.'primary-location'.'geo-location'.'longitude'.text();
                    int longitude =  (location.'location-on-link'.'primary-location'.'geo-location'.'longitude'.text()).toInteger();
                   // println "longitude: "+microDegreesToDegrees(longitude);
                    double [] coordinates = [new Double(microDegreesToDegrees(longitude)),new Double(microDegreesToDegrees(latitude))];
                    myData.coordinates = coordinates;

                    //println " Location on link : linear reference: "+location.'location-on-link'.'primary-location'.'linear-reference'.text();
                    //println " Location on link : link direction: "+location.'location-on-link'.'link-direction'.text();
                    //println " Location on link : linear reference: "+location.'location-on-link'.'linear-reference-version'.text();

                    myData.travelDirection = location.'location-on-link'.'link-direction'.text();
                    myData.travelAlignment = location.'location-on-link'.'link-alignment'.text();


                }

                // times
                // time when incident was confirmed
                //println " times update-time: "+detail.times.'update-time'.date.text()+" time: "+detail.times.'update-time'.time.text()+" offset :"+detail.times.'update-time'.'utc-offset'.text();
                //def newdate = new Date().parse("yyyyMMdd Hms Z", '20130423 124141 -0500');
                def incidentYyyyMMddValue = detail.times.'update-time'.date.text();
                def incidentHmsValue = detail.times.'update-time'.time.text();
                def incidentZValue = detail.times.'update-time'.'utc-offset'.text();
                def submittedDate = new Date().parse("yyyyMMdd Hms Z", "${incidentYyyyMMddValue} ${incidentHmsValue} ${incidentZValue}");
                //println "submittedDate  : "+submittedDate;
                myData.submittedDate = submittedDate;

                // how long is the incident valid for
                if(!GenericValidator.isBlankOrNull(detail.times.'valid-period'.'end-time'.date.text())){
                    //println " times valid-period: "+detail.times.'valid-period'.'end-time'.date.text()+" time: "+detail.times.'valid-period'.'end-time'.time.text()+" offset :"+detail.times.'valid-period'.'end-time'.'utc-offset'.text();
                    def incidentValidUntilYyyyMMddValue = detail.times.'valid-period'.'end-time'.date.text();
                    def incidentValidUntilHmsValue = detail.times.'valid-period'.'end-time'.time.text();
                    def incidentValidUntilZValue = detail.times.'valid-period'.'end-time'.'utc-offset'.text();
                    def incidentValidUntilDate = new Date().parse("yyyyMMdd Hms Z", "${incidentValidUntilYyyyMMddValue} ${incidentValidUntilHmsValue} ${incidentValidUntilZValue}");
                    myData. incidentValidUntilDate =  incidentValidUntilDate;
                    //println "incidentValidUntilDate  : "+incidentValidUntilDate;
                }



                //optional. when is the incident supposed to start... if it is empty, it means immediately.
                if(!GenericValidator.isBlankOrNull(detail.times.'valid-period'.'start-time'.date.text())){
                    //println " times start-time: "+detail.times.'start-time'.date.text()+" time: "+detail.times.'start-time'.time.text()+" offset :"+detail.times.'start-time'.'utc-offset'.text();
                    def incidentStartYyyyMMddValue = detail.times.'start-time'.date.text();
                    def incidentStartHmsValue = detail.times.'start-time'.time.text();
                    def incidentStartZValue = detail.times.'start-time'.'utc-offset'.text();
                    def incidentStartDate = new Date().parse("yyyyMMdd Hms Z", "${incidentStartYyyyMMddValue} ${incidentStartHmsValue} ${incidentStartZValue}");
                    myData. incidentStartDate =  incidentStartDate;
                    //println "incidentStartDate  : "+incidentStartDate;
                }

                //TODO make use of this.
                // one or more time periods during which an event may occur
                /*(0..< detail.times.'recurrent-times'.'recurrent-time'.size()).each {
                    def recurrentTime = detail.times.'recurrent-times'.'recurrent-time'.get(it);

                    (0..< recurrentTime.'recurrent-period'.'days-of-the-week'.'day-of-the-week'.size()).each {
                        def dayOfTheWeek = recurrentTime.'recurrent-period'.'days-of-the-week'.'day-of-the-week'.get(it);
                        println dayOfTheWeek.text();
                    }

                    (0..<  recurrentTime.'schedule-times'.'schedule-time'.size()).each {
                        def scheduleTime =  recurrentTime.'schedule-times'.'schedule-time'.get(it);
                        println scheduleTime.text();
                    }

                    println  recurrentTime.'schedule-times'.'utc-offset'.text();

                }*/

            }
     /*   }*/

        return myData;
    }

    private def  saveOrUpdateSite(def myData){

       /* println " ===============MY DATA STARTS============================ ";
        println " coordinate : "+myData.coordinates;
        println " address : "+myData.address;
        println " state : "+myData.state;
        println " country : "+myData.country;
        println " submittedBy : "+myData.submittedBy;
        println " updatedBy : "+myData.updatedBy;
        println " updatedDate : "+myData.updatedDate;
        println " submittedDate : "+myData.submittedDate;
        println " incidentStartDate : "+myData.incidentStartDate;
        println " incidentValidUntilDate : "+myData.incidentValidUntilDate;
        println "fileName: "+myData.fileName
        println " flagTypes: "+myData.flagTypes;
        println " teaser: "+myData.teaser;
        println " guid: "+myData.guid;
        println " description: "+myData.description;
        println " travelDirection: "+myData.travelDirection;
        println " travelAlignment: "+myData.travelAlignment;
       println "teaser : "+myData.teaser+" "+myData.address+" "+myData.submittedDate;
        //these came later
        println " zip: "+myData.zip;
        println " status: "+myData.status;
        println " resolution: "+myData.resolution;
        println " ===============MY DATA ENDS============================ ";*/


        Location location = new Location();
        location.setCoordinates(myData.coordinates);
        location.setAddress(myData.address);
        location.setAddress1(myData.address1);//this came later2
        location.setCity(myData.city);//this came later2.
        location.setState(myData.state) ;
        location.setZip(myData.zip);// this came later
        location.setCountry(myData.country);
        location.setSubmittedBy(myData.submittedBy);
        location.setUpdatedBy(myData.updatedBy);
        location.setUpdatedDate(new Date());
        location.setSubmittedDate(myData.submittedDate);

        Site site = new Site();
        site.setFileName(myData.fileName);
        site.setFlagTypes(myData.flagTypes);
        site.setTeaser(myData.teaser+" "+myData.address+" "+myData.submittedDate);
        site.setStatus(myData.status);//this came later
        site.setResolution(myData.resolution);//this came later.
        site.setIncidentStartDate(myData.incidentStartDate);//this came later
        site.setIncidentValidUntilDate(myData.incidentValidUntilDate); //this came later
        site.setTravelDirection(myData.travelDirection);//this came later
        site.setTravelAlignment(myData.travelAlignment);//this came later
        site.setDescription(myData.description);
        site.setSubmittedDate(new Date());
        site.setSubmittedBy(myData.submittedBy);
        site.setGuid(myData.guid);
        site.setUpdatedBy(myData.updatedBy);
        site.setUpdatedDate(new Date());
        site.setLocation(location);
        siteService.saveOrUpdateParkingInfo(site);


    }

    @Override
    @Async
    void californiaHighwayPatrolFeeds(String url) {

        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS");
        String fileName= "californiaHighwayPatrol-"+fmt.format(new Date())+ ".xml";
        String xmlAsString =   getRawFeed(url);

        String fileId = saveToGrid(xmlAsString,fileName,'californiaHighwayPatrol',url);// save the xml to grid so have a copy of it.
        //println " fileId: "+fileId+" for filename: "+fileName+" url: "+url;

        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd yyyy hh:mma");// for the dates coming in SF area.
        sdf.setTimeZone (TimeZone.getTimeZone ("America/Los_Angeles"));
        //println sdf.parse("Aug 17 2013  7:40AM")

        def State = new XmlParser().parseText(xmlAsString);
        //def State = new XmlParser().parse(url);
        (0..< State.Center.size()).each {
            def Center =    State.Center.get(it);
            (0..< Center.Dispatch.size()).each {
                def Dispatch =  Center.Dispatch.get(it);
                (0..< Dispatch.Log.size()).each {
                    def Log =  Dispatch.Log.get(it);
                    def myData = prepareCaliforniaHighwayPatrolData(Log, sdf);
                    myData.submittedBy=Center.'@ID'+"-"+Dispatch.'@ID';
                    myData.state = 'CA';
                    myData.country = "USA";
                    myData.updatedBy = "amit";
                    myData.fileName = fileName;
                    saveOrUpdateSite(myData);

                }


            }

        }

    }


    private def prepareCaliforniaHighwayPatrolData(def Log, def sdf){

        def flagList = []
        def descriptionList = []

        // map of label codes for flag types
        def flagTypesMap = [
                "ANIMAL-Live or Dead Animal":"dead_animal",
                "CFIRE-Car Fire":"caution",
                "CLOSURE of a Road":"closedroad",
                "CZP-Assist with Construction":"construction",
                "DOT-Request CalTrans Notify":"driver_vehicle_service",
                "FIRE-Report of Fire":"smoke_alarm",
                "MZP-Assist CT with Maintenance":"street_bridge_maintenance",
                "TADV-Traffic Advisory":"caution",
                "WIND Advisory":"wind",
                "1013-Road/Weather Conditions":"weather-condition",
                "1125-Traffic Hazard":"caution",
                "1125A-Animal Hazard":"caution",
                "1166-Defective Traffic Signals":"traffic_signal",
                "1179-Trfc Collision-1141 Enrt":"caraccident",
                "1180-Trfc Collision-Major Inj":"caraccident",
                "1181-Trfc Collision-Minor Inj":"caraccident",
                "1182-Trfc Collision-No Inj":"caraccident",
                "1183-Trfc Collision-Unkn Inj":"caraccident",
                "20001-Hit and Run w/Injuries":"caution",
                "20002-Hit and Run No Injuries":"caution",
                "23114-Object Flying From Veh":"caution",
                "":"caution"
        ]


        def flagTypeTeaserMap = [
                "ANIMAL-Live or Dead Animal":"Live or Dead Animal",
                "CFIRE-Car Fire":"Car Fire",
                "CLOSURE of a Road":"Closed Road",
                "CZP-Assist with Construction":"Construction",
                "DOT-Request CalTrans Notify":"DOT Request",
                "FIRE-Report of Fire":"Report of Fire",
                "MZP-Assist CT with Maintenance":"Maintenance",
                "TADV-Traffic Advisory":"Traffic Advisory",
                "WIND Advisory":"Wind Advisory",
                "1013-Road/Weather Conditions":"Road or Weather Condition",
                "1125-Traffic Hazard":"Traffic Hazard",
                "1125A-Animal Hazard":"Animal Hazard",
                "1166-Defective Traffic Signals":"Defective Traffic Signal",
                "1179-Trfc Collision-1141 Enrt":"Traffic Collision Ambulance Enroute",
                "1180-Trfc Collision-Major Inj":"Traffic Collision Major Injuries",
                "1181-Trfc Collision-Minor Inj":"Traffic Collision Minor Injuries",
                "1182-Trfc Collision-No Inj":"Traffic Collision No Injuries",
                "1183-Trfc Collision-Unkn Inj":"Traffic Collision Unknown Injuries",
                "20001-Hit and Run w/Injuries":"Hit and Run with Injuries",
                "20002-Hit and Run No Injuries":"Hit and Run No Injuries",
                "23114-Object Flying From Veh":"Object flying from vehicle",
                "":"caution"
        ]

        def myData =  new Expando();

        myData.guid = Log.'@ID';
        //println " ID: "+Log.'@ID';

        //println   'LogTime: '+Log.LogTime.text();
        //println " parsed time: "+sdf.parse(Log.LogTime.text().replaceAll("\"",""));
        myData.submittedDate= sdf.parse(Log.LogTime.text().replaceAll("\"",""));

        //println   'LogType: '+Log.LogType.text();
        String code =   Log.LogType.text().replaceAll("\"","");
        if(flagTypesMap.containsKey(code)){
            flagList <<   flagTypesMap[code]
            myData.teaser = flagTypeTeaserMap[code]
        }

        if(flagList.isEmpty()){
            flagList << "caution"
            myData.teaser =code;
        }
        //String[] flagTypes = ["caution"];
        myData.flagTypes = (flagList as String[]);

        //println   'Location: '+Log.Location.text();
        //println   'LocationDesc: '+Log.LocationDesc.text();
        myData.address=Log.Location.text().replaceAll("\"","");
        myData.address1=Log.LocationDesc.text().replaceAll("\"","");

        //println   'Area: '+Log.Area.text();
        myData.city=Log.Area.text().replaceAll("\"","");

        //println   'LATLON: '+Log.LATLON.text();
        def latLon = Log.LATLON.text().replaceAll("\"","");
        //println "after removing quotes : "+latLon;
        def (latitude, longitude) = latLon.split(":")
        //println " latty: "+microDegreesToDegrees(latitude.toInteger());
        //println " longy: that needs negativ in front: "+("-"+microDegreesToDegrees(longitude.toInteger()));
        double [] coordinates = [new Double("-"+microDegreesToDegrees(longitude.toInteger())),new Double(microDegreesToDegrees(latitude.toInteger()))];
        myData.coordinates = coordinates;

        (0..< Log.LogDetails.details.size()).each {
            def details = Log.LogDetails.details.get(it);
            //println "--- detailTime : "+details.DetailTime.text();
            if(!GenericValidator.isBlankOrNull(details.DetailTime.text().replaceAll("\"",""))){
                //println " parsed time: "+sdf.parse(details.DetailTime.text().replaceAll("\"",""));
                //println "--- IncidentDetail : "+details.IncidentDetail.text();
                descriptionList << "Incident Detail: "+sdf.parse(details.DetailTime.text().replaceAll("\"",""))+":"+details.IncidentDetail.text().replaceAll("\"","");
            }
        }

        (0..< Log.LogDetails.units.size()).each {
            def units = Log.LogDetails.units.get(it);
            //println "--- unitTime : "+units.UnitTime.text();
            if(!GenericValidator.isBlankOrNull(units.UnitTime.text().replaceAll("\"",""))){
               // println " parsed unit time: "+sdf.parse(units.UnitTime.text().replaceAll("\"",""));
               // println "--- UnitDetail : "+units.UnitDetail.text();
                descriptionList << "Unit Detail: "+sdf.parse(units.UnitTime.text().replaceAll("\"",""))+":"+units.UnitDetail.text().replaceAll("\"","");
            }
        }

        myData.description= descriptionList.join("\n");
        return myData;

    }


}

