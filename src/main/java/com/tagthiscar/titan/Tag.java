package com.tagthiscar.titan;

import java.util.Date;

/**
 * @author amit
 */
public class Tag {

    private String tagId;// any id.

    private String hashTag; // #cedarhurstaccident

    private String teaser; // 'Accident at Cedarhurst'

    private String description;// long description

    private String createdBy;// who created it

    private Date createDate;// when was it created

    private String updatedBy;// last updated by

    private Date updateDate;// When was it last updated.

    private String direction;// 'EAST' 'WEST' 'NORTH', 'SOUTH'

    private String impact;// 'SEVERE', 'MAJOR', 'MEDIUM', 'MINOR'

    private Integer numberOfLanesImpacted;// how many lanes.

    private String lanesImpacted;//All/ Right/ Left

    private String sidesImpacted;// Same/ Both sides.

    private String status;// 'OPEN', 'CLOSED'

    private String resolution;// 'UNKNOWN','FIXED'

    private Date incidentStartDate;// when is it supposed to start

    private Date incidentEndDate;// when it is supposed to end

    private String guid;//uniqueid that feeds identify.

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    public String getHashTag() {
        return hashTag;
    }

    public void setHashTag(String hashTag) {
        this.hashTag = hashTag;
    }

    public String getTeaser() {
        return teaser;
    }

    public void setTeaser(String teaser) {
        this.teaser = teaser;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getImpact() {
        return impact;
    }

    public void setImpact(String impact) {
        this.impact = impact;
    }

    public Integer getNumberOfLanesImpacted() {
        return numberOfLanesImpacted;
    }

    public void setNumberOfLanesImpacted(Integer numberOfLanesImpacted) {
        this.numberOfLanesImpacted = numberOfLanesImpacted;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public Date getIncidentStartDate() {
        return incidentStartDate;
    }

    public void setIncidentStartDate(Date incidentStartDate) {
        this.incidentStartDate = incidentStartDate;
    }

    public Date getIncidentEndDate() {
        return incidentEndDate;
    }

    public void setIncidentEndDate(Date incidentEndDate) {
        this.incidentEndDate = incidentEndDate;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getLanesImpacted() {
        return lanesImpacted;
    }

    public void setLanesImpacted(String lanesImpacted) {
        this.lanesImpacted = lanesImpacted;
    }

    public String getSidesImpacted() {
        return sidesImpacted;
    }

    public void setSidesImpacted(String sidesImpacted) {
        this.sidesImpacted = sidesImpacted;
    }
}
