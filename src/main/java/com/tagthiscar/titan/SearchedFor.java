package com.tagthiscar.titan;

import java.util.Date;

/**
 * This will capture edge for every time user searches for something.
 * @author amit
 */
public class SearchedFor {

    private Date searchDate;//when did user search.

    private String sessionId;// capture every specific visit.

    public Date getSearchDate() {
        return searchDate;
    }

    public void setSearchDate(Date searchDate) {
        this.searchDate = searchDate;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}
