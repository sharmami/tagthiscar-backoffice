package com.tagthiscar.titan;

import java.util.Date;

/**
 * This will capture edge for every search that leads to a location.
 * It will help me find who else is searching for this location and when.
 * @author amit
 */
public class SearchFound {

    private Date foundDate;//when was location found?

    private String sessionId;// capture every specific visit.

    public Date getFoundDate() {
        return foundDate;
    }

    public void setFoundDate(Date foundDate) {
        this.foundDate = foundDate;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}
