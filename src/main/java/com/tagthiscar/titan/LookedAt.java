package com.tagthiscar.titan;

import java.util.Date;

/**
 * This will capture Edges that indicate User looking at tags.
 * @author amit
 */
public class LookedAt {

    private Date when;//when did user look at this tag.

    private String sessionId;// capture every specific visit.

    public Date getWhen() {
        return when;
    }

    public void setWhen(Date when) {
        this.when = when;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}
