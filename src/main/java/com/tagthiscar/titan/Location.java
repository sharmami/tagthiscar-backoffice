package com.tagthiscar.titan;


import java.util.Date;
import com.thinkaurelius.titan.core.attribute.Geoshape;

/**
 * @author asharma
 */
public class Location {

    private String locationId;

    private String createdBy;

    private Date createDate;

    private String address;

    private String address1;

    private String city;

    private String state;

    private String country;

    private String zip;

    private Geoshape place;

    private String roadType;//highway, cul-de-sac, avenue, blvd etc. see http://en.wikipedia.org/wiki/Types_of_road

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public Geoshape getPlace() {
        return place;
    }

    public void setPlace(Geoshape place) {
        this.place = place;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getRoadType() {
        return roadType;
    }

    public void setRoadType(String roadType) {
        this.roadType = roadType;
    }
}