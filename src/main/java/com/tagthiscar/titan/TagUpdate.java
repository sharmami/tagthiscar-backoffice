package com.tagthiscar.titan;

import java.util.Date;

/**
 * @author amit
 */
public class TagUpdate {

    private String tagUpdateId; // unique id.

    private String update;// update

    private String updatedBy;// last updated by

    private Date updateDate;// When was it last updated.

    private String direction;// 'EAST' 'WEST' 'NORTH', 'SOUTH'

    private String impact;// 'SEVERE', 'MAJOR', 'MEDIUM', 'MINOR'

    private Integer numberOfLanesImpacted;// how many lanes.

    private String status;// 'OPEN', 'CLOSED'

    private String resolution;// 'UNKNOWN','FIXED'

    public String getTagUpdateId() {
        return tagUpdateId;
    }

    public void setTagUpdateId(String tagUpdateId) {
        this.tagUpdateId = tagUpdateId;
    }

    public String getUpdate() {
        return update;
    }

    public void setUpdate(String update) {
        this.update = update;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getImpact() {
        return impact;
    }

    public void setImpact(String impact) {
        this.impact = impact;
    }

    public Integer getNumberOfLanesImpacted() {
        return numberOfLanesImpacted;
    }

    public void setNumberOfLanesImpacted(Integer numberOfLanesImpacted) {
        this.numberOfLanesImpacted = numberOfLanesImpacted;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

}
