package com.tagthiscar.titan;

import java.util.Date;

/**
 * @author amit
 */
public class User {

    private String userId;// unique Id

    private String username;//'amit'

    private String ipAddress;// if user isn't there, IP is.

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }
}
