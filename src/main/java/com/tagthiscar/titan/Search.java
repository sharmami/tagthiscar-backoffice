package com.tagthiscar.titan;

import org.springframework.data.mongodb.core.geo.Metric;
import org.springframework.data.mongodb.core.geo.Metrics;

/**
 * This is almost the Search in domain.
 * The only difference being it only captures what the user searched for
 * @author asharma
 */
public class Search {

    private String searchTerm;// source term user entered.

    private String destinationTerm;//destination that user entered.

    private String csvFlagTypes;//accident or caution or construction

    private double miles=1;// 1 of 1 mile.

    private Metric metric = Metrics.KILOMETERS;// mile of 1 mile.

    private Boolean avoidHighways = false;

    private Boolean avoidTolls = false;

    private String travelMode = "DRIVING";

    private Integer noOfDays = -1; // find tags within 1 day from today.

    private Integer limit = 25;// how many results to return.

    private Boolean parkingEnabled = false;// does the user want to see parking tags?

    private Boolean skipDate = false;// dont worry about the date. search as far back as you want.

    private String sortBy = "submittedDate";

    private Boolean skipLimit = false;// search as many results as you want.

    public String getSearchTerm() {
        return searchTerm;
    }

    public void setSearchTerm(String searchTerm) {
        this.searchTerm = searchTerm;
    }

    public String getCsvFlagTypes() {
        return csvFlagTypes;
    }

    public void setCsvFlagTypes(String csvFlagTypes) {
        this.csvFlagTypes = csvFlagTypes;
    }

    public Metric getMetric() {
        return metric;
    }

    public void setMetric(Metric metric) {
        this.metric = metric;
    }

    public String getDestinationTerm() {
        return destinationTerm;
    }

    public void setDestinationTerm(String destinationTerm) {
        this.destinationTerm = destinationTerm;
    }

    public Boolean getAvoidHighways() {
        return avoidHighways;
    }

    public void setAvoidHighways(Boolean avoidHighways) {
        this.avoidHighways = avoidHighways;
    }

    public Boolean getAvoidTolls() {
        return avoidTolls;
    }

    public void setAvoidTolls(Boolean avoidTolls) {
        this.avoidTolls = avoidTolls;
    }

    public String getTravelMode() {
        return travelMode;
    }

    public void setTravelMode(String travelMode) {
        this.travelMode = travelMode;
    }

    public Integer getNoOfDays() {
        return noOfDays;
    }

    public void setNoOfDays(Integer noOfDays) {
        this.noOfDays = noOfDays;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Boolean getParkingEnabled() {
        return parkingEnabled;
    }

    public void setParkingEnabled(Boolean parkingEnabled) {
        this.parkingEnabled = parkingEnabled;
    }

    public Boolean getSkipDate() {
        return skipDate;
    }

    public void setSkipDate(Boolean skipDate) {
        this.skipDate = skipDate;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public Boolean getSkipLimit() {
        return skipLimit;
    }

    public void setSkipLimit(Boolean skipLimit) {
        this.skipLimit = skipLimit;
    }

    public double getMiles() {
        return miles;
    }

    public void setMiles(double miles) {
        this.miles = miles;
    }
}
