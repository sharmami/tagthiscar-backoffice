/*
 * Copyright 2010-2011 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tagthiscar.config;

import org.springframework.context.annotation.Configuration;

/**
 * Amit messing with this to figure this guy out... I have it almost!
 * 
 * Spring Social Configuration.
 * Allows Greenhouse users to connect to SaaS providers such as Twitter and Facebook.
 * @author Keith Donald
 */
@Configuration
public class SocialConfig {

	/*@Autowired
	private ConnectionRepository connectionRepository;
	
	@Autowired
	private ConnectionFactoryLocator connectionFactoryLocator;
	
	@Autowired
	private UsersConnectionRepository usersConnectionRepository;*/
	
	/*@Bean
	@Scope(value="request", proxyMode=ScopedProxyMode.INTERFACES)	
	public Facebook facebook() {
		System.out.println("SocialConfig : facebook: 1 ");
		Connection<Facebook> facebook = connectionRepository.findPrimaryConnection(Facebook.class);
		System.out.println("SocialConfig : facebook: 2 ");
		return facebook != null ? facebook.getApi() : new FacebookTemplate();
	}*/

	/*@Bean
	@Scope(value="request", proxyMode=ScopedProxyMode.INTERFACES)	
	public Twitter twitter() {
		System.out.println("SocialConfig : twitter: 1 ");
		Connection<Twitter> twitter = connectionRepository.findPrimaryConnection(Twitter.class);
		System.out.println("SocialConfig : twitter: 2 ");
		return twitter != null ? twitter.getApi() : new TwitterTemplate();
	}*/

	/*@Bean
	public ProviderSignInController providerSignInController() {
		System.out.println("SocialConfig : ProviderSignInController : providerSignInController : 1 ");
		return new ProviderSignInController(connectionFactoryLocator, usersConnectionRepository, new AccountSignInAdapter());
	}*/
	
}
