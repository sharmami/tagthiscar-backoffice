package com.tagthiscar.controllers;

import com.tagthiscar.domain.Label;
import com.tagthiscar.domain.Location;
import com.tagthiscar.domain.Search;
import com.tagthiscar.domain.Site;
import com.tagthiscar.service.LabelService;
import com.tagthiscar.service.SiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.geo.Metrics;
import org.springframework.data.web.PageableDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author asharma
 *
 */
@Controller
@RequestMapping("/sites")
public class SitesController {

    @Autowired
    SiteService siteService;

    @Autowired
    LabelService labelService;

    @RequestMapping(value="/{siteId}", method=RequestMethod.GET)
    public String activity(@PathVariable("siteId") String siteId, Model model) {

        Site site = siteService.findById(siteId);
        site.setLocation(siteService.findByLocationId(site.getLocation().getId()));

        model.addAttribute("site",site);

        Search search = new Search();
        //TODO can we have lat/long of the type long consistently.
        search.setLatitude(String.valueOf(site.getLocation().getLatitude()));
        search.setLongitude(String.valueOf(site.getLocation().getLongitude()));

        //1 mile radius.
        search.setValue(1);
        search.setMetric(Metrics.MILES);
        model.addAttribute("locationsWithinOneMile",(List<Location>)siteService.findTagsOrSites(search).get("automotive"));

        //5 mile radius
        search.setValue(5);
        search.setMetric(Metrics.MILES);
        model.addAttribute("locationsWithinFiveMile",(List<Location>)siteService.findTagsOrSites(search).get("automotive"));
        return "sites";
    }

    @RequestMapping(method=RequestMethod.GET)
    public String catchAll(Model model,@PageableDefaults(pageNumber = 0, value = 10) Pageable pageable) {
        model.addAttribute("page", siteService.findAll(pageable));
        return "sites-all";
    }

    @RequestMapping(value="/search",method=RequestMethod.GET)
    public String searchPage(Model model) {
        return "sites-search";
    }

    @RequestMapping(value="/addToLabel", method=RequestMethod.POST,params={"addToLabels"})
    public String addSitesToLabel(@RequestParam(value = "selectedSiteIds",required = true) String[] selectedSiteIds,@RequestParam(value = "label",required = true) String labelId) {
        Label label = labelService.findById(labelId);
        List<Site> sites = new ArrayList<Site>();
        if(label.getSites()!=null){
            sites = label.getSites();
        }

        for(String siteId: selectedSiteIds){
            Site site = siteService.findById(siteId);
            if(site!=null && !sites.contains(site)){
                sites.add(site);
                // add it to the site as well.
                site.getLabels().add(labelService.findById(labelId));//THIS CAUSES PROBLEM If you use the same label.
                siteService.save(site);
            }
        }

        label.setSites(new ArrayList<Site>(sites));
        labelService.save(label);
        return "redirect:/sites/search";
    }

    @RequestMapping(value="/addToLabel",method=RequestMethod.POST,params={"deleteSites"})
    public String deleteSites(@RequestParam(value = "selectedSiteIds",required = true) String[] selectedSiteIds) {
        for(String id: selectedSiteIds){
            siteService.deleteSite(String.valueOf(id));
        }
        return "redirect:/sites/search";
    }

    @RequestMapping(value="/search", method=RequestMethod.POST)
    public String save(Model model, Principal user,Search search) {
        Map<String,Object> result = siteService.findSites(search);
        model.addAttribute("sites",result.get("automotive"));
        return "sites-search";
    }

    @ModelAttribute("allLabels")
    public Iterable<Label> getAllLabels()
    {
        return labelService.findAll();
    }

}
