package com.tagthiscar.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;

/**
 * 
 * @author asharma
 *
 */
@Controller
@RequestMapping("/privacy_policy")
public class PrivacyController {

    @RequestMapping(method=RequestMethod.GET)
    public String showAll(Principal user, Model model) {
        if(user !=null){
            System.out.println("PrivacyController : User : "+user.getName()+" read terms and conditions. ");
        }
        else{
            System.out.println("PrivacyController: Anonymous User from 10.1.1.0 read the terms and conditions.");
        }
        return "privacy";
    }

}
