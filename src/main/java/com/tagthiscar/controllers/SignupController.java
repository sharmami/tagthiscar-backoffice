package com.tagthiscar.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 
 * @author asharma
 *
 */
@Controller
public class SignupController {
	
	/**
	 * Render the signin form to the person as HTML in their web browser.
	 * Returns void and relies in request-to-view-name translation to kick-in to resolve the view template to render.
	 */
	@RequestMapping(value="/signup", method=RequestMethod.GET)
	public void signup() {
	}
}
