package com.tagthiscar.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;

/**
 * 
 * @author asharma
 *
 */
@Controller
@RequestMapping("/faq")
public class FAQController {

    @RequestMapping(method=RequestMethod.GET)
    public String showAll(Principal user, Model model) {
        return "faq";
    }

}
