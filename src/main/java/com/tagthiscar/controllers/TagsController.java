package com.tagthiscar.controllers;

import com.tagthiscar.domain.Vehicle;
import com.tagthiscar.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

/**
 * 
 * @author asharma
 *
 */
@Controller
@RequestMapping("/tags")
public class TagsController {

    @Autowired
    VehicleService vehicleService;

    @RequestMapping(value="/{vehicle}", method=RequestMethod.GET)
    public String show(Principal user,@PathVariable("vehicle") String vehicleId, Model model) {
        //TODO see if i can totally eliminate this call and just use the /find_my_tags to get everything.
        Vehicle vehicle = vehicleService.findById(vehicleId);
        model.addAttribute("vehicle",vehicle);
        return "tags";
    }

    @RequestMapping(value="/add", method=RequestMethod.GET)
    public String add(Principal user,Model model) {
        System.out.println("TagsController : Add a tag : User : "+user.getName());
        return "add";
    }

    @RequestMapping(method=RequestMethod.GET)
    public void showAll(Principal user, Model model) {
        System.out.println("TagsController : Show All Tags for user : "+user.getName());
    }

    @RequestMapping(value="/find_my_tags", method=RequestMethod.GET)
    public @ResponseBody Vehicle findMyTags(Principal user,HttpServletRequest request) {
        //TODO make sites sort by date so the tags can be numbered.
        //TODO make sure to check for css hack in request param.
        Vehicle vehicle = vehicleService.findById(request.getParameter("vehicleId"));
        return vehicle;
    }
}
