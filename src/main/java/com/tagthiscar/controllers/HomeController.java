package com.tagthiscar.controllers;

import com.tagthiscar.domain.Search;
import com.tagthiscar.domain.UserStats;
import com.tagthiscar.service.AnalyticsService;
import com.tagthiscar.service.ExperimentService;
import com.tagthiscar.service.SiteService;
import com.tagthiscar.util.SiteUtils;
import org.apache.commons.validator.GenericValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.geo.Metrics;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author asharma
 *
 */
@Controller
public class HomeController {

	@Autowired
    AnalyticsService analyticsService;

    @Autowired
    SiteService siteService;

    @Autowired
    ExperimentService experimentService;

	@RequestMapping(value="/home", method=RequestMethod.GET)
	public String home(Principal user, HttpServletRequest request) {
		return user != null ? "homeNotSignedIn" : "homeNotSignedIn";
	}

    @RequestMapping(value={"/"}, method=RequestMethod.POST)
    public String submit(Principal user, HttpServletRequest request, Model model, Search search) {
        return showResults(user,request,model,search);
    }

    @RequestMapping(value="/share", method=RequestMethod.GET)
    public String share(Principal user, HttpServletRequest request, Model model, Search search) {
        return showResults(user,request,model,search);
    }

    @RequestMapping(value="/", method=RequestMethod.GET)
    public String land(Principal user, HttpServletRequest request) {
        return "land";
    }

    @RequestMapping(value="/find_my_loc", method=RequestMethod.GET)
    public @ResponseBody Map<String,Object> findLocationFromLatLong(HttpServletRequest request) {

        //System.out.println("HomeController : find_my_loc : "+request.getParameter("zoom")+" "+request.getParameter("longitude")+" "+request.getParameter("latitude")+" "+request.getParameter("searchTerm"));

        //TODO do the CSS hacking check here.
        Search search = new Search();
        search.setZoomLevel(request.getParameter("zoom"));
        search.setLongitude(request.getParameter("longitude"));
        search.setLatitude(request.getParameter("latitude"));
        search.setSearchTerm(request.getParameter("searchTerm"));
        if(StringUtils.hasText(request.getParameter("noOfMiles"))){
            search.setValue(Double.valueOf(request.getParameter("noOfMiles")));
        }
        search.setCsvFlagTypes(request.getParameter("csvFlagTypes"));
        search.setLabels(request.getParameter("labels"));
        search.setParkingEnabled(Boolean.valueOf(request.getParameter("parkingEnabled")));
        search.setSortBy(request.getParameter("sortBy"));
        if(GenericValidator.isInt(request.getParameter("limit"))){
            search.setLimit(Integer.valueOf(request.getParameter("limit")));
        } else{
            search.setLimit(25);
        }

        if(GenericValidator.isInt(request.getParameter("noOfDays"))){
            search.setNoOfDays(Integer.valueOf(request.getParameter("noOfDays")));
        }else{
            search.setNoOfDays(-1);
        }

        search.setMetric(Metrics.MILES);

        //return siteService.findTagsOrSites(search);
        return siteService.findSites(search);
    }

    @RequestMapping(value="/find_my_trip", method=RequestMethod.GET)
    public @ResponseBody Map<String,Object> findMyTrip(HttpServletRequest request) {
        List<String> allPointsList = new ArrayList<String>();
        //TODO do the CSS hacking check here.
        //TODO why am i having to deserialize here... we shouldnt have to do this here.
        String[] allPointsRaw = request.getParameterValues("allPoints");
        for(String point: allPointsRaw){
            point = point.replace("[","");
            point = point.replace("]","");
            String[] temp = point.split(",");
            for(String latLngCSV : temp){
                latLngCSV = latLngCSV.replace("\"","");
                String[] latLng = latLngCSV.split(":");
                allPointsList.add(latLng[0]+","+latLng[1]);
            }
        }

        String[] allPoints = new String[ allPointsList.size()];
        allPointsList.toArray(allPoints);

        Search search = new Search();
        search.setAllPoints(allPoints);

        //pass other things user searched for as well in the search.
        //TODO eventually we want find_my_trip and find_my_loc to be combined.
        search.setSearchTerm(request.getParameter("searchTerm"));
        search.setDestinationTerm(request.getParameter("destinationTerm"));
        search.setCsvFlagTypes(request.getParameter("csvFlagTypes"));
        search.setParkingEnabled(Boolean.valueOf(request.getParameter("parkingEnabled")));
        if(GenericValidator.isInt(request.getParameter("noOfDays"))){
            search.setNoOfDays(Integer.valueOf(request.getParameter("noOfDays")));
        }else{
            search.setNoOfDays(-1);
        }
        return siteService.findSites(search);
    }

    @RequestMapping(value="/dummy_data", method=RequestMethod.GET)
    public String someDummyData(HttpServletRequest request) {
        experimentService.someDummyData();
        return "land";
    }

    /**
     *
     * @param user
     * @param request
     * @param model
     * @param search
     * @return
     */
    private String showResults(Principal user, HttpServletRequest request, Model model, Search search){
        UserStats userStats = SiteUtils.addToUserStats(user,request);
        userStats.setComments("SearchTerm: "+search.getSearchTerm()+" miles: "+search.getValue()+" DestinationTerm: "+search.getDestinationTerm());
        analyticsService.addToUserStats(userStats);

        search.setAddress(search.getSearchTerm());//TODO this is temporary.
        search.setDestination(search.getDestinationTerm());// TODO i dont think we need this
        search.setAvoidHighways(search.getAvoidHighways());// TODO i dont think we need this
        search.setAvoidTolls(search.getAvoidTolls());// TODO i dont think we need this
        search.setCsvFlagTypes(search.getCsvFlagTypes()); // TODO i dont think we need this
        search.setLabels(search.getLabels());
        search.setParkingEnabled(search.getParkingEnabled());
        search.setSortBy(search.getSortBy());
        search.setNoOfDays(search.getNoOfDays());

        if(request.getParameterValues("hwyTollOptions")!=null && request.getParameterValues("hwyTollOptions").length>0){
            for(String option : request.getParameterValues("hwyTollOptions")){
              if("avoidHighways".equalsIgnoreCase(option)){
                  search.setAvoidHighways(true);
              }
                if("avoidTolls".equalsIgnoreCase(option)){
                    search.setAvoidTolls(true);
                }
            }
        }

        Map < String, String > searchResults = new HashMap<String, String>();//TODO do we need this.
        model.addAttribute("search",search);

        //return user != null ? "homeNotSignedIn" : "homeNotSignedIn";
        if(!GenericValidator.isBlankOrNull(search.getDestination())){
            return "directionsPage";
        }
        return "homeNotSignedIn";
    }


    @ModelAttribute("flagTypes")
    public List<String> getFlagTypes()
    {
        return siteService.getDistinctFlagTypes();
    }

}
