package com.tagthiscar.controllers;

import com.tagthiscar.service.AnalyticsService;
import org.apache.commons.validator.GenericValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * 
 * @author asharma
 *
 */
@Controller
@RequestMapping("/subscribe")
public class SubscribeController {

    @Autowired
    AnalyticsService analyticsService;

    @RequestMapping(method=RequestMethod.GET)
    public @ResponseBody void subscribe(HttpServletRequest request) {
        //TODO check css hack.
        if(GenericValidator.isEmail(request.getParameter("email"))){
            analyticsService.subscribe(request.getParameter("email"));
        }
    }

    @RequestMapping(value="/all",method=RequestMethod.GET)
    public String subscriptions(Model model) {
        model.addAttribute("subscriptions",analyticsService.getSubscriptions());
        return "subscriptions";
    }
}
