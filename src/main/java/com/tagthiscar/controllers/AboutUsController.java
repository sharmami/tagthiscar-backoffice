package com.tagthiscar.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;

/**
 * 
 * @author asharma
 *
 */
@Controller
@RequestMapping("/about_us")
public class AboutUsController {
	
	/**
	 * Note the request is WebRequest from spring and not a regular http request. This is handy to retrieve Connection Info thats stored in the session by Spring Social
	 * before it sent me to this page.
	 * 
	 * @param request
	 */
	@RequestMapping(method=RequestMethod.GET)
	public String aboutUs(WebRequest request,Model model) {
		System.out.println("AboutUsController : about_us  ");
		 return "aboutus";
	}
}
