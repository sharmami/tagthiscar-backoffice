package com.tagthiscar.controllers;

import com.tagthiscar.service.AnalyticsService;
import com.tagthiscar.service.ExperimentService;
import com.tagthiscar.util.SiteUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

/**
 * 
 * @author asharma
 *
 */
@Controller
@RequestMapping("/analytics")
public class AnalyticsController {

	@Autowired
    AnalyticsService analyticsService;

    @Autowired
    ExperimentService experimentService;

	@RequestMapping(method= RequestMethod.GET)
	public void analytics(Principal user, HttpServletRequest request) {
        analyticsService.addToUserStats(SiteUtils.addToUserStats(user, request));
	}

    @RequestMapping(value="/all",method=RequestMethod.GET)
    public String subscriptions(Model model,@PageableDefaults(pageNumber = 0, value = 10) Pageable pageable) {
        //model.addAttribute("userStats",analyticsService.getUserStats());
       Pageable pageable1 = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), Sort.Direction.DESC, "submittedDate");
        model.addAttribute("page",analyticsService.findAll(pageable1));
        return "subscriptions";
    }


    @RequestMapping(value="/delete",method=RequestMethod.GET)
    public String deleteLocalAnalytics(Model model) {
        experimentService.deleteLocalhostStats();
        return "subscriptions";
    }

}
