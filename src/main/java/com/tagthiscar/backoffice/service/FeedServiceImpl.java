package com.tagthiscar.backoffice.service;

import com.tagthiscar.backoffice.dao.FeedDao;
import com.tagthiscar.backoffice.domain.Feed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * @author asharma
 */
@Service
public class FeedServiceImpl implements FeedService {


    @Autowired
    FeedDao feedDao;

    @Override
    public Iterable<Feed> findAll() {
        return feedDao.findAll();
    }

    @Override
    public Feed addFeed(Feed feed) {
         return feedDao.save(feed);
    }

    @Override
    public Feed findOne(String id) {
        return feedDao.findOne(id);
    }

    @Override
    public void deleteAll() {
        feedDao.deleteAll();
    }

    @Override
    public Page<Feed> findAll(Pageable pageable){
          return feedDao.findAll(pageable);
    }
}
