package com.tagthiscar.backoffice.service;

import com.tagthiscar.backoffice.domain.FeedSource;
import com.tagthiscar.dao.FeedSourceDao;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author asharma
 */
@Service
public class FeedSourceServiceImpl implements FeedSourceService {

    @Autowired
    FeedSourceDao feedSourceDao;

    @Override
    public Iterable<FeedSource> findAll() {
        return feedSourceDao.findAll();
    }

    @Override
    public FeedSource save(FeedSource feedSource) {
        return feedSourceDao.save(feedSource);
    }


    @Override
    public void deleteAll() {
        feedSourceDao.deleteAll();
    }

    @Override
    public FeedSource findOne(String id) {
        return feedSourceDao.findOne(id);
    }

    @Override
    public void delete(String id) {
        feedSourceDao.delete(id);
    }


   @Override
   //TODO check if this is needed anymore?
    public void insertFeedSources(){

        Map<String,String> feedUrlMap = new HashMap<String,String>();

        for (Map.Entry<String, String> entry : feedUrlMap.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            //System.out.println(" Key : "+key+" value : "+value);
            FeedSource feedSource = new FeedSource();
            feedSource.setId(ObjectId.get().toString());
            feedSource.setFeedName(key);
            feedSource.setFeedUrl(value);
            feedSource.setSubmittedBy("amit");
            feedSource.setSubmittedDate(new Date());

            feedSourceDao.save(feedSource);
        }

    }

}
