package com.tagthiscar.backoffice.service;

import com.tagthiscar.backoffice.domain.Feed;

import java.util.List;

/**
 * @author asharma
 */
public interface RssService {

    public List<Feed> rssFeed(String fileNamePattern, String url);

    public void serviceRequestsDc311Feeds(String fileName);

    public String getRawFeed(String url);

    public void processFeed(String key, String value);

    public void tryJsonSlurping();

    public void crimeIncidentsInDc311Feeds(String fileName);

    public void readParkingData(String fileName);

    public void readMarylandStateIncidents(String fileName);

    public String get511feedsFor7States(String url,String username,String password,String state);

    public void californiaHighwayPatrolFeeds(String fileName);
}
