package com.tagthiscar.backoffice.service;

import com.tagthiscar.backoffice.domain.FeedSource;

/**
 * @author asharma
 */
public interface FeedSourceService {

    Iterable<FeedSource> findAll();

    FeedSource save(FeedSource feedSource);

    public FeedSource findOne(String id);

    void deleteAll();

      public void delete(String id);

    public void insertFeedSources();

}
