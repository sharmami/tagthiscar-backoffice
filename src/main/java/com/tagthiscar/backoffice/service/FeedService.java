package com.tagthiscar.backoffice.service;

import com.tagthiscar.backoffice.domain.Feed;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @author asharma
 */
public interface FeedService {

     public Iterable<Feed> findAll();

    public Page<Feed> findAll(Pageable pageable);

    public Feed addFeed(Feed feed);

    public Feed findOne(String id);

    public void deleteAll();

}
