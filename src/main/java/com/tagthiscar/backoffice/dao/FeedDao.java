package com.tagthiscar.backoffice.dao;

import com.tagthiscar.backoffice.domain.Feed;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * @author asharma
 */
@Repository
public interface FeedDao extends PagingAndSortingRepository<Feed, String> {

         public Feed findByGuid(String guid);
}