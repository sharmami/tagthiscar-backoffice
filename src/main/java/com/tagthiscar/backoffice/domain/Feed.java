package com.tagthiscar.backoffice.domain;

import com.tagthiscar.domain.Site;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Arrays;
import java.util.Date;

/**
 * @author asharma
 */
@Document
public class Feed {

    @Id
    private String id;

    private String title; //teaser

    private String link; //https://alert.loudoun.gov/latest.php#37513

    private String description;  // the actual text.

    private String author;  //lcas@loudoun.gov

    private String pubDate; //Fri, 29 Mar 2013 22:41:04 GMT

    private String comments;// placeholder for something I am not aware of.

    private String guid;//

    private String fileName;//xml file name for rss feed. alert-loudoun-gov-2013-03-29-23-33-10-169.xml

    private String submittedBy;

    private Date submittedDate;

    private String updatedBy;

    private Date updatedDate;

    private String geoRssPolygon;

    private double latitude;

    private double longitude;

    private String[] flagTypes;

    private String csvLabels;//'10-most-congested-cities,10-most-polluted-citi

   /* @DBRef*/
    private Site rawSite;

    private String fileId;//this is the fileId in GridFSDBFile in case you need the feed xml.

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /*public RawSite getRawSite() {
        return rawSite;
    }

    public void setRawSite(RawSite rawSite) {
        this.rawSite = rawSite;
    }*/

    public String getSubmittedBy() {
        return submittedBy;
    }

    public void setSubmittedBy(String submittedBy) {
        this.submittedBy = submittedBy;
    }

    public Date getSubmittedDate() {
        return submittedDate;
    }

    public void setSubmittedDate(Date submittedDate) {
        this.submittedDate = submittedDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getGeoRssPolygon() {
        return geoRssPolygon;
    }

    public void setGeoRssPolygon(String geoRssPolygon) {
        this.geoRssPolygon = geoRssPolygon;
    }

   public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public Site getRawSite() {
        return rawSite;
    }

    public void setRawSite(Site rawSite) {
        this.rawSite = rawSite;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String[] getFlagTypes() {
        return flagTypes;
    }

    public void setFlagTypes(String[] flagTypes) {
        this.flagTypes = flagTypes;
    }

    public String getCsvLabels() {
        return csvLabels;
    }

    public void setCsvLabels(String csvLabels) {
        this.csvLabels = csvLabels;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (!(o instanceof Feed)) return false;

        Feed feed = (Feed) o;

        if (Double.compare(feed.latitude, latitude) != 0) return false;
        if (Double.compare(feed.longitude, longitude) != 0) return false;
        if (author != null ? !author.equals(feed.author) : feed.author != null) return false;
        if (comments != null ? !comments.equals(feed.comments) : feed.comments != null) return false;
        if (csvLabels != null ? !csvLabels.equals(feed.csvLabels) : feed.csvLabels != null) return false;
        if (description != null ? !description.equals(feed.description) : feed.description != null) return false;
        if (fileId != null ? !fileId.equals(feed.fileId) : feed.fileId != null) return false;
        if (fileName != null ? !fileName.equals(feed.fileName) : feed.fileName != null) return false;
        if (!Arrays.equals(flagTypes, feed.flagTypes)) return false;
        if (geoRssPolygon != null ? !geoRssPolygon.equals(feed.geoRssPolygon) : feed.geoRssPolygon != null)
            return false;
        if (guid != null ? !guid.equals(feed.guid) : feed.guid != null) return false;
        if (id != null ? !id.equals(feed.id) : feed.id != null) return false;
        if (link != null ? !link.equals(feed.link) : feed.link != null) return false;
        if (pubDate != null ? !pubDate.equals(feed.pubDate) : feed.pubDate != null) return false;
        if (rawSite != null ? !rawSite.equals(feed.rawSite) : feed.rawSite != null) return false;
        if (submittedBy != null ? !submittedBy.equals(feed.submittedBy) : feed.submittedBy != null) return false;
        if (submittedDate != null ? !submittedDate.equals(feed.submittedDate) : feed.submittedDate != null)
            return false;
        if (title != null ? !title.equals(feed.title) : feed.title != null) return false;
        if (updatedBy != null ? !updatedBy.equals(feed.updatedBy) : feed.updatedBy != null) return false;
        if (updatedDate != null ? !updatedDate.equals(feed.updatedDate) : feed.updatedDate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id != null ? id.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (link != null ? link.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (author != null ? author.hashCode() : 0);
        result = 31 * result + (pubDate != null ? pubDate.hashCode() : 0);
        result = 31 * result + (comments != null ? comments.hashCode() : 0);
        result = 31 * result + (guid != null ? guid.hashCode() : 0);
        result = 31 * result + (fileName != null ? fileName.hashCode() : 0);
        result = 31 * result + (submittedBy != null ? submittedBy.hashCode() : 0);
        result = 31 * result + (submittedDate != null ? submittedDate.hashCode() : 0);
        result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
        result = 31 * result + (updatedDate != null ? updatedDate.hashCode() : 0);
        result = 31 * result + (geoRssPolygon != null ? geoRssPolygon.hashCode() : 0);
        temp = Double.doubleToLongBits(latitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(longitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (flagTypes != null ? Arrays.hashCode(flagTypes) : 0);
        result = 31 * result + (csvLabels != null ? csvLabels.hashCode() : 0);
        result = 31 * result + (rawSite != null ? rawSite.hashCode() : 0);
        result = 31 * result + (fileId != null ? fileId.hashCode() : 0);
        return result;
    }
}