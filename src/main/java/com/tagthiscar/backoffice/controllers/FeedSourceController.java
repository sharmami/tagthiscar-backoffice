package com.tagthiscar.backoffice.controllers;

import com.tagthiscar.backoffice.domain.FeedSource;
import com.tagthiscar.backoffice.model.FeedSourceForm;
import com.tagthiscar.backoffice.service.FeedSourceService;
import org.apache.commons.validator.GenericValidator;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;

import java.security.Principal;

/**
 * 
 * @author asharma
 *
 */
@Controller
@RequestMapping("/feedSource")
public class FeedSourceController {

    @Autowired
    FeedSourceService feedSourceService;

	@RequestMapping(method=RequestMethod.GET)
	public String list(WebRequest request,Model model) {
        model.addAttribute("feedSources",feedSourceService.findAll());
         return "feedSource-index";
	}

    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public String show(Principal user,@PathVariable("id") String feedSourceId, Model model) {
        FeedSource feedSource = feedSourceService.findOne(feedSourceId);
        model.addAttribute("feedSource",feedSource);
        return "feedSource-detail";
    }

    @RequestMapping(value="/delete", method=RequestMethod.POST)
    public String delete(Principal user,FeedSourceForm feedSourceForm) {
        for(String feedSource : feedSourceForm.getCheckedIds()){
            feedSourceService.delete(feedSource);
        }
        return "redirect:/feedSource";
    }

    @RequestMapping(value="/save", method=RequestMethod.POST)
    public String save(Principal user,FeedSource feedSource) {
        System.out.println(" FeedSource saved: id: "+feedSource.getId() + " feedname: "+feedSource.getFeedName()+" feed url: "+feedSource.getFeedUrl());

        if(!GenericValidator.isBlankOrNull(feedSource.getId())){
            FeedSource newFeedSource = feedSourceService.findOne(feedSource.getId());
            newFeedSource.setFeedName(feedSource.getFeedName());
            newFeedSource.setFeedUrl(feedSource.getFeedUrl());
            feedSourceService.save(newFeedSource);
        }else{
            feedSource.setId(ObjectId.get().toString());
            feedSourceService.save(feedSource);
        }
        return "redirect:/feedSource/"+feedSource.getId();
    }

}
