package com.tagthiscar.backoffice.controllers;

import com.mongodb.gridfs.GridFSDBFile;
import com.tagthiscar.backoffice.domain.Feed;
import com.tagthiscar.backoffice.domain.FeedSource;
import com.tagthiscar.backoffice.service.FeedService;
import com.tagthiscar.backoffice.service.FeedSourceService;
import com.tagthiscar.backoffice.service.RssService;
import com.tagthiscar.domain.Label;
import com.tagthiscar.domain.Site;
import com.tagthiscar.service.ExperimentService;
import com.tagthiscar.service.LabelService;
import com.tagthiscar.service.SiteService;
import org.apache.commons.io.IOUtils;
import org.apache.commons.validator.GenericValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * 
 * @author asharma
 *
 */
@Controller
@RequestMapping("/feed")
public class FeedController {

    @Autowired
    RssService rssService;

    @Autowired
    FeedSourceService feedSourceService;

    @Autowired
    FeedService feedService;

    @Autowired
    SiteService siteService;

    @Autowired
    ExperimentService experimentService;

    @Autowired
    LabelService labelService;

	@RequestMapping(method=RequestMethod.GET)
	public String list(WebRequest request,Model model,@PageableDefaults(pageNumber = 0, value = 10) Pageable pageable) {
        //model.addAttribute("rssFeedList",feedService.findAll());
        model.addAttribute("page",feedService.findAll(pageable));
         return "feed-index";
	}

    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public String show(Principal user,@PathVariable("id") String feedId, Model model) {
        Feed feed = feedService.findOne(feedId);
        if(GenericValidator.isBlankOrNull(feed.getFileId()) && !GenericValidator.isBlankOrNull(feed.getFileName())){
            try{
                String feedFileId = experimentService.getFileIdByFilename(feed.getFileName());
                if(feedFileId!=null){
                    feed.setFileId(feedFileId);
                }
            }catch (Exception e){
                System.out.println("FeedController : show : ERROR : ");
                e.printStackTrace();
            }
        }

        model.addAttribute("feed",feed);
        List<String> myFlagTypes = new ArrayList<String>();
        if(feed.getRawSite()!=null && feed.getRawSite().getFlagTypes()!=null && feed.getRawSite().getFlagTypes().length>0){
            myFlagTypes = Arrays.asList (feed.getRawSite().getFlagTypes());
        }
        model.addAttribute("flagTypes", myFlagTypes);

        return "feed-detail";
    }

    @RequestMapping(value="/add",method=RequestMethod.POST)
    public String add(Feed feed) {

        double[] latLong = new double[2];
        latLong[1] = feed.getLatitude();
        latLong[0] = feed.getLongitude();

        feed.getRawSite().getLocation().setCoordinates(latLong);
        //TODO verify these types
        feed.getRawSite().setFlagTypes(feed.getFlagTypes());

        List<Label> labels = new ArrayList<Label>();

        if(!GenericValidator.isBlankOrNull(feed.getCsvLabels())){
            String [] labelIds =  feed.getCsvLabels().split(",") ;
            for(String labelId: labelIds){
                Label label = labelService.findById(labelId);

                if(label!=null){
                    labels.add(label);
                }
            }
        }




        if(labels!=null && !labels.isEmpty()){
            feed.getRawSite().setLabels(labels);
        }


        feed.getRawSite().setSubmittedDate(new Date());
       Site site = siteService.saveOrUpdateTag(feed);

        feed = feedService.findOne(feed.getId());
        if(feed!=null){
            feed.setRawSite(site);// make sure feed has all the latest we just saved.
            feedService.addFeed(feed);
            return "redirect:/feed/"+feed.getId();
        }
        return "redirect:/feed/new";
    }

    @RequestMapping(value="/fetch",method=RequestMethod.GET)
    public String fetch(WebRequest request,Model model) {
        for(FeedSource feedSource: feedSourceService.findAll()){
            String key = feedSource.getFeedName();
            String value = feedSource.getFeedUrl();
            rssService.processFeed(key, value);
        }
        rssService.readMarylandStateIncidents("http://www.chart.state.md.us/rss/ProduceRSS.aspx?Type=TIandRCXML&filter=ALL");
        return "redirect:/feed";
    }

    @RequestMapping(value="/read",method=RequestMethod.GET)
    public String read(WebRequest request,Model model) {
       //rssService.serviceRequests311Feeds("src_current2.xml"); if you use a local file, make sure to remove the getRawFeed method.
        //rssService.californiaHighwayPatrolFeeds("http://media.chp.ca.gov/sa_xml/sa.xml");
        rssService.get511feedsFor7States("http://id.carsprogram.org/hub/data/feu-g.xml","ASharma","tagcarHuB","ID");
        return "redirect:/feed/new";
    }


    @RequestMapping(value={"/file","/file/list"},method=RequestMethod.GET)
    public String getFilesList(Model model) {
        List<GridFSDBFile> result = experimentService.findAll();
        model.addAttribute("allFeedFiles",result);
        return "feed-files";
    }

    @RequestMapping(value="/file/{id}",method=RequestMethod.GET,produces = "application/xml")
    public @ResponseBody String getFile(@PathVariable("id") String feedFileId, Model model) {
        GridFSDBFile file = experimentService.get(feedFileId);
        try{
            String fileString =  IOUtils.toString(file.getInputStream());
            return fileString;
        } catch (Exception ex){
            System.out.println("FeedController : /file/"+feedFileId+" can't seem to be giving me feed xml back.");
            ex.printStackTrace();
        }
        return "DONE";
    }


    @RequestMapping(value="/download/{id}",method=RequestMethod.GET,produces = "text/plain")
    @ResponseBody
    public  byte[] download(@PathVariable("id") String feedFileId, Model model) {
        GridFSDBFile file = experimentService.get(feedFileId);
        try{
            return FileCopyUtils.copyToByteArray(file.getInputStream());
        } catch (Exception ex){
            System.out.println("FeedController : /file/"+feedFileId+" can't seem to be giving me feed xml back.");
            ex.printStackTrace();
        }
        return null;
    }


    @RequestMapping(value="/addParking",method=RequestMethod.GET)
    public String addParking(WebRequest request,Model model) {
                //rssService.readParkingData("ParkingDestinations.xml");
                return "redirect:/feed";
    }

    @RequestMapping(value="/new",method=RequestMethod.GET)
    public String newFeed(WebRequest request,Model model) {
        model.addAttribute("feed",new Feed());
        List<String> myFlagTypes = new ArrayList<String>();
        model.addAttribute("flagTypes", myFlagTypes);
        return "feed-detail";
    }

    @ModelAttribute("flagTypesList")
    public List<String> getFlagTypes()
    {

        List<String> flagTypes =siteService.getDistinctFlagTypes();
        //System.out.println(" HERE size: "+flagTypes);
        return flagTypes;
        /*return siteService.getDistinctFlagTypes();*/
    }

    @ModelAttribute("allFlagTypes")
    public List<String> getAllFlagTypes()
    {
         String flags = "abandoned_vehicle,accesdenied,accident,adjudication,aircraftsmall,airport,airport_apron,airport_runway,airport_terminal,airquality,amber_alert,boat,bulk_collection,bulldozer,bus,busstop,cablecar,car,caraccident,carrental,carwash,caution,closedroad,closure,construction,convertible,crossingguard,cycling,dead_animal,delay,device-status,disabledvehicle,disaster,disturbance,doublebendright,driver_vehicle_service,fallingrocks,ferry,fillingstation,footprint,fourbyfour,funicolar-22x22,fyi,graffiti,harbor,headsup,helicopter,highway,hotairbaloon,icy_road,illegal_dumping,incident,jeep,junction,kingair,levelcrossing,mainroad,mobile-situation,motorcycle,obstruction,oil-2,parade,parkandride,parking-information,parking-meter-export,parkinggarage,parking_enforcement,pavement-condition,pedestriancrossing,policeactivity,precipitation,recycling,repair,restriction,road,roadtype_gravel,roadwork,robbery,sanitation_enforcement,signs,slipway,smoke_alarm,special-event,speedhump,speed_50,sporting-event,sportscar,sportutilityvehicle,steamtrain,stop,street_bridge_maintenance,street_cleaning,street_repair,supercans,system-information,taxi,taxiboat,taxiway,temperature,thunderstorm,tidaldiamond,tires,tollstation,trafficcamera,trafficlight,traffic_signal,train,tramway,trash_collection,tree_pruning,truck3,tunnel,underground,unusual-driving,van,vespa,visibility-air-quality,watercraft,watershortage,weather-condition,wind,winter-driving-index,winter-driving-restriction";
        return Arrays.asList(flags.split(","));
    }

    @ModelAttribute("allLabels")
    public Iterable<Label> getAllLabels()
    {
          return labelService.findAll();
    }
}
