package com.tagthiscar.backoffice.controllers;

import com.tagthiscar.backoffice.model.FeedSourceForm;
import com.tagthiscar.backoffice.model.LabelForm;
import com.tagthiscar.domain.Label;
import com.tagthiscar.domain.Site;
import com.tagthiscar.service.LabelService;
import com.tagthiscar.service.SiteService;
import org.apache.commons.validator.GenericValidator;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 
 * @author asharma
 *
 */
@Controller
@RequestMapping("/labels")
public class LabelsController {

    @Autowired
    LabelService labelService;

    @Autowired
    SiteService siteService;

	@RequestMapping(method=RequestMethod.GET)
	public String list(WebRequest request,Model model) {
        model.addAttribute("labels",labelService.findAll());
         return "labels-index";
	}

    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public String show(Principal user,@PathVariable("id") String labelId, Model model) {
        model.addAttribute("label",labelService.findById(labelId));
        return "labels-detail";
    }

    @RequestMapping(value="/delete", method=RequestMethod.POST)
    public String delete(Principal user,FeedSourceForm labelForm) {
       for(String labelId : labelForm.getCheckedIds()){
            Label label = labelService.findById(labelId);
            if(label!=null){
                for(Site site : label.getSites()){
                    site.getLabels().remove(label);
                    siteService.save(site);
                }
                labelService.delete(labelId);
            }
        }
        return "redirect:/labels";
    }


    @RequestMapping(value="/save", method=RequestMethod.POST,params={"deletSitesFromLabel"})
    public String deletSitesFromLabel(@RequestParam(value = "selectedSiteIds",required = true) String[] selectedSiteIds,LabelForm labelForm) {
        Label label = labelService.findById(labelForm.getLabel().getId());

        List<Site> sites = new ArrayList<Site>();
        for(String siteId: selectedSiteIds){
            Site site = siteService.findById(siteId);
            site.getLabels().remove(label);// remove the label from the site.
            siteService.save(site);
            label.getSites().remove(site);// remove the site link from label first.
        }
        labelService.save(label);
        return "redirect:/labels/"+labelForm.getLabel().getId();
    }

    @RequestMapping(value="/save", method=RequestMethod.POST)
    public String save(Principal user,LabelForm labelForm) {
        Label label = labelForm.getLabel();

        if(!GenericValidator.isBlankOrNull(label.getId())){
            //System.out.println(" LabelsController:  saved: id: "+label.getId() + " label name: "+label.getName());
            Label newLabel = labelService.findById(label.getId());
            newLabel.setName(label.getName());
            newLabel.setTitle(label.getTitle());
            newLabel.setGuid(label.getGuid());
            newLabel.setDescription(label.getDescription());
            newLabel.setGuid(label.getGuid());
            newLabel.setUpdatedBy(user.getName());
            newLabel.setUpdatedDate(new Date());
            labelService.save(newLabel);
        }else{
            label.setId(ObjectId.get().toString());
            label.setCreatedDate(new Date());
            label.setCreatedBy(user.getName());
            labelService.save(label);
        }
        return "redirect:/labels/"+label.getId();
    }

}
