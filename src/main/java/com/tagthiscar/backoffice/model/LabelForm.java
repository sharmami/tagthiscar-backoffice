package com.tagthiscar.backoffice.model;

import com.tagthiscar.domain.Label;

/**
 * @author asharma
 */
public class LabelForm {

    private Label label;

    private String [] checkedIds;

    public String[] getCheckedIds() {
        return checkedIds;
    }

    public void setCheckedIds(String[] checkedIds) {
        this.checkedIds = checkedIds;
    }

    public Label getLabel() {
        return label;
    }

    public void setLabel(Label label) {
        this.label = label;
    }
}
