package com.tagthiscar.backoffice.model;

/**
 * @author asharma
 */
public class FeedSourceForm {

    private String [] checkedIds;

    public String[] getCheckedIds() {
        return checkedIds;
    }

    public void setCheckedIds(String[] checkedIds) {
        this.checkedIds = checkedIds;
    }
}
