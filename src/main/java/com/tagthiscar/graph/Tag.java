package com.tagthiscar.graph;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.frames.Adjacency;
import com.tinkerpop.frames.Incidence;
import com.tinkerpop.frames.Property;
import com.tinkerpop.frames.VertexFrame;

/**
 * @author asharma
 */
public interface Tag extends VertexFrame {

    @Property("name")
    public String getName();

    @Property("tagId")
    public String getTagId();

    @Property("teaser")
    public String getTeaser();

    @Property("description")
    public String getDescription();

    @Adjacency(label="hasLocation")
    public Iterable<Location> getLocations();


    //@GremlinGroovy("it.out('knows').out('knows').dedup") //Make sure you use the GremlinGroovy module! #1
    //public Iterable<Site> getFriendsOfAFriend();

    @Incidence(label = "hasLocation", direction = Direction.OUT)
    public Iterable<hasLocation> getHasLocations();
}
