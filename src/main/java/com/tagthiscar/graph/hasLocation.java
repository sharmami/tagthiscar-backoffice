package com.tagthiscar.graph;

import com.thinkaurelius.titan.core.attribute.Geoshape;
import com.tinkerpop.frames.EdgeFrame;
import com.tinkerpop.frames.InVertex;
import com.tinkerpop.frames.OutVertex;
import com.tinkerpop.frames.Property;

/**
 * @author asharma
 */
public interface hasLocation extends EdgeFrame {

    @Property("number")
    public Integer getLocationNumber();

    @OutVertex
    Location getLocation();

    @InVertex
    Tag getTag();
}
