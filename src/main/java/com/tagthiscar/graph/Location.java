package com.tagthiscar.graph;

import com.thinkaurelius.titan.core.attribute.Geoshape;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.frames.Adjacency;
import com.tinkerpop.frames.Property;
import com.tinkerpop.frames.VertexFrame;


/**
 * @author asharma
 */
public interface Location extends VertexFrame {

    @Property("name")
    public String getName();

    @Property("locationId")
    public String getTagId();

    @Property("address")
    public String getAddress();

    @Property("address1")
    public String getAddress1();

    @Property("city")
    public String getCity();

    @Property("state")
    public String getState();

    @Property("country")
    public String getCountry();

    @Property("zip")
    public String getZip();

    @Property("place")
    public Geoshape getGeoShape();

    @Adjacency(label="hasLocation",direction = Direction.IN)
    public Iterable<Tag> getTags();
}
