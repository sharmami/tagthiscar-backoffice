package com.tagthiscar.graph;

import com.thinkaurelius.titan.core.attribute.Geoshape;
import com.tinkerpop.frames.EdgeFrame;
import com.tinkerpop.frames.InVertex;
import com.tinkerpop.frames.OutVertex;
import com.tinkerpop.frames.Property;

/**
 * @author asharma
 */
public interface Battled extends EdgeFrame {

    @Property("time")
    public Integer getTime();

    @Property("place")
    public Geoshape getShape();

    @OutVertex
    Site getOutSite();

    @InVertex
    Site getInSite();
}
