package com.tagthiscar.graph;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.frames.Incidence;
import com.tinkerpop.frames.Property;
import com.tinkerpop.frames.VertexFrame;

/**
 * @author asharma
 */
public interface Site extends VertexFrame {

    @Property("name")
    public String getName();

    @Property("type")
    public String getType();

    @Property("age")
    public Integer getAge();

   // @Adjacency(label="battled")
   // public Iterable<Site> getLocations();

    /*@Adjacency(label="knows")
    public void addKnowsPerson(final Site person);*/

    //@GremlinGroovy("it.out('knows').out('knows').dedup") //Make sure you use the GremlinGroovy module! #1
   // public Iterable<Site> getFriendsOfAFriend();

    @Incidence(label = "battled", direction = Direction.OUT)
    public Iterable<Battled> getBattledInfo();
}
