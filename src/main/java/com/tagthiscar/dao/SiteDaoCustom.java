package com.tagthiscar.dao;

import com.tagthiscar.domain.Search;
import com.tagthiscar.domain.Site;

import java.util.List;

/**
 * @author asharma
 * This is not a Spring Data Repository. see SiteDao for how this is hooked up.
 */

public interface SiteDaoCustom  {

    public List<Site> findSites(Search search);

    public List<String> getDistinctFlagTypes();

}