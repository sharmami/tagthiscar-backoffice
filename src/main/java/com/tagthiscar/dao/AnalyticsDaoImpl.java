package com.tagthiscar.dao;

import org.springframework.stereotype.Repository;

@Repository
public class AnalyticsDaoImpl implements AnalyticsDao {


    @Override
    public void dummy(String userId, String url) {
        System.out.println("AnalyticsDaoImpl. dummy method");
    }
}
