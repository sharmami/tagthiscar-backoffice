package com.tagthiscar.dao;

import com.mongodb.DBCollection;
import com.tagthiscar.domain.Search;
import com.tagthiscar.domain.Site;
import org.apache.commons.validator.GenericValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.geo.Distance;
import org.springframework.data.mongodb.core.geo.Point;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.util.StopWatch;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Repository
public class SiteDaoImpl implements SiteDaoCustom {

    @Autowired
    private MongoOperations mongoTemplate;

    @Override
    public List<Site> findSites(Search search) {
        //Boolean skipDate = false;
        final Double MILES = 69.0d;
        //System.out.println(" Search params: lat : "+search.getLatitude()+" long: "+search.getLongitude()+" "+search.getValue()+" metroc: "+search.getMetric()+" searcjterm: "+search.getSearchTerm());
        int numberOfDays = -1;

        Query query4 = new Query();
        if(!GenericValidator.isBlankOrNull(search.getLatitude())
                /*&& GenericValidator.isDouble(search.getLongitude())*/
                && !(search.getLatitude().equalsIgnoreCase("0")) ){
            Point point = new Point(Double.parseDouble(search.getLongitude()),Double.parseDouble(search.getLatitude()));
            Distance distance = new Distance(search.getValue(),search.getMetric());//TODO is this needed?
            query4.addCriteria(new Criteria("location.coordinates").near(point).maxDistance(search.getValue()/MILES));
            //System.out.println(" Lat Long exists. CRITERIA 1 ");
        } else{
            // lat long doesnt exist. try search term.
            if(!GenericValidator.isBlankOrNull(search.getSearchTerm())){
                String searchTerm = search.getSearchTerm();
                Criteria wildSearchCriteria = Criteria.where("location.address").exists(true)
                        .orOperator(
                                Criteria.where("location.address").regex(searchTerm,"i"),
                                Criteria.where("location.address1").regex(searchTerm,"i"),
                                Criteria.where("location.city").regex(searchTerm,"i"),
                                Criteria.where("description").regex(searchTerm,"i"),
                                Criteria.where("teaser").regex(searchTerm,"i")
                        );
                query4.addCriteria(wildSearchCriteria);
                //System.out.println(" wildcard search exists. CRITERIA 2 ");
            }
        }

        // if there are any flagTypes.
        //System.out.println(" flagtypes : "+search.getCsvFlagTypes());
        if(!GenericValidator.isBlankOrNull(search.getCsvFlagTypes())){
            String[] flagTypes = search.getCsvFlagTypes().split(",");
            if(flagTypes.length>0){
                query4.addCriteria(Criteria.where("flagTypes").in(flagTypes));
                //System.out.println("flagTypes exists. CRITERIA 3 flagTypes: "+ Arrays.toString(flagTypes));
            }
        }

        //10-Most-Congested-Cities, make sure to ignore number of days or miles etc.
        if(!GenericValidator.isBlankOrNull(search.getLabels())){
            numberOfDays = -10000;   // set it really high.
            String[] labels = search.getLabels().split(",");
            if(labels.length>0){
                query4.addCriteria(Criteria.where("labels.name").in(labels));
                //System.out.println("flagTypes exists. CRITERIA 4 labels: "+ Arrays.toString(labels));
            }
        }


        if(search.getNoOfDays()!=null){
            numberOfDays = search.getNoOfDays().intValue();
            //numberOfDays = -30;
            //System.out.println(" Number of days : "+numberOfDays);
        }
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, numberOfDays);
        Date numberOfDaysAgo = cal.getTime();

        if(!search.getSkipDate()){
            //System.out.println(" Not skipping date");
            query4.addCriteria(Criteria.where("submittedDate").gt(numberOfDaysAgo).lte(new Date()));
        }

        //System.out.println(" Sort By: "+search.getSortBy());

        //sort by submittedDate
        query4.with(new Sort(Sort.Direction.DESC, search.getSortBy()));  // sort by submitted date
       // query4.with(new Sort(Sort.Direction.ASC, "location.address")); // sort by addreess
       //query4.with(new Sort(Sort.Direction.DESC, "submittedDate"));  // sort by submitted date
        //query4.with(new Sort(Sort.Direction.DESC, "id"));  // sort by submitted date
        //query4.with(new Sort(Sort.Direction.DESC, "submittedDate"));

        int limit = 25;
        if(!search.getSkipLimit() && search.getLimit()!=null){
            limit = search.getLimit().intValue();
            query4.limit(limit);
        }

        //System.out.println("query4 - " + query4.toString());

        StopWatch stopWatch = new StopWatch("result");
        stopWatch.start("mainquery");

        try{
           /*List<Site> result = mongoTemplate.find(query4, Site.class);
            System.out.println("  result : "+result.size());

            stopWatch.stop();
            System.out.println(stopWatch.prettyPrint());
            return result;*/

            return mongoTemplate.find(query4, Site.class);
        }catch (Exception e){
            //System.out.println(" HERE : 1 ERROR");
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public List<String> getDistinctFlagTypes() {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        DBCollection sites = mongoTemplate.getCollection("site");
        List<String> flagTypes = sites.distinct("flagTypes");
        Collections.sort(flagTypes);
        stopWatch.stop();
        stopWatch.prettyPrint();
        return flagTypes;
    }



}
