package com.tagthiscar.dao;

import com.tagthiscar.domain.Subscription;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * @author asharma
 */
@Repository
public interface SubscriptionDao extends PagingAndSortingRepository<Subscription, String> {


}