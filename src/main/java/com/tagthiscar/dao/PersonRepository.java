package com.tagthiscar.dao;

import com.tagthiscar.domain.Person;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.geo.Distance;
import org.springframework.data.mongodb.core.geo.GeoResults;
import org.springframework.data.mongodb.core.geo.Point;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonRepository extends PagingAndSortingRepository<Person, String> { 
	
	List<Person> findByName(String name);
	
	//Just equip your method signature with a Pageable parameter and let the method return a Page instance 
	// and we will automatically page the query accordingly.
	Page<Person> findByName(String name, Pageable pageable);
	
	//Person findByShippingAddresses(Address address);
	
	// try these below. if you dont want to use the query name for figuring stuff out. 
	//explicitly specify it using org.springframework.data.mongodb.repository.Query	
	//@Query("{ 'firstname' : ?0 }")
 	//List<Person> findByThePersonsFirstname(String firstname);
 	
 	// Define what can be returned below.
 	//@Query(value="{ 'firstname' : ?0 }", fields="{ 'firstname' : 1, 'lastname' : 1}")
    //List<Person> findByThePersonsFirstname(String firstname);

    // {'geoNear' : 'location', 'near' : [x, y] }
    GeoResults<Person> findByLocationNear(Point location);

    // No metric: {'geoNear' : 'person', 'near' : [x, y], maxDistance : distance }
    // Metric: {'geoNear' : 'person', 'near' : [x, y], 'maxDistance' : distance,
    // 'distanceMultiplier' : metric.multiplier, 'spherical' : true }
    //List<Person> findByLocationNear(Point location, Distance distance);
    GeoResults<Person> findByLocationNear(Point location, Distance distance);

    // { 'location' : { '$near' : [point.x, point.y], '$maxDistance' : distance}}
    //List<Person> findByLocationNear(Point location, Distance distance);

}