package com.tagthiscar.dao;

import com.tagthiscar.domain.Label;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * @author asharma
 */
@Repository
public interface LabelDao extends PagingAndSortingRepository<Label, String> {



}