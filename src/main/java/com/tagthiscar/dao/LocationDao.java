package com.tagthiscar.dao;

import com.tagthiscar.domain.Location;
import com.tagthiscar.domain.Site;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.geo.Box;
import org.springframework.data.mongodb.core.geo.Distance;
import org.springframework.data.mongodb.core.geo.Point;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author asharma
 */
@Repository
public interface LocationDao extends PagingAndSortingRepository<Location, String> {

    List<Location> findByCoordinatesNear(Point point, Distance distance);

    List<Location> findByCoordinatesWithin(Box box);

     List<Location> findBySites(List<Site> sites);

    List<Location> findByCoordinates(double[] coordinates);

    Page<Location> findByAddress(String address, Pageable pageable);

    Page<Location> findByCoordinatesNear(Point point, Distance distance,Pageable pageable );

}