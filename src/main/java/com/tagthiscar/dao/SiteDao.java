package com.tagthiscar.dao;

import com.tagthiscar.domain.Site;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.geo.Box;
import org.springframework.data.mongodb.core.geo.Distance;
import org.springframework.data.mongodb.core.geo.Point;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author asharma
 */
@Repository
public interface SiteDao extends PagingAndSortingRepository<Site, String>, SiteDaoCustom {

    //List<Site> findByCoordinatesNear(Point point, Distance distance);

    List<Site> findByDescriptionIgnoreCaseContaining(String description);

    List<Site> findByGuid(String guid);

    Page<Site> findPageByDescriptionIgnoreCaseContaining(String description, Pageable pageable);

    Page<Site> findPageByLocationAddressIgnoreCaseContaining(String address, Pageable pageable);

    Page<Site> findPageByLocationCityIgnoreCaseContaining(String address, Pageable pageable);


    Page<Site> findAll(Pageable pageable);

    Page<Site> findByLocationAddress(String address,Pageable pageable);

    Page<Site> findByFlagTypes(String[] flagTypes,Pageable pageable);

    List<Site> findByLocationCoordinatesNearAndFlagTypes(Point point, Distance distance,String[] flagTypes,Pageable pageable);

    Page<Site> findByLocationCoordinatesNear(Point point, Distance distance,Pageable pageable);

    Page<Site> findByLocationCoordinatesWithin(Box box, Pageable pageable);



}