package com.tagthiscar.dao;

import com.tagthiscar.backoffice.domain.FeedSource;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * @author asharma
 */
@Repository
public interface FeedSourceDao extends PagingAndSortingRepository<FeedSource, String> {

    //List<Site> findByCoordinatesNear(Point point, Distance distance);

}