package com.tagthiscar.dao;

import com.tagthiscar.domain.UserStats;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author asharma
 */
@Repository
public interface UserStatsDao extends PagingAndSortingRepository<UserStats, String> {

                      public List<UserStats> findByIpAddress(String ipAddress);

                      public List<UserStats>  findByUsername(String username);
}