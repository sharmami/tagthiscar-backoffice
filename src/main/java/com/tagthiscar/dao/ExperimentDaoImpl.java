package com.tagthiscar.dao;

import com.tagthiscar.domain.Account;
import com.tagthiscar.domain.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.index.GeospatialIndex;
import org.springframework.data.mongodb.core.index.Index;
import org.springframework.data.mongodb.core.index.Index.Duplicates;
import org.springframework.data.mongodb.core.index.IndexInfo;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Order;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;
import static org.springframework.data.mongodb.core.query.Update.update;

@Repository
public class ExperimentDaoImpl implements ExperimentDao {

	
	// inject the actual template
	/*@Autowired
	private RedisTemplate<String, String> redisTemplate;*/
	
	// inject the template as ListOperations
	//@Autowired
	//private ListOperations<String, String> listOps;
	
	public void addLink(String userId, String url) {
		System.out.println("AnalyticsDaoImpl: userId : "+userId+" url:"+url);
		//listOps = redisTemplate.opsForList();
		//listOps.leftPush(userId, /*url.toExternalForm()*/ url);
	}
	
	/**
	 * You can use callbacks as well while you interact with Redis.
	 */
	public void useCallback() {
		
		/*redisTemplate.execute(new RedisCallback<Object>() {
			public Object doInRedis(RedisConnection connection)
					throws DataAccessException {
				Long size = connection.dbSize();
				return size;
				
			}
		});*/
	}
	
	
	/*  Mongodb play starts here */
	@Autowired
	MongoOperations mongoOperations;

	public void tryMongo(){
	
		
		System.out.println("AnalyticsDaoImpl : tryMongo : 1 ");
		if (mongoOperations.collectionExists(Person.class)) {
			System.out.println("AnalyticsDaoImpl : tryMongo : 2 ");
			mongoOperations.dropCollection(Person.class);
		}
		
		
		System.out.println("AnalyticsDaoImpl : tryMongo : 3 ");
		mongoOperations.createCollection(Person.class);

		System.out.println("AnalyticsDaoImpl : tryMongo : 4 ");
		Person p = new Person("John", 39);

        p.setLocation(new double[] { -73.99171, 40.738868});//I think this is new york.

		Account a = new Account("1234-59873-893-1", Account.Type.SAVINGS, 123.45D);
		p.getAccounts().add(a);
		
		System.out.println("AnalyticsDaoImpl : tryMongo : 5 ");
		mongoOperations.insert(p);

		System.out.println("AnalyticsDaoImpl : tryMongo : 6 ");
		
		List<Person> results = mongoOperations.findAll(Person.class);
		System.out.println("AnalyticsDaoImpl : Results: " + results);
	
		// Find
		p = mongoOperations.findById(p.getId(), Person.class); 
		System.out.println("AnalyticsDaoImpl :Found: " + p);
	
		
		// Another way to find person
		System.out.println(" REsult by name : "+mongoOperations.findOne(new Query(where("name").is("John")), Person.class));
		System.out.println(" REsult by age: "+mongoOperations.findOne(new Query(where("age").is(39)), Person.class));
		
		// Another set of example: returning a collection of documents.
		//Assuming that we have a number of Person objects with name and age stored as documents in a collection 
		// and that each person has an embedded account document with a balance. We can now run a query using the following code.
		// We recommend using a static import for org.springframework.data.mongodb.core.query.Criteria.where 
		//and Query.query to make the query more readable.
		List<Person> result = mongoOperations.find(query(where("age").lt(50).and("accounts.balance").gt(100.00d)), Person.class);
        System.out.println("AnalyticsDaoImpl : age lt 50 and accounts.balance gt 100.00d is : "+result);
		//mongoOperations.count(query(where("")),Person.class);
		
		// Update
		mongoOperations.updateFirst(query(where("name").is("John")), update("age", 35), Person.class); 
		p = mongoOperations.findOne(query(where("name").is("John")), Person.class); 
		System.out.println("Updated: " + p);

		// Delete
		mongoOperations.remove(p);
		    
		// Check that deletion worked
		List<Person> people = mongoOperations.findAll(Person.class); 
		System.out.println("Number of people = : " + people.size());    
				
		// Another set of example: Use of findAndModify that can update and return the old or the new results.
		
		Person tom = new Person("Tom", 21);
        tom.setLocation(new double[] { -77.42316089999997, 38.999581});
        mongoOperations.insert(tom);

        Person dick = new Person("Dick",22);
        dick.setLocation(new double[] {-77.381212, 39.0148204});
		mongoOperations.insert(dick);

		//mongoOperations.insert(new Person("Harry", 23));
        Person harry = new Person("Harry", 23);
        harry.setLocation(new double[] { -77.38036199999999, 39.03719299999999});
        mongoOperations.insert(harry);

        Person larry = new Person("Larry", 24);
        larry.setLocation(new double[] {-77.38036199999999, 38.71067108271207});
        mongoOperations.insert(larry);

		List<Person> peopleList = mongoOperations.findAll(Person.class); 
		System.out.println("Number of people = : " + peopleList.size());
		
		Query query = new Query(Criteria.where("name").is("Harry"));
		Update update = new Update().inc("age", 1);
		
		/**
		 * findAndModify by default won't return the updated document. You have to explicitly tell it.
		 */
		Person harryo = mongoOperations.findAndModify(query, update,Person.class);
		// return's old dirty harry person object
		System.out.println(" Updating Harry's age to 24 but this should return old result: "+ harryo);
		//assertThat(harryo.getName(), is("Harry"));
		//assertThat(harryo.getAge(), is(23));
		
		Person harry1 = mongoOperations.findOne(query, Person.class); 
		System.out.println(" What is harry1 : "+harry1);
		System.out.println(" What is harry1's age : "+harry1.getAge());
		//assertThat(harry1.getAge(), is(24));
		
		/**
		 * Here is how i am explicitly teling findAndModify to return updated record.
		 * //The FindAndModifyOptions lets you set the options of returnNew, upsert, and remove
		 */
		// Now return the newly updated document when updating
		Person harryNew = mongoOperations.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), Person.class); 
		// return new
		//assertThat(harryNew.getAge(), is(25));
		
		/*
		* The FindAndModifyOptions lets you set the options of returnNew, upsert, and remove
		* An example that uses returnNew and upsert is below:
		* Which basically means find him and if he is not there add him.
		*/
		Query query2 = new Query(Criteria.where("name").is("Mary"));
		Person mary = mongoOperations.findAndModify(query2, update, new FindAndModifyOptions().returnNew(true).upsert(true), Person.class);
		//assertThat(mary.getName(), is("Mary"));
		//assertThat(mary.getAge(), is(1));
		
		List<Person> peopleListAgain = mongoOperations.findAll(Person.class); 
		System.out.println("Number of people = : " + peopleListAgain.size());
		
		
		
		/*
		 * Create and view Indexes.
		 * 
		 * See all the indexes.
		 * // Answer should look like this : 
		 *	// [IndexInfo [fieldSpec={_id=ASCENDING}, name=_id_, unique=false, dropDuplicates=false, sparse=false],
		 *	//  IndexInfo [fieldSpec={age=DESCENDING}, name=age_-1, unique=true, dropDuplicates=true, sparse=false]]
		 * 
		 */
		// Another set of example: Creating index.
		mongoOperations.indexOps(Person.class).ensureIndex(new Index().on("name",Order.ASCENDING));
		mongoOperations.indexOps(Person.class).ensureIndex(new Index().on("age", Order.DESCENDING).unique(Duplicates.DROP));
        GeospatialIndex geospatialIndex = new GeospatialIndex("location");
        mongoOperations.indexOps(Person.class).ensureIndex(geospatialIndex);

		List<IndexInfo> indexInfoList = mongoOperations.indexOps(Person.class).getIndexInfo();
		System.out.println(" AnalyticsDaoImpl : indexInfoList : "+indexInfoList);
		
		//System.out.println("AnalyticsDaoImpl : tryMongo : Person Collection Dropped!");
		//mongoOperations.dropCollection("person");
		// OR you can say just the class
		//mongoOperations.dropCollection(Person.class);
		
	}

}
