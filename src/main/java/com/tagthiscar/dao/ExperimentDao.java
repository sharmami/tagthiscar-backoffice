package com.tagthiscar.dao;

/**
 * @author asharma
 */
public interface ExperimentDao {

    public void addLink(String userId, String url);

    public void tryMongo();

}
