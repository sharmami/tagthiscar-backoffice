package com.tagthiscar.dao;

import com.tagthiscar.domain.Vehicle;
import org.springframework.data.mongodb.core.geo.Distance;
import org.springframework.data.mongodb.core.geo.Point;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author asharma
 */
@Repository
public interface VehicleDao extends PagingAndSortingRepository<Vehicle, String> {


}