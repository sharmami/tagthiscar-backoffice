package com.tagthiscar.service;

import com.tagthiscar.domain.Location;
import com.tagthiscar.domain.Search;
import com.tagthiscar.domain.Vehicle;

import java.util.Map;

/**
 * @author asharma
 */
public interface VehicleService {

    Vehicle findById(String vehicleId);

}
