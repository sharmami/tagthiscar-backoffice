package com.tagthiscar.service;

import com.mongodb.gridfs.GridFSDBFile;

import java.io.InputStream;
import java.util.List;

/**
 * @author asharma
 */
public interface ExperimentService {


    public void tryMongo();

    void someDummyData();

    public void addLink(String userId, String url);

    String save(InputStream inputStream, /*String contentType, */String filename,String meta1, String meta2);

    GridFSDBFile get (String id);

    GridFSDBFile getByFilename (String filename);

    public List<GridFSDBFile> findAll();



    public String getFileIdByFilename(String filename);

    public void deleteLocalhostStats();

}
