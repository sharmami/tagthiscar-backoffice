package com.tagthiscar.service;

import com.tagthiscar.domain.Subscription;
import com.tagthiscar.domain.UserStats;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @author asharma
 */
public interface AnalyticsService {

    public void subscribe(String email);

    public Iterable<Subscription> getSubscriptions();

    public void addToUserStats(UserStats userStats);

    public Iterable<UserStats> getUserStats();

    public Page<UserStats> findAll(Pageable pageable);
}
