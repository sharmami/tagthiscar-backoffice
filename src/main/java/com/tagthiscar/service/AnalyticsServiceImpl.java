package com.tagthiscar.service;

import com.tagthiscar.dao.SubscriptionDao;
import com.tagthiscar.dao.UserStatsDao;
import com.tagthiscar.domain.Subscription;
import com.tagthiscar.domain.UserStats;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author asharma
 */
@Service
public class AnalyticsServiceImpl implements AnalyticsService {

    @Autowired
    SubscriptionDao subscriptionDao;

    @Autowired
    UserStatsDao userStatsDao;

    @Override
    public void subscribe(String email) {
        Subscription subscription = new Subscription();
        subscription.setEmailAddress(email);
        subscription.setSubmittedDate(new Date());
        subscriptionDao.save(subscription);
    }

    @Override
    public Iterable<Subscription> getSubscriptions() {
        return subscriptionDao.findAll();
    }

    @Override
    public void addToUserStats(UserStats userStats) {
        userStatsDao.save(userStats);
    }

    @Override
    public Iterable<UserStats> getUserStats() {
        return userStatsDao.findAll();
    }

    @Override
    public Page<UserStats> findAll(Pageable pageable) {
        return userStatsDao.findAll(pageable);
    }
}
