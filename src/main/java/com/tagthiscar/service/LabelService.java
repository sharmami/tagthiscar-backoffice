package com.tagthiscar.service;

import com.tagthiscar.domain.Label;

/**
 * @author asharma
 */
public interface LabelService {

    Iterable<Label> findAll();

    Label findById(String id);

    public void delete(String id);

    public Label save(Label label);

}
