package com.tagthiscar.service;

import com.tagthiscar.backoffice.domain.Feed;
import com.tagthiscar.dao.LocationDao;
import com.tagthiscar.dao.SiteDao;
import com.tagthiscar.dao.VehicleDao;
import com.tagthiscar.domain.Label;
import com.tagthiscar.domain.Location;
import com.tagthiscar.domain.Search;
import com.tagthiscar.domain.Site;
import org.apache.commons.validator.GenericValidator;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.geo.Box;
import org.springframework.data.mongodb.core.geo.Distance;
import org.springframework.data.mongodb.core.geo.Metrics;
import org.springframework.data.mongodb.core.geo.Point;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author asharma
 */
@Service
public class SiteServiceImpl implements SiteService {

    @Autowired
    LocationDao locationDao;

    @Autowired
    VehicleDao vehicleDao;

    @Autowired
    SiteDao siteDao;

    @Override
    public Site findById(String id){
        return siteDao.findOne(id);
    }

    @Override
    public Location findByLocationId(String locationId) {
        return locationDao.findOne(locationId);
    }

    @Override
    public Map<String,Object> findSites(Search search) {

        Map<String,Object> result = new HashMap<String,Object>();

        List<Site> sites = new ArrayList<Site>();
        //Pageable pageable = new PageRequest(0, 4, Sort.Direction.DESC, "updatedDate");

        if(search.getAllPoints()!=null && search.getAllPoints().length>0){
            String[] allPoints = search.getAllPoints();
            Set<Site> set = new HashSet<Site>();// for all parking points that dont duplicate.

            for(int i=0;i<allPoints.length;i++){
                //System.out.println(" Enter Point : "+allPoints[i]);
                String[] latLng = allPoints[i].trim().split(",");

                Point pointy = new Point(Double.parseDouble(latLng[1].trim()),Double.parseDouble(latLng[0].trim())); //Long first, lat later.
                Search pointSearch = search;
                pointSearch.setLatitude(latLng[0].trim());
                pointSearch.setLongitude(latLng[1].trim());
                pointSearch.setValue(0.25);
                pointSearch.setSkipLimit(true);
                set.addAll(siteDao.findSites(pointSearch));

                if((i+1)==allPoints.length){
                    //System.out.println(" I aM at the end of hte list.");
                }   else{
                    //System.out.println(" Enter PointTwo : "+allPoints[i+1]);

                    /*String[] latLngNext = allPoints[i+1].trim().split(",");
                    Point pointTwo = new Point(Double.parseDouble(latLngNext[1].trim()),Double.parseDouble(latLngNext[0].trim())); //Long first, lat later.
                    Box box = new Box(pointy,pointTwo);
                    Page<Site> boxSite = siteDao.findByLocationCoordinatesWithin(box,pageable);
                    System.out.println(" boxies gave me : sites : "+boxSite.getContent().size());
                    set.addAll(boxSite.getContent());*/
                }

                // get the parking tags
                if(search.getParkingEnabled()){

                    Search parkingSearch = new Search()/*pointSearch*/;//grab everything from search first.
                    parkingSearch.setLongitude(pointSearch.getLongitude());
                    parkingSearch.setLatitude(pointSearch.getLatitude());
                    parkingSearch.setSkipLimit(true);
                    parkingSearch.setValue(0.15); //0.15
                    parkingSearch.setCsvFlagTypes("parking-meter-export");// just look for parking tags.
                    parkingSearch.setSkipDate(true);
                    set.addAll(siteDao.findSites(parkingSearch));
                }
            }
            //System.out.println(" Set size : "+set.size());
            //sites.addAll(new ArrayList<Site>(set));

            sites = new ArrayList<Site>(set);

        } else{
            sites = siteDao.findSites(search);

            // get the parking tags
            if(search.getParkingEnabled()){
                Search parkingSearch = search;//grab everything from search first.
                parkingSearch.setCsvFlagTypes("parking-meter-export");// just look for parking tags.
                parkingSearch. setSkipDate(true);
                sites.addAll(siteDao.findSites(parkingSearch));
            }

        }

        //Set<Site> set = new HashSet<Site>(sites);
        //System.out.println(" DONE 3 -------- sites  size as set: "+set.size());
        result.put("automotive",/*new ArrayList<Site>(set)*/sites);

        // this is used for the analysis.
        Map<String,Object> analysis = new HashMap<String,Object>();
        analysis.put("totalReports",/*set.size()*/sites.size());

        //labels
        List<Label> labels = new ArrayList<Label>();

        Set<String> locations = new HashSet<String>();
        Set<String> duplicateLocationCoordinates = new HashSet<String>();

        //group all the categories.
            for(Site site: sites)
            {
                if(site.getFlagTypes()!=null)
                {
                   String coordinates =  Arrays.toString(site.getLocation().getCoordinates());
                   if(!locations.contains(coordinates)){
                        locations.add(coordinates);
                    }   else{
                        duplicateLocationCoordinates.add(site.getLocation().getLatitude()+", "+site.getLocation().getLongitude());
                    }


                    for(String flagType : site.getFlagTypes())
                    {
                        if(!analysis.containsKey(flagType)){
                            analysis.put(flagType,new Integer(1));
                        } else {
                            Integer existingValue = (Integer) analysis.get(flagType);
                            analysis.put(flagType,new Integer(existingValue.intValue()+1));
                        }
                    }

                    // all labels
                    if(site.getLabels()!=null && !site.getLabels().isEmpty()){
                        for(Label label : site.getLabels()){
                            if(!labels.contains(label)){
                                labels.add(label);
                            }
                        }
                    }

                }
            }

        analysis.put("totalLocations",locations.size());
        result.put("duplicateLocationCoordinates",duplicateLocationCoordinates);
        result.put("analysis",analysis);
        result.put("labels",labels);

        return result;


    }

    @Override
    public Map<String, Object> findTagsOrSites(Search search) {
        Map<String,Object> result = new HashMap<String,Object>();
        List<Location> allTagsAndSites = new ArrayList<Location>();
        Pageable pageable = new PageRequest(0, 25, Sort.Direction.DESC, "updatedDate");
        if(search.getAllPoints()!=null && search.getAllPoints().length>0){

            String[] allPoints = search.getAllPoints();

            for(int i=0;i<allPoints.length;i++){
                //System.out.println(" Enter Point : "+allPoints[i]);
                String[] latLng = allPoints[i].trim().split(",");
                Point pointy = new Point(Double.parseDouble(latLng[1].trim()),Double.parseDouble(latLng[0].trim())); //Long first, lat later.
                Page<Location> locationPage = locationDao.findByCoordinatesNear(pointy,new Distance(0.1, Metrics.KILOMETERS),pageable);
                List<Location> locations = locationPage.getContent();
                //System.out.println(" points gave me : locations : "+locations.size());
                allTagsAndSites.addAll(locations);

                if((i+1)==allPoints.length){
                    //System.out.println(" I aM at the end of hte list.");
                }   else{
                    //System.out.println(" Enter PointTwo : "+allPoints[i+1]);
                    String[] latLngNext = allPoints[i+1].trim().split(",");
                    Point pointTwo = new Point(Double.parseDouble(latLngNext[1].trim()),Double.parseDouble(latLngNext[0].trim())); //Long first, lat later.
                    Box box = new Box(pointy,pointTwo);
                    List<Location> boxies = locationDao.findByCoordinatesWithin(box);
                    //System.out.println(" boxies gave me : locations : "+boxies.size());
                    allTagsAndSites.addAll(boxies);
                }

            }
        }
        else{
            //Geo Queries
            //TODO capture what is being searched for somewhere.
            //System.out.println("SiteServiceImpl : Longitude: "+search.getLongitude()+" Latitude: "+search.getLatitude()+" term: "+search.getSearchTerm()+" address:"+search.getAddress());
            Point point = new Point(Double.parseDouble(search.getLongitude()),Double.parseDouble(search.getLatitude()));
            //Distance distance = new Distance(1, Metrics.KILOMETERS);
            Distance distance = new Distance(search.getValue(),search.getMetric());

            // this is used for drawing the map.
            StopWatch stopWatch = new StopWatch("Mywatch");

            stopWatch.start("start coordinates check");
            //allTagsAndSites = locationDao.findByCoordinatesNear(point,distance);
            Page<Location> locationPage = locationDao.findByCoordinatesNear(point,distance,pageable);
            /*List<Location> locations*/ allTagsAndSites = locationPage.getContent();
            stopWatch.stop();
            //long endTime = System.currentTimeMillis();
            //System.out.println("That took " + (endTime - startTime) + " milliseconds");

            // let users search for tags by entering them in the address bar
            if(search.getSearchTerm()!=null){
                stopWatch.start("find by description");
                List<Site> sites = siteDao.findByDescriptionIgnoreCaseContaining(search.getSearchTerm());
                for(Site site : sites){
                    System.out.println(" Site found "+site.getId()+" "+site.getTeaser());
                    Point wildcardPoint = new Point(site.getLocation().getLongitude(),site.getLocation().getLatitude());
                    Distance wildcardDistance = new Distance(0, Metrics.MILES); // dont search for anything nearby.
                    allTagsAndSites.addAll(locationDao.findByCoordinatesNear(wildcardPoint, wildcardDistance));
                }
                stopWatch.stop();
            }

            //System.out.println("RESULT::  "+stopWatch.prettyPrint());

        }

        //Collections.sort(allTagsAndSites, new BeanComparator());

        //remove duplicates
        Set<Location> set = new HashSet<Location>(allTagsAndSites);
        result.put("automotive",/*allTagsAndSites*/new ArrayList<Location>(set));// this could be tags or sites.

        // this is used for the analysis.
        Map<String,Object> analysis = new HashMap<String,Object>();
        analysis.put("totalReports",set.size());

        //group all the categories.
        for(Location location: set)
        {
            for(Site site: location.getSites())
            {
                if(site.getFlagTypes()!=null)
                {
                    for(String flagType : site.getFlagTypes())
                    {
                        //String flagType = location.getFlagType();
                        if(!analysis.containsKey(flagType)){
                            analysis.put(flagType,new Integer(1));
                        } else {
                            Integer existingValue = (Integer) analysis.get(flagType);
                            analysis.put(flagType,new Integer(existingValue.intValue()+1));
                        }
                    }
                }
            }

        }

        result.put("analysis",analysis);

        return result;
    }

    @Override
    public Site addTag(String address, String address1, String city, String state, String country, String zip, double latitude, double longitude, String[] flagTypes, String teaser, String submittedBy, String description, String lanesImpacted, String problemFixed, String sidesImpacted, String trafficImpacted, Integer noOfLanesImpacted, Date date) {
        Location location = saveLocation(address,address1, city, state, country, zip, latitude,longitude);
        Site site = saveSite(flagTypes,teaser,submittedBy,description,lanesImpacted,problemFixed,sidesImpacted,trafficImpacted,noOfLanesImpacted,date);
        site.setLocation(location);
        siteDao.save(site);
        location.getSites().add(site);
        locationDao.save(location);
        return site;
    }

    @Override
    public Site addSite(Site site) {
       return siteDao.save(site);
    }

    private Location saveLocation(String address,String address1, String city, String state, String country, String zip, double latitude, double longitude){
        Location location = new Location();
        location.setId(ObjectId.get().toString());
        location.setCoordinates(new double [] {longitude,latitude});
        location.setAddress(address);
        location.setAddress1(address1);
        location.setCity(city);
        location.setState(state);
        location.setCountry(country);
        location.setZip(zip);

        locationDao.save(location);
        return location;
    }

    private Site saveSite(String[] flagTypes,
                          String teaser,
                          String submittedBy,
                          String description,
                          String lanesImpacted,
                          String problemFixed,
                          String sidesImpacted,
                          String trafficImpacted,
                          Integer noOfLanesImpacted,
                          Date date
    ){

        Site site = new Site();
        site.setId(ObjectId.get().toString());
        site.setFlagTypes(flagTypes);
        site.setTeaser(teaser);
        site.setSubmittedBy(submittedBy);
        site.setDescription(description);
        site.setLanesImpacted(lanesImpacted); //All / left / right
        site.setProblemFixed(problemFixed);//No or yes or partially or other or anything.
        site.setSidesImpacted(sidesImpacted);//Same or OTHER or both
        site.setTrafficImpacted(trafficImpacted);// Severe / Major / Medium/ Minor
        site.setNoOfLanesImpacted(noOfLanesImpacted); // 1, 2, 3 or 0.
        site.setSubmittedDate(date);
        siteDao.save(site);
        return site;
    }


    @Override
    public Site saveOrUpdateTag(Feed feed) {

        //Location location = saveOrUpdateLocation(feed.getRawSite().getLocation());
        Location location;
        List<Location> locations = locationDao.findByCoordinates(feed.getRawSite().getLocation().getCoordinates());
        if(locations!=null && !locations.isEmpty()){
            //System.out.println(" SiteServiceImpl : found a location : ");
            location = locations.get(0);
            //System.out.println(" SiteServiceImpl : found a location : id : "+location.getId());

        }  else{
            location = saveOrUpdateLocation(feed.getRawSite().getLocation());
            //System.out.println(" SiteServiceImpl : cant find an existing one. ");
        }

        Site site = saveOrUpdateSite(feed.getRawSite());
        site.setLocation(location);
        siteDao.save(site);
        location.getSites().add(site);
        locationDao.save(location);
        return site;

    }

    private Location saveOrUpdateLocation(Location location){


        if(GenericValidator.isBlankOrNull(location.getId())){
           // System.out.println(" saveOrUpdateLocation Location : location : id : is null... give it one.");
            location.setId(ObjectId.get().toString());
        }
        else{
            //System.out.println(" saveOrUpdateLocation Location : location : id : "+location.getId());
        }

        locationDao.save(location);
        return location;

    }

    private Site saveOrUpdateSite(Site site){

        if(GenericValidator.isBlankOrNull(site.getId())){
            //System.out.println(" saveOrUpdateSite : site : id : is null... give it one.");
            site.setId(ObjectId.get().toString());
        }
        else{
            //System.out.println(" saveOrUpdateSite : site : id : "+site.getId());

        }

        siteDao.save(site);
        return site;
    }


    public void deleteSite(String id) {
        Site site = findById(id);
        //locationDao.delete(site.getLocation().getId());// a location can have multiple sites

        Location location = site.getLocation();
        location.getSites().remove(site);
        locationDao.save(location);
        //System.out.println("SiteServiceImpl : deleteSite : Id :  "+site.getId());

        siteDao.delete(id);
       }

    public Iterable<Site> allSites(){
        return siteDao.findAll();
    }


    @Override
    public Site saveOrUpdateParkingInfo(Site site) {


        Location location;
        List<Location> locations = locationDao.findByCoordinates(site.getLocation().getCoordinates());
        if(locations!=null && !locations.isEmpty()){
            //System.out.println(" SiteServiceImpl : found a location : ");
            location = locations.get(0);
            //System.out.println(" SiteServiceImpl : found a location : id : "+location.getId());

        }  else{
            location = saveOrUpdateLocation(site.getLocation());
            //System.out.println(" SiteServiceImpl : cant find an existing one. ");
        }

        //Location location = saveOrUpdateLocation(site.getLocation());

        List<Site> sitesWithGuid = siteDao.findByGuid(site.getGuid());
        if(sitesWithGuid!=null && !sitesWithGuid.isEmpty()){
              System.out.println(" SiteServiceImpl: Site with GUID :"+sitesWithGuid.get(0).getGuid()+" GUID"+site.getGuid()+" already exists. Skipping the whole save");
        }  else{
            site = saveOrUpdateSite(site);

            site.setLocation(location);
            siteDao.save(site);
            location.getSites().add(site);
            locationDao.save(location);
        }

        return site;

    }


    @Override
    public Page<Site> findAll(Pageable pageable) {
        return siteDao.findAll(pageable);
    }


    @Override
    public List<String> getDistinctFlagTypes() {
        return siteDao.getDistinctFlagTypes();
    }

    @Override
    public Site save(Site site) {
        return siteDao.save(site);
    }


}
