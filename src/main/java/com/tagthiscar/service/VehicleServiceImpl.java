package com.tagthiscar.service;

import com.tagthiscar.dao.VehicleDao;
import com.tagthiscar.domain.Vehicle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author asharma
 */
@Service
public class VehicleServiceImpl implements VehicleService {

    @Autowired
    VehicleDao vehicleDao;

    @Override
    public Vehicle findById(String id){
        return vehicleDao.findOne(id);
    }




}
