package com.tagthiscar.service;

import com.tagthiscar.dao.LabelDao;
import com.tagthiscar.domain.Label;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author asharma
 */
@Service
public class LabelServiceImpl implements LabelService {

    @Autowired
    LabelDao labelDao;

    @Override
    public Iterable<Label> findAll() {
        return labelDao.findAll();
    }

    @Override
    public Label findById(String id) {
        return labelDao.findOne(id);
    }

    @Override
    public void delete(String id) {
        labelDao.delete(id);
    }


    @Override
    public Label save(Label label) {
        return labelDao.save(label);
    }
}
