package com.tagthiscar.service;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSFile;
import com.tagthiscar.backoffice.service.FeedSourceService;
import com.tagthiscar.dao.AnalyticsDao;
import com.tagthiscar.dao.ExperimentDao;
import com.tagthiscar.dao.LocationDao;
import com.tagthiscar.dao.PersonRepository;
import com.tagthiscar.dao.SiteDao;
import com.tagthiscar.dao.UserStatsDao;
import com.tagthiscar.dao.VehicleDao;
import com.tagthiscar.domain.Location;
import com.tagthiscar.domain.Person;
import com.tagthiscar.domain.Site;
import com.tagthiscar.domain.UserStats;
import com.tagthiscar.domain.Vehicle;
import org.bson.types.ObjectId;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.geo.Distance;
import org.springframework.data.mongodb.core.geo.GeoResult;
import org.springframework.data.mongodb.core.geo.GeoResults;
import org.springframework.data.mongodb.core.geo.Metrics;
import org.springframework.data.mongodb.core.geo.Point;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * @author asharma
 */
@Service
public class ExperimentServiceImpl implements ExperimentService {

    @Autowired
    private GridFsTemplate gridFsTemplate;

    @Autowired
    FeedSourceService feedSourceService;

    @Autowired
    PersonRepository personRepository;

    @Autowired
    LocationDao locationDao;

    @Autowired
    VehicleDao vehicleDao;

    @Autowired
    SiteDao siteDao;

    @Autowired
    AnalyticsDao analyticsDao;

    @Autowired
    ExperimentDao experimentDao;

    @Autowired
    UserStatsDao userStatsDao;

    //TODO delete this.
    public void someDummyData(){
        prepLocations();
        addTag("broderick drive, ashburn, va",39.0015021,-77.44890579999998,new String[]{"caution"},"AOL Headquarters","amit", " Now some of them are Raytheon buildings. ","right","No","Same","Major",1,new Date());
        addTag("Chantilly, VA, USA",38.8942786,-77.4310992,new String[]{"caution"},"That couch that you're missing","amit", " yeah, you left it in the middle of Route 28 in Chantilly. Hope you weren't planning to use it for anything. ","right","No","Same","Major",1,new Date());

        //"E hh:mm:ss a z MMM dd yyyy"
        //dd/MM/yyyy HH:mm:ss
        DateTimeFormatter formatter = DateTimeFormat.forPattern("E, d MMM y HH:mm:ss z");
        //"E, d MMM y HH:mm:ss z"

        //DateTime dt = formatter.parseDateTime("Sat, 30 Mar 2013 01:24:20 GMT");
        SimpleDateFormat fmt = new SimpleDateFormat("E, d MMM y HH:mm:ss z");
        Date date = null;
        try {
             date = fmt.parse("Sat, 30 Mar 2013 01:24:20 GMT");
        } catch (ParseException e) {
            System.out.println("Date parsing for : Sat, 30 Mar 2013 01:24:20 GMT failed:??");
            e.printStackTrace();
        }
        //Sat, 30 Mar 2013 01:24:20 GMT  , 38.863032,-76.996026
        addTag(null, 38.863032, -76.996026, new String[]{"caution"}, "District of Columbia reports Correction:  The ramp from southbound S", "lcas@loudoun.gov", "District of Columbia reports Correction:  The ramp from southbound S. Capitol St. onto southbound I-295 will be closed next week.  The ramp from southbound I-295 to S. Capitol St. will remain open. Alert Loudoun is powered by Cooper Notification RSAN", "right", "No", "Same", "Major", 1, /*dt.toDate()*/date);
    }

    private void prepLocations()
    {
        locationDao.deleteAll();

        Location location1 = new Location();
        location1.setId(ObjectId.get().toString());
        location1.setCoordinates(new double [] {-77.42316089999997,38.999581});
        location1.setAddress("Stablehouse drive, sterling, va");
        locationDao.save(location1);

        Location location2 = new Location();
        location2.setId(ObjectId.get().toString());
        location2.setCoordinates(new double [] {-77.381212,39.0148204});
        location2.setAddress("calamary circle, sterling, va");
        locationDao.save(location2);

        Location location3 = new Location();
        location3.setId(ObjectId.get().toString());
        location3.setCoordinates(new double [] {-77.38036199999999,39.03719299999999});
        location3.setAddress("sugarland rd, sterling, va");
        locationDao.save(location3);

        //Warrenton, Va
        Location location4 = new Location();
        location4.setId(ObjectId.get().toString());
        location4.setCoordinates(new double [] {-77.7952712,38.7134516});
        location4.setAddress("warrenton, va");
        locationDao.save(location4);

        // Concorde pkwy
        //38.909143,-77.451698
        Location location5 = new Location();
        location5.setId(ObjectId.get().toString());
        location5.setCoordinates(new double []{-77.451698,38.909143});
        location5.setAddress("Concorde pkwy, Chantilly, va");
        locationDao.save(location5);

        // Add vehicles
        vehicleDao.deleteAll();

        //vehicle
        Vehicle vehicle = new Vehicle();
        vehicle.setId(ObjectId.get().toString());
        vehicle.setNamePlate("JLS 3915");
        vehicle.setColor("teal");
        vehicle.setEdition("LE");
        vehicle.setMake("Toyota");
        vehicle.setModel("Corolla");
        vehicle.setType("Sedan");
        vehicle.setSubmittedBy("amit");
        vehicle.setSubmittedDate(new Date());
        vehicle.setUpdatedBy("amit");
        vehicle.setUpdatedDate(new Date());
        vehicle.setVinNumber("VIN1ADASFSSSEQWE42142312312312");
        // this vehicle was spotted at 3 places.
        //vehicle.getLocations().add(location1);
        //vehicle.getLocations().add(location2);
        //vehicle.getLocations().add(location3);
        vehicleDao.save(vehicle);

        //vehicle 1
        Vehicle vehicle1 = new Vehicle();
        vehicle1.setId(ObjectId.get().toString());
        vehicle1.setNamePlate("JRE 8314");
        vehicle1.setColor("black");
        vehicle1.setEdition("CE");
        vehicle1.setMake("Toyota");
        vehicle1.setModel("Corolla");
        vehicle1.setType("Sedan");
        vehicle1.setSubmittedBy("amit");
        vehicle1.setSubmittedDate(new Date());
        vehicle1.setUpdatedBy("amit");
        vehicle1.setUpdatedDate(new Date());
        vehicle1.setVinNumber("VIN2SADASFSEWQEWQEQW42142312312312");
        //vehicle1.getLocations().add(location5);
        /*vehicle1.getSites().add(site1);
        vehicle1.getSites().add(site3);*/
        vehicleDao.save(vehicle1);

        //vehicle 2
        Vehicle vehicle2 = new Vehicle();
        vehicle2.setId(ObjectId.get().toString());
        vehicle2.setNamePlate("XLS 2921");
        vehicle2.setColor("grey");
        vehicle2.setEdition("ME");
        vehicle2.setMake("Toyota");
        vehicle2.setModel("Sienna");
        vehicle2.setType("Van");
        vehicle2.setSubmittedBy("amit");
        vehicle2.setSubmittedDate(new Date());
        vehicle2.setUpdatedBy("amit");
        vehicle2.setUpdatedDate(new Date());
        vehicle2.setVinNumber("VIN3SADASFSEWQEWQEQW42142312312312");
        //vehicle2.getLocations().add(location5);
        vehicleDao.save(vehicle2);

        siteDao.deleteAll();

        Site site1 = new Site();
        site1.setFlagTypes(new String[]{"accident","construction"});
        site1.setTeaser("Accident on Stablehouse Drive");
        site1.setSubmittedBy("amit");
        site1.setSubmittedDate(new Date());
        site1.setDescription(" this was due to negligience of the driver coming from north.");
        site1.setLanesImpacted("All"); //All / left / right
        site1.setProblemFixed("Nope");//No or yes or partially or other or anything.
        site1.setSidesImpacted("Both");//Same or OTHER or both
        site1.setTrafficImpacted("Severe");// Severe / Major / Medium/ Minor
        site1.setNoOfLanesImpacted(null); // 1, 2, 3 or 0.
        site1.getVehicles().add(vehicle); // 3915
        site1.getVehicles().add(vehicle1); // 8314
        site1.setLocation(location1);
        siteDao.save(site1);
        location1.getSites().add(site1);
        locationDao.save(location1);
        vehicle1.getSites().add(site1); // 8314 was spotted at stablehouse and sugarland
        vehicle.getSites().add(site1);// add the backwards relation.
        vehicleDao.save(vehicle1);
        vehicleDao.save(vehicle);

        Site site2 = new Site();
        site2.setFlagTypes(new String[]{"caution","construction"});
        site2.setTeaser("28 S Traffic Jam next to Stablehouse Dr");
        site2.setSubmittedBy("ramesh");
        site2.setSubmittedDate(new Date());
        site2.setDescription(" I see lot of police cars here. dont know whats going on .");
        site2.setLanesImpacted("Left"); //All / left / right
        site2.setProblemFixed("No");//No or yes or partially or other or anything.
        site2.setSidesImpacted("Same");//Same or OTHER or both
        site2.setTrafficImpacted("Major");// Severe / Major / Medium/ Minor
        site2.setNoOfLanesImpacted(2); // 1, 2, 3 or 0.
        site2.setLocation(location1);
        siteDao.save(site2);

        location1.getSites().add(site2);// add 28 S traffic jam to stablehouse.
        locationDao.save(location1);

        Site site3 = new Site();
        //search3.setFlagTypes(new String[]{"construction"});
        site3.setFlagTypes(null);
        site3.setTeaser("Construction going on at Sugarland Dr");
        site3.setSubmittedBy("bob");
        site3.setSubmittedDate(new Date());
        site3.setDescription(" the road is being repaired.");
        site3.setLanesImpacted("Right"); //All / left / right
        site3.setProblemFixed("Yes");//No or yes or partially or other or anything.
        site3.setSidesImpacted("Other");//Same or OTHER or both
        site3.setTrafficImpacted("Medium");// Severe / Major / Medium/ Minor
        site3.setNoOfLanesImpacted(1); // 1, 2, 3 or 0.
        site3.setLocation(location3);
        site3.getVehicles().add(vehicle1); // 8314 was also at sugarland.
        siteDao.save(site3);

        location3.getSites().add(site3);
        locationDao.save(location3);

        vehicle1.getSites().add(site3); // 8314 was spotted at stablehouse and sugarland
        vehicleDao.save(vehicle1);

        Site site4 = new Site();
        site4.setFlagTypes(new String[]{"caution","construction"});
        site4.setTeaser("North bound traffic on Calamary Circle is packed");
        site4.setSubmittedBy("tara");
        site4.setSubmittedDate(new Date());
        site4.setDescription(" something is going on. I cant quite say what is causing this");
        site4.setLanesImpacted("All"); //All / left / right
        site4.setProblemFixed("Dont know");//No or yes or partially or other or anything.
        site4.setSidesImpacted("Both");//Same or OTHER or both
        site4.setTrafficImpacted("Major");// Severe / Major / Medium/ Minor
        site4.setNoOfLanesImpacted(2); // 1, 2, 3 or 0.
        site4.setLocation(location2);
        List<Vehicle> vehicleList2 = new ArrayList<Vehicle>();
        vehicleList2.add(vehicle2);
        site4.setVehicles(vehicleList2);
        siteDao.save(site4);

        location2.getSites().add(site4);
        locationDao.save(location2);

        vehicle2.getSites().add(site4);// add the backwards relation.
        vehicleDao.save(vehicle2);

        Site site5 = new Site();
        site5.setFlagTypes(new String[]{"caution","construction"});
        site5.setTeaser("Intersection of Avion and Concorde pkwy is very slippery");
        site5.setSubmittedBy("amit");
        site5.setSubmittedDate(new Date());
        site5.setDescription("  The ice accumulation is bad at this intersection. there are no snow plows here yet. ");
        site5.setLanesImpacted("right"); //All / left / right
        site5.setProblemFixed("No");//No or yes or partially or other or anything.
        site5.setSidesImpacted("Same");//Same or OTHER or both
        site5.setTrafficImpacted("Major");// Severe / Major / Medium/ Minor
        site5.setNoOfLanesImpacted(1); // 1, 2, 3 or 0.
        site5.setLocation(location5);
        site5.getVehicles().add(vehicle1);  // 8314
        siteDao.save(site5);

        location5.getSites().add(site5);
        locationDao.save(location5);

        Site site6 = new Site();
        site6.setFlagTypes(new String[]{"accident","construction"});
        site6.setTeaser("Stablehouse Drive is very slippery");
        site6.setSubmittedBy("amit");
        site6.setSubmittedDate(new Date());
        site6.setDescription("  The ice accumulation is bad at this intersection. there are no snow plows here yet. ");
        site6.setLanesImpacted("right"); //All / left / right
        site6.setProblemFixed("No");//No or yes or partially or other or anything.
        site6.setSidesImpacted("Same");//Same or OTHER or both
        site6.setTrafficImpacted("Major");// Severe / Major / Medium/ Minor
        site6.setNoOfLanesImpacted(1); // 1, 2, 3 or 0.
        site6.setLocation(location1);
        siteDao.save(site6);
        location1.getSites().add(site6);
        locationDao.save(location1);

    }

    private Site saveSite(String[] flagTypes,
                          String teaser,
                          String submittedBy,
                          String description,
                          String lanesImpacted,
                          String problemFixed,
                          String sidesImpacted,
                          String trafficImpacted,
                          Integer noOfLanesImpacted,
                          Date date
                          ){

        Site site = new Site();
        site.setId(ObjectId.get().toString());
        site.setFlagTypes(flagTypes);
        site.setTeaser(teaser);
        site.setSubmittedBy(submittedBy);
        site.setDescription(description);
        site.setLanesImpacted(lanesImpacted); //All / left / right
        site.setProblemFixed(problemFixed);//No or yes or partially or other or anything.
        site.setSidesImpacted(sidesImpacted);//Same or OTHER or both
        site.setTrafficImpacted(trafficImpacted);// Severe / Major / Medium/ Minor
        site.setNoOfLanesImpacted(noOfLanesImpacted); // 1, 2, 3 or 0.
        site.setSubmittedDate(date);
        siteDao.save(site);
        return site;
    }

    private Location saveLocation(String address,double latitude, double longitude){
        Location location = new Location();
        location.setId(ObjectId.get().toString());
        location.setCoordinates(new double [] {longitude,latitude});
        location.setAddress(address);
        locationDao.save(location);
        return location;
    }

    private void addTag(String address,
                        double latitude,
                        double longitude,
                        String[] flagTypes,
                        String teaser,
                        String submittedBy,
                        String description,
                        String lanesImpacted,
                        String problemFixed,
                        String sidesImpacted,
                        String trafficImpacted,
                        Integer noOfLanesImpacted,
                        Date date){
        Location location = saveLocation(address,latitude,longitude);
        Site site = saveSite(flagTypes,teaser,submittedBy,description,lanesImpacted,problemFixed,sidesImpacted,trafficImpacted,noOfLanesImpacted,date);
        site.setLocation(location);
        siteDao.save(site);
        location.getSites().add(site);
        locationDao.save(location);
    }

    @Override
    public void tryMongo() {
        experimentDao.tryMongo();

        /*
		System.out.println("AdminController : let's try some paging below ---- ");
		// reads first page correctly.
		Page<Person> persons = personRepository.findAll(new PageRequest(0, 10));

		assertThat(persons.isFirstPage(), is(true));
		System.out.println("AdminController : some more functions : 1 : persons.getNumber: "+persons.getNumber());
		System.out.println("AdminController : some more functions : 2 : persons.getNumberOfElements() : "+persons.getNumberOfElements());
		System.out.println("AdminController : some more functions : 3 : persons.getSize() : "+persons.getSize());
		System.out.println("AdminController : some more functions : 4 : persons.getTotalElements() : "+persons.getTotalElements());
		System.out.println("AdminController : some more functions : 5 : persons.getTotalPages() : "+persons.getTotalPages());
		System.out.println("AdminController : some more functions : 6 : persons.getSort() : "+persons.getSort());
		System.out.println("AdminController : some more functions : 7 : persons.hasNextPage() : "+persons.hasNextPage());
		System.out.println("AdminController : some more functions : 8 : persons.hasContent() : "+persons.hasContent());
		System.out.println("AdminController : some more functions : 9 : persons.hasPreviousPage() : "+persons.hasPreviousPage());
		System.out.println("AdminController : some more functions : 10 : persons.isFirstPage() : "+persons.isFirstPage());
		System.out.println("AdminController : some more functions : 11 : persons.isLastPage() : "+persons.isLastPage());

		System.out.println("AdminController : some more functions : 12 : persons.getContent() : "+persons.getContent());

		Iterator<Person> iter = persons.iterator();
		while(iter.hasNext()){
			Person pers = iter.next();
			System.out.println("AdminController :  Iterator : Person  --> : "+pers);
		}
    */

        // some other trials
        //persons = personRepository.findAll(new PageRequest(0, 10));
        System.out.println("AdminController : findByName for Tom: "+(personRepository.findByName("Tom", new PageRequest(0,10))).getContent());
        System.out.println(" AdminController : findByName for Larry  : "+personRepository.findByName("Larry"));

        //Geo Queries
        Point point = new Point(/*43.7, 48.8*/ /*-73.99171, 40.738868*/-77.3905372,39.0467049);
        Distance distance = new Distance(50, Metrics.MILES);


        // {'location' : {'$nearSphere' : [43.7, 48.8], '$maxDistance' : 0.03135711885774796}}
        /*
        List<Person> locations = personRepository.findByLocationNear(point, distance);
        System.out.println("AdminController: locations size: "+locations.size()+" locations: "+locations);
        */

        GeoResults<Person> personLocs = personRepository.findByLocationNear(point, distance);
        Iterator<GeoResult<Person>> iterator = personLocs.iterator();
        while(iterator.hasNext())
        {
            GeoResult<Person> geoPerson = iterator.next();
            //System.out.println("GeoPerson : "+geoPerson.toString());
            //System.out.println("GeoPerson : Distance : Multiplier: "+geoPerson.getDistance().getMetric().getMultiplier());
            //System.out.println("GeoPerson : Distance: Metric: "+geoPerson.getDistance().getMetric());
            System.out.println("GeoPerson : Person: "+geoPerson.getContent().toString());
        }

        /*GeoResults<Person> personLocs = personRepository.findByLocationNear(point);
        Iterator<GeoResult<Person>> iterator = personLocs.iterator();
        while(iterator.hasNext())
        {
            GeoResult<Person> geoPerson = iterator.next();
            //System.out.println("GeoPerson : "+geoPerson.toString());
            System.out.println("GeoPerson : Distance: "+geoPerson.getDistance().getMetric().getMultiplier());
            System.out.println("GeoPerson : Person: "+geoPerson.getContent().toString());
        }*/


    }


    @Override
    public void addLink(String userId, String url) {
        experimentDao.addLink(userId,url);
    }


    @Override
    public String save(InputStream inputStream, /*String contentType,*/ String filename, String meta1, String meta2) {
        //System.out.println("ExperimentServiceImpl : filename : "+filename);
        //System.out.println("ExperimentServiceImpl : contentType : "+contentType);


        /*
        "avi" = "video/x-msvideo";
  "exe" = "application/octet-stream";
  "html" = "text/html";
  "jpg" = "image/jpeg";
  "js" = "application/x-javascript";
  "mp3" = "audio/mpeg";
  "pdf" = "application/pdf";
  "png" = "image/png";
  "txt" = "text/plain";
  "xml" = "application/xml";
  "zip" = "application/zip"
         */
        DBObject metaData = new BasicDBObject();
        metaData.put("meta1", meta1);
        metaData.put("meta2", meta2);

        try{
            GridFSFile file = gridFsTemplate.store(inputStream, filename, "application/xml",metaData);
            return file.getId().toString();
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public GridFSDBFile get(String id) {
        //System.out.println("ExperimentServiceImpl : Finding by ID: " + id);
        return gridFsTemplate.findOne(new Query(Criteria.where("_id").is(new ObjectId(id))));
    }


    @Override
    public GridFSDBFile getByFilename(String filename) {
        //System.out.println("ExperimentServiceImpl : Finding by name: " + filename);
        return gridFsTemplate.findOne(new Query(Criteria.where("filename").is(filename)));

    }

    @Override
    public String getFileIdByFilename(String filename) {
        //System.out.println("ExperimentServiceImpl : Finding by name: " + filename);
        GridFSDBFile file = gridFsTemplate.findOne(new Query(Criteria.where("filename").is(filename)));
        return String.valueOf(file.getId());
    }

    @Override
    public List<GridFSDBFile> findAll() {
        /*List<GridFSDBFile> files = gridFsTemplate.find(null);
        for (GridFSDBFile file: files) {
            System.out.println("EXperimentServiceImpl: file: "+file);
        }
        return files;*/
        return gridFsTemplate.find(null);
    }




    @Override
    public void deleteLocalhostStats() {

        List<UserStats> allStatsList = new ArrayList<UserStats>();

        List<UserStats> userStatsListByUserName = userStatsDao.findByUsername("amit");
        allStatsList.addAll(userStatsListByUserName);

        List<UserStats> adminStats = userStatsDao.findByUsername("admin");
        allStatsList.addAll(adminStats);

        String[] ips = {
                "71.62.92.228, 127.0.0.1",
                "71.62.92.228",
                "98.244.102.116, 127.0.0.1",
                "208.247.58.200, 127.0.0.1",
                "208.247.58.200, 172.30.8.253",
                "98.244.102.116, 172.30.8.253",
                "172.1.152.83, 172.30.8.253",
                "208.247.58.105, 172.30.8.253",
                "173.79.104.176, 172.30.8.253",
                "208.247.58.105",
                "173.72.139.122",
                "172.30.49.27",
                "172.30.49.28",
                "172.30.49.30",
                "172.30.49.32",
                "174.79.189.252",
                "172.30.49.29",
                "208.247.58.200",
                "98.244.102.116",
                "127.0.0.1"
        };

        for(String ipAdd: ips){
            List<UserStats> ipList = userStatsDao.findByIpAddress(ipAdd);
            //System.out.println(" ipList Size: "+allStatsList.size());
            allStatsList.addAll(ipList);
        }

        userStatsDao.delete(allStatsList);

    }
}

