package com.tagthiscar.service;

import com.tagthiscar.backoffice.domain.Feed;
import com.tagthiscar.domain.Location;
import com.tagthiscar.domain.Search;
import com.tagthiscar.domain.Site;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author asharma
 */
public interface SiteService {

    Map<String,Object> findTagsOrSites(Search search);

    Site findById(String siteId);

    Location findByLocationId(String locationId);


    /**
     * @deprecated Use saveOrUpdateTag
     */
    public Site addTag(String address,
                       String address1,
                       String city,
                       String state,
                       String country,
                       String zip,
                       double latitude,
                       double longitude,
                       String[] flagTypes,
                       String teaser,
                       String submittedBy,
                       String description,
                       String lanesImpacted,
                       String problemFixed,
                       String sidesImpacted,
                       String trafficImpacted,
                       Integer noOfLanesImpacted,
                       Date date);

    public Site addSite(Site site);

    public Site saveOrUpdateTag(Feed feed);

    public void deleteSite(String id);

    public Iterable<Site> allSites();

    public Site saveOrUpdateParkingInfo(Site site);

    public Page<Site> findAll(Pageable pageable);

    Map<String,Object> findSites(Search search);

    public List<String> getDistinctFlagTypes();

    public Site save(Site site);


}
