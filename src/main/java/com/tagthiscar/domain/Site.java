package com.tagthiscar.domain;

import com.tagthiscar.util.SiteUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author asharma
 */
@Document
public class Site implements Serializable {

    @Id
    private String id;

    private String guid;//a unique id that is not assigned by mongodb e.g. feeds have something uniqueid about them from other systems.

    @Indexed
    private String teaser;// quick liner that goes on the map.

    private String submittedBy;

    @Indexed
    private Date submittedDate;

    private String updatedBy;

   // @Indexed(direction = IndexDirection.DESCENDING)
    private Date updatedDate;

    @Indexed
    private String[] flagTypes; //accident or caution or construction

    @Indexed
    private String description;

    // # of RIGHT/Left lanes on SAME/BOTH sides of the road.
    private String trafficImpacted; // sever / major / medium / minor

    private Integer noOfLanesImpacted=0;//1 or 2 or 0

    private String lanesImpacted;//All/ Right/ Left

    private String sidesImpacted;// Same/ Both sides.

    //TODO we may want to retire this in favor of status and resolution.
    private String problemFixed; //yes/no/partially

    @Indexed
    private String status; // OPEN

    @Indexed
    private String resolution;//UNKNOWN

    private String travelAlignment; // North, South or East etc.

    private String travelDirection;//both directions, not directional etc.

    private Date incidentValidUntilDate;// how long is the incident supposed to last

    private Date incidentStartDate;//when is it supposed to start. if this is empty than immediately.

    private List<Vehicle> vehicles = new ArrayList<Vehicle>();

    private List<Label> labels = new ArrayList<Label>();

    //@Indexed(direction = IndexDirection.ASCENDING)
    private Location location;

    /**
     * urls that can automatically become sites without becoming feeds must capture the raw content.
     * This fileName will be our key to accessing our raw xml from the grid.
     */
    private String fileName;

    @Transient
    private String csvFlagTypes;

    @Transient
    private String submittedDateStr;

    @Transient
    private String updatedDateStr;


    public Site() {
    }

    public Site(double[] coordinates, String flagType, String teaser){
        super();
        this.teaser = teaser;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    @Override
    public String toString() {
        //return "Site [id=" + id + ",coordinates=" + Arrays.toString(location.getCoordinates()) + ", teaser="+teaser+"]";
        return "Site [id="+id+"]";
    }

    public String getTeaser() {
        return teaser;
    }

    public void setTeaser(String teaser) {
        this.teaser = teaser;
    }

    public String getSubmittedBy() {
        return submittedBy;
    }

    public void setSubmittedBy(String submittedBy) {
        this.submittedBy = submittedBy;
    }

    public Date getSubmittedDate() {
        return submittedDate;
    }

    public void setSubmittedDate(Date submittedDate) {
        this.submittedDate = submittedDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String[] getFlagTypes() {
        return flagTypes;
    }

    public void setFlagTypes(String[] flagTypes) {
        this.flagTypes = flagTypes;
    }

    public String getCsvFlagTypes() {
        return StringUtils.arrayToCommaDelimitedString(flagTypes);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTrafficImpacted() {
        return trafficImpacted;
    }

    public void setTrafficImpacted(String trafficImpacted) {
        this.trafficImpacted = trafficImpacted;
    }

    public String getLanesImpacted() {
        return lanesImpacted;
    }

    public void setLanesImpacted(String lanesImpacted) {
        this.lanesImpacted = lanesImpacted;
    }

    public String getSidesImpacted() {
        return sidesImpacted;
    }

    public void setSidesImpacted(String sidesImpacted) {
        this.sidesImpacted = sidesImpacted;
    }

    public Integer getNoOfLanesImpacted() {
        return noOfLanesImpacted;
    }

    public void setNoOfLanesImpacted(Integer noOfLanesImpacted) {
        this.noOfLanesImpacted = noOfLanesImpacted;
    }

    public String getProblemFixed() {
        return problemFixed;
    }

    public void setProblemFixed(String problemFixed) {
        this.problemFixed = problemFixed;
    }

    public List<Vehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }

    // add a single vehicle
    public void addVehicle(Vehicle vehicle) {
        this.vehicles.add(vehicle);
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getSubmittedDateStr() {
        return SiteUtils.formatDateStr(this.submittedDate);
    }

    public String getUpdatedDateStr() {
        return SiteUtils.formatDateStr(this.updatedDate);
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getTravelDirection() {
        return travelDirection;
    }

    public void setTravelDirection(String travelDirection) {
        this.travelDirection = travelDirection;
    }

    public String getTravelAlignment() {
        return travelAlignment;
    }

    public void setTravelAlignment(String travelAlignment) {
        this.travelAlignment = travelAlignment;
    }

    public Date getIncidentValidUntilDate() {
        return incidentValidUntilDate;
    }

    public void setIncidentValidUntilDate(Date incidentValidUntilDate) {
        this.incidentValidUntilDate = incidentValidUntilDate;
    }

    public Date getIncidentStartDate() {
        return incidentStartDate;
    }

    public void setIncidentStartDate(Date incidentStartDate) {
        this.incidentStartDate = incidentStartDate;
    }

    public List<Label> getLabels() {
        return labels;
    }

    public void setLabels(List<Label> labels) {
        this.labels = labels;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Site)) return false;

        Site site = (Site) o;

        if (!id.equals(site.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}