package com.tagthiscar.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * @author asharma
 */
@Document
public class Subscription {

    @Id
    private String id;

    private Date submittedDate;

    private String emailAddress;

    public Subscription() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public Date getSubmittedDate() {
        return submittedDate;
    }

    public void setSubmittedDate(Date submittedDate) {
        this.submittedDate = submittedDate;
    }

    @Override
    public String toString() {
        //return "Site [id=" + id + ",coordinates=" + Arrays.toString(location.getCoordinates()) + ", teaser="+teaser+"]";
        return "Subscription [email="+emailAddress+", date="+this.submittedDate+"]";
    }
}