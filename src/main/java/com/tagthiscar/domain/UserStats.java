package com.tagthiscar.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * @author asharma
 */
@Document
public class UserStats {

    public enum User {
        ANONYMOUS
    }

    @Id
    private String id;

    private String operatingSystem;// Windows or Mac or Linux

    private Date submittedDate; // when did this happen

    private String sessionId;// browser session

    private String browser;//firefox, chrome or IE

    private String version;// browser version.

    private String ipAddress; // IP

    private String username; // if anyone was logged in

    private String hostName; // domain name of IP.

    private String screenResolution;//screen res.

    private String device;// What was he on?

    private String pageUrl;//where was he in the site?

    private String comments;// what was he looking for.

    public UserStats() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getSubmittedDate() {
        return submittedDate;
    }

    public void setSubmittedDate(Date submittedDate) {
        this.submittedDate = submittedDate;
    }

    /*@Override
    public String toString() {
        return "UserStats [id="+id+", date="+this.submittedDate+" , OS="+operatingSystem+", sessionId = "+sessionId+", browser="+browser+", version="+version+", username="+username+", pageUrl="+pageUrl+", ipAddress="+ipAddress+", hostName="+hostName+", screenResolution="+screenResolution+", comments="+comments+" ]";
    }*/

    public String getOperatingSystem() {
        return operatingSystem;
    }

    public void setOperatingSystem(String operatingSystem) {
        this.operatingSystem = operatingSystem;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getScreenResolution() {
        return screenResolution;
    }

    public void setScreenResolution(String screenResolution) {
        this.screenResolution = screenResolution;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getPageUrl() {
        return pageUrl;
    }

    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }

    @Override
    public String toString() {
        return "UserStats{" +
                "id='" + id + '\'' +
                ", operatingSystem='" + operatingSystem + '\'' +
                ", submittedDate=" + submittedDate +
                ", sessionId='" + sessionId + '\'' +
                ", browser='" + browser + '\'' +
                ", version='" + version + '\'' +
                ", ipAddress='" + ipAddress + '\'' +
                ", username='" + username + '\'' +
                ", hostName='" + hostName + '\'' +
                ", screenResolution='" + screenResolution + '\'' +
                ", device='" + device + '\'' +
                ", pageUrl='" + pageUrl + '\'' +
                ", comments='" + comments + '\'' +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserStats)) return false;

        UserStats userStats = (UserStats) o;

        if (!browser.equals(userStats.browser)) return false;
        if (!comments.equals(userStats.comments)) return false;
        if (!device.equals(userStats.device)) return false;
        if (!hostName.equals(userStats.hostName)) return false;
        if (!id.equals(userStats.id)) return false;
        if (!ipAddress.equals(userStats.ipAddress)) return false;
        if (!operatingSystem.equals(userStats.operatingSystem)) return false;
        if (!pageUrl.equals(userStats.pageUrl)) return false;
        if (!screenResolution.equals(userStats.screenResolution)) return false;
        if (!sessionId.equals(userStats.sessionId)) return false;
        if (!submittedDate.equals(userStats.submittedDate)) return false;
        if (!username.equals(userStats.username)) return false;
        if (!version.equals(userStats.version)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + operatingSystem.hashCode();
        result = 31 * result + submittedDate.hashCode();
        result = 31 * result + sessionId.hashCode();
        result = 31 * result + browser.hashCode();
        result = 31 * result + version.hashCode();
        result = 31 * result + ipAddress.hashCode();
        result = 31 * result + username.hashCode();
        result = 31 * result + hostName.hashCode();
        result = 31 * result + screenResolution.hashCode();
        result = 31 * result + device.hashCode();
        result = 31 * result + pageUrl.hashCode();
        result = 31 * result + comments.hashCode();
        return result;
    }
}