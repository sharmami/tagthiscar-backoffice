package com.tagthiscar.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author asharma
 */
@Document
public class Label {

    @Id
    private String id;

    private String guid;

    @Indexed
    private String name;  //e.g. '10-most-congested-cities'

    @Indexed
    private String title;// '10 Most congested cities'

    private String createdBy;

    @Indexed
    private Date createdDate;

    @Indexed
    private String updatedBy;

    private Date updatedDate;

    @Indexed
    private String description;// e.g. 'These are 10 most congested cities'

    @DBRef
    private List<Site> sites = new ArrayList<Site>();// individual sites that will become part of this label.

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Site> getSites() {
        return sites;
    }

    public void setSites(List<Site> sites) {
        this.sites = sites;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Label)) return false;

        Label label = (Label) o;

        if (createdBy != null ? !createdBy.equals(label.createdBy) : label.createdBy != null) return false;
        if (createdDate != null ? !createdDate.equals(label.createdDate) : label.createdDate != null) return false;
        if (description != null ? !description.equals(label.description) : label.description != null) return false;
        if (guid != null ? !guid.equals(label.guid) : label.guid != null) return false;
        if (id != null ? !id.equals(label.id) : label.id != null) return false;
        if (name != null ? !name.equals(label.name) : label.name != null) return false;
        if (title != null ? !title.equals(label.title) : label.title != null) return false;
        if (updatedBy != null ? !updatedBy.equals(label.updatedBy) : label.updatedBy != null) return false;
        if (updatedDate != null ? !updatedDate.equals(label.updatedDate) : label.updatedDate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (guid != null ? guid.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        result = 31 * result + (createdDate != null ? createdDate.hashCode() : 0);
        result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
        result = 31 * result + (updatedDate != null ? updatedDate.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }
}