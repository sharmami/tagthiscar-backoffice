package com.tagthiscar.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author asharma
 */
@Document
public class Location {

    @Id
    private String id;

    @GeoSpatialIndexed(name="location.coordinates")
    private double[] coordinates;

    @Transient
    private double latitude;

    @Transient
    private double longitude;

    private String submittedBy;

    private Date submittedDate;

    private String updatedBy;

    private Date updatedDate;

    @Indexed
    private String address;

    @Indexed
    private String address1;

    @Indexed
    private String city;

    @Indexed
    private String state;

    private String country;

    @Indexed
    private String zip;

    @DBRef
    private List<Site> sites = new ArrayList<Site>();

    private List<Vehicle> tags = new ArrayList<Vehicle>();

    public Location() {
    }

    public Location(double[] coordinates, String flagType, String teaser){
        super();
        this.coordinates = coordinates;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    @Override
    public String toString() {
        return "Location [id=" + id + ",coordinates=" + Arrays.toString(coordinates) + "]";
    }

    public double getLatitude() {
        return coordinates[1];
    }

    public double getLongitude() {
        return coordinates[0];
    }

    public String getSubmittedBy() {
        return submittedBy;
    }

    public void setSubmittedBy(String submittedBy) {
        this.submittedBy = submittedBy;
    }

    public Date getSubmittedDate() {
        return submittedDate;
    }

    public void setSubmittedDate(Date submittedDate) {
        this.submittedDate = submittedDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public double[] getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(double[] coordinates) {
        this.coordinates = coordinates;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Site> getSites() {
        return sites;
    }

    public void setSites(List<Site> sites) {
        this.sites = sites;
    }

    public List<Vehicle> getTags() {
        return tags;
    }

    public void setTags(List<Vehicle> tags) {
        this.tags = tags;
    }


    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Location)) return false;

        Location location = (Location) o;

        if (Double.compare(location.latitude, latitude) != 0) return false;
        if (Double.compare(location.longitude, longitude) != 0) return false;
        if (address != null ? !address.equals(location.address) : location.address != null) return false;
        if (address1 != null ? !address1.equals(location.address1) : location.address1 != null) return false;
        if (city != null ? !city.equals(location.city) : location.city != null) return false;
        if (!Arrays.equals(coordinates, location.coordinates)) return false;
        if (country != null ? !country.equals(location.country) : location.country != null) return false;
        if (id != null ? !id.equals(location.id) : location.id != null) return false;
        if (sites != null ? !sites.equals(location.sites) : location.sites != null) return false;
        if (state != null ? !state.equals(location.state) : location.state != null) return false;
        if (submittedBy != null ? !submittedBy.equals(location.submittedBy) : location.submittedBy != null)
            return false;
        if (submittedDate != null ? !submittedDate.equals(location.submittedDate) : location.submittedDate != null)
            return false;
        if (tags != null ? !tags.equals(location.tags) : location.tags != null) return false;
        if (updatedBy != null ? !updatedBy.equals(location.updatedBy) : location.updatedBy != null) return false;
        if (updatedDate != null ? !updatedDate.equals(location.updatedDate) : location.updatedDate != null)
            return false;
        if (zip != null ? !zip.equals(location.zip) : location.zip != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id != null ? id.hashCode() : 0;
        result = 31 * result + (coordinates != null ? Arrays.hashCode(coordinates) : 0);
        temp = Double.doubleToLongBits(latitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(longitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (submittedBy != null ? submittedBy.hashCode() : 0);
        result = 31 * result + (submittedDate != null ? submittedDate.hashCode() : 0);
        result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
        result = 31 * result + (updatedDate != null ? updatedDate.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (address1 != null ? address1.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (zip != null ? zip.hashCode() : 0);
        result = 31 * result + (sites != null ? sites.hashCode() : 0);
        result = 31 * result + (tags != null ? tags.hashCode() : 0);
        return result;
    }
}