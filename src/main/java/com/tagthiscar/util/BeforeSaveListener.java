package com.tagthiscar.util;

import com.mongodb.DBObject;
import com.tagthiscar.domain.Person;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;

public class BeforeSaveListener extends AbstractMongoEventListener<Person> {
	@Override
	public void onBeforeSave(Person p, DBObject dbo) {
		System.out.println("BeforeSaveListener : ... change values, delete them, whatever ...");
	}
}