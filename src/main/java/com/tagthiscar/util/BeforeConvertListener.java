package com.tagthiscar.util;

import com.tagthiscar.domain.Person;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;

public class BeforeConvertListener extends AbstractMongoEventListener<Person> {
	@Override
	public void onBeforeConvert(Person p) {
		System.out.println("BeforeConvertListener : ... does some auditing manipulation, set timestamps, whatever ...");
	}
}