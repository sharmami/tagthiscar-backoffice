package com.tagthiscar.util;

import com.tagthiscar.domain.UserStats;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Date;
import java.util.Locale;

/**
 * @author asharma
 */
public class SiteUtils {

    /**
     * Format a date into 'Sun 02:44:24 PM EST Feb 17 2013'
     * @param date
     * @return
     */
    public static String formatDateStr(Date date) {
        //TODO  java.util.Date doesnt contain any timezone. maybe store all the dates as Joda DateTime?
        DateTimeFormatter format = DateTimeFormat.forPattern("E hh:mm:ss a z MMM dd yyyy");
        return format.withLocale(new Locale("en","us")).print(new DateTime(date));
    }

    /**
     * prep userStats with the request.
     * @param user
     * @param request
     * @return
     */
    public static UserStats addToUserStats(Principal user, HttpServletRequest request){

        //TODO do css hack check here.

        UserStats userStats = new UserStats();
        if(user!=null){
            //System.out.println(" AnalyticsController: addToUserStats: user: "+user.getName());
            userStats.setUsername(user.getName());
        }
        else{
            //System.out.println(" AnalyticsController : addToUserStats : user is anonymous.");
            userStats.setUsername(UserStats.User.ANONYMOUS.name());
        }

        //is client behind something?
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress != null) {
            userStats.setIpAddress(ipAddress);
        }
        else{
            userStats.setIpAddress(request.getRemoteAddr());
        }

        //System.out.println(" AnalyticsController : Remote Address: "+request.getRemoteAddr());

        if(request.getHeader("X-HOST")!=null){
            userStats.setHostName(request.getHeader("X-HOST"));
        }else{
            userStats.setHostName(request.getRemoteHost());
        }
        //System.out.println(" AnalyticsController : Remote Host: "+request.getRemoteHost());

        userStats.setSessionId(request.getRequestedSessionId());
        //System.out.println(" AnalyticsController : getRequestedSessionId: "+request.getRequestedSessionId());

        //System.out.println(" AnalyticsController : getRequestURL: "+request.getRequestURL());
        //System.out.println(" AnalyticsController : getServerName: "+request.getServerName());


        // Javascript gave me this information
        userStats.setPageUrl(request.getParameter("page_url"));
        //System.out.println("AnalyticsController : Page Url: "+request.getParameter("page_url"));

        userStats.setBrowser(request.getParameter("browser"));
        //System.out.println(" AnalyticsController : Browser : "+request.getParameter("browser"));

        userStats.setVersion(request.getParameter("version"));
        //System.out.println(" AnalyticsController : version : "+request.getParameter("version"));

        userStats.setOperatingSystem(request.getParameter("operating_system"));
        //System.out.println(" AnalyticsController : OS : "+request.getParameter("operating_system"));

        userStats.setScreenResolution(request.getParameter("screen_resolution"));
        //System.out.println(" AnalyticsController : Screen Resolution : "+request.getParameter("screen_resolution"));

        userStats.setSubmittedDate(new Date());
        return userStats;
    }

}
