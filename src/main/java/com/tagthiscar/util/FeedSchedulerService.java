package com.tagthiscar.util;

import com.tagthiscar.backoffice.domain.FeedSource;
import com.tagthiscar.backoffice.service.FeedSourceService;
import com.tagthiscar.backoffice.service.RssService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author asharma
 */
@Service
public class FeedSchedulerService {

    @Autowired
    FeedSourceService feedSourceService;

    @Autowired
    RssService rssService;

    //@Scheduled(/*fixedDelay=5000*/cron = "0 0 7,19 * * ?")
    public void fetch() {
        System.out.println("IS it kicking off at 7 am and 7pm? : "+new Date());
        for(FeedSource feedSource: feedSourceService.findAll()){
            String key = feedSource.getFeedName();
            String value = feedSource.getFeedUrl();
            System.out.println(" FETCHING : "+key+" value: "+value);
            rssService.processFeed(key, value);
        }
        rssService.readMarylandStateIncidents("http://www.chart.state.md.us/rss/ProduceRSS.aspx?Type=TIandRCXML&filter=ALL");
    }


    //@Scheduled(/*fixedDelay=5000*/cron = "0 0 6,18 * * ?")
    public void schedule511FeedsFor7states() {
        System.out.println("IS it kicking off at 6 am and 6pm? : "+new Date());
        rssService.get511feedsFor7States("http://id.carsprogram.org/hub/data/feu-g.xml","ASharma","tagcarHuB","ID");
        rssService.get511feedsFor7States("http://ia.carsprogram.org/hub/data/feu-g.xml","ASharma","tagcarHuB","IA");
        rssService.get511feedsFor7States("http://ky.carsprogram.org/hub/data/feu-g.xml","ASharma","tagcarHuB","KY");
        rssService.get511feedsFor7States("http://la.carsprogram.org/hub/data/feu-g.xml","ASharma","tagcarHuB","LA");
        rssService.get511feedsFor7States("http://me.carsprogram.org/hub/data/feu-g.xml","ASharma","tagcarHuB","ME");
        rssService.get511feedsFor7States("http://mn.carsprogram.org/hub/data/feu-g.xml","ASharma","tagcarHuB","MN");
        rssService.get511feedsFor7States("http://nh.carsprogram.org/hub/data/feu-g.xml","ASharma","tagcarHuB","NH");
    }

    //@Scheduled(/*fixedDelay=5000*/cron = "0 0/45 * * * ?")
    public void scheduleServiceRequestsDc311Feeds() {
        System.out.println("IS it kicking off every 45 minutes: "+new Date());
        rssService.serviceRequestsDc311Feeds("http://data.octo.dc.gov/feeds/src/src_current.xml");
    }

    //@Scheduled(/*fixedDelay=5000*/cron = "0 0/10 * * * ?")
    public void scheduleCaliforniaHighwayPatrolFeeds() {
        System.out.println("IS it kicking off every 10 minutes: "+new Date());
        rssService.californiaHighwayPatrolFeeds("http://media.chp.ca.gov/sa_xml/sa.xml");
    }

}
