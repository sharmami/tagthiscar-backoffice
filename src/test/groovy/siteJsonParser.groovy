/**
 * @author asharma
 */

@Grab('com.xlson.groovycsv:groovycsv:1.0')
import com.xlson.groovycsv.CsvParser
import groovy.json.JsonSlurper

new File("site.json").eachLine {
        def list = new JsonSlurper().parseText(it)
        //println list.teaser

        /*if(list."_id"){
            println list."_id"['$oid']
        } else{
            println "id is null "
        }*/
        //println list.submittedBy

        if(list.submittedDate){
            println list.submittedDate['$date']

            //Timestamp stamp = new Timestamp(System.currentTimeMillis());
            Date date = new Date(list.submittedDate['$date']);
            println "date added: "+date;

        }  else{
            println "submittedDate is null for : ${list.'_id'}"
        }

    if(list.flagTypes){
           println list.flagTypes
            list.flagTypes?.each{
                println it
            }
        }   else{
            println "flagTypes is null for : ${list.'_id'}"
        }

    /*if(list.description){
        println list.description
    }  else{
        println "description is null for : ${list.'_id'}"
    }*/

    //println list.trafficImpacted

        //println list.lanesImpacted
       // println list.sidesImpacted
       // println list.problemFixed

        //println list.location
        //println list.location?.coordinates[1]

}

