/**
 * @author asharma
 */

@Grab('com.xlson.groovycsv:groovycsv:1.0')
import com.xlson.groovycsv.CsvParser
import groovy.json.JsonSlurper

new File("location.csv").withReader {
    def csvFile = CsvParser.parseCsv( it )
    csvFile.each {
        //println it._id
        //println it.coordinates
        //println 'address: '+it.address
       //println 'address1 '+it.address1
        //println  it.city
        //println it.state
        //println it.country
       // println it.zip
      println it.sites
        // Parse the response
      def list = new JsonSlurper().parseText(it.sites)
        // Print them out to make sure
        if(list.size()>0) {
            list.each {
                println 'List: '+it.'$id'
                def id = it.'$id';
                if(id instanceof HashMap){
                    println 'oid: '+id['$oid']
                }
            }
        }
    }
}
