/**
 * @author asharma
 */



import com.thinkaurelius.titan.core.Order
import com.thinkaurelius.titan.core.TitanFactory
import com.thinkaurelius.titan.core.TitanGraph
import com.thinkaurelius.titan.core.TitanKey
import com.thinkaurelius.titan.core.attribute.Geoshape
import com.thinkaurelius.titan.core.util.TitanCleanup
import com.thinkaurelius.titan.example.GraphOfTheGodsFactory
import com.tinkerpop.blueprints.Edge
import com.tinkerpop.blueprints.Vertex
@Grab('com.xlson.groovycsv:groovycsv:1.0')
import com.xlson.groovycsv.CsvParser
import groovy.json.JsonSlurper
import org.bson.types.ObjectId
import org.springframework.util.StopWatch

class JsonParser {

    def parse() {
        TitanGraph g = TitanFactory.open("/home/amit/titan-server-0.4.2/conf/titan-cassandra-es.properties");
        g.shutdown();
        TitanCleanup.clear(g);

        // now load the GraphOfTheGodsFactory again.
        TitanGraph graph = TitanFactory.open("/home/amit/titan-server-0.4.2/conf/titan-cassandra-es.properties");
        //GraphOfTheGodsFactory.load(graph);
        addTagThisCarIndicesToGraph(graph);

	
        StopWatch stopWatch = new StopWatch("mongoload");
        stopWatch.start();

        //create a vertex that will hold all tags.
        Vertex sites = graph.addVertex(null);
	sites.setProperty("tags","tags");

        new File("site.json").eachLine {

            def tag = new JsonSlurper().parseText(it)
            if(tag."_id"){
            }else{
                //println "No ID? skip this tag: "+tag
                return;
            }
            Vertex tagV = null;
            Iterable<Vertex> tags = graph.getVertices("tagId",tag."_id"['$oid']);
            if(tags && tags.iterator().hasNext()){
                println " tag already exists :"+(tag."_id"['$oid'])
                return;
            }

            tagV = getTagAsVertex(tag, graph);
            // add vertex-centric edge.
            Edge lastUpdated = sites.addEdge("lastUpdated",tagV);
            lastUpdated.setProperty("timestamp",((Date)tagV.getProperty("updateDate")).getTime());

	    //test starts
            /*
	    if(tag.updatedDate){
             lastUpdated.setProperty("timestamp",Long.parseLong(tag.updatedDate['$date']));
            }else{
             lastUpdated.setProperty("timestamp",Long.parseLong(tag.submittedDate['$date']));
            }
	    */
            //test ends	
            graph.commit();
		
            //hasLocation
            if(tag.location && tag.location."_id"){
                Vertex locationV = getLocationAsVertex(tag.location,graph)
                if(locationV){
                    graph.addEdge(null,tagV,locationV,"hasLocation");
                    graph.commit();
                }
            }else{
                println "Strange. No location for tag with Id :"+(tag."_id"['$oid'])
            }

            //hasFlag
            tag.flagTypes?.each{
                //println it
                Vertex flagV = getFlagAsVertex(it,graph)
                graph.addEdge(null,tagV,flagV,"hasFlag");
                graph.commit();
            }
            //println " finished: "+tag."_id"['$oid']
        }
        stopWatch.stop();
        println stopWatch.prettyPrint();
	
    }

    def getTagAsVertex(def tag, def graph){
        Vertex vertex = graph.addVertex(null);

        vertex.setProperty("teaser",tag.teaser);
        vertex.setProperty("tagId",tag."_id"['$oid']);
        vertex.setProperty("createdBy",tag.submittedBy?tag.submittedBy:"amit");
        vertex.setProperty("description",tag.description?tag.description:"None");
        vertex.setProperty("direction","");//travelalignment
        vertex.setProperty("hashTag","");
        vertex.setProperty("impact",tag.trafficImpacted?tag.trafficImpacted:"");//MAJOR,MINOR
        vertex.setProperty("numberOfLanesImpacted",0); // how many lanes.
        vertex.setProperty("resolution","");
        vertex.setProperty("status","");

        Date submittedDate = new Date();
        if(tag.submittedDate){
         String epoch1 = tag.submittedDate['$date'];
		 submittedDate = new Date(Long.parseLong(epoch1));
        }

        Date updatedDate = null;
        if(tag.updatedDate){
            String epoch2 = tag.updatedDate['$date'];
            updatedDate =  new Date(Long.parseLong(epoch2));
        }else{
            updatedDate = submittedDate;
        }

        vertex.setProperty("createDate",submittedDate);
        vertex.setProperty("incidentStartDate",updatedDate);
        vertex.setProperty("incidentEndDate",updatedDate);
        vertex.setProperty("updateDate",updatedDate);
        vertex.setProperty("lanesImpacted","");//All/ Right/ Left
        vertex.setProperty("sidesImpacted",""); // Same/ Both sides.
        vertex.setProperty("guid",tag.guid?tag.guid:"");
	graph.commit();
        return vertex;
    }

    def getLocationAsVertex(def location, def graph){

        Iterable<Vertex> locations = graph.getVertices("locationId",location."_id"['$oid']);
        Vertex locationV =  null;
        if(locations && locations.iterator().hasNext()){
            locationV = locations.iterator().next();
            println "location already exists: "+locationV
        }
        else{
            try{
		if(location.coordinates && location.coordinates[0] && location.coordinates[1] && Geoshape.isValidCoordinate(location.coordinates[1],location.coordinates[0])){                
			locationV = graph.addVertex(null);
                	locationV.setProperty("locationId",location."_id"['$oid']);
                	locationV.setProperty("address",location.address);
               	 	locationV.setProperty("address1",location.address1?location.address1:"");
                	locationV.setProperty("city",location.city?location.city:"");
                	locationV.setProperty("state",location.state?location.state:"");
                        locationV.setProperty("coordinates",Geoshape.point(location.coordinates[1],location.coordinates[0]));
	                locationV.setProperty("country",location.country?location.country:"");
	                locationV.setProperty("roadType","");
	                locationV.setProperty("createdBy",location.createdBy?location.createdBy:"amit");
	                locationV.setProperty("createDate", location.createDate?location.createDate['$date']:new Date());
	                locationV.setProperty("zip",location.zip?location.zip:"");
        	        graph.commit();
		}	else{
			println "no lat long. "+(location."_id"['$oid'])
		}
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        return locationV;
    }

    def getFlagAsVertex(def flag, def graph){
        Iterable<Vertex> flags = graph.getVertices("flagName",flag);
        Vertex flagV = null;
        if(flags && flags.iterator().hasNext()){
            flagV = flags.iterator().next();
            //println "flag already exists: "+flagV
        }
        else{
            flagV = graph.addVertex(null);
            flagV.setProperty("flagId",ObjectId.get().toString());
            flagV.setProperty("flagName",flag);
            flagV.setProperty("flagDescription",flag);
            flagV.setProperty("createdBy","amit");
            flagV.setProperty("createDate",new Date());

            graph.commit();
        }
        return flagV;
    }

    def addTagThisCarIndicesToGraph(def graph){

        def INDEX_NAME = "search";

	//tags for vertex-based indices
	graph.makeKey("tags").dataType(String.class).indexed(Vertex.class).unique().make();
        
	//if you want to do CONTAINS on description, you need ES index below.
        //graph.makeKey("description").dataType(String.class).indexed(Vertex.class).make();
        graph.makeKey("description").dataType(String.class).indexed(INDEX_NAME, Vertex.class).make();

        graph.makeKey("teaser").dataType(String.class).indexed(INDEX_NAME, Vertex.class).make();
        //graph.makeKey("teaser").dataType(String.class).indexed(Vertex.class).make();

        //unique
        graph.makeKey("hashTag").dataType(String.class).indexed(Vertex.class)/*.unique()*/.make();
        graph.makeKey("tagId").dataType(String.class).indexed(Vertex.class).unique().make();

        //standard keys
        graph.makeKey("createdBy").dataType(String.class).indexed(Vertex.class).make();
        graph.makeKey("updatedBy").dataType(String.class).indexed(Vertex.class).make();
        graph.makeKey("direction").dataType(String.class).indexed(Vertex.class).make();
        graph.makeKey("impact").dataType(String.class).indexed(Vertex.class).make();
        graph.makeKey("status").dataType(String.class).indexed(Vertex.class).make();
        graph.makeKey("resolution").dataType(String.class).indexed(Vertex.class).make();
        graph.makeKey("guid").dataType(String.class).indexed(Vertex.class).make();
        graph.makeKey("lanesImpacted").dataType(String.class).indexed(Vertex.class).make();
        graph.makeKey("sidesImpacted").dataType(String.class).indexed(Vertex.class).make();

        // standard dates
        graph.makeKey("createDate").dataType(Date.class).indexed(Vertex.class).make();
        graph.makeKey("updateDate").dataType(Date.class).indexed(Vertex.class).make();
        graph.makeKey("incidentStartDate").dataType(Date.class).indexed(Vertex.class).make();
        graph.makeKey("incidentEndDate").dataType(Date.class).indexed(Vertex.class).make();

        //vertex-centric indices
        final TitanKey timestamp = graph.makeKey("timestamp").dataType(Long.class).make();
        //final TitanType time = graph.getType("time");
        graph.makeLabel("lastUpdated").sortKey(timestamp).sortOrder(Order.DESC).make();

        graph.makeKey("numberOfLanesImpacted").dataType(Integer.class).indexed(Vertex.class).make();

        // TagUpdate keys
        graph.makeKey("tagUpdateId").dataType(String.class).indexed(Vertex.class).unique().make();
        graph.makeKey("update").dataType(String.class).indexed(Vertex.class).make();
        //graph.makeKey("updatedBy").dataType(String.class).indexed(Vertex.class).make();

        //Location keys.
        graph.makeKey("locationId").dataType(String.class).indexed(Vertex.class).unique().make();
        graph.makeKey("address").dataType(String.class).indexed(INDEX_NAME, Vertex.class).make();
        //graph.makeKey("address").dataType(String.class).indexed(Vertex.class).make();

        graph.makeKey("city").dataType(String.class).indexed(INDEX_NAME, Vertex.class).make();
        //graph.makeKey("city").dataType(String.class).indexed(Vertex.class).make();

        graph.makeKey("address1").dataType(String.class).indexed(INDEX_NAME,Vertex.class).make();
        graph.makeKey("state").dataType(String.class).indexed(INDEX_NAME,Vertex.class).make();
        graph.makeKey("country").dataType(String.class).indexed(INDEX_NAME,Vertex.class).make();
        graph.makeKey("zip").dataType(String.class).indexed(INDEX_NAME,Vertex.class).make();

        graph.makeKey("roadType").dataType(String.class).indexed(Vertex.class).make();
        graph.makeKey("coordinates").dataType(Geoshape.class).indexed(INDEX_NAME, Vertex.class).make();

        // User keys
        graph.makeKey("userId").dataType(String.class).indexed(Vertex.class).unique().make();
        graph.makeKey("username").dataType(String.class).indexed(Vertex.class).unique().make();
        graph.makeKey("ipAddress").dataType(String.class).indexed(Vertex.class).make();

        //Search Keys
        graph.makeKey("searchTerm").dataType(String.class).indexed(Vertex.class).make();
        graph.makeKey("destinationTerm").dataType(String.class).indexed(Vertex.class).make();
        graph.makeKey("csvFlagTypes").dataType(String.class).indexed(Vertex.class).make();
        graph.makeKey("miles").dataType(String.class).indexed(Vertex.class).make();
        graph.makeKey("metric").dataType(String.class).indexed(Vertex.class).make();
        graph.makeKey("avoidHighways").dataType(String.class).indexed(Vertex.class).make();
        graph.makeKey("avoidTolls").dataType(String.class).indexed(Vertex.class).make();
        graph.makeKey("travelMode").dataType(String.class).indexed(Vertex.class).make();
        graph.makeKey("noOfDays").dataType(String.class).indexed(Vertex.class).make();
        graph.makeKey("limit").dataType(String.class).indexed(Vertex.class).make();
        graph.makeKey("parkingEnabled").dataType(String.class).indexed(Vertex.class).make();
        graph.makeKey("skipDate").dataType(String.class).indexed(Vertex.class).make();
        graph.makeKey("sortBy").dataType(String.class).indexed(Vertex.class).make();
        graph.makeKey("skipLimit").dataType(String.class).indexed(Vertex.class).make();

        //SearchFound Edge key
        graph.makeKey("foundDate").dataType(Date.class).indexed(Edge.class).make();

        //SearchedFor Edge key
        graph.makeKey("searchDate").dataType(Date.class).indexed(Edge.class).make();
        graph.makeKey("sessionId").dataType(String.class).indexed(Edge.class).make();

        //LookedAt Edge key
        graph.makeKey("when").dataType(Date.class).indexed(Edge.class).make();

        //Flag related.
        graph.makeKey("flagId").dataType(String.class).indexed(Vertex.class).unique().make();
        graph.makeKey("flagName").dataType(String.class).indexed(Vertex.class).unique().make();
        graph.makeKey("flagDescription").dataType(String.class).indexed(Vertex.class).unique().make();

        //weather ID
        graph.makeKey("weatherId").dataType(String.class).indexed(Vertex.class).unique().make();

        graph.makeLabel("hasLocation").manyToMany().make();

        //graph.makeLabel("searchedFor").manyToMany().make();
        graph.makeLabel("searchedFor").sortKey(timestamp).sortOrder(Order.DESC).make();

        //graph.makeLabel("lookedAt").manyToMany().make();
        graph.makeLabel("lookedAt").sortKey(timestamp).sortOrder(Order.DESC).make();

        //graph.makeLabel("searchFound").manyToMany().make();
        graph.makeLabel("searchFound").sortKey(timestamp).sortOrder(Order.DESC).make();

        graph.makeLabel("hasFlag").manyToMany().make();

        graph.makeLabel("hasWeather").oneToOne().make();

        graph.commit();
    }
}

def jsonParser = new JsonParser()
jsonParser.parse()
