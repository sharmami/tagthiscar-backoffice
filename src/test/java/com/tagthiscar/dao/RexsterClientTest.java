package com.tagthiscar.dao;

import com.thinkaurelius.titan.core.TitanFactory;
import com.thinkaurelius.titan.core.TitanGraph;
import com.thinkaurelius.titan.core.attribute.Geoshape;
import com.thinkaurelius.titan.core.util.TitanCleanup;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.rexster.client.RexsterClient;
import com.tinkerpop.rexster.client.RexsterClientFactory;
import org.apache.log4j.Logger;
import org.bson.types.ObjectId;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Iterator;
import java.util.List;
import java.util.Map;


//@RunWith(SpringJUnit4ClassRunner.class)
/*@ContextConfiguration(locations = {
        "file:src/main/webapp/WEB-INF/tagthiscar/spring/tagthiscar-context.xml"
})*/
public class RexsterClientTest
 {

     private static final Logger logger = Logger.getLogger(RexsterClientTest.class);

     public static final String INDEX_NAME = "search";

    @Test
    //@Ignore
    public void simpleTest() throws Exception {
        System.out.println(" HERE ");

        //http://54.227.45.182/graphs
        RexsterClient client = RexsterClientFactory.open("localhost", "graph");
        //List<Map<String, Object>> results = client.execute("g=rexster.getGraph(\"graph\");g.V('name','saturn').next().map");

        //System.out.println(client.execute("g.getClass()"));
        //This works.
        /*List< Map <String, Object>> results = client.execute("g.V('name','saturn').next().map");
        Map<String, Object> map = results.get(0);
        System.out.println(map.get("name"));
        System.out.println("Map :  "+map);*/


        //This works.
        /*List<Map<String,Object>> result;
        result = client.execute("g.V('name','saturn').in('father').map");
        System.out.println(result.toString());*/

        //This works.
        List<Map<String,Object>> result;
        /*Map<String,Object> params = new HashMap<String,Object>();
        params.put("name","saturn");
        result = client.execute("g.V('name',name).in('father').map",params);
        System.out.println(result.toString());*/

        //
        //List<Map<String,Object>> result;
        //Map<String,Object> params = new HashMap<String,Object>();
        //params.put("name","saturn");
        //result = client.execute("g.V('name',name).next().map",params);// saturn.

        //System.out.println(client.execute("g.query().has(\"name\",EQUAL,\"accident at cedarhurst drive\").vertices()"));
        //System.out.println(result.toString());
        //System.out.println("BEOFRE QUERY");

        //result = client.execute(" g.query().has('place',WITHIN,Geoshape.circle(38.90723089999999,-77.03646409999999,5)).vertices()");
        result = client.execute(" g.query().has('place',WITHIN,Geoshape.circle(38.90723089999999,-77.03646409999999,5)).vertices()");

        System.out.println("RESULT: "+result.toString());
        Iterator<Map<String,Object>> iter = result.iterator();
        while(iter.hasNext()){
            Map<String,Object> map = iter.next();

            /*for(Map.Entry entry:map.entrySet()){
               System.out.println(" Key:  "+entry.getKey()+" Value: "+entry.getValue());

            }*/
            //System.out.println(" Class: "+map.get("_properties").getClass());
            Map<String,Object> propertiesMap = (Map<String,Object>) map.get("_properties");
            for(Map.Entry entry:propertiesMap.entrySet()){
               System.out.println(" Key:  "+entry.getKey()+" Value: "+entry.getValue());
            }

            System.out.println("Id: "+map.get("_id"));
            //List<Map<String,Object>> tagsMap = client.execute(" g.v(60).in('hasLocation').map");
            List<Map<String,Object>> tagsMap = client.execute(" g.v(60).both");
            System.out.println(" tagmap: "+tagsMap.toString());

        }
        client.close();



    }




 }