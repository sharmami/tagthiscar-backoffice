package com.tagthiscar.dao;

import com.tagthiscar.domain.Location;
import com.tagthiscar.domain.Search;
import com.tagthiscar.domain.Site;
import com.tagthiscar.domain.UserStats;
import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.geo.Distance;
import org.springframework.data.mongodb.core.geo.Metrics;
import org.springframework.data.mongodb.core.geo.Point;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.StopWatch;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "file:src/main/webapp/WEB-INF/tagthiscar/spring/tagthiscar-context.xml"
})
public class SiteDaoTest
 {

     private static final Logger logger = Logger.getLogger(SiteDaoTest.class);

     @Autowired
    private UserStatsDao userStatsDao;

     @Autowired
     private SiteDao siteDao;

     @Autowired
     private LocationDao locationDao;

     @Autowired
     private MongoOperations mongoTemplate;

    @Test
    @Ignore
    public void simpleTest() {
        List<UserStats> userStatsList = userStatsDao.findByUsername("admin");
        for(UserStats userStats: userStatsList){
            System.out.println(" uerstat: "+userStats.getIpAddress());
        }
        System.out.println(" size:  "+userStatsList.size());
    }


   @Test
   @Ignore
     public void searchForSitesWithATerm() {
         Search search = new Search();
         //Pageable pageable = new PageRequest(0, 4, Sort.Direction.ASC, "submittedDate");// thisworks.
         Pageable pageable = new PageRequest(0, 4, Sort.Direction.ASC, "updatedDate");

         //Page<Site> sitePage= siteDao.findAll(pageable);
         //Page<Site> sitePage= siteDao.findPageByLocationAddressIgnoreCaseContaining("Dulles", pageable);
       Page<Site> sitePage= siteDao.findPageByLocationCityIgnoreCaseContaining("Sterling", pageable);

         List<Site> sites = sitePage.getContent();
         System.out.println("  page result size:"+sites.size());
         System.out.println(" Total Pages:"+sitePage.getTotalPages());
         System.out.println(" Number: "+sitePage.getNumber());
         System.out.println(" Number of Elements: "+sitePage.getNumberOfElements());
         System.out.println(" Total Elements: "+sitePage.getTotalElements());
         System.out.println("  Page size: "+sitePage.getSize());
         System.out.println(" Sort: "+sitePage.getSort());
         System.out.println(" HAsContent?: "+sitePage.hasContent());
         System.out.println(" has next page?: "+sitePage.hasNextPage());
         System.out.println(" has previous page?: "+sitePage.hasPreviousPage());
         System.out.println(" is first page?: "+sitePage.isFirstPage());
         System.out.println(" is last page?: "+sitePage.isLastPage());

         for(Site site: sites){
             System.out.println(" Teaser: "+site.getTeaser()+ " date: "+site.getUpdatedDate());
         }

     }

     @Ignore
     @Test
     public void findAllLocation(){
        Pageable pageable = new PageRequest(0, 4, Sort.Direction.ASC, "updatedDate");
        Page<Location> locationPage= locationDao.findByAddress("Dulles International Airport", pageable);
         List<Location> locations = locationPage.getContent();
         for(Location location1: locations){
             System.out.println(" City: "+location1.getCity()+ " id: "+location1.getId());
         }
     }


     @Ignore
     @Test
     public void searchForSitesWithAFlagType() {
        Search search = new Search();
        Pageable pageable = new PageRequest(0, 4, Sort.Direction.ASC, "updatedDate");
        Page<Site> sitePage= siteDao.findByFlagTypes(new String[]{"airport"}, pageable);

        List<Site> sites = sitePage.getContent();
         System.out.println("  page result size:"+sites.size());
         System.out.println(" Total Pages:"+sitePage.getTotalPages());
         System.out.println(" Number: "+sitePage.getNumber());
         System.out.println(" Number of Elements: "+sitePage.getNumberOfElements());
         System.out.println(" Total Elements: "+sitePage.getTotalElements());
         System.out.println("  Page size: "+sitePage.getSize());
         System.out.println(" Sort: "+sitePage.getSort());
         System.out.println(" HAsContent?: "+sitePage.hasContent());
         System.out.println(" has next page?: "+sitePage.hasNextPage());
         System.out.println(" has previous page?: "+sitePage.hasPreviousPage());
         System.out.println(" is first page?: "+sitePage.isFirstPage());
         System.out.println(" is last page?: "+sitePage.isLastPage());

         for(Site site: sites){
             System.out.println(" Teaser: "+site.getTeaser()+ " date: "+site.getUpdatedDate());
         }

     }

     @Ignore
     @Test
     public void test1() {
         //DC : Longitude: -77.03646409999999 Latitude: 38.90723089999999
         //   Airport : Lat : 38.953381 Long : -77.44771200000002
         Search search = new Search();
         search.setLatitude("38.90723089999999");
         search.setLongitude("-77.03646409999999");
         search.setValue(50);
         search.setMetric(Metrics.MILES);
         //search.setAddress("middlefield");

         StopWatch stopWatch = new StopWatch("siteDaotestwatch");
         stopWatch.start("results");
         Pageable pageable = new PageRequest(0, 25, Sort.Direction.ASC, "updatedDate");

         Point point = new Point(Double.parseDouble(search.getLongitude()),Double.parseDouble(search.getLatitude()));
         //Distance distance = new Distance(1, Metrics.KILOMETERS);
         Distance distance = new Distance(search.getValue(),search.getMetric());
         Page<Location> locationPage= locationDao.findByCoordinatesNear(point, distance, pageable);
         System.out.println(" Location total elements: "+locationPage.getTotalElements()+" number of elements : "+locationPage.getNumberOfElements());
         for(Location location: locationPage.getContent()){
             //System.out.println(" City: "+location.getCity()+ " Address: "+location.getAddress());
             System.out.println(" number of sites:  "+location.getSites().size());
         }
         stopWatch.stop();
         System.out.println(stopWatch.prettyPrint());
     }

     @Ignore
     @Test
     public void test2() {

         Search search = new Search();
         search.setLatitude("38.70745");
         search.setLongitude("-121.309802");
         search.setValue(1);
         search.setMetric(Metrics.MILES);

         StopWatch stopWatch = new StopWatch("siteDaotestwatch");
         stopWatch.start("results");
         Pageable pageable = new PageRequest(0, 5, Sort.Direction.DESC, "submittedDate");

         Point point = new Point(Double.parseDouble(search.getLongitude()),Double.parseDouble(search.getLatitude()));
         Distance distance = new Distance(search.getValue(),search.getMetric());

         //List<Site> sites = siteDao.findByLocationCoordinatesNear(point,distance,pageable);
         List<Site> sites = siteDao.findByLocationCoordinatesNearAndFlagTypes(point, distance, new String[]{"dead_animal", "caution"}, pageable);
         System.out.println(" result : "+sites.size());
         for(Site site: sites){
             System.out.println(" Site here 2: "+site.getTeaser()+" Date: "+site.getSubmittedDate()+" lat: "+site.getLocation().getLatitude()+" long: "+site.getLocation().getLongitude());
         }
         stopWatch.stop();
         System.out.println(stopWatch.prettyPrint());
     }

     @Ignore
     @Test
     public void test3(){
          final Double MILES = 69.0d;

         Search search = new Search();
        // antelope road lat long
        /* search.setLatitude("38.70745");
         search.setLongitude("-121.309802");*/

         //DC : Longitude: -77.03646409999999 Latitude: 38.90723089999999
         //   Airport : Lat : 38.953381 Long : -77.44771200000002

         // airport latlong
        /* search.setLatitude("38.953381");
         search.setLongitude("-77.44771200000002");*/

         search.setLatitude("38.90723089999999");
         search.setLongitude("-77.03646409999999");

         search.setValue(15);
         search.setMetric(Metrics.MILES);

         Point point = new Point(Double.parseDouble(search.getLongitude()),Double.parseDouble(search.getLatitude()));
         Distance distance = new Distance(search.getValue(),search.getMetric());

         //Query query4 = new Query();

         // if there are any flagTypes.
         Criteria flagTypesCriteria = Criteria.where("flagTypes").in(new String[]{"caution","airport"});
         //query4.addCriteria(Criteria.where("flagTypes").in(new String[]{/*"caution",*/"airport"}));

         // this is equivalent of findByCoordinatesNear
         Criteria latLngCriteria = new Criteria("location.coordinates").near(point).maxDistance(search.getValue()/MILES);
         //query4.addCriteria(latLngCriteria);

         // find by address
         String searchTerm = "Howard St";
         Criteria wildSearchCriteria = Criteria.where("location.address").exists(true)
                  .orOperator(
                          Criteria.where("location.address").regex(searchTerm,"i"),
                          Criteria.where("location.address1").regex(searchTerm,"i"),
                          Criteria.where("location.city").regex(searchTerm,"i"),
                          Criteria.where("description").regex(searchTerm,"i"),
                          Criteria.where("teaser").regex(searchTerm,"i")
                  );

         /*query4.addCriteria(
                 Criteria.where("location.address").exists(true)
                 .orOperator(
                         Criteria.where("location.address").regex("airport"),
                         Criteria.where("location.address1").regex("airport"),
                         Criteria.where("location.city").regex("airport"),
                         Criteria.where("description").regex("airport"),
                         Criteria.where("teaser").regex("airport")
                 )
         );*/

        Query query4 = new Query(new Criteria().orOperator(
                 flagTypesCriteria
                // ,
                //latLngCriteria
                //,
                //wildSearchCriteria
         ));

         //Query query4 = new Query(latLngCriteria);


         //sort by submittedDate
         query4.with(new Sort(Sort.Direction.DESC, "submittedDate"));

         Calendar cal = Calendar.getInstance();
         cal.add(Calendar.DAY_OF_MONTH, -1);
         Date sevenDaysAgo = cal.getTime();

         query4.addCriteria(Criteria.where("submittedDate").gt(sevenDaysAgo).lte(new Date()));

         //query4.limit(25);
         System.out.println("query4 - " + query4.toString());

         StopWatch stopWatch = new StopWatch("result");
         stopWatch.start("theonlyquery");
         List<Site> result = mongoTemplate.find(query4, Site.class);
         System.out.println("  result : "+result.size());
         for(Site site : result){
             System.out.println("  Site : "+site.getTeaser()+" Site Flag: "+ Arrays.toString(site.getFlagTypes())+" Site ID: "+site.getId()+" date: "+site.getSubmittedDate()+" address:"+site.getLocation().getAddress()+" address1:"+site.getLocation().getAddress1()+" city :"+site.getLocation().getCity());
         }
        stopWatch.stop();
         System.out.println(stopWatch.prettyPrint());
     }


     @Ignore
     @Test
     public void test4(){
              Search search = new Search();
              List<Site> sites = siteDao.findSites(search);
              System.out.println( " Sites found: " +sites.size());
     }


     @Test
     @Ignore
     public void test5(){
         List<String> flagTypes = siteDao.getDistinctFlagTypes();
        System.out.println( " FlagTypes found: " +flagTypes.size());
         for (String flagType: flagTypes){
             System.out.println(" FlagType: "+flagType);
         }
     }


     @Test
     @Ignore
     public void findBylabelsTest(){
         Criteria labelsCriteria = Criteria.where("labels.name").is("10-Most-Congested-Cities");
         //Criteria sitesCriteria = Criteria.where("guid").is(/*"523513797d5601edc5d318d6",*/"130921HM00044");
         Query query = new Query(labelsCriteria);
         List<Site> result = mongoTemplate.find(query, Site.class);
         System.out.println("  labels result : "+result.size());
         for(Site site : result){
             //System.out.println("  Site : "+site.getTeaser()+" Site Flag: "+ Arrays.toString(site.getFlagTypes())+" Site ID: "+site.getId()+" date: "+site.getSubmittedDate()+" address:"+site.getLocation().getAddress()+" address1:"+site.getLocation().getAddress1()+" city :"+site.getLocation().getCity());
             System.out.println(" Label again:  "+site.getLabels().get(0).getTitle()+" site id : "+site.getId()+" GUID: "+site.getGuid());
         }

     }


     @Test
     @Ignore
     public void wildCardSearchTest(){
         String searchTerm = "FLOOD-Roadway";
         Criteria wildSearchCriteria = Criteria.where("location.address").exists(true)
                 .orOperator(
                         Criteria.where("location.address").regex(searchTerm,"i"),
                         Criteria.where("location.address1").regex(searchTerm,"i"),
                         Criteria.where("location.city").regex(searchTerm,"i"),
                         Criteria.where("description").regex(searchTerm,"i"),
                         Criteria.where("teaser").regex(searchTerm,"i")
                 );
         Query query = new Query(wildSearchCriteria);
         System.out.println(" SiteDaoTest Query : "+query.toString());
         List<Site> result = mongoTemplate.find(query, Site.class);
         System.out.println("  site result : "+result.size());
         for(Site site : result){
             System.out.println("  Site : "+site.getTeaser()+" Site Flag: "+ Arrays.toString(site.getFlagTypes())+" Site ID: "+site.getId()+" date: "+site.getSubmittedDate()+" address:"+site.getLocation().getAddress()+" address1:"+site.getLocation().getAddress1()+" city :"+site.getLocation().getCity());
             //System.out.println(" Label again:  "+site.getLabels().get(0).getTitle()+" site id : "+site.getId()+" GUID: "+site.getGuid());
         }

     }


     @Ignore
     @Test
     public void findAllSites(){
         Pageable pageable = new PageRequest(0, 4, Sort.Direction.ASC, "updatedDate");
         Page<Site> sitePage = siteDao.findAll(pageable);
         List<Site> sites = sitePage.getContent();
         for(Site site: sites){
            System.out.println(" Site : "+site.getTeaser());
         }

     }



 }