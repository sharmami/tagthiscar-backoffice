package com.tagthiscar.dao;

import com.tagthiscar.domain.Label;
import com.tagthiscar.domain.Site;
import org.apache.log4j.Logger;
import org.bson.types.ObjectId;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "file:src/main/webapp/WEB-INF/tagthiscar/spring/tagthiscar-context.xml"
})
public class LabelDaoTest
 {

     private static final Logger logger = Logger.getLogger(LabelDaoTest.class);

     @Autowired
    private LabelDao labelDao;

     @Autowired
     private SiteDao siteDao;

     @Autowired
     private MongoOperations mongoTemplate;

    @Test
    //@Ignore
    public void simpleTest() {

        Label label = new Label();
        label.setId(ObjectId.get().toString());
        label.setTitle("10 Most Congested Cities");
        label.setName("10-Most-Congested-Cities");//it shouldnt have any spaces or other special characters ideally.
        label.setCreatedBy("amit");
        label.setCreatedDate(new Date());
        label.setDescription("10 Most Congested Cities");
        Site site1 =  siteDao.findByGuid("130921HM00055").get(0);
        System.out.println(" I see there is a site 1: "+site1.getGuid());

        try{
            List<Label> labels = new ArrayList<Label>();
            labels.add(label);
            site1.setLabels(labels);
            siteDao.save(site1);
        }catch (Exception e){
            System.out.println(" Site save failed: ");
            e.printStackTrace();
        }
       /* site1.getLabels().add(label);
        siteDao.save(site1);*/

        //label.getSites().add(site1);
        try{
            List<Site> sites = new ArrayList<Site>();
            sites.add(/*site1*/siteDao.findByGuid("130921HM00044").get(0));
            label.setSites(sites);
            labelDao.save(label);
        }catch (Exception e){
            System.out.println(" Label save failed: ");
            e.printStackTrace();
        }



    }


     @Test
     @Ignore
     public void findAllLabelsTest() {

        for(Label label: labelDao.findAll()){
            System.out.println(" Label Name: "+label.getName());
            System.out.println(" Label Title: "+label.getTitle());
            for(Site site: label.getSites()){
                System.out.println("Site GUID: "+site.getGuid());
            }
        }
     }


    /* @Test
     @Ignore
     public void updateLabelWithSiteTest() {

        Label label = labelDao.findOne("523db866dcb8aea6f6a0ac54");
        label.getSites().add(siteDao.findByGuid("130921MY00123").get(0));
         labelDao.save(label);
         System.out.println(" DOINE");
     }*/

     @Test
     @Ignore
     public void updateSiteWithLabelTest() {
         Site site = siteDao.findOne("523dd03fdcb86d1f427f8801");
         site.setLabels(new ArrayList<Label>());
         siteDao.save(site);
         System.out.println(" DONE label size: "+site.getLabels().size());

     }

 }