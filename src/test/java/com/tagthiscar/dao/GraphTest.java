package com.tagthiscar.dao;

import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.tagthiscar.domain.Site;

import com.tagthiscar.graph.Location;
import com.tagthiscar.graph.Tag;
import com.thinkaurelius.titan.core.TitanFactory;
import com.thinkaurelius.titan.core.TitanGraph;
import com.thinkaurelius.titan.core.attribute.Geo;
import com.thinkaurelius.titan.core.attribute.Geoshape;

import com.thinkaurelius.titan.example.GraphOfTheGodsFactory;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.util.wrappers.batch.BatchGraph;
import com.tinkerpop.blueprints.util.wrappers.batch.VertexIDType;
import com.tinkerpop.frames.FramedGraph;
import com.tinkerpop.frames.FramedGraphFactory;
import com.tinkerpop.frames.modules.gremlingroovy.GremlinGroovyModule;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.bson.types.ObjectId;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.StopWatch;

import java.util.Iterator;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "file:src/main/webapp/WEB-INF/tagthiscar/spring/tagthiscar-context.xml"
})
public class GraphTest
 {

     private static final Logger logger = Logger.getLogger(GraphTest.class);

     public static final String INDEX_NAME = "search";

     @Autowired
     private SiteDao siteDao;


     @Autowired
     private MongoOperations mongoTemplate;


     @Test
     //@Ignore
     public void importMongodbData(){
      System.out.println(" GraphTest: importMongodbData: 1");
     }


     @Test
     @Ignore
     public void batchLoadMongoDbLocations(){

         TitanGraph g = TitanFactory.open("/home/amit/titan-server-0.4.1/conf/titan-cassandra-es.properties");
         BatchGraph bgraph = new BatchGraph(g,VertexIDType.STRING,500);
         bgraph.setVertexIdKey("name");
         //bgraph.setLoadingFromScratch(false);

         StopWatch stopWatch = new StopWatch("batch");
         stopWatch.start();

         int count = 0;
         Pageable pageable = new PageRequest(0, 200, Sort.Direction.DESC, "id");
         Page<Site> sitePage = siteDao.findAll(pageable);
         System.out.println(" toal pages: " + sitePage.getTotalPages());
         for(int i=0; i<=sitePage.getTotalPages();i++){
             System.out.println(" PAGE :  "+i);
             List<Site> sites = sitePage.getContent();
             for(Site site: sites){
                 Boolean tagExists = false;
                 Boolean locExists = false;
                 //System.out.println(" Site : id:"+site.getId()+" Teaser : "+site.getTeaser());
                 com.tagthiscar.domain.Location location = site.getLocation();
                 //System.out.println(" address: "+location.getAddress()+" lat:"+location.getLatitude()+" long:"+location.getLongitude()+" city:"+location.getCity()+" "+location.getId());
                 try{


                 Vertex tag = null;
                 /*Iterable<Vertex> tVertices = batchGraph.getVertices("name",site.getId());
                 if(tVertices!=null){
                     for(Vertex vertex : tVertices){
                         //System.out.println(" found a veretx for "+site.getId());
                         tag = vertex;
                         tagExists = true;
                         break;
                     }
                 }*/
                 if(tag==null){
                     //System.out.println(" tag is  NUll... ");
                     tag = bgraph.addVertex(site.getId());
                 }

                 tag.setProperty("name",site.getId());
                 tag.setProperty("teaser",site.getTeaser());
                 tag.setProperty("description",site.getDescription());

                 Vertex loca = null;

                 String locationId = null;
                 if(location.getId()!=null && !GenericValidator.isBlankOrNull(location.getId())){
                     locationId = location.getId();
                 } else{
                     locationId = ObjectId.get().toString();
                 }

                 /*Iterable<Vertex> gVertices = batchGraph.getVertices("name",locationId);
                 if(gVertices!=null){
                     for(Vertex vertex : gVertices){
                         //System.out.println(" found a veretx for "+location.getAddress()+" id : "+location.getId());
                         loca = vertex;
                         locExists = true;
                         break;
                     }
                 }*/
                 if(loca==null){
                     //System.out.println(" NUll... ");
                     loca = bgraph.addVertex(locationId);
                 }
                 loca.setProperty("name",locationId);
                 loca.setProperty("address",location.getAddress()!=null?location.getAddress():"");
                 loca.setProperty("city",location.getCity()!=null?location.getCity():"");

                 if(GenericValidator.isDouble(String.valueOf(location.getLatitude()))
                         && GenericValidator.isDouble(String.valueOf(location.getLongitude()))
                 ){
                     //DC : Longitude: -77.03646409999999 Latitude: 38.90723089999999
                     loca.setProperty("geoshape",Geoshape.point(/*location.getLatitude()*/38.90723089999999,/*location.getLongitude()*/-77.03646409999999));
                 }

                 //Edge edge = g.addEdge(null,tag,loca,"hasLocation");
                 //edge.setProperty("name",ObjectId.get().toString());

                 if(locExists && tagExists){
                     System.out.println(" Skipping "+site.getTeaser()+" having location: "+location.getAddress());
                 }
                 else{
                     tag.addEdge("hasLocation",loca);
                     //Edge edge = graph.addEdge(null,tag,loca,"hasLocation");
                     System.out.println(" count so far : "+count+" siteId: "+site.getId()+" locationid:"+locationId);
                     count++;
                 }

                 //TODO enable this graph.commit();

                 }catch(Exception ex){
                     System.out.print(" ERROR:  "+ex.getLocalizedMessage());
                     ex.printStackTrace();

                 }
             }

             sitePage = siteDao.findAll(new PageRequest(++i, 200, Sort.Direction.ASC, "id"));
         }

         System.out.println(" count is : "+count);
         stopWatch.stop();
         System.out.println(stopWatch.prettyPrint());



     }


     @Test
     @Ignore
     public void testBatchLoadMongoDbLocations(){

         StopWatch stopWatch = new StopWatch("mongodbload");
         stopWatch.start();

         int count = 0;
         int numPages = ((int) Math.floor(72126 / 5000)) + 1;
         DBCursor dbCursor = mongoTemplate.getCollection("site").find()/*.sort(new BasicDBObject("_id", 1))*/;
         //DBCursor dbCursor = mongoTemplate.getCollection("site").find().batchSize(5000);

         for(int i=0;i<numPages;i++){
         Iterator<DBObject> dbObjects = dbCursor.skip((i) * 5000).limit(5000).iterator();
         //Iterator<DBObject> dbObjects = dbCursor.iterator();
         while(dbObjects.hasNext()){
                 try{
                     DBObject dbObject = dbObjects.next();
                     Site site = mongoTemplate.getConverter().read(Site.class, dbObject);
                     //System.out.println(" site : ID :  "+site.getId()+" teaser: "+site.getTeaser());
                     com.tagthiscar.domain.Location location = site.getLocation();
                     //System.out.println(" location : ID :  "+location.getId()+" latlng: "+location.getCoordinates());
                     System.out.println(" count so far : "+count+" siteId: "+site.getId()+" locationid:"+location.getId());
                     count++;
                 }catch (Exception ex){
                     ex.printStackTrace();
                 }
                //if(count==20000 || count==25000 || count==30000) System.gc();
             }
         }

         stopWatch.stop();
         System.out.println(stopWatch.prettyPrint());
     }

     @Test
     @Ignore
     public void simpleTest() throws Exception {
         System.out.println(" HERE ");

         TitanGraph g = TitanFactory.open("/home/amit/titan-server-0.4.0/conf/titan-cassandra-es.properties");
         //TitanGraph g = TitanFactory.open("graph");
         //TitanGraph g = TitanFactory.open("/home/amit/titan-server-0.4.0/conf/titan-cassandra-es.properties");
         //GraphOfTheGodsFactory.load(g);
         //load(g);


         FramedGraphFactory factory = new FramedGraphFactory(new GremlinGroovyModule());
         FramedGraph framedGraph = factory.create(g);

         StopWatch stopWatch = new StopWatch("coordinates");
         stopWatch.start();

         for(Vertex v: g.getVertices()){
             System.out.println(" V: "+v);
             for(String property: v.getPropertyKeys()){
                 System.out.println(" Property: "+property+" value:  "+v.getProperty(property));
             }
         }
         //g.V('name','saturn').next().map

         //DC : Longitude: -77.03646409999999 Latitude: 38.90723089999999
         //   Airport : Lat : 38.953381 Long : -77.44771200000002
         // SF lat : 37.773972 long : -122.431297
         //geo search start here

         // wild card search
        /*Iterable<Vertex> vertices = g.query().has("description", Text.CONTAINS, "parking").vertices();
        Iterable<Tag> tags = framedGraph.frameVertices(vertices,Tag.class);
        for(Tag tag: tags){
            System.out.println(" --- Tag : "+tag.getName()+" "+tag.getTeaser());
            for(Location location: tag.getLocations()){
                System.out.println(" location : "+location.getAddress()+" "+location.getGeoShape().toString());
            }
        }*/
         //Iterable<Vertex> vertices = g.query().has("geoshape", Geo.WITHIN, Geoshape.circle(37.97,23.72,50)).vertices();
         System.out.println(" START ---- ");
         Iterable<Vertex> vertices = g.query().has("place", Geo.WITHIN, Geoshape.circle(38.90723089999999,-77.03646409999999,5000)).vertices();
         //Iterable<Vertex> vertices = g.query().has("geoshape", Geo.WITHIN, Geoshape.circle(38.953381,-77.44771200000002,100)).vertices();
         //Iterable<Vertex> vertices = g.query().has("geoshape", Geo.WITHIN, Geoshape.circle(37.773972,-122.431297,50)).vertices();
         for(Vertex vertex: vertices){
             System.out.println(" Id: "+vertex.getId());
         }

         Iterable<Location> locations = framedGraph.frameVertices(vertices,Location.class);
        /*stopWatch.stop();
        System.out.println(stopWatch.prettyPrint());*/

         for(Location location: locations){
             System.out.println(" Name:  "+location.getName()+" address:"+location.getAddress()+" Id: "+location.asVertex().getId());
             //System.out.println(" geoshape:  "+location.getGeoShape().getPoint().toString());

             int count = 0;
             for(Tag tag: location.getTags()){
                 System.out.println(" --- Tag : "+tag.getName()+" "+tag.getTeaser());
                 count++;
                 if(count>1){
                     System.out.println("MORE THAN ONE");
                 }
             }

         }
         System.out.println(" END ---- ");

        /*Iterable<Vertex> vertexes = g.query().has("name","51a56871af8826870b3b6b3e").vertices();
        Iterable<Location> locas = framedGraph.frameVertices(vertexes,Location.class);
        for(Location location: locas){
            System.out.println(" name:  "+location.getName());
            for(Tag tag : location.getTags()){
                System.out.println(" --- Tag : "+tag.getName()+" "+tag.getTeaser());
            }
        }*/

        /*Iterable<Vertex> vertexes = g.query().has("name","51a56871af8826870b3b6b3f").vertices();
        Iterable<Tag> tags = framedGraph.frameVertices(vertexes,Tag.class);
        for(Tag tag : tags){
            System.out.println(" Tag: "+tag.getName()+" "+tag.getTeaser());
            for(Location location: tag.getLocations()){
                System.out.println("  ++++++++location: "+location.getName()+" "+location.getAddress());
                *//*for(Tag taggy: location.getTags()){
                  System.out.println(" -----> taggy : "+taggy.getName()+" "+tag.getTeaser());
                }*//*
            }
        }*/

       /* stopWatch.stop();
        System.out.println(stopWatch.prettyPrint());*/

     }

 }