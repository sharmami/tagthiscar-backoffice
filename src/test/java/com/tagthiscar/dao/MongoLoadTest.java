package com.tagthiscar.dao;

import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.tagthiscar.domain.Site;
import com.tagthiscar.graph.Location;
import com.tagthiscar.graph.Tag;
import com.tagthiscar.titan.Flag;
import com.thinkaurelius.titan.core.TitanFactory;
import com.thinkaurelius.titan.core.TitanGraph;
import com.thinkaurelius.titan.core.attribute.Geo;
import com.thinkaurelius.titan.core.attribute.Geoshape;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.util.wrappers.batch.BatchGraph;
import com.tinkerpop.blueprints.util.wrappers.batch.VertexIDType;
import com.tinkerpop.frames.FramedGraph;
import com.tinkerpop.frames.FramedGraphFactory;
import com.tinkerpop.frames.modules.gremlingroovy.GremlinGroovyModule;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.bson.types.ObjectId;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.StopWatch;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "file:src/main/webapp/WEB-INF/tagthiscar/spring/tagthiscar-context.xml"
})
public class MongoLoadTest
 {

     private static final Logger logger = Logger.getLogger(MongoLoadTest.class);


     @Autowired
     private SiteDao siteDao;

     @Test
     @Ignore
     public void importMongodbData() throws Exception {
        System.out.println(" GraphTest: importMongodbData: 1");
        StopWatch stopWatch = new StopWatch("mongoload");
        stopWatch.start();

        TitanGraph graph = TitanFactory.open("/home/amit/titan-server-0.4.1/conf/titan-cassandra-es.properties");

        // load starts here
         final int pageLimit = 200;
         int pageNumber = 125;
         Page<Site> sitePage = siteDao.findAll(new PageRequest(pageNumber, pageLimit,Sort.Direction.DESC, "id"));
         while (sitePage.hasNextPage()) {
             System.out.println(" GraphTest: importMongodbData: 2 pageNumber:"+pageNumber);
             processPageContent(sitePage.getContent(),graph);
            sitePage = siteDao.findAll(new PageRequest(++pageNumber, pageLimit));
         }
         System.out.println(" GraphTest: importMongodbData: 3");
         processPageContent(sitePage.getContent(),graph);
         System.out.println(" GraphTest: importMongodbData: 4");
        //load ends here
        stopWatch.stop();
        System.out.println(stopWatch.prettyPrint());
     }

     private void processPageContent(List<Site> sites,TitanGraph graph) throws Exception {
         for(Site site: sites){
             Vertex tagV = getTagAsVertex(site, graph);
             // add vertex-centric edge.
             Edge lastUpdated = tagV.addEdge("lastUpdated",tagV);
             lastUpdated.setProperty("timestamp",((Date)tagV.getProperty("updateDate")).getTime());
         }
     }

     private static Vertex getTagAsVertex(Site site,TitanGraph graph) throws Exception{
         Vertex vertex = graph.addVertex(null);

         vertex.setProperty("teaser",site.getTeaser()==null?"":site.getTeaser());
         vertex.setProperty("tagId",site.getId()==null?"":site.getId());
         vertex.setProperty("createdBy",site.getSubmittedBy()==null?"amit":site.getSubmittedBy());
         vertex.setProperty("description",site.getDescription()==null?"":site.getDescription());
         vertex.setProperty("direction",site.getTravelAlignment()==null?"":site.getTravelAlignment());
         vertex.setProperty("hashTag","");
         vertex.setProperty("impact",site.getLanesImpacted()==null?"":site.getLanesImpacted());
         vertex.setProperty("numberOfLanesImpacted",site.getNoOfLanesImpacted()==null?"":site.getNoOfLanesImpacted());
         vertex.setProperty("resolution",site.getResolution()==null?"":site.getResolution());
         vertex.setProperty("status",site.getStatus()==null?"":site.getStatus());

         Date submittedDate = site.getSubmittedDate();
         if(submittedDate==null) submittedDate = new Date();

         Date updatedDate = site.getUpdatedDate();
         if(updatedDate==null) {
             updatedDate = submittedDate;
         }

         vertex.setProperty("createDate",submittedDate);
         vertex.setProperty("incidentStartDate",updatedDate);
         vertex.setProperty("incidentEndDate",updatedDate);
         vertex.setProperty("updateDate",updatedDate);

         vertex.setProperty("lanesImpacted",site.getLanesImpacted()==null?"":site.getLanesImpacted());
         vertex.setProperty("sidesImpacted",site.getSidesImpacted()==null?"":site.getSidesImpacted());
         vertex.setProperty("guid",site.getGuid()==null?"":site.getGuid());

         graph.commit();
         return vertex;
     }


     /*private static Vertex getLocationAsVertex(com.tagthiscar.titan.Location location,TitanGraph graph) throws Exception{
         Vertex vertex = graph.addVertex(null);

         vertex.setProperty("locationId",location.getLocationId());
         vertex.setProperty("address",location.getAddress());
         vertex.setProperty("city",location.getCity());
         vertex.setProperty("place",location.getPlace());
         vertex.setProperty("createdBy",location.getCreatedBy());
         vertex.setProperty("address1",location.getAddress1());
         vertex.setProperty("country",location.getCountry());
         vertex.setProperty("createDate", location.getCreateDate());
         vertex.setProperty("zip",location.getZip());

         graph.commit();
         return vertex;
     }


     public static void addTag(Vertex site, Vertex location,TitanGraph graph){
         graph.addEdge(null,site,location,"hasLocation");
         graph.commit();
     }

     private static void loadFlags(final TitanGraph graph,Vertex tagV, String[] flagTypes) throws Exception {

        for(String flagType: flagTypes){
            Flag flag = new Flag();
            flag.setFlagId(ObjectId.get().toString());
            flag.setFlagName(flagType);
            flag.setFlagDescription(flagType);
            flag.setCreatedBy("amit");
            flag.setCreateDate(new Date());
            Vertex flagV = getFlagAsVertex(flag, graph);
            graph.addEdge(null,tagV,flagV,"hasFlag");
            graph.commit();
        }

     }

     private static Vertex getFlagAsVertex(Flag flag,TitanGraph graph) throws Exception{
         Vertex vertex = graph.addVertex(null);

         vertex.setProperty("flagId",flag.getFlagId());
         vertex.setProperty("flagName",flag.getFlagName());
         vertex.setProperty("flagDescription",flag.getFlagDescription());
         vertex.setProperty("createdBy",flag.getCreatedBy());
         vertex.setProperty("createDate",flag.getCreateDate());

         graph.commit();
         return vertex;
     }*/

 }