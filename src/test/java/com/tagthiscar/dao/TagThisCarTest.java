package com.tagthiscar.dao;

import com.tagthiscar.titan.Flag;
import com.tagthiscar.titan.Location;
import com.tagthiscar.titan.Tag;
import com.thinkaurelius.titan.core.Order;
import com.thinkaurelius.titan.core.TitanFactory;
import com.thinkaurelius.titan.core.TitanGraph;
import com.thinkaurelius.titan.core.TitanKey;
import com.thinkaurelius.titan.core.TitanType;
import com.thinkaurelius.titan.core.attribute.Geoshape;
import com.thinkaurelius.titan.core.util.TitanCleanup;
import com.thinkaurelius.titan.example.GraphOfTheGodsFactory;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "file:src/main/webapp/WEB-INF/tagthiscar/spring/tagthiscar-context.xml"
})
public class TagThisCarTest
 {

     private static final Logger logger = Logger.getLogger(TagThisCarTest.class);

     public static final String INDEX_NAME = "search";


     @Test
     @Ignore
     public void timeStampTest(){
        /*System.out.println("1 minute in ms: "+TimeUnit.MILLISECONDS.convert(1, TimeUnit.MINUTES));
        System.out.println("5 minute in ms: "+TimeUnit.MILLISECONDS.convert(5, TimeUnit.MINUTES));
        System.out.println("10 minute in ms: "+TimeUnit.MILLISECONDS.convert(10, TimeUnit.MINUTES));
        System.out.println("15 minute in ms: "+TimeUnit.MILLISECONDS.convert(15, TimeUnit.MINUTES));
        System.out.println("30 minute in ms: "+TimeUnit.MILLISECONDS.convert(30, TimeUnit.MINUTES));
        System.out.println("1 hour in ms: "+TimeUnit.MILLISECONDS.convert(1, TimeUnit.HOURS));
        System.out.println("1 day in ms: "+TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS));
        System.out.println("2 days in ms: "+TimeUnit.MILLISECONDS.convert(2, TimeUnit.DAYS));*/

         /*Calendar cal = Calendar.getInstance();
         cal.add(Calendar.HOUR, -1);
         Date thendate = cal.getTime();*/

         DateTime dateTime = new DateTime(new Date());
         System.out.println("1 min ago: "+dateTime.minusMinutes(1).getMillis());
         System.out.println("5 min ago: "+dateTime.minusMinutes(5).getMillis());
         System.out.println("5 Minutes ago: "+dateTime.minusMinutes(10).getMillis());
         System.out.println("30 min ago: "+dateTime.minusMinutes(30).getMillis());
         System.out.println("1 hour ago: "+dateTime.minusHours(1).getMillis());
         System.out.println("1 day ago: "+dateTime.minusDays(1).getMillis());
         System.out.println("2 day ago: "+dateTime.minusDays(2).getMillis());


     }

     @Test
     //@Ignore
     public void cleanupTest() throws Exception {
         TitanGraph graph = TitanFactory.open("/home/amit/titan-server-0.4.2/conf/titan-cassandra-es.properties");
         graph.shutdown();
         TitanCleanup.clear(graph);

         // now load the GraphOfTheGodsFactory again.
         TitanGraph g = TitanFactory.open("/home/amit/titan-server-0.4.2/conf/titan-cassandra-es.properties");
         GraphOfTheGodsFactory.load(g);
         addTagThisCarIndicesToGraph(g);
         /*loadTag(g);
         loadUserSearchedFor(g);
         loadSearchedFound(g);
         loadUserLookedAt(g);
         loadFlags(g);
         loadMultipleLocations(g);
         loadConcordeTag(g);
         loadWarrentonTag(g);
         loadPropertiesTag(g);*/
     }

     @Test
     @Ignore
     public void minorWorkTest() throws Exception {
         TitanGraph g = TitanFactory.open("/home/amit/titan-server-0.4.2/conf/titan-cassandra-es.properties");
         //addMoreIndicesToGraph(g);
         //loadUserSearchedFor(g);
         //loadSearchedFound(g);
         //loadUserLookedAt(g);

         /*Iterable<Vertex> accidentFlags = g.getVertices("flagName","accident");
         Vertex anyAccidentV = accidentFlags.iterator().next();
         deleteVertex(anyAccidentV,g);*/
     }

     @Test
     @Ignore
     public void loadDataTest() throws Exception {
         TitanGraph g = TitanFactory.open("/home/amit/titan-server-0.4.2/conf/titan-cassandra-es.properties");
         //loadTag(g);
         //loadUserSearchedFor(g);
         //loadSearchedFound(g);
         //loadUserLookedAt(g);
         //loadFlags(g);
         //loadMultipleLocations(g);
         //loadConcordeTag(g);
         //loadWarrentonTag(g);
         //loadPropertiesTag(g);

     }

     private static Vertex getTagAsVertex(Tag tag,TitanGraph graph) throws Exception{
         Vertex vertex = graph.addVertex(null);

         vertex.setProperty("teaser",tag.getTeaser());
         vertex.setProperty("tagId",tag.getTagId());
         vertex.setProperty("createdBy",tag.getCreatedBy());
         vertex.setProperty("description",tag.getDescription());
         vertex.setProperty("direction",tag.getDirection());
         vertex.setProperty("hashTag",tag.getHashTag());
         vertex.setProperty("impact",tag.getImpact());
         vertex.setProperty("numberOfLanesImpacted",tag.getNumberOfLanesImpacted());
         vertex.setProperty("resolution",tag.getResolution());
         vertex.setProperty("status",tag.getStatus());
         vertex.setProperty("createDate",tag.getCreateDate());
         vertex.setProperty("incidentStartDate",tag.getIncidentStartDate());
         vertex.setProperty("incidentEndDate",tag.getIncidentEndDate());
         vertex.setProperty("updateDate",tag.getUpdateDate());

         graph.commit();
         return vertex;
     }

     private static Vertex getLocationAsVertex(Location location,TitanGraph graph) throws Exception{
         Vertex vertex = graph.addVertex(null);

         vertex.setProperty("locationId",location.getLocationId());
         vertex.setProperty("address",location.getAddress());
         vertex.setProperty("city",location.getCity());
         vertex.setProperty("place",location.getPlace());
         vertex.setProperty("createdBy",location.getCreatedBy());
         vertex.setProperty("address1",location.getAddress1());
         vertex.setProperty("country",location.getCountry());
         vertex.setProperty("createDate", location.getCreateDate());
         vertex.setProperty("zip",location.getZip());

         graph.commit();
         return vertex;
     }

     private static void loadMultipleLocations(final TitanGraph graph) throws Exception {
         //Lets add dulles airport to the same tag as our house.#tryingmultiple

         // Airport Lat : 38.953381 Long : -77.44771200000002
         Location location = new Location();
         location.setAddress("1 Saarinen Cir");
         location.setCity("Sterling");
         location.setPlace(Geoshape.point(38.953381,-77.44771200000002));
         location.setCreatedBy("amit");
         location.setAddress1("Dulles Airport");
         location.setCountry("US");
         location.setCreateDate(new Date());
         location.setLocationId(ObjectId.get().toString());
         location.setZip("20166");
         Vertex dullesAirportV = getLocationAsVertex(location,graph);

         Iterable<Vertex> tags = graph.getVertices("teaser","Accident at Cedarhurst Drive");
         Vertex houseTagV = tags.iterator().next();

         addTag(houseTagV,dullesAirportV,graph);

     }

     private static void loadTag(final TitanGraph graph) throws Exception {

         Tag tag = new Tag();
         tag.setTeaser("Accident at Cedarhurst Drive");
         tag.setTagId(ObjectId.get().toString());
         tag.setCreatedBy("amit");
         tag.setDescription("A full description of what happened at the incident");
         tag.setDirection("EAST");
         tag.setHashTag("accidentsterling");
         tag.setImpact("MAJOR");
         tag.setNumberOfLanesImpacted(2);
         tag.setResolution("OPEN");
         tag.setStatus("FIXED");

         Date incidentDate = new Date();
         tag.setIncidentStartDate(incidentDate);
         tag.setIncidentEndDate(incidentDate);
         tag.setCreateDate(incidentDate);
         tag.setUpdateDate(incidentDate);


         Location location = new Location();
         location.setAddress("46527 Cedarhurst Drive");
         location.setCity("Sterling");
         location.setPlace(Geoshape.point(38.90723089999999,-77.03646409999999));
         location.setCreatedBy("amit");
         location.setAddress1("");
         location.setCountry("US");
         location.setCreateDate(new Date());
         location.setLocationId(ObjectId.get().toString());
         location.setZip("20165");


         Vertex tagV = getTagAsVertex(tag, graph);

         // add vertex-centric edge.
         Edge lastUpdated = tagV.addEdge("lastUpdated",tagV);
         lastUpdated.setProperty("timestamp",incidentDate.getTime());

         Vertex locationV = getLocationAsVertex(location,graph);

         addTag(tagV,locationV,graph);

         //graph.commit();
     }

     private static void loadConcordeTag(final TitanGraph graph) throws Exception {

         Tag tag = new Tag();
         tag.setTeaser("Concorde Pkwy is slippery");
         tag.setTagId(ObjectId.get().toString());
         tag.setCreatedBy("amit");
         tag.setDescription("Concorde Pkwy is slippery and the whole description.");
         tag.setDirection("EAST");
         tag.setHashTag("concorde_pkwy_slippery");
         tag.setImpact("SEVERE");

         Date incidentDate = new Date();
         tag.setCreateDate(incidentDate);
         tag.setUpdateDate(incidentDate);
         tag.setIncidentStartDate(incidentDate);
         tag.setIncidentEndDate(incidentDate);

         tag.setNumberOfLanesImpacted(2);
         tag.setResolution("OPEN");
         tag.setStatus("UNKNOWN");

         Location location = new Location();
         location.setAddress("Concorde pkwy");
         location.setCity("Chantilly");
         location.setState("VA");
         location.setPlace(Geoshape.point(38.909143,-77.451698));
         location.setCreatedBy("amit");
         location.setAddress1("");
         location.setCountry("US");
         location.setCreateDate(new Date());
         location.setLocationId(ObjectId.get().toString());
         location.setZip("20151");

         Vertex tagV = getTagAsVertex(tag, graph);
         // add vertex-centric edge.
         Edge lastUpdated = tagV.addEdge("lastUpdated",tagV);
         lastUpdated.setProperty("timestamp",incidentDate.getTime());

         Vertex locationV = getLocationAsVertex(location,graph);
         addTag(tagV,locationV,graph);


         // Add something else.


         //graph.commit();
     }

     private static void loadWarrentonTag(final TitanGraph graph) throws Exception {

         Tag tag = new Tag();
         tag.setTeaser("Historic Site: Warrenton");
         tag.setTagId(ObjectId.get().toString());
         tag.setCreatedBy("amit");
         tag.setDescription("There is a movie being shot at Warrenton with client eastwood.");
         tag.setDirection("EAST");
         tag.setHashTag("moviewarrenton");
         tag.setImpact("MINOR");

         Date incidentDate = new Date();
         tag.setCreateDate(incidentDate);
         tag.setIncidentStartDate(incidentDate);
         tag.setIncidentEndDate(incidentDate);
         tag.setUpdateDate(incidentDate);

         tag.setNumberOfLanesImpacted(2);
         tag.setResolution("OPEN");
         tag.setStatus("HOLD");

         Location location = new Location();
         location.setLocationId(ObjectId.get().toString());
         location.setAddress("");
         location.setAddress1("");
         location.setCity("Warrenton");
         location.setState("VA");
         location.setCountry("US");
         location.setZip("");
         location.setPlace(Geoshape.point(38.7134516,-77.7952712));
         location.setCreatedBy("amit");
         location.setCreateDate(new Date());

         Vertex tagV = getTagAsVertex(tag, graph);
         // add vertex-centric edge.
         Edge lastUpdated = tagV.addEdge("lastUpdated",tagV);
         lastUpdated.setProperty("timestamp",incidentDate.getTime());

         Vertex locationV = getLocationAsVertex(location,graph);
         addTag(tagV,locationV,graph);

         // Add something else.


         //graph.commit();
     }

     private static void loadPropertiesTag(final TitanGraph graph) throws Exception {

         Tag tag = new Tag();
         tag.setTeaser("Our properties");
         tag.setTagId(ObjectId.get().toString());
         tag.setCreatedBy("amit");
         tag.setDescription("our properties");
         tag.setDirection("");
         tag.setHashTag("realestate");
         tag.setImpact("NONE");

         Date incidentDate = new Date();
         tag.setCreateDate(incidentDate);
         tag.setIncidentStartDate(incidentDate);
         tag.setIncidentEndDate(incidentDate);
         tag.setUpdateDate(incidentDate);

         tag.setNumberOfLanesImpacted(2);
         tag.setResolution("OPEN");
         tag.setStatus("HOLD");

         Location calamary = new Location();
         calamary.setLocationId(ObjectId.get().toString());
         calamary.setAddress("21700 Calamary Circle");
         calamary.setAddress1("");
         calamary.setCity("Sterling");
         calamary.setState("VA");
         calamary.setCountry("USA");
         calamary.setZip("20164");
         calamary.setPlace(Geoshape.point(39.0148204,-77.381212));
         calamary.setCreatedBy("amit");
         calamary.setCreateDate(new Date());

         Location stablehouse = new Location();
         stablehouse.setLocationId(ObjectId.get().toString());
         stablehouse.setAddress("22413 Stablehouse Drive");
         stablehouse.setAddress1("");
         stablehouse.setCity("Sterling");
         stablehouse.setState("VA");
         stablehouse.setCountry("USA");
         stablehouse.setZip("");
         stablehouse.setPlace(Geoshape.point(38.999581,-77.42316089999997));
         stablehouse.setCreatedBy("amit");
         stablehouse.setCreateDate(new Date());

         Vertex tagV = getTagAsVertex(tag, graph);

         // add vertex-centric edge.
         Edge lastUpdated = tagV.addEdge("lastUpdated",tagV);
         lastUpdated.setProperty("timestamp",incidentDate.getTime());

         Vertex calamaryV = getLocationAsVertex(calamary,graph);

         Vertex stablehouseV = getLocationAsVertex(stablehouse,graph);

         Iterable<Vertex> locations = graph.getVertices("address","46527 Cedarhurst Drive");
         Vertex cedarhurstV = locations.iterator().next();

         addTag(tagV,calamaryV,graph);
         addTag(tagV,stablehouseV,graph);
         addTag(tagV,cedarhurstV,graph);

         // Add something else.


         //graph.commit();
     }


     private static void loadUserSearchedFor(final TitanGraph graph) throws Exception {

         // lets identify the user.
         Vertex user = graph.addVertex(null);
         user.setProperty("userId","001");
         user.setProperty("username","amit");
         user.setProperty("ipAddress","10.1.1.1");

         // lets capture what he is searching for.
         Vertex search = graph.addVertex(null);
         search.setProperty("searchTerm","cedarhurst dr");
         search.setProperty("destinationTerm","");
         search.setProperty("csvFlagTypes","caution,accident");
         search.setProperty("miles","1");
         search.setProperty("metric","MILES");
         search.setProperty("avoidHighways","false");
         search.setProperty("avoidTolls","false");
         search.setProperty("travelMode","DRIVING");
         search.setProperty("noOfDays","-1");
         search.setProperty("limit","25");
         search.setProperty("parkingEnabled","false");
         search.setProperty("skipDate","false");
         search.setProperty("sortBy","submittedDate");
         search.setProperty("skipLimit","skipLimit");

         // set his sessionID and when he did his search.
         Edge searchedFor = graph.addEdge(null,user,search,"searchedFor");
         searchedFor.setProperty("sessionId","12242132132132");
         Date date = new Date();
         searchedFor.setProperty("searchDate",date);
         // add vertex-centric edge.
         searchedFor.setProperty("timestamp",date.getTime());

         graph.commit();
     }

     private static void loadSearchedFound(final TitanGraph graph) throws Exception {

         //find the searchterm we used in loadUserSearchedFor
         Iterable<Vertex> searches = graph.getVertices("searchTerm","cedarhurst dr");
         Vertex search = searches.iterator().next();

         // find the location it led to
         Iterable<Vertex> locations = graph.getVertices("address","46527 Cedarhurst Drive");
         Vertex location = locations.iterator().next();

         // set his sessionID and when he did his search.
         Edge searchFound = graph.addEdge(null,search,location,"searchFound");
         searchFound.setProperty("sessionId","12242132132132");
         Date date = new Date();
         searchFound.setProperty("foundDate",date);
         // add vertex-centric edge.
         searchFound.setProperty("timestamp",date.getTime());

         graph.commit();
     }

     private static void loadUserLookedAt(final TitanGraph graph) throws Exception {

         //find the user
         Iterable<Vertex> users = graph.getVertices("userId","001");
         Vertex user = users.iterator().next();

         // find the tags this user looked at.
         Iterable<Vertex> tags = graph.getVertices("teaser","Accident at Cedarhurst Drive");
         Vertex tag = tags.iterator().next();

         // set his sessionID and when he did his search.
         Edge lookedAt = graph.addEdge(null,user,tag,"lookedAt");
         lookedAt.setProperty("sessionId","12242132132132");
         Date date = new Date();
         lookedAt.setProperty("when",date);
         // add vertex-centric edge.
         lookedAt.setProperty("timestamp",date.getTime());

         graph.commit();
     }

     public static void addTag(Vertex site, Vertex location,TitanGraph graph){
        graph.addEdge(null,site,location,"hasLocation");
        graph.commit();
     }

     private static void addTagThisCarIndicesToGraph(final TitanGraph graph){

         //if you want to do CONTAINS on description, you need ES index below.
         //graph.makeKey("description").dataType(String.class).indexed(Vertex.class).make();
         graph.makeKey("description").dataType(String.class).indexed(INDEX_NAME, Vertex.class).make();

         graph.makeKey("teaser").dataType(String.class).indexed(INDEX_NAME, Vertex.class).make();
         //graph.makeKey("teaser").dataType(String.class).indexed(Vertex.class).make();

         //unique
         graph.makeKey("hashTag").dataType(String.class).indexed(Vertex.class)/*.unique()*/.make();
         graph.makeKey("tagId").dataType(String.class).indexed(Vertex.class).unique().make();

         //standard keys
         graph.makeKey("createdBy").dataType(String.class).indexed(Vertex.class).make();
         graph.makeKey("updatedBy").dataType(String.class).indexed(Vertex.class).make();
         graph.makeKey("direction").dataType(String.class).indexed(Vertex.class).make();
         graph.makeKey("impact").dataType(String.class).indexed(Vertex.class).make();
         graph.makeKey("status").dataType(String.class).indexed(Vertex.class).make();
         graph.makeKey("resolution").dataType(String.class).indexed(Vertex.class).make();
         graph.makeKey("guid").dataType(String.class).indexed(Vertex.class).make();
         graph.makeKey("lanesImpacted").dataType(String.class).indexed(Vertex.class).make();
         graph.makeKey("sidesImpacted").dataType(String.class).indexed(Vertex.class).make();

         // standard dates
         graph.makeKey("createDate").dataType(Date.class).indexed(Vertex.class).make();
         graph.makeKey("updateDate").dataType(Date.class).indexed(Vertex.class).make();
         graph.makeKey("incidentStartDate").dataType(Date.class).indexed(Vertex.class).make();
         graph.makeKey("incidentEndDate").dataType(Date.class).indexed(Vertex.class).make();

         //vertex-centric indices
         final TitanKey timestamp = graph.makeKey("timestamp").dataType(Long.class).make();
         //final TitanType time = graph.getType("time");
         graph.makeLabel("lastUpdated").sortKey(timestamp).sortOrder(Order.DESC).make();

         graph.makeKey("numberOfLanesImpacted").dataType(Integer.class).indexed(Vertex.class).make();

         // TagUpdate keys
         graph.makeKey("tagUpdateId").dataType(String.class).indexed(Vertex.class).unique().make();
         graph.makeKey("update").dataType(String.class).indexed(Vertex.class).make();
         //graph.makeKey("updatedBy").dataType(String.class).indexed(Vertex.class).make();

         //Location keys.
         graph.makeKey("locationId").dataType(String.class).indexed(Vertex.class).unique().make();
         graph.makeKey("address").dataType(String.class).indexed(INDEX_NAME, Vertex.class).make();
         //graph.makeKey("address").dataType(String.class).indexed(Vertex.class).make();

         graph.makeKey("city").dataType(String.class).indexed(INDEX_NAME, Vertex.class).make();
         //graph.makeKey("city").dataType(String.class).indexed(Vertex.class).make();

         graph.makeKey("address1").dataType(String.class).indexed(INDEX_NAME,Vertex.class).make();
         graph.makeKey("state").dataType(String.class).indexed(INDEX_NAME,Vertex.class).make();
         graph.makeKey("country").dataType(String.class).indexed(INDEX_NAME,Vertex.class).make();
         graph.makeKey("zip").dataType(String.class).indexed(INDEX_NAME,Vertex.class).make();

         graph.makeKey("roadType").dataType(String.class).indexed(Vertex.class).make();
	     graph.makeKey("coordinates").dataType(Geoshape.class).indexed(INDEX_NAME, Vertex.class).make();

         // User keys
         graph.makeKey("userId").dataType(String.class).indexed(Vertex.class).unique().make();
         graph.makeKey("username").dataType(String.class).indexed(Vertex.class).unique().make();
         graph.makeKey("ipAddress").dataType(String.class).indexed(Vertex.class).make();

         //Search Keys
         graph.makeKey("searchTerm").dataType(String.class).indexed(Vertex.class).make();
         graph.makeKey("destinationTerm").dataType(String.class).indexed(Vertex.class).make();
         graph.makeKey("csvFlagTypes").dataType(String.class).indexed(Vertex.class).make();
         graph.makeKey("miles").dataType(String.class).indexed(Vertex.class).make();
         graph.makeKey("metric").dataType(String.class).indexed(Vertex.class).make();
         graph.makeKey("avoidHighways").dataType(String.class).indexed(Vertex.class).make();
         graph.makeKey("avoidTolls").dataType(String.class).indexed(Vertex.class).make();
         graph.makeKey("travelMode").dataType(String.class).indexed(Vertex.class).make();
         graph.makeKey("noOfDays").dataType(String.class).indexed(Vertex.class).make();
         graph.makeKey("limit").dataType(String.class).indexed(Vertex.class).make();
         graph.makeKey("parkingEnabled").dataType(String.class).indexed(Vertex.class).make();
         graph.makeKey("skipDate").dataType(String.class).indexed(Vertex.class).make();
         graph.makeKey("sortBy").dataType(String.class).indexed(Vertex.class).make();
         graph.makeKey("skipLimit").dataType(String.class).indexed(Vertex.class).make();

         //SearchFound Edge key
         graph.makeKey("foundDate").dataType(Date.class).indexed(Edge.class).make();

         //SearchedFor Edge key
         graph.makeKey("searchDate").dataType(Date.class).indexed(Edge.class).make();
         graph.makeKey("sessionId").dataType(String.class).indexed(Edge.class).make();

         //LookedAt Edge key
         graph.makeKey("when").dataType(Date.class).indexed(Edge.class).make();

         //Flag related.
         graph.makeKey("flagId").dataType(String.class).indexed(Vertex.class).unique().make();
         graph.makeKey("flagName").dataType(String.class).indexed(Vertex.class).unique().make();
         graph.makeKey("flagDescription").dataType(String.class).indexed(Vertex.class).unique().make();

         //weather ID
         graph.makeKey("weatherId").dataType(String.class).indexed(Vertex.class).unique().make();

         graph.makeLabel("hasLocation").manyToMany().make();

         //graph.makeLabel("searchedFor").manyToMany().make();
         graph.makeLabel("searchedFor").sortKey(timestamp).sortOrder(Order.DESC).make();

         //graph.makeLabel("lookedAt").manyToMany().make();
         graph.makeLabel("lookedAt").sortKey(timestamp).sortOrder(Order.DESC).make();

         //graph.makeLabel("searchFound").manyToMany().make();
         graph.makeLabel("searchFound").sortKey(timestamp).sortOrder(Order.DESC).make();

         graph.makeLabel("hasFlag").manyToMany().make();

         graph.makeLabel("hasWeather").oneToOne().make();

         graph.commit();

     }


     private static void addMoreIndicesToGraph(final TitanGraph graph){

         //Flag related.
         //graph.makeKey("flagId").dataType(String.class).indexed(Vertex.class).unique().make();
         //graph.makeKey("flagName").dataType(String.class).indexed(Vertex.class).make();
         //graph.makeKey("flagDescription").dataType(String.class).indexed(Vertex.class).make();

         //graph.makeLabel("hasFlag").manyToMany().make();

        //graph.makeKey("sessionId").dataType(String.class).indexed(Edge.class).make();
        //graph.makeKey("metric").dataType(String.class).indexed(Vertex.class).make();
        //graph.commit();
     }


     private static void loadFlags(final TitanGraph graph) throws Exception {

         // find the tags this user looked at.
         Iterable<Vertex> tags = graph.getVertices("teaser","Accident at Cedarhurst Drive");
         Vertex tagV = tags.iterator().next();

         Flag accident = new Flag();
         accident.setFlagId(ObjectId.get().toString());
         accident.setFlagName("accident");
         accident.setFlagDescription("Accident");
         accident.setCreatedBy("amit");
         accident.setCreateDate(new Date());


         Flag caution = new Flag();
         caution.setFlagId(ObjectId.get().toString());
         caution.setFlagName("caution");
         caution.setFlagDescription("Caution");
         caution.setCreatedBy("amit");
         caution.setCreateDate(new Date());

         Vertex cautionV = getFlagAsVertex(caution, graph);
         Vertex accidentV = getFlagAsVertex(accident, graph);

         // set his sessionID and when he did his search.
         graph.addEdge(null,tagV,cautionV,"hasFlag");
         graph.addEdge(null,tagV,accidentV,"hasFlag");

         graph.commit();
     }


     private static Vertex getFlagAsVertex(Flag flag,TitanGraph graph) throws Exception{
         Vertex vertex = graph.addVertex(null);

         vertex.setProperty("flagId",flag.getFlagId());
         vertex.setProperty("flagName",flag.getFlagName());
         vertex.setProperty("flagDescription",flag.getFlagDescription());
         vertex.setProperty("createdBy",flag.getCreatedBy());
         vertex.setProperty("createDate",flag.getCreateDate());

         graph.commit();
         return vertex;
     }

     private static void deleteVertex(Vertex vertex,TitanGraph graph){
         graph.removeVertex(vertex);
         graph.commit();
     }
 }
