package com.tagthiscar.dao;

import com.tagthiscar.graph.Battled;
import com.tagthiscar.graph.Site;
import com.thinkaurelius.titan.core.TitanFactory;
import com.thinkaurelius.titan.core.TitanGraph;
import com.thinkaurelius.titan.core.attribute.Geo;
import com.thinkaurelius.titan.core.attribute.Geoshape;
import com.thinkaurelius.titan.example.GraphOfTheGodsFactory;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.frames.FramedGraph;
import com.tinkerpop.frames.FramedGraphFactory;
import com.tinkerpop.frames.modules.gremlingroovy.GremlinGroovyModule;
import com.tinkerpop.rexster.client.RexsterClient;
import com.tinkerpop.rexster.client.RexsterClientFactory;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.StopWatch;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "file:src/main/webapp/WEB-INF/tagthiscar/spring/tagthiscar-context.xml"
})
public class TitanFrameTest
 {

     private static final Logger logger = Logger.getLogger(TitanFrameTest.class);



    @Test
    //@Ignore
    public void simpleTest() throws Exception {
        System.out.println(" HERE ");

        TitanGraph g = TitanFactory.open("/home/amit/titan-server-0.4.0/conf/titan-cassandra-embedded-es.properties");
        //GraphOfTheGodsFactory.load(g);
        FramedGraphFactory factory = new FramedGraphFactory(new GremlinGroovyModule());
        FramedGraph framedGraph = factory.create(g);

        /*Iterable<Edge> edges = g.query().has("place", Geo.WITHIN, Geoshape.circle(37.97,23.72,50)).edges();
        for(Edge edge: edges){
            System.out.println(" Edge: "+edge.getLabel());
        }*/

        StopWatch stopWatch = new StopWatch("coordinates ");
        stopWatch.start();
        Iterable<Edge> edges1 = framedGraph.query().has("place", Geo.WITHIN, Geoshape.circle(37.97,23.72,50)).edges();
         /*for(Edge edge: edges1) {
             System.out.println(" -------<> edge:  "+edge.getClass());
         }*/
        Iterable<Battled> battleds = framedGraph.frameEdges(edges1, Battled.class);
        for(Battled battled: battleds){
            System.out.println(" Who Battled : "+battled.getInSite().getName());
            System.out.println(" Battled Who: "+battled.getOutSite().getName());
        }
        stopWatch.stop();
        System.out.println(stopWatch.prettyPrint());
        /*Iterable<Site> sites = framedGraph.getVertices("name","hercules",Site.class);
        for(Site site : sites){
            System.out.println(" Site name : "+site.getName()+": "+site.getAge()+" "+site.getType());
        }*/
    }


 }